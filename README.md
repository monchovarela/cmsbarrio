# Barrio CMS

El CMS que se adapta a cualquier proyecto

### Primeros pasos.

[Demo](https://monchovarela.es/cmsbarrio)


[**Documentación disponible y actualizada en Notion**](https://nakome.notion.site/Barrio-CMS-238c9b5f9b0c49258b2d11a16fdefc9c)


#### Requisitos de Apache

Aunque la mayoría de las distribuciones de Apache vienen con todo lo necesario, en aras de la exhaustividad, aquí hay una lista de los módulos de Apache necesarios:

    mod_rewrite

También debe asegurarse de tener **AllowOverride All** configurado en los bloques **<Directory>** y / o **<VirtualHost>** para que el archivo **.htaccess** se procese correctamente y las reglas de reescritura surtan efecto.

#### Requerimientos PHP

La mayoría de los proveedores de alojamiento e incluso las configuraciones locales de **LAMP** tienen **PHP** preconfigurado con todo lo que necesita para que Barrio CMS se ejecute.
La version de **PHP** compatible ahora es minimo `7.4.0`.


### Instalación.

Si descargó el archivo ZIP y luego planea moverlo a su raíz web, mueva **TODA LA CARPETA** porque contiene varios archivos ocultos (como .htaccess) que no se seleccionarán de manera predeterminada. La omisión de estos archivos ocultos puede causar problemas al ejecutar Barrio CMS.

### Configuración.

En el archivo <mark>config.php</mark> encontraras la configuración de la web, la url del sitio, el titulo, la descripción o la configuración del menu de navegación.

Puedes crear tu propia configuración pero <mark>no borres las variables que hay por defecto</mark>.

Puedes llamar cualquier variable de `config.php`si estas en el archivo _.md_ con `{Config name=nombre}`. 

