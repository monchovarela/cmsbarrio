Title: ABC Music Editor
Description: Escribe música de manera fácil
Image: public/notfound.jpg
Template: index
----


[Esc]
    Title: ABC music notation
    Description: Escribe musica de manera facil
    Template: index
    ----
    [Script local=true src='public/apps/abc/abcjs-basic-min.js']
    [Script local=true src='public/apps/abc/index.js']
    [Style local=true src='public/apps/abc/abcjs-audio.css']
    [Style local=true src='public/apps/abc/index.css']
    [Html src='/public/apps/abc/index.txt']
[/Esc]

[Script local=true src='public/apps/abc/abcjs-basic-min.js']
[Script local=true src='public/apps/abc/index.js']
[Style local=true src='public/apps/abc/abcjs-audio.css']
[Style local=true src='public/apps/abc/index.css']
[Html src='/public/apps/abc/index.txt']
