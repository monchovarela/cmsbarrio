Title: Kitchen Sink
Description: De todo un poco como en bótica.
Image: public/notfound.jpg
Template: post
----

[Styles minify=true]
.demo1,.demo2,.demo3,.demo4 {
  padding: 0.2rem 0;background: var(--bs-light);
  color: var(--bs-dark);font-weight: bold;
  letter-spacing: 3.3px;
  text-transform: uppercase;
  display:flex;
  justify-content: space-around;
  align-items: center;
  height:4rem;
}
.demo1 *,
.demo2 *,
.demo3 *,
.demo4 *{
  margin:0;
}
.demo1,
.demo2,
.demo3,
.demo4 {
  animation:flash 3s infinite ease-in-out;
}
@keyframes flash{
  50%{
    background:var(--bs-gray)
  }
}
[/Styles]

### Elementos {#elementos}
<ul>
  <li><a href="#columnas">Columnas</a></li>
  <li><a href="#details">Details</a></li>
  <li><a href="#imagenes">Imagenes</a></li>
  <li><a href="#textos">Textos</a></li>
  <li><a href="#divisores">Divisores</a></li>
  <li><a href="#php">Php</a></li>
  <li><a href="#estilos-y-javascript">Estilos Css y Javascript</a></li>
  <li><a href="#tablas">Tablas</a></li>
  <li><a href="#lista-definicion">Listas de definición</a></li>
  <li><a href="#footnotes">Notas al pié</a></li>
</ul>


#### Columnas {#columnas}
Las columnas tienen que sumar entre ellas 12, por ejemplo por defecto la columna tiene 
6 asi que serian dos 6+6  tambien pueden ser 4+8, 8+4, 3+3+6 etc...

    [Esc]
    [Row]
      [Col]Columna[/Col]
      [Col]Columna[/Col]
    [/Row]   
    [/Esc]


[Row]
  [Col class='demo1']Columna[/Col]
  [Col class='demo2']Columna[/Col]
[/Row]  

[Divider type=br]

    [Esc]
    [Row]
      [Col num=4]Columna[/Col]
      [Col num=4]Columna[/Col]
      [Col num=4]Columna[/Col]
    [/Row]   
    [/Esc]


[Row]
  [Col num=4 class='demo1']Columna[/Col]
  [Col num=4 class='demo2']Columna[/Col]
  [Col num=4 class='demo3']Columna[/Col]
[/Row]  

[Divider type=br]

    [Esc]
    [Row]
      [Col num=3]Columna[/Col]
      [Col num=3]Columna[/Col]
      [Col num=3]Columna[/Col]
      [Col num=3]Columna[/Col]
    [/Row]   
    [/Esc]


[Row]
  [Col num=3 class='demo1']Columna[/Col]
  [Col num=3 class='demo2']Columna[/Col]
  [Col num=3 class='demo3']Columna[/Col]
  [Col num=3 class='demo4']Columna[/Col]
[/Row]


[Divider type=br]

**Otras:**

[Row]
  [Col num=3 class='demo2']Columna 3[/Col]
  [Col num=3 class='demo2']Columna 3[/Col]
  [Col num=6 class='demo1']Columna 6[/Col]
[/Row]
[Row]
  [Col num=6 class='demo1']Columna 6[/Col]
  [Col num=3 class='demo2']Columna 3[/Col]
  [Col num=3 class='demo2']Columna 3[/Col]
[/Row]
[Row]
  [Col num=3 class='demo2']Columna 3[/Col]
  [Col num=6 class='demo1']Columna 6[/Col]
  [Col num=3 class='demo2']Columna 3[/Col]
[/Row]


#### Details {#details}

  [Esc]
    [Details title='Texto oculto']
      Markdown Hidden content 
    [/Details]
  [/Esc]


[Details title='Texto oculto']
Estoy escondidoo.
[/Details]


    [Esc]
    [Row]
      [Col]
      [Details title='Texto oculto']
        Estoy escondidoo.
      [/Details]
      [/Col]
      [Col]
      [Details title='Texto oculto']
        Estoy escondidoo.
      [/Details]
      [/Col]
    [/Row]  
    [/Esc]

[Row]
  [Col]
  [Details title='Texto oculto']
    Estoy escondidoo.
  [/Details]
  [/Col]
  [Col]
  [Details title='Texto oculto']
    Estoy escondidoo.
  [/Details]
  [/Col]
[/Row]  


[Divider type=br]


#### Imagenes {#imagenes}

**Normal:**

    [Esc][Img src='public/notfound.jpg'][/Esc]

[Img src='public/notfound.jpg']


**Con clases css:**

    [Esc][Img class='clase css' src='public/notfound.jpg'][/Esc]

[Img class='border border-width-2 border-primary shadow-lg' src='public/notfound.jpg']


**También añadir enlaces:**

    [Esc][Img url='//google.es' src='public/notfound.jpg'][/Esc]

[Img url='//google.es' src='public/notfound.jpg']

**O también añadir texto:**

    [Esc][Img title='Hello' src='public/notfound.jpg'][/Esc]

[Img title='Hello' src='public/notfound.jpg']

**Si el enlace es de otro dominio**

    [Esc][Img title='Hello' ext='true' src='//monchovarela.es/public/proyectos/colectoxo.png'][/Esc]

[Img title='Hello' ext='true' src='//monchovarela.es/public/images/proyectos/colectoxo.png']

**Nota:** Los enlaces de otro dominio no hace falta poner `https:` solo basta con poner `//` por ejemplo `//example.com/image.png`


[Divider type=br]

#### Textos {#textos}

A veces necesitamos añadir un texto o nota con un color diferente, para eso usaremos `[Esc][Text][/Esc]`


    [Esc]
    [Text bg='blue' color='white']
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
    veniam, quis nostrud exercitation  ullamco laboris nisi ut aliquip 
    ex ea commodo consequat. Duis aute irure dolor in** reprehenderit 
    in voluptate velit esse cillum dolore eu fugiat nulla pariatur**. 
    [/Text]
    [/Esc]

[Text bg='LightCoral' color='Maroon' p=1]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in **reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur**. 
[/Text]

[Divider type=br]

#### Divisores (separadores) {#divisores}


    <br/> // normal
    <hr/> // linea normal
    [Esc]
    [Divider] // numero de espacios 1
    [Divider type='br'] // sin linea // numero de espacios 1
    [Divider color='Khaki'] // color
    [Divider class=demo6] // clase opcional
    [Divider num='2'] // numero de espacios maximo 3
    [Divider type='br' num='2'] // sin linea numero de espacios maximo 3
    [/Esc]

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
<hr/>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[Divider color=Indigo]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[Divider num='3' color=MediumSpringGreen]
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.


[Divider type=br]

#### Php {#php}


    [Esc]
    [Php]
      function sayName($name)[
        print($name);
      ]
      sayName('Hello World');
    [/Php]
    [/Esc]



#### Estilos Css y Javascript {#estilos-y-javascript}

Podemos incluir en cada pagina un Css o Jvascript personalizado.
El Css se añadirá automaticamente al head y el Javascript al footer.


**Css normal:**

    [Esc]
    [Styles]
    body[
      background:red;
      color:white;
    ]
    [/Styles]
    [/Esc]

**Css en archivo:**

    [Esc]
    // no hace falta incluir https
    [Style href='linkto.css']
    [/Esc]

**Javascript normal:**


    [Esc]
    [Scripts]
    function sayName(name)[
      console.log(name);
    ]
    [/Scripts]
    [/Esc]

**Javascript en archivo:**


    [Esc]
    // no hace falta incluir https
    [Script src='linkto.js']
    [/Esc]


[Divider type=br]


#### Tablas {#tablas}

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |

*Alineación:*

| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Here's this   |
| Paragraph   | Text        | And more      |

[Divider type=br]

#### Listas de definición {#lista-definicion}

First Term
: This is the definition of the first term.

Second Term
: This is one definition of the second term.
: This is another definition of the second term.

#### Notas al pié {#footnotes}

Here's a simple footnote,[^1] and here's a longer one.[^bignote]

[^1]: This is the first footnote.

[^bignote]: Here's one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.



[&uarr; Volver arriba](#elementos)