Title: Rubus loganobaccus L.H. Bailey
Description: Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.
Image: https://source.unsplash.com/random/?yellow
Date: 21/4/2021
Keywords: cms
Category: Zaam-Dox
Author: Rhycero
Tags: cms
Published: true
Background: #9dcc5d
Color: #95fbd0
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Idemne, quod iucunde? Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Ita prorsus, inquam; Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor; Duo Reges: constructio interrete. Sed in rebus apertissimis nimium longi sumus. Nam si amitti vita beata potest, beata esse non potest. Quibusnam praeteritis? Atque his de rebus et splendida est eorum et illustris oratio. Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Verum hoc idem saepe faciamus. 


	Omnia peccata paria dicitis.
	Equidem, sed audistine modo de Carneade?
	Et hercule-fatendum est enim, quod sentio -mirabilis est apud illos contextus rerum.
	Quod enim dissolutum sit, id esse sine sensu, quod autem sine sensu sit, id nihil ad nos pertinere omnino.




	Sedulo, inquam, faciam.
	Bonum negas esse divitias, praeposėtum esse dicis?
	Quid de Pythagora?
	Deinde dolorem quem maximum?
	Sed haec omittamus;
	Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?
	Eam stabilem appellas.
	Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum.



Eaedem enim utilitates poterunt eas labefactare atque pervertere. Nemo igitur esse beatus potest. Simus igitur contenti his. Morbo gravissimo affectus, exul, orbus, egens, torqueatur eculeo: quem hunc appellas, Zeno? Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Quia dolori non voluptas contraria est, sed doloris privatio. Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? Indicant pueri, in quibus ut in speculis natura cernitur. 


	Huius, Lyco, oratione locuples, rebus ipsis ielunior.
	Primum cur ista res digna odio est, nisi quod est turpis?



An vero, inquit, quisquam potest probare, quod perceptfum, quod. Nobis Heracleotes ille Dionysius flagitiose descivisse videtur a Stoicis propter oculorum dolorem. Et nemo nimium beatus est; Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba. A mene tu? Quae hic rei publicae vulnera inponebat, eadem ille sanabat. 


	Atque etiam ad iustitiam colendam, ad tuendas amicitias et reliquas caritates quid natura valeat haec una cognitio potest tradere.



Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Cur igitur, cum de re conveniat, non malumus usitate loqui? Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere. 

Quamvis enim depravatae non sint, pravae tamen esse possunt. Tu vero, inquam, ducas licet, si sequetur; Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Quid me istud rogas? Sit sane ista voluptas. Habent enim et bene longam et satis litigiosam disputationem. Earum etiam rerum, quas terra gignit, educatio quaedam et perfectio est non dissimilis animantium. Inde igitur, inquit, ordiendum est. Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum. 

At negat Epicurus-hoc enim vestrum lumen estquemquam, qui honeste non vivat, iucunde posse vivere. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Qui enim existimabit posse se miserum esse beatus non erit. Illum mallem levares, quo optimum atque humanissimum virum, Cn. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Idemne, quod iucunde? Idemne potest esse dies saepius, qui semel fuit? Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam. Haec quo modo conveniant, non sane intellego. 


	Proclivi currit oratio.
	Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio.
	Mihi quidem Homerus huius modi quiddam vidisse videatur in iis, quae de Sirenum cantibus finxerit.
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?




	Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.
	Et hanc quidem primam exigam a te operam, ut audias me quae a te dicta sunt refellentem.
	Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur.
	Te autem hortamur omnes, currentem quidem, ut spero, ut eos, quos novisse vis, imitari etiam velis.
	At miser, si in flagitiosa et vitiosa vita afflueret voluptatibus.
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.




	Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est.



Estne, quaeso, inquam, sitienti in bibendo voluptas? Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Quod, inquit, quamquam voluptatibus quibusdam est saepe iucundius, tamen expetitur propter voluptatem. Cetera illa adhibebat, quibus demptis negat se Epicurus intellegere quid sit bonum. Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Nec enim, omnes avaritias si aeque avaritias esse dixerimus, sequetur ut etiam aequas esse dicamus. Hoc non est positum in nostra actione. Est tamen ea secundum naturam multoque nos ad se expetendam magis hortatur quam superiora omnia. Quo igitur, inquit, modo? Hoc mihi cum tuo fratre convenit. 

Quonam modo? Intrandum est igitur in rerum naturam et penitus quid ea postulet pervidendum; Multoque hoc melius nos veriusque quam Stoici. Nunc agendum est subtilius. Efficiens dici potest. Tollenda est atque extrahenda radicitus. At coluit ipse amicitias. 

Aliter homines, aliter philosophos loqui putas oportere? Haec quo modo conveniant, non sane intellego. Non autem hoc: igitur ne illud quidem. Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. 


Omne enim animal, simul et ortum est, se ipsum et omnes
partes suas diligit duasque, quae maximae sunt, in primis
amplectitur, animum et corpus, deinde utriusque partes.

Quis contra in illa aetate pudorem, constantiam, etiamsi sua
nihil intersit, non tamen diligat?



Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Iam illud quale tandem est, bona praeterita non effluere sapienti, mala meminisse non oportere? Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Quid ad utilitatem tantae pecuniae? Ita ne hoc quidem modo paria peccata sunt. 


	Magna laus.
	Quamquam te quidem video minime esse deterritum.
	Ita credo.
	Quae cum essent dicta, finem fecimus et ambulandi et disputandi.
	Sedulo, inquam, faciam.
	Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt.
	Haec dicuntur inconstantissime.
	Non enim iam stirpis bonum quaeret, sed animalis.
	Recte, inquit, intellegis.
	Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest.




Hoc enim constituto in philosophia constituta sunt omnia.

Teneamus enim illud necesse est, cum consequens aliquod
falsum sit, illud, cuius id consequens sit, non posse esse
verum.




