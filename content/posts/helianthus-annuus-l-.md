Title: Helianthus annuus L.
Description: Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.
Image: https://source.unsplash.com/random/?design
Date: 15/7/2021
Keywords: article
Category: Flexidy
Author: Skyba
Tags: article
Published: true
Background: #588e10
Color: #da0f0c
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quare hoc videndum est, possitne nobis hoc ratio philosophorum dare. Tenesne igitur, inquam, Hieronymus Rhodius quid dicat esse summum bonum, quo putet omnia referri oportere? Duo enim genera quae erant, fecit tria. Videsne, ut haec concinant? Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. Duo Reges: constructio interrete. Ego vero volo in virtute vim esse quam maximam; Idemque diviserunt naturam hominis in animum et corpus. 


	Nam hunc ipsum sive finem sive extremum sive ultimum definiebas id esse, quo omnia, quae recte fierent, referrentur neque id ipsum usquam referretur.




	Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur.
	Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat.
	At ille pellit, qui permulcet sensum voluptate.
	Eam tum adesse, cum dolor omnis absit;



Conferam avum tuum Drusum cum C. An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Zenonis est, inquam, hoc Stoici. Faceres tu quidem, Torquate, haec omnia; Primum quid tu dicis breve? Cur deinde Metrodori liberos commendas? Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Quod autem principium officii quaerunt, melius quam Pyrrho; At hoc in eo M. 


	Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.
	Gracchum patrem non beatiorem fuisse quam fillum, cum alter stabilire rem publicam studuerit, alter evertere.
	Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio.
	Quia nec honesto quic quam honestius nec turpi turpius.



Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Urgent tamen et nihil remittunt. Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest. Estne, quaeso, inquam, sitienti in bibendo voluptas? 

Aliud igitur esse censet gaudere, aliud non dolere. Quae diligentissime contra Aristonem dicuntur a Chryippo. Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Est, ut dicis, inquit; Non autem hoc: igitur ne illud quidem. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. 

Expectoque quid ad id, quod quaerebam, respondeas. Falli igitur possumus. Tubulo putas dicere? Aliter homines, aliter philosophos loqui putas oportere? Negat enim summo bono afferre incrementum diem. Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus. Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. Age nunc isti doceant, vel tu potius quis enim ista melius? Et ais, si una littera commota sit, fore tota ut labet disciplina. Quod autem satis est, eo quicquid accessit, nimium est; 


	Atqui reperies, inquit, in hoc quidem pertinacem;
	Et ille ridens: Age, age, inquit,-satis enim scite me nostri sermonis principium esse voluisti-exponamus adolescenti,.
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Deinde qui fit, ut ego nesciam, sciant omnes, quicumque Epicurei esse voluerunt?
	Multoque hoc melius nos veriusque quam Stoici.
	Facillimum id quidem est, inquam.




	Neque enim civitas in seditione beata esse potest nec in discordia dominorum domus;



Quid ergo attinet gloriose loqui, nisi constanter loquare? Nihil enim iam habes, quod ad corpus referas; Idemne, quod iucunde? An tu me de L. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Haec quo modo conveniant, non sane intellego. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. 

Hic nihil fuit, quod quaereremus. Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur. Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere. Tanta vis admonitionis inest in locis; Illum mallem levares, quo optimum atque humanissimum virum, Cn. Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Claudii libidini, qui tum erat summo ne imperio, dederetur. 


Ut bacillum aliud est inflexum et incurvatum de industria,
aliud ita natum, sic ferarum natura non est illa quidem
depravata mala disciplina, sed natura sua.

Sed erat aequius Triarium aliquid de dissensione nostra
iudicare.




	Nihil sane.
	Vulgo enim dicitur: Iucundi acti labores, nec male Euripidesconcludam, si potero, Latine;
	Sed fortuna fortis;
	Qualis ista philosophia est, quae non interitum afferat pravitatis, sed sit contenta mediocritate vitiorum?
	A mene tu?
	Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret?




Aut etiam, ut vestitum, sic sententiam habeas aliam
domesticam, aliam forensem, ut in fronte ostentatio sit,
intus veritas occultetur?

At modo dixeras nihil in istis rebus esse, quod interesset.




	Confecta res esset.
	Dic in quovis conventu te omnia facere, ne doleas.
	Quis negat?
	Quem ad modum quis ambulet, sedeat, qui ductus oris, qui vultus in quoque sit?
	Stoicos roga.
	Quam illa ardentis amores excitaret sui! Cur tandem?
	Quae sequuntur igitur?
	Eiuro, inquit adridens, iniquum, hac quidem de re;
	Sint ista Graecorum;
	Quid autem habent admirationis, cum prope accesseris?



Pudebit te, inquam, illius tabulae, quam Cleanthes sane commode verbis depingere solebat. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Quid autem habent admirationis, cum prope accesseris? Consequentia exquirere, quoad sit id, quod volumus, effectum. 

Nihil opus est exemplis hoc facere longius. Quasi ego id curem, quid ille aiat aut neget. Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus. Ut in geometria, prima si dederis, danda sunt omnia. Omnia contraria, quos etiam insanos esse vultis. 

Ne in odium veniam, si amicum destitero tueri. Quam ob rem tandem, inquit, non satisfacit? Inde sermone vario sex illa a Dipylo stadia confecimus. Quid enim de amicitia statueris utilitatis causa expetenda vides. Quid ait Aristoteles reliquique Platonis alumni? Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Quorum sine causa fieri nihil putandum est. Id mihi magnum videtur. 


	Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;
	Tollenda est atque extrahenda radicitus.
	Hoc enim constituto in philosophia constituta sunt omnia.
	At ille pellit, qui permulcet sensum voluptate.




