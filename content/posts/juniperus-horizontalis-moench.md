Title: Juniperus horizontalis Moench
Description: Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.
Image: https://source.unsplash.com/random/?android
Date: 16/9/2021
Keywords: work
Category: Y-Solowarm
Author: Photobean
Tags: cms
Published: true
Background: #6308ec
Color: #152a1e
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid de Platone aut de Democrito loquar? Conferam tecum, quam cuique verso rem subicias; Addidisti ad extremum etiam indoctum fuisse. Isto modo, ne si avia quidem eius nata non esset. Duo Reges: constructio interrete. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. 


Epicurei num desistunt de isdem, de quibus et ab Epicuro
scriptum est et ab antiquis, ad arbitrium suum scribere?

Sin dicit obscurari quaedam nec apparere, quia valde parva
sint, nos quoque concedimus;



Consequatur summas voluptates non modo parvo, sed per me nihilo, si potest; Quid affers, cur Thorius, cur Caius Postumius, cur omnium horum magister, Orata, non iucundissime vixerit? Eadem nunc mea adversum te oratio est. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Omnes enim iucundum motum, quo sensus hilaretur. 


	Maximeque eos videre possumus res gestas audire et legere velle, qui a spe gerendi absunt confecti senectute.
	Cum salvum esse flentes sui respondissent, rogavit essentne fusi hostes.




Nos grave certamen belli clademque tenemus, Graecia quam
Troiae divino numine vexit, Omniaque e latis rerum vestigia
terris.

Praeterea et appetendi et refugiendi et omnino rerum
gerendarum initia proficiscuntur aut a voluptate aut a
dolore.




	Nec hoc ille non vidit, sed verborum magnificentia est et gloria delectatus.
	In omni enim arte vel studio vel quavis scientia vel in ipsa virtute optimum quidque rarissimum est.
	Quid enim de amicitia statueris utilitatis causa expetenda vides.
	Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt.
	Ne discipulum abducam, times.




	Numquam facies.
	Traditur, inquit, ab Epicuro ratio neglegendi doloris.
	Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse.
	Sed ea mala virtuti magnitudine obruebantur.




	Hic si Peripateticus fuisset, permansisset, credo, in sententia, qui dolorem malum dicunt esse, de asperitate autem eius fortiter ferenda praecipiunt eadem, quae Stoici.



Qui ita affectus, beatum esse numquam probabis; Primum Theophrasti, Strato, physicum se voluit; Licet hic rursus ea commemores, quae optimis verbis ab Epicuro de laude amicitiae dicta sunt. Sit, inquam, tam facilis, quam vultis, comparatio voluptatis, quid de dolore dicemus? Quis negat? Dempta enim aeternitate nihilo beatior Iuppiter quam Epicurus; 


	Hoc non est positum in nostra actione.
	Et si in ipsa gubernatione neglegentia est navis eversa, maius est peccatum in auro quam in palea.
	Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia.




	Optime, inquam.
	Ex quo, id quod omnes expetunt, beate vivendi ratio inveniri et comparari potest.
	Easdemne res?
	Ne amores quidem sanctos a sapiente alienos esse arbitrantur.
	Ille incendat?
	Hic ambiguo ludimur.
	Sedulo, inquam, faciam.
	Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.
	Sint ista Graecorum;
	Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest.



Itaque hic ipse iam pridem est reiectus; Honesta oratio, Socratica, Platonis etiam. Quorum altera prosunt, nocent altera. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Apparet statim, quae sint officia, quae actiones. An hoc usque quaque, aliter in vita? 

Istam voluptatem, inquit, Epicurus ignorat? Aeque enim contingit omnibus fidibus, ut incontentae sint. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Qui est in parvis malis. Quae quidem sapientes sequuntur duce natura tamquam videntes; Quid, de quo nulla dissensio est? 

Numquam facies. Id enim natura desiderat. Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. Itaque hic ipse iam pridem est reiectus; Facillimum id quidem est, inquam. 

Qui autem esse poteris, nisi te amor ipse ceperit? Quid, de quo nulla dissensio est? An ea, quae per vinitorem antea consequebatur, per se ipsa curabit? Aliter enim nosmet ipsos nosse non possumus. Tum Torquatus: Prorsus, inquit, assentior; Post enim Chrysippum eum non sane est disputatum. 

Tibi hoc incredibile, quod beatissimum. At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Estne, quaeso, inquam, sitienti in bibendo voluptas? Tubulo putas dicere? Uterque enim summo bono fruitur, id est voluptate. Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. Sed ea mala virtuti magnitudine obruebantur. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Quid ait Aristoteles reliquique Platonis alumni? Ita prorsus, inquam; Sed videbimus. Cur iustitia laudatur? Quid est, quod ab ea absolvi et perfici debeat? 


	Atque haec contra Aristippum, qui eam voluptatem non modo summam, sed solam etiam ducit, quam omnes unam appellamus voluptatem.




	Quo modo?
	Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene.
	An eiusdem modi?
	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.



Praeteritis, inquit, gaudeo. Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus. Sed haec omittamus; Explanetur igitur. Quod si ita se habeat, non possit beatam praestare vitam sapientia. Tum Triarius: Posthac quidem, inquit, audacius. Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? 

Si qua in iis corrigere voluit, deteriora fecit. A quibus propter discendi cupiditatem videmus ultimas terras esse peragratas. Quod totum contra est. Quae diligentissime contra Aristonem dicuntur a Chryippo. Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. 


