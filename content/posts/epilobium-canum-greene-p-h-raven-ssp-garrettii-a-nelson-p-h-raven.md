Title: Epilobium canum (Greene) P.H. Raven ssp. garrettii (A. Nelson) P.H. Raven
Description: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.
Image: https://source.unsplash.com/random/?beauty
Date: 10/7/2021
Keywords: cms
Category: Ronstring
Author: Photospace
Tags: post
Published: true
Background: #a3f48f
Color: #5d2e53
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Ne in odium veniam, si amicum destitero tueri. Et harum quidem rerum facilis est et expedita distinctio. Utilitatis causa amicitia est quaesita. 


	Non semper, inquam;
	Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus.
	Quid Zeno?
	Ex rebus enim timiditas, non ex vocabulis nascitur.
	Falli igitur possumus.
	Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit.




	Aliud igitur esse censet gaudere, aliud non dolere.
	Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas.
	Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere.
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.




	Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem.
	Negat enim summo bono afferre incrementum diem.



Obsecro, inquit, Torquate, haec dicit Epicurus? Est enim effectrix multarum et magnarum voluptatum. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Tu quidem reddes; Si id dicis, vicimus. Immo videri fortasse. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Id est enim, de quo quaerimus. 

Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare; Gracchum patrem non beatiorem fuisse quam fillum, cum alter stabilire rem publicam studuerit, alter evertere. ALIO MODO. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Quae cum dixisset, finem ille. Equidem, sed audistine modo de Carneade? Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Cur iustitia laudatur? 


	Nec vero potest quisquam de bonis et malis vere iudicare nisi omni cognita ratione naturae et vitae etiam deorum, et utrum conveniat necne natura hominis cum universa.



Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Sit hoc ultimum bonorum, quod nunc a me defenditur; Illa tamen simplicia, vestra versuta. Graecum enim hunc versum nostis omnes-: Suavis laborum est praeteritorum memoria. Omnes enim iucundum motum, quo sensus hilaretur. Non dolere, inquam, istud quam vim habeat postea videro; Nulla profecto est, quin suam vim retineat a primo ad extremum. Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. 

Primum divisit ineleganter; Duo Reges: constructio interrete. Sed tempus est, si videtur, et recta quidem ad me. Id est enim, de quo quaerimus. Erit enim mecum, si tecum erit. Invidiosum nomen est, infame, suspectum. Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Cui Tubuli nomen odio non est? Nihil sane. 


	Quare conare, quaeso.
	Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi.



Quid est igitur, inquit, quod requiras? Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. Quia nec honesto quic quam honestius nec turpi turpius. Longum est enim ad omnia respondere, quae a te dicta sunt. Est autem a te semper dictum nec gaudere quemquam nisi propter corpus nec dolere. Duo enim genera quae erant, fecit tria. Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. 

Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Ne discipulum abducam, times. Satisne ergo pudori consulat, si quis sine teste libidini pareat? Tecum optime, deinde etiam cum mediocri amico. Dic in quovis conventu te omnia facere, ne doleas. Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. Sed nimis multa. Hanc ergo intuens debet institutum illud quasi signum absolvere. Ita enim vivunt quidam, ut eorum vita refellatur oratio. Dici enim nihil potest verius. Hoc unum Aristo tenuit: praeter vitia atque virtutes negavit rem esse ullam aut fugiendam aut expetendam. Quamquam tu hanc copiosiorem etiam soles dicere. 


	Haec non erant eius, qui innumerabilis mundos infinitasque regiones, quarum nulla esset ora, nulla extremitas, mente peragravisset.
	Nam adhuc, meo fortasse vitio, quid ego quaeram non perspicis.
	Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit?



Omnia peccata paria dicitis. Tum ille timide vel potius verecunde: Facio, inquit. Duarum enim vitarum nobis erunt instituta capienda. Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Habent enim et bene longam et satis litigiosam disputationem. Non igitur bene. 


Suis cuiusque sensibus sic, ut, contra si quis dicere velit,
non audiatur -, tamen, ne quid praetermittamus, rationes
quoque, cur hoc ita sit, afferendas puto.

Nunc omni virtuti vitium contrario nomine opponitur.




Atqui si, ut convenire debet inter nos, est quaedam
appetitio naturalis ea, quae secundum naturam sunt,
appetens, eorum omnium est aliquae summa facienda.

Ita multo sanguine profuso in laetitia et in victoria est
mortuus.




	Atqui si, ut convenire debet inter nos, est quaedam appetitio naturalis ea, quae secundum naturam sunt, appetens, eorum omnium est aliquae summa facienda.




	Si quae forte-possumus.
	Atque hoc dabitis, ut opinor, si modo sit aliquid esse beatum, id oportere totum poni in potestate sapientis.
	In schola desinis.
	Aliter enim nosmet ipsos nosse non possumus.
	Ita prorsus, inquam;
	Voluptatem cum summum bonum diceret, primum in eo ipso parum vidit, deinde hoc quoque alienum;



Et adhuc quidem ita nobis progresso ratio est, ut ea duceretur omnis a prima commendatione naturae. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Quia nec honesto quic quam honestius nec turpi turpius. Tum Quintus: Est plane, Piso, ut dicis, inquit. Quis istum dolorem timet? Nonne igitur tibi videntur, inquit, mala? Non quam nostram quidem, inquit Pomponius iocans; Quis hoc dicit? Illum mallem levares, quo optimum atque humanissimum virum, Cn. 

Hoc loco tenere se Triarius non potuit. Pauca mutat vel plura sane; Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; Bonum valitudo: miser morbus. Nemo igitur esse beatus potest. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Efficiens dici potest. 


