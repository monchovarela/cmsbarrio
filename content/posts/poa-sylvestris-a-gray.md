Title: Poa sylvestris A. Gray
Description: Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.
Image: https://source.unsplash.com/random/?modern
Date: 23/10/2020
Keywords: web
Category: Greenlam
Author: Thoughtbridge
Tags: portfolio
Published: true
Background: #b63dcf
Color: #32dec5
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Atqui reperies, inquit, in hoc quidem pertinacem; Aut haec tibi, Torquate, sunt vituperanda aut patrocinium voluptatis repudiandum. In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Duo Reges: constructio interrete. Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Pudebit te, inquam, illius tabulae, quam Cleanthes sane commode verbis depingere solebat. Quod cum dixissent, ille contra. An eiusdem modi? 

Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Vitae autem degendae ratio maxime quidem illis placuit quieta. Et nunc quidem quod eam tuetur, ut de vite potissimum loquar, est id extrinsecus; Quid loquor de nobis, qui ad laudem et ad decus nati, suscepti, instituti sumus? Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti. Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. 


	Avaritiamne minuis?
	Ne in odium veniam, si amicum destitero tueri.
	Magna laus.
	Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia.
	Respondeat totidem verbis.
	At iam decimum annum in spelunca iacet.
	Sullae consulatum?
	Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus?
	Magna laus.
	Vadem te ad mortem tyranno dabis pro amico, ut Pythagoreus ille Siculo fecit tyranno?
	Immo alio genere;
	Tecum optime, deinde etiam cum mediocri amico.




Illud vero minime consectarium, sed in primis hebes, illorum
scilicet, non tuum, gloriatione dignam esse beatam vitam,
quod non possit sine honestate contingere, ut iure quisquam
glorietur.

Hic si Peripateticus fuisset, permansisset, credo, in
sententia, qui dolorem malum dicunt esse, de asperitate
autem eius fortiter ferenda praecipiunt eadem, quae Stoici.



Maximus dolor, inquit, brevis est. Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare? Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Hoc sic expositum dissimile est superiori. Quam ob rem tandem, inquit, non satisfacit? Ego vero isti, inquam, permitto. Quid de Platone aut de Democrito loquar? Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret? Quam tu ponis in verbis, ego positam in re putabam. 


	Quoniam igitur, ut medicina valitudinis, navigationis gubernatio, sic vivendi ars est prudente, necesse est eam quoque ab aliqua re esse constitutam et profectam.



Quibus ego vehementer assentior. Duae sunt enim res quoque, ne tu verba solum putes. Disserendi artem nullam habuit. Tecum optime, deinde etiam cum mediocri amico. Qui enim existimabit posse se miserum esse beatus non erit. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? 


	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus.
	Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere?
	Facillimum id quidem est, inquam.
	Vitiosum est enim in dividendo partem in genere numerare.
	Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia.
	Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere.



Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? Ita multo sanguine profuso in laetitia et in victoria est mortuus. Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Quid enim? Bona autem corporis huic sunt, quod posterius posui, similiora. Quod cum dixissent, ille contra. Sit ista in Graecorum levitate perversitas, qui maledictis insectantur eos, a quibus de veritate dissentiunt. Si quicquam extra virtutem habeatur in bonis. Memini vero, inquam; Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Illud dico, ea, quae dicat, praeclare inter se cohaerere. 


	Quo tandem modo?
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Ut optime, secundum naturam affectum esse possit.
	Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere?




	Proclivi currit oratio.
	Ergo hoc quidem apparet, nos ad agendum esse natos.
	Itaque contra est, ac dicitis;
	Quodsi ipsam honestatem undique pertectam atque absolutam.



Nullus est igitur cuiusquam dies natalis. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Deinde dolorem quem maximum? Minime vero, inquit ille, consentit. Ita multo sanguine profuso in laetitia et in victoria est mortuus. Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Ait enim se, si uratur, Quam hoc suave! dicturum. Laelius clamores sof�w ille so lebat Edere compellans gumias ex ordine nostros. 


	Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi.
	Igitur neque stultorum quisquam beatus neque sapientium non beatus.
	Primum cur ista res digna odio est, nisi quod est turpis?
	In parvis enim saepe, qui nihil eorum cogitant, si quando iis ludentes minamur praecipitaturos alicunde, extimescunt.



Quod non faceret, si in voluptate summum bonum poneret. Animum autem reliquis rebus ita perfecit, ut corpus; Quas enim kakaw Graeci appellant, vitia malo quam malitias nominare. 

Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; Sed mehercule pergrata mihi oratio tua. Itaque hic ipse iam pridem est reiectus; Ita multa dicunt, quae vix intellegam. Dolere malum est: in crucem qui agitur, beatus esse non potest. Tecum optime, deinde etiam cum mediocri amico. 


	Cum autem progrediens confirmatur animus, agnoscit ille quidem naturae vim, sed ita, ut progredi possit longius, per se sit tantum inchoata.



Primum quid tu dicis breve? Cyrenaici quidem non recusant; Quid sequatur, quid repugnet, vident. Qui ita affectus, beatum esse numquam probabis; Sed haec omittamus; Hoc tu nunc in illo probas. Gerendus est mos, modo recte sentiat. Illud dico, ea, quae dicat, praeclare inter se cohaerere. 


Quae cum dixissem, magis ut illum provocarem quam ut ipse
loquerer, tum Triarius leniter arridens: Tu quidem, inquit,
totum Epicurum paene e philosophorum choro sustulisti.

Nec tamen ullo modo summum pecudis bonum et hominis idem
mihi videri potest.



Ubi ut eam caperet aut quando? Si quicquam extra virtutem habeatur in bonis. Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. In schola desinis. 


	Facete M.
	An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia?
	Itaque ab his ordiamur.
	Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur;
	Quis negat?
	Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus.
	Scaevolam M.
	Tuo vero id quidem, inquam, arbitratu.




