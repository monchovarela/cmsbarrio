Title: Oenothera brachycarpa A. Gray
Description: Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.
Image: https://source.unsplash.com/random/?modern
Date: 19/5/2021
Keywords: web
Category: Duobam
Author: Flipopia
Tags: work
Published: true
Background: #ca4bea
Color: #e2b614
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hoc sane concedamus. Qua igitur re ab deo vincitur, si aeternitate non vincitur? Nescio quo modo praetervolavit oratio. Duo Reges: constructio interrete. Sed plane dicit quod intellegit. Age, inquies, ista parva sunt. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; Tamen aberramus a proposito, et, ne longius, prorsus, inquam, Piso, si ista mala sunt, placet. In schola desinis. Eaedem enim utilitates poterunt eas labefactare atque pervertere. 

Quid autem habent admirationis, cum prope accesseris? Primum in nostrane potestate est, quid meminerimus? Audeo dicere, inquit. Iubet igitur nos Pythius Apollo noscere nosmet ipsos. Cur post Tarentum ad Archytam? 


Res enim se praeclare habebat, et quidem in utraque parte.

An me, inquam, nisi te audire vellem, censes haec dicturum
fuisse?




Illud urgueam, non intellegere eum quid sibi dicendum sit,
cum dolorem summum malum esse dixerit.

An ea, quae per vinitorem antea consequebatur, per se ipsa
curabit?



Partim cursu et peragratione laetantur, congregatione aliae coetum quodam modo civitatis imitantur; Non autem hoc: igitur ne illud quidem. Haec quo modo conveniant, non sane intellego. Pauca mutat vel plura sane; 


	Sed non alienum est, quo facilius vis verbi intellegatur, rationem huius verbi faciendi Zenonis exponere.
	Sed tamen est aliquid, quod nobis non liceat, liceat illis.



Urgent tamen et nihil remittunt. At certe gravius. Quare conare, quaeso. Sin laboramus, quis est, qui alienae modum statuat industriae? Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? Suam denique cuique naturam esse ad vivendum ducem. Si autem id non concedatur, non continuo vita beata tollitur. Quodsi ipsam honestatem undique pertectam atque absolutam. Falli igitur possumus. 


	Pollicetur certe.
	Igitur neque stultorum quisquam beatus neque sapientium non beatus.
	Sed fortuna fortis;
	Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus;
	Negare non possum.
	Quid igitur dubitamus in tota eius natura quaerere quid sit effectum?
	Stoici scilicet.
	Nihil opus est exemplis hoc facere longius.



Quis est, qui non oderit libidinosam, protervam adolescentiam? Hoc sic expositum dissimile est superiori. Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt. Illa tamen simplicia, vestra versuta. Illa videamus, quae a te de amicitia dicta sunt. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Sed ad haec, nisi molestum est, habeo quae velim. 

Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere? Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Respondent extrema primis, media utrisque, omnia omnibus. Non semper, inquam; Nam neque virtute retinetur ille in vita, nec iis, qui sine virtute sunt, mors est oppetenda. Quae cum dixisset, finem ille. Vide, quantum, inquam, fallare, Torquate. 

Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. De malis autem et bonis ab iis animalibus, quae nondum depravata sint, ait optime iudicari. Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus. Illa videamus, quae a te de amicitia dicta sunt. Potius ergo illa dicantur: turpe esse, viri non esse debilitari dolore, frangi, succumbere. Quod autem ratione actum est, id officium appellamus. Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. Ea possunt paria non esse. 

Efficiens dici potest. Murenam te accusante defenderem. Nihil ad rem! Ne sit sane; Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Istam voluptatem perpetuam quis potest praestare sapienti? 

Sed tamen intellego quid velit. Praetereo multos, in bis doctum hominem et suavem, Hieronymum, quem iam cur Peripateticum appellem nescio. Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? Invidiosum nomen est, infame, suspectum. Esse enim quam vellet iniquus iustus poterat inpune. Tum Quintus: Est plane, Piso, ut dicis, inquit. Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. 


	Non enim hilaritate nec lascivia nec risu aut ioco, comite levitatis, saepe etiam tristes firmitate et constantia sunt beati.




	Si enim idem dicit, quod Hieronymus, qui censet summum bonum esse sine ulla molestia vivere, cur mavult dicere voluptatem quam vacuitatem doloris, ut ille facit, qui quid dicat intellegit?




	Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est;
	In qua quid est boni praeter summam voluptatem, et eam sempiternam?




	Ut non sine causa ex iis memoriae ducta sit disciplina.
	Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat;
	Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
	Quid iudicant sensus?
	Itaque a sapientia praecipitur se ipsam, si usus sit, sapiens ut relinquat.
	Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres.




	Audeo dicere, inquit.
	At cum de plurimis eadem dicit, tum certe de maximis.
	Sullae consulatum?
	Numquam hoc ita defendit Epicurus neque Metrodorus aut quisquam eorum, qui aut saperet aliquid aut ista didicisset.
	Ut pulsi recurrant?
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.
	Quonam modo?
	Quae tamen a te agetur non melior, quam illae sunt, quas interdum optines.
	Scrupulum, inquam, abeunti;
	Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant.




	Non potes ergo ista tueri, Torquate, mihi crede, si te ipse et tuas cogitationes et studia perspexeris;
	Quae autem natura suae primae institutionis oblita est?
	An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia?
	Ergo id est convenienter naturae vivere, a natura discedere.
	Sin est etiam corpus, ista explanatio naturae nempe hoc effecerit, ut ea, quae ante explanationem tenebamus, relinquamus.
	Quid, cum volumus nomina eorum, qui quid gesserint, nota nobis esse, parentes, patriam, multa praeterea minime necessaria?



Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia. An nisi populari fama? Quae cum magnifice primo dici viderentur, considerata minus probabantur. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. An est aliquid, quod te sua sponte delectet? Aliter homines, aliter philosophos loqui putas oportere? Omnes enim iucundum motum, quo sensus hilaretur. Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt; Sed ego in hoc resisto; 


