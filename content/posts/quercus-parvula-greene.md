Title: Quercus parvula Greene
Description: Phasellus in felis. Donec semper sapien a libero. Nam dui.
Image: https://source.unsplash.com/random/?design
Date: 13/2/2021
Keywords: cms
Category: Keylex
Author: Zooxo
Tags: work
Published: true
Background: #9c9f20
Color: #2aca64
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bonum negas esse divitias, praeposėtum esse dicis? Conferam avum tuum Drusum cum C. Sit sane ista voluptas. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Duo Reges: constructio interrete. Ita fit cum gravior, tum etiam splendidior oratio. Et si in ipsa gubernatione neglegentia est navis eversa, maius est peccatum in auro quam in palea. Itaque haec cum illis est dissensio, cum Peripateticis nulla sane. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Sed ad illum redeo. Hoc enim constituto in philosophia constituta sunt omnia. 

Claudii libidini, qui tum erat summo ne imperio, dederetur. Mihi enim satis est, ipsis non satis. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quid ergo attinet dicere: Nihil haberem, quod reprehenderem, si finitas cupiditates haberent? 

Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Et ille ridens: Video, inquit, quid agas; Haec quo modo conveniant, non sane intellego. 


	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Nondum autem explanatum satis, erat, quid maxime natura vellet.
	Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum.
	A primo, ut opinor, animantium ortu petitur origo summi boni.
	Quia nec honesto quic quam honestius nec turpi turpius.




	Idemque diviserunt naturam hominis in animum et corpus.



Quis istud, quaeso, nesciebat? Illa tamen simplicia, vestra versuta. At cum de plurimis eadem dicit, tum certe de maximis. Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest. Recte, inquit, intellegis. Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Sit sane ista voluptas. Nam Pyrrho, Aristo, Erillus iam diu abiecti. 


	Negare non possum.
	Sed finge non solum callidum eum, qui aliquid improbe faciat, verum etiam praepotentem, ut M.
	Sed haec omittamus;
	Ut alios omittam, hunc appello, quem ille unum secutus est.
	Praeclare hoc quidem.
	De maximma autem re eodem modo, divina mente atque natura mundum universum et eius maxima partis administrari.
	Est, ut dicis, inquam.
	Sit ista in Graecorum levitate perversitas, qui maledictis insectantur eos, a quibus de veritate dissentiunt.




	Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere.



Tria genera cupiditatum, naturales et necessariae, naturales et non necessariae, nec naturales nec necessariae. Facit igitur Lucius noster prudenter, qui audire de summo bono potissimum velit; Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Hoc non est positum in nostra actione. Quae in controversiam veniunt, de iis, si placet, disseramus. Isto modo ne improbos quidem, si essent boni viri. Videsne, ut haec concinant? Illud non continuo, ut aeque incontentae. 


	Ille incendat?
	Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.
	Quonam, inquit, modo?
	An me, inquam, nisi te audire vellem, censes haec dicturum fuisse?
	Recte, inquit, intellegis.
	Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur.



At ille pellit, qui permulcet sensum voluptate. Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Tria genera bonorum; Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Respondent extrema primis, media utrisque, omnia omnibus. Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. 

Inde sermone vario sex illa a Dipylo stadia confecimus. Satisne ergo pudori consulat, si quis sine teste libidini pareat? Ea, quae dialectici nunc tradunt et docent, nonne ab illis instituta sunt aut inventa sunt? Quae si potest singula consolando levare, universa quo modo sustinebit? Sin autem est in ea, quod quidam volunt, nihil impedit hanc nostram comprehensionem summi boni. 


An dubium est, quin virtus ita maximam partem optineat in
rebus humanis, ut reliquas obruat?

Pungunt quasi aculeis interrogatiunculis angustis, quibus
etiam qui assentiuntur nihil commutantur animo et idem
abeunt, qui venerant.




Semper enim ex eo, quod maximas partes continet latissimeque
funditur, tota res appellatur.

Teneo, inquit, finem illi videri nihil dolere.




	Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus;
	Sed ne, dum huic obsequor, vobis molestus sim.



Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; In qua si nihil est praeter rationem, sit in una virtute finis bonorum; Cur, nisi quod turpis oratio est? Contemnit enim disserendi elegantiam, confuse loquitur. Atqui reperies, inquit, in hoc quidem pertinacem; In qua quid est boni praeter summam voluptatem, et eam sempiternam? Non enim iam stirpis bonum quaeret, sed animalis. Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus. 


	Non enim in selectione virtus ponenda erat, ut id ipsum, quod erat bonorum ultimum, aliud aliquid adquireret.
	Idemne potest esse dies saepius, qui semel fuit?




	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.
	Verba tu fingas et ea dicas, quae non sentias?
	Naturales divitias dixit parabiles esse, quod parvo esset natura contenta.
	Videamus animi partes, quarum est conspectus illustrior;



Sequitur disserendi ratio cognitioque naturae; Duo enim genera quae erant, fecit tria. Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. Non dolere, inquam, istud quam vim habeat postea videro; Eaedem res maneant alio modo. 

Que Manilium, ab iisque M. Quo modo? 


