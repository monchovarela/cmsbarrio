Title: Polycnemum L.
Description: Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.
Image: https://source.unsplash.com/random/?css3
Date: 24/8/2021
Keywords: article
Category: Solarbreeze
Author: Tagchat
Tags: cms
Published: true
Background: #a390ac
Color: #3c3c8d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? Et nemo nimium beatus est; Memini vero, inquam; Tu enim ista lenius, hic Stoicorum more nos vexat. Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur. At enim, qua in vita est aliquid mali, ea beata esse non potest. Duo Reges: constructio interrete. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. 


	Scrupulum, inquam, abeunti;
	Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit?
	Si quae forte-possumus.
	Non semper, inquam;
	Nos commodius agimus.
	Amicitiae vero locus ubi esse potest aut quis amicus esse cuiquam, quem non ipsum amet propter ipsum?
	Equidem e Cn.
	Immo sit sane nihil melius, inquam-nondum enim id quaero-, num propterea idem voluptas est, quod, ut ita dicam, indolentia?



Quis est enim, in quo sit cupiditas, quin recte cupidus dici possit? Audeo dicere, inquit. Quamquam id quidem, infinitum est in hac urbe; Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. Quod si ita se habeat, non possit beatam praestare vitam sapientia. 


Atque ut reliqui fures earum rerum, quas ceperunt, signa
commutant, sic illi, ut sententiis nostris pro suis
uterentur, nomina tamquam rerum notas mutaverunt.

Idem etiam dolorem saepe perpetiuntur, ne, si id non
faciant, incidant in maiorem.



Restincta enim sitis stabilitatem voluptatis habet, inquit, illa autem voluptas ipsius restinctionis in motu est. Hunc vos beatum; Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? 


	Vives, inquit Aristo, magnifice atque praeclare, quod erit cumque visum ages, numquam angere, numquam cupies, numquam timebis.




	Quid adiuvas?
	Optime, inquam.
	Ita credo.
	Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere.
	Hunc vos beatum;
	Ratio enim nostra consentit, pugnat oratio.
	Istic sum, inquit.
	Quodsi ipsam honestatem undique pertectam atque absolutam.
	Sullae consulatum?
	Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur.



Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Idem iste, inquam, de voluptate quid sentit? Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Primum Theophrasti, Strato, physicum se voluit; Quae similitudo in genere etiam humano apparet. Ratio quidem vestra sic cogit. Faceres tu quidem, Torquate, haec omnia; Quae quo sunt excelsiores, eo dant clariora indicia naturae. 

Idemque diviserunt naturam hominis in animum et corpus. Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio. An potest, inquit ille, quicquam esse suavius quam nihil dolere? An est aliquid, quod te sua sponte delectet? Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Qualis ista philosophia est, quae non interitum afferat pravitatis, sed sit contenta mediocritate vitiorum? Ut in geometria, prima si dederis, danda sunt omnia. Peccata paria. Beatus autem esse in maximarum rerum timore nemo potest. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? 

Sed haec nihil sane ad rem; Qualem igitur hominem natura inchoavit? Scisse enim te quis coarguere possit? Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. Conferam tecum, quam cuique verso rem subicias; Ut pulsi recurrant? Quae cum essent dicta, discessimus. Quare ad ea primum, si videtur; 

Te enim iudicem aequum puto, modo quae dicat ille bene noris. Scio enim esse quosdam, qui quavis lingua philosophari possint; Qui ita affectus, beatum esse numquam probabis; Huius ego nunc auctoritatem sequens idem faciam. 


	Negat enim summo bono afferre incrementum diem.
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.
	At enim, qua in vita est aliquid mali, ea beata esse non potest.




	At ille pellit, qui permulcet sensum voluptate.
	In omni enim arte vel studio vel quavis scientia vel in ipsa virtute optimum quidque rarissimum est.
	Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant.
	Ita similis erit ei finis boni, atque antea fuerat, neque idem tamen;



Maximas vero virtutes iacere omnis necesse est voluptate dominante. Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis. Ut proverbia non nulla veriora sint quam vestra dogmata. 


Physicae quoque non sine causa tributus idem est honos,
propterea quod, qui convenienter naturae victurus sit, ei
proficiscendum est ab omni mundo atque ab eius procuratione.

Piso igitur hoc modo, vir optimus tuique, ut scis,
amantissimus.




	Hic ambiguo ludimur.
	Quare attende, quaeso.
	Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit?
	Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur.



Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Quod cum dixissent, ille contra. Illa tamen simplicia, vestra versuta. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. De illis, cum volemus. Sin kakan malitiam dixisses, ad aliud nos unum certum vitium consuetudo Latina traduceret. 


	Sed tempus est, si videtur, et recta quidem ad me.
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?
	Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres.
	Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant.
	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.



Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Sic enim maiores nostri labores non fugiendos tristissimo tamen verbo aerumnas etiam in deo nominaverunt. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. 


	Non enim, si omnia non sequebatur, idcirco non erat ortus illinc.




