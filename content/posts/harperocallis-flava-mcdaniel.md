Title: Harperocallis flava McDaniel
Description: In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
Image: https://source.unsplash.com/random/?iphone
Date: 21/6/2021
Keywords: cms
Category: Duobam
Author: Skidoo
Tags: article
Published: true
Background: #638b80
Color: #60ae00
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Duo Reges: constructio interrete. Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit? Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Pungunt quasi aculeis interrogatiunculis angustis, quibus etiam qui assentiuntur nihil commutantur animo et idem abeunt, qui venerant. Sed virtutem ipsam inchoavit, nihil amplius. 

Hanc ergo intuens debet institutum illud quasi signum absolvere. Non enim solum Torquatus dixit quid sentiret, sed etiam cur. Eaedem res maneant alio modo. Nam si amitti vita beata potest, beata esse non potest. Illa videamus, quae a te de amicitia dicta sunt. 


Non ego iam Epaminondae, non Leonidae mortem huius morti
antepono, quorum alter cum vicisset Lacedaemonios apud
Mantineam atque ipse gravi vulnere exanimari se videret, ut
primum dispexit, quaesivit salvusne esset clipeus.

Hic, qui utrumque probat, ambobus debuit uti, sicut facit
re, neque tamen dividit verbis.



Quorum sine causa fieri nihil putandum est. Pauca mutat vel plura sane; Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim? Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus. Immo alio genere; Qui est in parvis malis. 

Quae quidem vel cum periculo est quaerenda vobis; Tum Torquatus: Prorsus, inquit, assentior; Qualem igitur hominem natura inchoavit? Quonam modo? Summum en�m bonum exposuit vacuitatem doloris; Dempta enim aeternitate nihilo beatior Iuppiter quam Epicurus; Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Cur, nisi quod turpis oratio est? 


	Peccata autem partim esse tolerabilia, partim nullo modo, propterea quod alia peccata plures, alia pauciores quasi numeros officii praeterirent.




	Itaque idem natura victus, cui obsisti non potest, dicit alio loco id, quod a te etiam paulo ante dictum est, non posse iucunde vivi nisi etiam honeste.




	Deque his rebus satis multa in nostris de re publica libris sunt dicta a Laelio.
	Te autem hortamur omnes, currentem quidem, ut spero, ut eos, quos novisse vis, imitari etiam velis.
	Minime vero, inquit ille, consentit.




	Dic in quovis conventu te omnia facere, ne doleas.
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	In eo enim positum est id, quod dicimus esse expetendum.
	Piso igitur hoc modo, vir optimus tuique, ut scis, amantissimus.



Ubi ut eam caperet aut quando? Gerendus est mos, modo recte sentiat. Quem ad modum quis ambulet, sedeat, qui ductus oris, qui vultus in quoque sit? Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat. 


	Quamquam tu hanc copiosiorem etiam soles dicere.
	Non potes, nisi retexueris illa.
	Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate.
	Videmus igitur ut conquiescere ne infantes quidem possint.
	Dolere malum est: in crucem qui agitur, beatus esse non potest.




	Restatis igitur vos;
	Stoicos roga.
	Idem adhuc;
	Istam voluptatem, inquit, Epicurus ignorat?
	Istic sum, inquit.
	Paulum, cum regem Persem captum adduceret, eodem flumine invectio?
	Si longus, levis;
	Quis enim redargueret?



Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Quare si potest esse beatus is, qui est in asperis reiciendisque rebus, potest is quoque esse. Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Aliter homines, aliter philosophos loqui putas oportere? Videmus igitur ut conquiescere ne infantes quidem possint. Si quicquam extra virtutem habeatur in bonis. A mene tu? Non quam nostram quidem, inquit Pomponius iocans; Quodsi ipsam honestatem undique pertectam atque absolutam. At enim hic etiam dolore. 


	Pollicetur certe.
	Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat.
	Nihil sane.
	Summum en�m bonum exposuit vacuitatem doloris;
	Quis hoc dicit?
	Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L.
	Istic sum, inquit.
	Quantum Aristoxeni ingenium consumptum videmus in musicis?
	Perge porro;
	Ut alios omittam, hunc appello, quem ille unum secutus est.



Quo modo autem philosophus loquitur? Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Omnia contraria, quos etiam insanos esse vultis. Contemnit enim disserendi elegantiam, confuse loquitur. Hic ego: Pomponius quidem, inquam, noster iocari videtur, et fortasse suo iure. Verum hoc idem saepe faciamus. Si quicquam extra virtutem habeatur in bonis. 


	Septem autem illi non suo, sed populorum suffragio omnium nominati sunt.
	Quam si explicavisset, non tam haesitaret.
	An me, inquam, nisi te audire vellem, censes haec dicturum fuisse?
	At miser, si in flagitiosa et vitiosa vita afflueret voluptatibus.




Iam autem Callipho aut Diodorus quo modo poterunt tibi istud
concedere, qui ad honestatem aliud adiungant, quod ex eodem
genere non sit?

An dubium est, quin virtus ita maximam partem optineat in
rebus humanis, ut reliquas obruat?



Hoc est non modo cor non habere, sed ne palatum quidem. Suo enim quisque studio maxime ducitur. Sedulo, inquam, faciam. Summum en�m bonum exposuit vacuitatem doloris; Si quae forte-possumus. Quare conare, quaeso. Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Cum salvum esse flentes sui respondissent, rogavit essentne fusi hostes. Ea, quae dialectici nunc tradunt et docent, nonne ab illis instituta sunt aut inventa sunt? 

Primum cur ista res digna odio est, nisi quod est turpis? Quo modo autem optimum, si bonum praeterea nullum est? Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Ita nemo beato beatior. Unum est sine dolore esse, alterum cum voluptate. Sed utrum hortandus es nobis, Luci, inquit, an etiam tua sponte propensus es? 

Nam Metrodorum non puto ipsum professum, sed, cum appellaretur ab Epicuro, repudiare tantum beneficium noluisse; Ille enim occurrentia nescio quae comminiscebatur; Si longus, levis. Certe non potest. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Eaedem res maneant alio modo. At certe gravius. Comprehensum, quod cognitum non habet? Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Sed fortuna fortis; Quam ob rem tandem, inquit, non satisfacit? 


