Title: Mentzelia goodrichii Thorne & S.L. Welsh
Description: Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.
Image: https://source.unsplash.com/random/?php
Date: 23/3/2021
Keywords: article
Category: Flowdesk
Author: Voolia
Tags: work
Published: true
Background: #34b196
Color: #6449d5
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bonum integritas corporis: misera debilitas. Cum audissem Antiochum, Brute, ut solebam, cum M. Que Manilium, ab iisque M. Ait enim se, si uratur, Quam hoc suave! dicturum. Duo Reges: constructio interrete. Quam si explicavisset, non tam haesitaret. Nemo nostrum istius generis asotos iucunde putat vivere. Tum mihi Piso: Quid ergo? 

Quamvis enim depravatae non sint, pravae tamen esse possunt. Quid iudicant sensus? Comprehensum, quod cognitum non habet? Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Non risu potius quam oratione eiciendum? 


	Nihilo magis.
	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Quid ergo?
	Quo modo?




Vives, inquit Aristo, magnifice atque praeclare, quod erit
cumque visum ages, numquam angere, numquam cupies, numquam
timebis.

Istam voluptatem, inquit, Epicurus ignorat?



Idem iste, inquam, de voluptate quid sentit? Quod cum dixissent, ille contra. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Graece donan, Latine voluptatem vocant. Zenonis est, inquam, hoc Stoici. Bonum patria: miserum exilium. Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere. Certe, nisi voluptatem tanti aestimaretis. At modo dixeras nihil in istis rebus esse, quod interesset. 

Quis enim redargueret? Quis istum dolorem timet? Que Manilium, ab iisque M. Efficiens dici potest. 

Cur haec eadem Democritus? Indicant pueri, in quibus ut in speculis natura cernitur. Respondent extrema primis, media utrisque, omnia omnibus. Eam tum adesse, cum dolor omnis absit; Eiuro, inquit adridens, iniquum, hac quidem de re; 


	Aliter homines, aliter philosophos loqui putas oportere?
	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Que Manilium, ab iisque M.
	Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti.




	Quamquam non negatis nos intellegere quid sit voluptas, sed quid ille dicat.
	Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster?
	Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis.
	Qui autem diffidet perpetuitati bonorum suorum, timeat necesse est, ne aliquando amissis illis sit miser.
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.




	Quo tandem modo?
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.
	Certe non potest.
	Quicquid porro animo cernimus, id omne oritur a sensibus;
	Quid iudicant sensus?
	Haec non erant eius, qui innumerabilis mundos infinitasque regiones, quarum nulla esset ora, nulla extremitas, mente peragravisset.




	Ita enim se Athenis collocavit, ut sit paene unus ex Atticis, ut id etiam cognomen videatur habiturus.
	-, sed ut hoc iudicaremus, non esse in iis partem maximam positam beate aut secus vivendi.
	Mihi quidem Homerus huius modi quiddam vidisse videatur in iis, quae de Sirenum cantibus finxerit.
	Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest.



Qualis ista philosophia est, quae non interitum afferat pravitatis, sed sit contenta mediocritate vitiorum? Ait enim se, si uratur, Quam hoc suave! dicturum. Ergo, inquit, tibi Q. Quae cum dixisset paulumque institisset, Quid est? In eo enim positum est id, quod dicimus esse expetendum. Dolere malum est: in crucem qui agitur, beatus esse non potest. 

Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. Quid de Pythagora? Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Ita credo. Quam si explicavisset, non tam haesitaret. Claudii libidini, qui tum erat summo ne imperio, dederetur. Occultum facinus esse potuerit, gaudebit; Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt. 

Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Voluptatem cum summum bonum diceret, primum in eo ipso parum vidit, deinde hoc quoque alienum; Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Sed haec in pueris; Immo videri fortasse. 


	Quid tibi, Torquate, quid huic Triario litterae, quid historiae cognitioque rerum, quid poetarum evolutio, quid tanta tot versuum memoria voluptatis affert?




	Hoc enim constituto in philosophia constituta sunt omnia.
	Hoc est dicere: Non reprehenderem asotos, si non essent asoti.




Itaque non ob ea solum incommoda, quae eveniunt inprobis,
fugiendam inprobitatem putamus, sed multo etiam magis, quod,
cuius in animo versatur, numquam sinit eum respirare,
numquam adquiescere.

Nos autem non solum beatae vitae istam esse oblectationem
videmus, sed etiam levamentum miseriarum.




	Itaque eos id agere, ut a se dolores, morbos, debilitates repellant.



Deinde dolorem quem maximum? Sed quanta sit alias, nunc tantum possitne esse tanta. Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Quibus ego vehementer assentior. Quare ad ea primum, si videtur; Quodsi ipsam honestatem undique pertectam atque absolutam. 

Quid, si non sensus modo ei sit datus, verum etiam animus hominis? Nihil opus est exemplis hoc facere longius. Quae in controversiam veniunt, de iis, si placet, disseramus. Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Prioris generis est docilitas, memoria; Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. 


