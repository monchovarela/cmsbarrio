Title: Hyssopus officinalis L.
Description: Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.
Image: https://source.unsplash.com/random/?beauty
Date: 3/5/2021
Keywords: web
Category: Subin
Author: Tazzy
Tags: cms
Published: true
Background: #1bc1dd
Color: #8b0805
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullus est igitur cuiusquam dies natalis. Sit enim idem caecus, debilis. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Non enim solum Torquatus dixit quid sentiret, sed etiam cur. Duo Reges: constructio interrete. Ut id aliis narrare gestiant? Est enim effectrix multarum et magnarum voluptatum. Omnia contraria, quos etiam insanos esse vultis. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. 


	Id enim volumus, id contendimus, ut officii fructus sit ipsum officium.
	Igitur neque stultorum quisquam beatus neque sapientium non beatus.




Ex quo magnitudo quoque animi existebat, qua facile posset
repugnari obsistique fortunae, quod maximae res essent in
potestate sapientis.

Quis, quaeso, inquit, est, qui quid sit voluptas nesciat,
aut qui, quo magis id intellegat, definitionem aliquam
desideret?




	Nisi mihi Phaedrum, inquam, tu mentitum aut Zenonem putas, quorum utrumque audivi, cum mihi nihil sane praeter sedulitatem probarent, omnes mihi Epicuri sententiae satis notae sunt.



Ergo illi intellegunt quid Epicurus dicat, ego non intellego? In eo enim positum est id, quod dicimus esse expetendum. Sed ad haec, nisi molestum est, habeo quae velim. Numquam facies. At certe gravius. Ego vero volo in virtute vim esse quam maximam; Primum in nostrane potestate est, quid meminerimus? Aliter homines, aliter philosophos loqui putas oportere? 

Nihil est enim, de quo aliter tu sentias atque ego, modo commutatis verbis ipsas res conferamus. Aliter enim explicari, quod quaeritur, non potest. Suo genere perveniant ad extremum; Compensabatur, inquit, cum summis doloribus laetitia. Aliud igitur esse censet gaudere, aliud non dolere. Certe, nisi voluptatem tanti aestimaretis. Omnia contraria, quos etiam insanos esse vultis. Atque his de rebus et splendida est eorum et illustris oratio. Istam voluptatem, inquit, Epicurus ignorat? Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. 

Non quam nostram quidem, inquit Pomponius iocans; Habent enim et bene longam et satis litigiosam disputationem. Quid, de quo nulla dissensio est? Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur. Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus. Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est. Videsne quam sit magna dissensio? 

Quippe: habes enim a rhetoribus; Egone quaeris, inquit, quid sentiam? Nobis aliter videtur, recte secusne, postea; Consequentia exquirere, quoad sit id, quod volumus, effectum. Nondum autem explanatum satis, erat, quid maxime natura vellet. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit. Nam si amitti vita beata potest, beata esse non potest. Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; 


	Consequentia exquirere, quoad sit id, quod volumus, effectum.
	Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur?



Aliter homines, aliter philosophos loqui putas oportere? Memini vero, inquam; Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Hoc est dicere: Non reprehenderem asotos, si non essent asoti. Hoc est non dividere, sed frangere. Transfer idem ad modestiam vel temperantiam, quae est moderatio cupiditatum rationi oboediens. Atqui reperies, inquit, in hoc quidem pertinacem; 


	Scripsit enim et multis saepe verbis et breviter arteque in eo libro, quem modo nominavi, mortem nihil ad nos pertinere.



Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Solum praeterea formosum, solum liberum, solum civem, stultost; Quod autem principium officii quaerunt, melius quam Pyrrho; A mene tu? 


	Idem fecisset Epicurus, si sententiam hanc, quae nunc Hieronymi est, coniunxisset cum Aristippi vetere sententia.
	Mihi, inquam, qui te id ipsum rogavi?
	Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc?
	Universa enim illorum ratione cum tota vestra confligendum puto.



Quid ad utilitatem tantae pecuniae? Videamus animi partes, quarum est conspectus illustrior; Sin autem est in ea, quod quidam volunt, nihil impedit hanc nostram comprehensionem summi boni. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Nihil enim iam habes, quod ad corpus referas; Illa tamen simplicia, vestra versuta. Eadem nunc mea adversum te oratio est. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; 


	Quare conare, quaeso.
	Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus.
	Cur iustitia laudatur?
	Crassus fuit, qui tamen solebat uti suo bono, ut hodie est noster Pompeius, cui recte facienti gratia est habenda;
	Restatis igitur vos;
	Quia voluptatem hanc esse sentiunt omnes, quam sensus accipiens movetur et iucunditate quadam perfunditur.
	Nos commodius agimus.
	Refert tamen, quo modo.



Huius ego nunc auctoritatem sequens idem faciam. Quis enim potest ea, quae probabilia videantur ei, non probare? Teneo, inquit, finem illi videri nihil dolere. At enim, qua in vita est aliquid mali, ea beata esse non potest. Quis istud possit, inquit, negare? Atque his de rebus et splendida est eorum et illustris oratio. Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? 


	Sed ego in hoc resisto;
	Consequentia exquirere, quoad sit id, quod volumus, effectum.
	Mihi quidem Homerus huius modi quiddam vidisse videatur in iis, quae de Sirenum cantibus finxerit.
	Bona autem corporis huic sunt, quod posterius posui, similiora.



In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Non est igitur voluptas bonum. Tuo vero id quidem, inquam, arbitratu. In quo etsi est magnus, tamen nova pleraque et perpauca de moribus. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. 


	Quid vero?
	Quoniam, si dis placet, ab Epicuro loqui discimus.
	Numquam facies.
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Immo videri fortasse.
	Progredientibus autem aetatibus sensim tardeve potius quasi nosmet ipsos cognoscimus.
	Reguli reiciendam;
	In motu et in statu corporis nihil inest, quod animadvertendum esse ipsa natura iudicet?
	Sint ista Graecorum;
	An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia?
	Quis enim redargueret?
	At ille pellit, qui permulcet sensum voluptate.
	Frater et T.
	Nam neque virtute retinetur ille in vita, nec iis, qui sine virtute sunt, mors est oppetenda.




Ergo nata est sententia veterum Academicorum et
Peripateticorum, ut finem bonorum dicerent secundum naturam
vivere, id est virtute adhibita frui primis a natura datis.

Nam constitui virtus nullo modo potesti nisi ea, quae sunt
prima naturae, ut ad summam pertinentia tenebit.




