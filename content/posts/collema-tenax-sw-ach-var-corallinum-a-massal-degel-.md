Title: Collema tenax (Sw.) Ach. var. corallinum (A. Massal.) Degel.
Description: In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
Image: https://source.unsplash.com/random/?php
Date: 23/9/2021
Keywords: web
Category: Stim
Author: Kaymbo
Tags: work
Published: true
Background: #e67d3e
Color: #311810
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Haec et tu ita posuisti, et verba vestra sunt. Sed et illum, quem nominavi, et ceteros sophistas, ut e Platone intellegi potest, lusos videmus a Socrate. Hanc ergo intuens debet institutum illud quasi signum absolvere. Duo Reges: constructio interrete. Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret? Bona autem corporis huic sunt, quod posterius posui, similiora. Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Iam in altera philosophiae parte. 

Sed quid sentiat, non videtis. An eiusdem modi? Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Odium autem et invidiam facile vitabis. Qualem igitur hominem natura inchoavit? Ergo id est convenienter naturae vivere, a natura discedere. An haec ab eo non dicuntur? Istic sum, inquit. Aliter enim nosmet ipsos nosse non possumus. Te enim iudicem aequum puto, modo quae dicat ille bene noris. 

Sed in rebus apertissimis nimium longi sumus. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Deinde qui fit, ut ego nesciam, sciant omnes, quicumque Epicurei esse voluerunt? Mihi quidem Antiochum, quem audis, satis belle videris attendere. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. 


	Sine ea igitur iucunde negat posse se vivere?
	Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere.




	Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.
	Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse;
	Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare?
	Videmus igitur ut conquiescere ne infantes quidem possint.




	Itaque non discedit ab eorum curatione, quibus praeposita vitam omnem debet gubernare, ut mirari satis istorum inconstantiam non possim.



Dic in quovis conventu te omnia facere, ne doleas. Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Qui potest igitur habitare in beata vita summi mali metus? 

Consequens enim est et post oritur, ut dixi. Sed quot homines, tot sententiae; Quonam, inquit, modo? Cetera illa adhibebat, quibus demptis negat se Epicurus intellegere quid sit bonum. Quid enim est a Chrysippo praetermissum in Stoicis? Deinde dolorem quem maximum? Quamquam id quidem, infinitum est in hac urbe; Hoc est non modo cor non habere, sed ne palatum quidem. 


	Non est enim vitium in oratione solum, sed etiam in moribus.
	Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur.
	Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est?
	Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest.




	Ego autem existimo, si honestum esse aliquid ostendero, quod sit ipsum vi sua propter seque expetendum, iacere vestra omnia.




	Qui enim existimabit posse se miserum esse beatus non erit.
	Sic enim censent, oportunitatis esse beate vivere.
	Tubulo putas dicere?
	Hi curatione adhibita levantur in dies, valet alter plus cotidie, alter videt.
	Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.
	Audio equidem philosophi vocem, Epicure, sed quid tibi dicendum sit oblitus es.



Aliter enim explicari, quod quaeritur, non potest. Quae tamen a te agetur non melior, quam illae sunt, quas interdum optines. Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Indicant pueri, in quibus ut in speculis natura cernitur. Eam stabilem appellas. Apparet statim, quae sint officia, quae actiones. Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia. Qui enim existimabit posse se miserum esse beatus non erit. Quae cum essent dicta, finem fecimus et ambulandi et disputandi. 


Quodsi non hominis summum bonum quaeremus, sed cuiusdam
animantis, is autem esset nihil nisi animus liceat enim
fingere aliquid eiusmodi, quo verum facilius reperiamus -,
tamen illi animo non esset hic vester finis.

Atque haec ita iustitiae propria sunt, ut sint virtutum
reliquarum communia.




	Avaritiamne minuis?
	Isto modo ne improbos quidem, si essent boni viri.
	Sed videbimus.
	Inquit, respondet: Quia, nisi quod honestum est, nullum est aliud bonum! Non quaero iam verumne sit;



Quid turpius quam sapientis vitam ex insipientium sermone pendere? Et nemo nimium beatus est; Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Erit enim mecum, si tecum erit. Hoc mihi cum tuo fratre convenit. 

Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus; Non igitur de improbo, sed de callido improbo quaerimus, qualis Q. Quae si potest singula consolando levare, universa quo modo sustinebit? Aliud igitur esse censet gaudere, aliud non dolere. Quae ista amicitia est? Quis istud possit, inquit, negare? Non pugnem cum homine, cur tantum habeat in natura boni; Utrum igitur tibi litteram videor an totas paginas commovere? Te enim iudicem aequum puto, modo quae dicat ille bene noris. Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat; 

Bonum valitudo: miser morbus. Utilitatis causa amicitia est quaesita. Sed plane dicit quod intellegit. Post enim Chrysippum eum non sane est disputatum. Non dolere, inquam, istud quam vim habeat postea videro; Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? Sin aliud quid voles, postea. Utrum igitur tibi litteram videor an totas paginas commovere? Duo enim genera quae erant, fecit tria. Nulla profecto est, quin suam vim retineat a primo ad extremum. 


Nihil acciderat ei, quod nollet, nisi quod anulum, quo
delectabatur, in mari abiecerat.

Quod idem cum vestri faciant, non satis magnam tribuunt
inventoribus gratiam.



Non enim solum Torquatus dixit quid sentiret, sed etiam cur. Quam tu ponis in verbis, ego positam in re putabam. Innumerabilia dici possunt in hanc sententiam, sed non necesse est. An eiusdem modi? Sed quid ages tandem, si utilitas ab amicitia, ut fit saepe, defecerit? Haec dicuntur inconstantissime. 


	Itaque fecimus.
	Quae sequuntur igitur?
	Tria genera bonorum;
	Res enim fortasse verae, certe graves, non ita tractantur, ut debent, sed aliquanto minutius.
	Venit ad extremum;
	Occultum facinus esse potuerit, gaudebit;




