Title: Astragalus pictiformis Barneby
Description: Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.
Image: https://source.unsplash.com/random/?blue
Date: 1/10/2021
Keywords: cms
Category: Matsoft
Author: Babblestorm
Tags: work
Published: true
Background: #98568f
Color: #cd6c8c
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam diligi et carum esse iucundum est propterea, quia tutiorem vitam et voluptatem pleniorem efficit. Hunc vos beatum; Eiuro, inquit adridens, iniquum, hac quidem de re; Nihilne te delectat umquam -video, quicum loquar-, te igitur, Torquate, ipsum per se nihil delectat? Fadio Gallo, cuius in testamento scriptum esset se ab eo rogatum ut omnis hereditas ad filiam perveniret. In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Duo Reges: constructio interrete. Mihi quidem Antiochum, quem audis, satis belle videris attendere. Facile est hoc cernere in primis puerorum aetatulis. 

Consequens enim est et post oritur, ut dixi. Hoc loco discipulos quaerere videtur, ut, qui asoti esse velint, philosophi ante fiant. Cur iustitia laudatur? Quare ad ea primum, si videtur; Quis est tam dissimile homini. Negat esse eam, inquit, propter se expetendam. Id Sextilius factum negabat. Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus. 

Ergo hoc quidem apparet, nos ad agendum esse natos. At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Nulla erit controversia. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Quae similitudo in genere etiam humano apparet. A mene tu? Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Mene ergo et Triarium dignos existimas, apud quos turpiter loquare? 


	Necesseque est, si quis sibi ipsi inimicus est, eum quae bona sunt mala putare, bona contra quae mala, et quae appetenda fugere, quae fugienda appetere, quae sine dubio vitae est eversio.



Consequentia exquirere, quoad sit id, quod volumus, effectum. Erat enim Polemonis. Nondum autem explanatum satis, erat, quid maxime natura vellet. Res tota, Torquate, non doctorum hominum, velle post mortem epulis celebrari memoriam sui nominis. Mihi quidem Antiochum, quem audis, satis belle videris attendere. Verum hoc idem saepe faciamus. Quid, si etiam iucunda memoria est praeteritorum malorum? Quod cum dixissent, ille contra. Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis. Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? 

Iam id ipsum absurdum, maximum malum neglegi. Theophrasti igitur, inquit, tibi liber ille placet de beata vita? Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer. Satisne igitur videor vim verborum tenere, an sum etiam nunc vel Graece loqui vel Latine docendus? Id mihi magnum videtur. Videsne quam sit magna dissensio? Praetereo multos, in bis doctum hominem et suavem, Hieronymum, quem iam cur Peripateticum appellem nescio. At iste non dolendi status non vocatur voluptas. 

Quae quo sunt excelsiores, eo dant clariora indicia naturae. His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. Nihil enim iam habes, quod ad corpus referas; Contemnit enim disserendi elegantiam, confuse loquitur. Quantum Aristoxeni ingenium consumptum videmus in musicis? His enim rebus detractis negat se reperire in asotorum vita quod reprehendat. Theophrastus mediocriterne delectat, cum tractat locos ab Aristotele ante tractatos? Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. 


	Quis enim redargueret?
	Satisne ergo pudori consulat, si quis sine teste libidini pareat?
	Itaque fecimus.
	Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia.




	Non est igitur summum malum dolor.
	Ut in geometria, prima si dederis, danda sunt omnia.
	Deque his rebus satis multa in nostris de re publica libris sunt dicta a Laelio.




	Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis.
	Res tota, Torquate, non doctorum hominum, velle post mortem epulis celebrari memoriam sui nominis.
	Omnia contraria, quos etiam insanos esse vultis.
	Satisne vobis videor pro meo iure in vestris auribus commentatus?
	Ita multa dicunt, quae vix intellegam.
	Inde igitur, inquit, ordiendum est.



Audeo dicere, inquit. Quid igitur, inquit, eos responsuros putas? Illum mallem levares, quo optimum atque humanissimum virum, Cn. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? 


Hoc autem tempore, etsi multa in omni parte Athenarum sunt
in ipsis locis indicia summorum virorum, tamen ego illa
moveor exhedra.

Parvi enim primo ortu sic iacent, tamquam omnino sine animo
sint.



Nec vero intermittunt aut admirationem earum rerum, quae sunt ab antiquis repertae, aut investigationem novarum. Ait enim se, si uratur, Quam hoc suave! dicturum. At iste non dolendi status non vocatur voluptas. Si verbum sequimur, primum longius verbum praepositum quam bonum. Conferam avum tuum Drusum cum C. Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat Epicurus voluptatem. 


	Quam tu ponis in verbis, ego positam in re putabam.
	Portenta haec esse dicit, neque ea ratione ullo modo posse vivi;
	Qui haec didicerunt, quae ille contemnit, sic solent: Duo genera cupiditatum, naturales et inanes, naturalium duo, necessariae et non necessariae.




	Venit ad extremum;
	Hoc est non dividere, sed frangere.
	Tria genera bonorum;
	Aliter homines, aliter philosophos loqui putas oportere?
	Quid enim?
	Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.
	Sint ista Graecorum;
	Nec vero intermittunt aut admirationem earum rerum, quae sunt ab antiquis repertae, aut investigationem novarum.



Quae quidem sapientes sequuntur duce natura tamquam videntes; Pauca mutat vel plura sane; Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret? Haec et tu ita posuisti, et verba vestra sunt. 


Pungunt quasi aculeis interrogatiunculis angustis, quibus
etiam qui assentiuntur nihil commutantur animo et idem
abeunt, qui venerant.

Vide ne ista sint Manliana vestra aut maiora etiam, si
imperes quod facere non possim.



Ergo hoc quidem apparet, nos ad agendum esse natos. Hoc ille tuus non vult omnibusque ex rebus voluptatem quasi mercedem exigit. Tuo vero id quidem, inquam, arbitratu. Qui ita affectus, beatum esse numquam probabis; Item de contrariis, a quibus ad genera formasque generum venerunt. Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Miserum hominem! Si dolor summum malum est, dici aliter non potest. Nescio quo modo praetervolavit oratio. 


	Quos quidem dies quem ad modum agatis et in quantam hominum facetorum urbanitatem incurratis, non diconihil opus est litibus-;




	Quid, si etiam iucunda memoria est praeteritorum malorum?
	Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.
	Philosophi autem in suis lectulis plerumque moriuntur.
	Teneo, inquit, finem illi videri nihil dolere.




