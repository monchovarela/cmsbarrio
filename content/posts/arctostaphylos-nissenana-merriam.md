Title: Arctostaphylos nissenana Merriam
Description: Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.
Image: https://source.unsplash.com/random/?developer
Date: 28/12/2020
Keywords: portfolio
Category: Redhold
Author: Feedmix
Tags: web
Published: true
Background: #14dac3
Color: #243645
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Est enim effectrix multarum et magnarum voluptatum. Pauca mutat vel plura sane; Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Praeclare hoc quidem. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Duo Reges: constructio interrete. Sed residamus, inquit, si placet. Illis videtur, qui illud non dubitant bonum dicere -; Quid loquor de nobis, qui ad laudem et ad decus nati, suscepti, instituti sumus? Non est ista, inquam, Piso, magna dissensio. 

Satis est ad hoc responsum. Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Et ais, si una littera commota sit, fore tota ut labet disciplina. Quid est, quod ab ea absolvi et perfici debeat? Familiares nostros, credo, Sironem dicis et Philodemum, cum optimos viros, tum homines doctissimos. Summum en�m bonum exposuit vacuitatem doloris; Sed haec in pueris; Primum in nostrane potestate est, quid meminerimus? 

Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Ego vero isti, inquam, permitto. Sequitur disserendi ratio cognitioque naturae; Nam si +omnino nos+ neglegemus, in Aristonea vitia incidemus et peccata obliviscemurque quae virtuti ipsi principia dederimus; Verba tu fingas et ea dicas, quae non sentias? Quae animi affectio suum cuique tribuens atque hanc, quam dico. Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Cur post Tarentum ad Archytam? Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Huius ego nunc auctoritatem sequens idem faciam. 


Nam me ipsum huc modo venientem convertebat ad sese Coloneus
ille locus, cuius incola Sophocles ob oculos versabatur,
quem scis quam admirer quemque eo delecter.

An dubium est, quin virtus ita maximam partem optineat in
rebus humanis, ut reliquas obruat?




	Sedulo, inquam, faciam.
	Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum.
	Praeteritis, inquit, gaudeo.
	Nunc agendum est subtilius.
	Praeteritis, inquit, gaudeo.
	Quod si ita sit, cur opera philosophiae sit danda nescio.
	Praeteritis, inquit, gaudeo.
	Quodsi ipsam honestatem undique pertectam atque absolutam.




	Hic ambiguo ludimur.
	Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest.
	Haeret in salebra.
	Nam neque virtute retinetur ille in vita, nec iis, qui sine virtute sunt, mors est oppetenda.



Huius, Lyco, oratione locuples, rebus ipsis ielunior. Iam enim adesse poterit. Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Stoici autem, quod finem bonorum in una virtute ponunt, similes sunt illorum; Tibi hoc incredibile, quod beatissimum. Quid de Platone aut de Democrito loquar? Qui autem esse poteris, nisi te amor ipse ceperit? Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. 


	Quodsi ipsam honestatem undique pertectam atque absolutam.
	Ergo id est convenienter naturae vivere, a natura discedere.
	Et harum quidem rerum facilis est et expedita distinctio.
	At quanta conantur! Mundum hunc omnem oppidum esse nostrum! Incendi igitur eos, qui audiunt, vides.



Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero. Ut alios omittam, hunc appello, quem ille unum secutus est. Recte, inquit, intellegis. Dici enim nihil potest verius. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Non autem hoc: igitur ne illud quidem. Expectoque quid ad id, quod quaerebam, respondeas. 


	Omnia contraria, quos etiam insanos esse vultis.
	Quid enim possumus hoc agere divinius?
	Quorum sine causa fieri nihil putandum est.
	Quamquam te quidem video minime esse deterritum.




	Quid affers, cur Thorius, cur Caius Postumius, cur omnium horum magister, Orata, non iucundissime vixerit?
	Igitur neque stultorum quisquam beatus neque sapientium non beatus.
	Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus.
	Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur.
	Non autem hoc: igitur ne illud quidem.
	Summus dolor plures dies manere non potest?




	Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L.



Satis est ad hoc responsum. Sed tempus est, si videtur, et recta quidem ad me. Confecta res esset. Qui est in parvis malis. Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam; Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. 

Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Primum cur ista res digna odio est, nisi quod est turpis? Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Quo studio Aristophanem putamus aetatem in litteris duxisse? Sed in rebus apertissimis nimium longi sumus. Quasi ego id curem, quid ille aiat aut neget. Consequens enim est et post oritur, ut dixi. 

Facile est hoc cernere in primis puerorum aetatulis. Huius ego nunc auctoritatem sequens idem faciam. Quo modo? Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Tuo vero id quidem, inquam, arbitratu. Quod si ita sit, cur opera philosophiae sit danda nescio. 


	Primum cur ista res digna odio est, nisi quod est turpis?
	Fortemne possumus dicere eundem illum Torquatum?
	Quibus natura iure responderit non esse verum aliunde finem beate vivendi, a se principia rei gerendae peti;
	Cui vero in voluptate summum bonum est, huic omnia sensu, non ratione sunt iudicanda, eaque dicenda optima, quae sint suavissima.



Videamus igitur sententias eorum, tum ad verba redeamus. Ut alios omittam, hunc appello, quem ille unum secutus est. Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Videmus igitur ut conquiescere ne infantes quidem possint. 


Quasi enim emendum eis sit, quod addant ad virtutem, primum
vilissimas res addunt, dein singulas potius, quam omnia,
quae prima natura approbavisset, ea cum honestate
coniungerent.

Praetereo multos, in bis doctum hominem et suavem,
Hieronymum, quem iam cur Peripateticum appellem nescio.



Nos commodius agimus. Istam voluptatem, inquit, Epicurus ignorat? Hunc vos beatum; Claudii libidini, qui tum erat summo ne imperio, dederetur. 


	Et quis a Stoicis et quem ad modum diceretur, tamen ego quoque exponam, ut perspiciamus, si potuerimus, quidnam a Zenone novi sit allatum.




