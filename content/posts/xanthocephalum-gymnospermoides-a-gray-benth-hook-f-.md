Title: Xanthocephalum gymnospermoides (A. Gray) Benth. & Hook. f.
Description: In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
Image: https://source.unsplash.com/random/?javascript
Date: 31/10/2020
Keywords: cms
Category: Keylex
Author: Meezzy
Tags: web
Published: true
Background: #d57469
Color: #5ff925
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere. Quid censes in Latino fore? Sapiens autem semper beatus est et est aliquando in dolore; An hoc usque quaque, aliter in vita? Duo Reges: constructio interrete. Quid est enim aliud esse versutum? Quis est, qui non oderit libidinosam, protervam adolescentiam? Nam ista vestra: Si gravis, brevis; 


	Conveniret, pluribus praeterea conscripsisset qui esset optimus rei publicae status, hoc amplius Theophrastus: quae essent in re publica rerum inclinationes et momenta temporum, quibus esset moderandum, utcumque res postularet.




	Nemo enim est, qui aliter dixerit quin omnium naturarum simile esset id, ad quod omnia referrentur, quod est ultimum rerum appetendarum.




	Quid ergo?
	Quae hic rei publicae vulnera inponebat, eadem ille sanabat.
	Haeret in salebra.
	At ille pellit, qui permulcet sensum voluptate.
	Haeret in salebra.
	Quod non faceret, si in voluptate summum bonum poneret.
	Non igitur bene.
	Quamquam id quidem, infinitum est in hac urbe;
	Sint ista Graecorum;
	Piso igitur hoc modo, vir optimus tuique, ut scis, amantissimus.




	Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur.
	Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore.
	Nihil sane.
	Paupertas si malum est, mendicus beatus esse nemo potest, quamvis sit sapiens.
	Re mihi non aeque satisfacit, et quidem locis pluribus.



Est, ut dicis, inquit; Indicant pueri, in quibus ut in speculis natura cernitur. Facit igitur Lucius noster prudenter, qui audire de summo bono potissimum velit; Easdemne res? Quae quo sunt excelsiores, eo dant clariora indicia naturae. Quo modo? Bonum incolumis acies: misera caecitas. Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. 


	Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum.
	Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia;
	Ergo et avarus erit, sed finite, et adulter, verum habebit modum, et luxuriosus eodem modo.
	Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem illam Aristippi in prima commendatione poneret.



Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Si longus, levis; Quodsi ipsam honestatem undique pertectam atque absolutam. Nec mihi illud dixeris: Haec enim ipsa mihi sunt voluptati, et erant illa Torquatis. Recte, inquit, intellegis. 

An quod ita callida est, ut optime possit architectari voluptates? At iam decimum annum in spelunca iacet. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Etiam beatissimum? 

Potius ergo illa dicantur: turpe esse, viri non esse debilitari dolore, frangi, succumbere. Sed quod proximum fuit non vidit. Uterque enim summo bono fruitur, id est voluptate. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Quod quidem iam fit etiam in Academia. An eiusdem modi? Restincta enim sitis stabilitatem voluptatis habet, inquit, illa autem voluptas ipsius restinctionis in motu est. 

An ea, quae per vinitorem antea consequebatur, per se ipsa curabit? Velut ego nunc moveor. Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Et harum quidem rerum facilis est et expedita distinctio. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Quamquam te quidem video minime esse deterritum. 

Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Quid, de quo nulla dissensio est? Sed tamen intellego quid velit. Non enim iam stirpis bonum quaeret, sed animalis. Est enim effectrix multarum et magnarum voluptatum. 


	Istic sum, inquit.
	Inde igitur, inquit, ordiendum est.
	Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia.




	Immo videri fortasse.
	At vero illa, quae Peripatetici, quae Stoici dicunt, semper tibi in ore sunt in iudiciis, in senatu.
	Si longus, levis.
	Sin laboramus, quis est, qui alienae modum statuat industriae?
	Certe non potest.
	Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus;



Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Facile est hoc cernere in primis puerorum aetatulis. Videsne quam sit magna dissensio? Aeque enim contingit omnibus fidibus, ut incontentae sint. 

Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Quae cum essent dicta, discessimus. Eaedem res maneant alio modo. Peccata paria. Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. Illi enim inter se dissentiunt. Quae autem natura suae primae institutionis oblita est? Magna laus. 

Ergo id est convenienter naturae vivere, a natura discedere. Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Tu enim ista lenius, hic Stoicorum more nos vexat. Ergo ita: non posse honeste vivi, nisi honeste vivatur? Vitiosum est enim in dividendo partem in genere numerare. Et nemo nimium beatus est; 


Perfecto enim et concluso neque virtutibus neque amicitiis
usquam locum esse, si ad voluptatem omnia referantur, nihil
praeterea est magnopere dicendum.

Quamquam non negatis nos intellegere quid sit voluptas, sed
quid ille dicat.




	Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur.
	Itaque hic ipse iam pridem est reiectus;




Quae adhuc, Cato, a te dicta sunt, eadem, inquam, dicere
posses, si sequerere Pyrrhonem aut Aristonem.

Fadio Gallo, cuius in testamento scriptum esset se ab eo
rogatum ut omnis hereditas ad filiam perveniret.




