Title: Calystegia sepium (L.) R. Br.
Description: Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.
Image: https://source.unsplash.com/random/?red
Date: 14/2/2021
Keywords: article
Category: Fixflex
Author: Minyx
Tags: design
Published: true
Background: #ded815
Color: #556068
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Teneo, inquit, finem illi videri nihil dolere. Non est enim vitium in oratione solum, sed etiam in moribus. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Duo Reges: constructio interrete. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Sin ea non neglegemus neque tamen ad finem summi boni referemus, non multum ab Erilli levitate aberrabimus. 

Illud non continuo, ut aeque incontentae. An, partus ancillae sitne in fructu habendus, disseretur inter principes civitatis, P. Quid ad utilitatem tantae pecuniae? Praetereo multos, in bis doctum hominem et suavem, Hieronymum, quem iam cur Peripateticum appellem nescio. Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt; 

Eadem fortitudinis ratio reperietur. Collatio igitur ista te nihil iuvat. Iam enim adesse poterit. Haec para/doca illi, nos admirabilia dicamus. Si quidem, inquit, tollerem, sed relinquo. Sed utrum hortandus es nobis, Luci, inquit, an etiam tua sponte propensus es? Quorum sine causa fieri nihil putandum est. Videsne quam sit magna dissensio? 


	Quid de Pythagora?
	Ita finis bonorum existit secundum naturam vivere sic affectum, ut optime is affici possit ad naturamque accommodatissime.
	Sint ista Graecorum;
	Cur ipse Pythagoras et Aegyptum lustravit et Persarum magos adiit?
	Quonam, inquit, modo?
	Negat enim summo bono afferre incrementum diem.
	Quid ergo?
	Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.



Illum mallem levares, quo optimum atque humanissimum virum, Cn. Si enim ad populum me vocas, eum. Quare conare, quaeso. Mihi enim satis est, ipsis non satis. Nullus est igitur cuiusquam dies natalis. Sic enim censent, oportunitatis esse beate vivere. Tenent mordicus. Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Quid enim possumus hoc agere divinius? 

Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Sed ad rem redeamus; Pisone in eo gymnasio, quod Ptolomaeum vocatur, unaque nobiscum Q. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Quae cum dixisset paulumque institisset, Quid est? Non risu potius quam oratione eiciendum? Quorum altera prosunt, nocent altera. 


Ut enim nos ex annalium monimentis testes excitamus eos,
quorum omnis vita consumpta est in laboribus gloriosis, qui
voluptatis nomen audire non possent, sic in vestris
disputationibus historia muta est.

Erat enim Polemonis.




	Aperiendum est igitur, quid sit voluptas;
	Nam si amitti vita beata potest, beata esse non potest.
	Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest.
	An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat?
	Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse;
	Ita graviter et severe voluptatem secrevit a bono.




	Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio.
	Quo minus animus a se ipse dissidens secumque discordans gustare partem ullam liquidae voluptatis et liberae potest.
	Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur.
	Qua tu etiam inprudens utebare non numquam.



Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. Non potes, nisi retexueris illa. Hi curatione adhibita levantur in dies, valet alter plus cotidie, alter videt. Teneo, inquit, finem illi videri nihil dolere. Sin laboramus, quis est, qui alienae modum statuat industriae? Facillimum id quidem est, inquam. 

Quicquid porro animo cernimus, id omne oritur a sensibus; Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur. Cenasti in vita numquam bene, cum omnia in ista Consumis squilla atque acupensere cum decimano. Suo enim quisque studio maxime ducitur. Nihil enim hoc differt. Omnia peccata paria dicitis. 


	Nihil sane.
	Philosophi autem in suis lectulis plerumque moriuntur.
	Poterat autem inpune;
	Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum;
	Quibusnam praeteritis?
	Illa videamus, quae a te de amicitia dicta sunt.
	Hic ambiguo ludimur.
	Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim?




	At vero illa, quae Peripatetici, quae Stoici dicunt, semper tibi in ore sunt in iudiciis, in senatu.
	Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere.
	Quid ergo attinet gloriose loqui, nisi constanter loquare?
	Et homini, qui ceteris animantibus plurimum praestat, praecipue a natura nihil datum esse dicemus?
	Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius.
	Inde sermone vario sex illa a Dipylo stadia confecimus.



Et non ex maxima parte de tota iudicabis? Sint modo partes vitae beatae. Est autem a te semper dictum nec gaudere quemquam nisi propter corpus nec dolere. Primum quid tu dicis breve? Id quaeris, inquam, in quo, utrum respondero, verses te huc atque illuc necesse est. Sed ad rem redeamus; At iste non dolendi status non vocatur voluptas. Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus. Quia nec honesto quic quam honestius nec turpi turpius. Aliud igitur esse censet gaudere, aliud non dolere. Quod totum contra est. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? 


Temporibus autem quibusdam et aut officiis debitis aut rerum
necessitatibus saepe eveniet, ut et voluptates repudiandae
sint et molestiae non recusandae.

Cum enim summum bonum in voluptate ponat, negat infinito
tempore aetatis voluptatem fieri maiorem quam finito atque
modico.



Dat enim intervalla et relaxat. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Non enim in selectione virtus ponenda erat, ut id ipsum, quod erat bonorum ultimum, aliud aliquid adquireret. Facile est hoc cernere in primis puerorum aetatulis. Quae cum essent dicta, finem fecimus et ambulandi et disputandi. 


	Ita graviter et severe voluptatem secrevit a bono.



Huius, Lyco, oratione locuples, rebus ipsis ielunior. Si enim ita est, vide ne facinus facias, cum mori suadeas. Prave, nequiter, turpiter cenabat; Ut pulsi recurrant? Stoici scilicet. Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat; An eiusdem modi? Negat enim tenuissimo victu, id est contemptissimis escis et potionibus, minorem voluptatem percipi quam rebus exquisitissimis ad epulandum. Sed tamen omne, quod de re bona dilucide dicitur, mihi praeclare dici videtur. Conferam avum tuum Drusum cum C. 


	Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia.
	Quippe: habes enim a rhetoribus;
	Itaque ne iustitiam quidem recte quis dixerit per se ipsam optabilem, sed quia iucunditatis vel plurimum afferat.




	Quo posito et omnium adsensu adprobato illud adsumitur, eum, qui magno sit animo atque forti, omnia, quae cadere in hominem possint, despicere ac pro nihilo putare.




