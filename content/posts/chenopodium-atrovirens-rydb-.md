Title: Chenopodium atrovirens Rydb.
Description: Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.
Image: https://source.unsplash.com/random/?code
Date: 2/10/2021
Keywords: post
Category: Job
Author: Brightbean
Tags: design
Published: true
Background: #ae0023
Color: #c48466
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. An ea, quae per vinitorem antea consequebatur, per se ipsa curabit? Paria sunt igitur. Nescio quo modo praetervolavit oratio. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Efficiens dici potest. Duo Reges: constructio interrete. 


	Quonam, inquit, modo?
	An quod ita callida est, ut optime possit architectari voluptates?
	Poterat autem inpune;
	Vitae autem degendae ratio maxime quidem illis placuit quieta.
	Confecta res esset.
	Sed quid sentiat, non videtis.
	Quare conare, quaeso.
	Mene ergo et Triarium dignos existimas, apud quos turpiter loquare?



Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Si quicquam extra virtutem habeatur in bonis. Cur haec eadem Democritus? Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Quid igitur, inquit, eos responsuros putas? Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. 

Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere. Quid est enim aliud esse versutum? Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere. Inquit, dasne adolescenti veniam? Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Summus dolor plures dies manere non potest? Et non ex maxima parte de tota iudicabis? Gerendus est mos, modo recte sentiat. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset. De vacuitate doloris eadem sententia erit. Negat enim summo bono afferre incrementum diem. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. 


Cum autem hominem in eo genere posuisset, ut ei tribueret
animi excellentiam, summum bonum id constituit, non ut
excelleret animus, sed ut nihil esse praeter animum
videretur.

Materiam vero rerum et copiam apud hos exilem, apud illos
uberrimam reperiemus.




Vos autem, Cato, quia virtus, ut omnes fatemur, altissimum
locum in homine et maxime excellentem tenet, et quod eos,
qui sapientes sunt, absolutos et perfectos putamus, aciem
animorum nostrorum virtutis splendore praestringitis.

Quis enim confidit semper sibi illud stabile et firmum
permansurum, quod fragile et caducum sit?



Sint modo partes vitae beatae. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Nam ista vestra: Si gravis, brevis; Quod totum contra est. Ita relinquet duas, de quibus etiam atque etiam consideret. Quo studio Aristophanem putamus aetatem in litteris duxisse? 

Quae cum essent dicta, finem fecimus et ambulandi et disputandi. Eam tum adesse, cum dolor omnis absit; Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Utrum igitur tibi litteram videor an totas paginas commovere? Mihi quidem Antiochum, quem audis, satis belle videris attendere. Quem ad modum quis ambulet, sedeat, qui ductus oris, qui vultus in quoque sit? Quid de Pythagora? 


	Negat enim summo bono afferre incrementum diem.
	Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur.
	Est, ut dicis, inquam.
	Nam si quae sunt aliae, falsum est omnis animi voluptates esse e corporis societate.



Quid Zeno? Effluit igitur voluptas corporis et prima quaeque avolat saepiusque relinquit causam paenitendi quam recordandi. Cur post Tarentum ad Archytam? Odium autem et invidiam facile vitabis. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Sed vos squalidius, illorum vides quam niteat oratio. 


	Amicitiae vero locus ubi esse potest aut quis amicus esse cuiquam, quem non ipsum amet propter ipsum?
	Quid interest, nisi quod ego res notas notis verbis appello, illi nomina nova quaerunt, quibus idem dicant?
	Non quam nostram quidem, inquit Pomponius iocans;
	Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus.



At hoc in eo M. Bestiarum vero nullum iudicium puto. Sin laboramus, quis est, qui alienae modum statuat industriae? Ita cum ea volunt retinere, quae superiori sententiae conveniunt, in Aristonem incidunt; Polycratem Samium felicem appellabant. Quid autem habent admirationis, cum prope accesseris? 

Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Videamus animi partes, quarum est conspectus illustrior; Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Non est ista, inquam, Piso, magna dissensio. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Eam tum adesse, cum dolor omnis absit; 


	Hanc se tuus Epicurus omnino ignorare dicit quam aut qualem esse velint qui honestate summum bonum metiantur.




	Quamquam id quidem, infinitum est in hac urbe;




	Praeclare enim Plato: Beatum, cui etiam in senectute contigerit, ut sapientiam verasque opiniones assequi possit.
	Illud dico, ea, quae dicat, praeclare inter se cohaerere.
	Verum esto: verbum ipsum voluptatis non habet dignitatem, nec nos fortasse intellegimus.
	Nunc haec primum fortasse audientis servire debemus.
	Tum Lucius: Mihi vero ista valde probata sunt, quod item fratri puto.




	Inde igitur, inquit, ordiendum est.
	Hoc loco tenere se Triarius non potuit.
	Nemo nostrum istius generis asotos iucunde putat vivere.
	Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt.
	Ut proverbia non nulla veriora sint quam vestra dogmata.
	Hos contra singulos dici est melius.




	Nihil sane.
	Quae quidem sapientes sequuntur duce natura tamquam videntes;
	Nihilo magis.
	Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum.
	Quid iudicant sensus?
	Illud dico, ea, quae dicat, praeclare inter se cohaerere.
	Explanetur igitur.
	Sed fortuna fortis;



Quas enim kakaw Graeci appellant, vitia malo quam malitias nominare. Aberat omnis dolor, qui si adesset, nec molliter ferret et tamen medicis plus quam philosophis uteretur. Cur deinde Metrodori liberos commendas? Sed tamen intellego quid velit. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Quae cum essent dicta, discessimus. Hoc enim constituto in philosophia constituta sunt omnia. Omnes enim iucundum motum, quo sensus hilaretur. Quid Zeno? 

Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? Quoniam, si dis placet, ab Epicuro loqui discimus. Equidem etiam Epicurum, in physicis quidem, Democriteum puto. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Qualem igitur hominem natura inchoavit? Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba. Sin kakan malitiam dixisses, ad aliud nos unum certum vitium consuetudo Latina traduceret. Homines optimi non intellegunt totam rationem everti, si ita res se habeat. 


