Title: Symphyotrichum lanceolatum (Willd.) G.L. Nesom ssp. lanceolatum var. latifolium (Semple & Chmielewski) G.L. Nesom
Description: Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.
Image: https://source.unsplash.com/random/?yellow
Date: 24/2/2021
Keywords: web
Category: Redhold
Author: Oloo
Tags: website
Published: true
Background: #c1e18b
Color: #ea4263
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit, inquam, tam facilis, quam vultis, comparatio voluptatis, quid de dolore dicemus? Et quidem, inquit, vehementer errat; Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. Duo Reges: constructio interrete. At hoc in eo M. Summum a vobis bonum voluptas dicitur. Teneo, inquit, finem illi videri nihil dolere. 

Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? Sed haec quidem liberius ab eo dicuntur et saepius. Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse. Sequitur disserendi ratio cognitioque naturae; Qui convenit? Non enim iam stirpis bonum quaeret, sed animalis. Mihi, inquam, qui te id ipsum rogavi? Sed virtutem ipsam inchoavit, nihil amplius. Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; 


Huic mori optimum esse propter desperationem sapientiae,
illi propter spem vivere.

Et tamen tantis vectigalibus ad liberalitatem utens etiam
sine hac Pyladea amicitia multorum te benivolentia praeclare
tuebere et munies.



Quoniam, si dis placet, ab Epicuro loqui discimus. Profectus in exilium Tubulus statim nec respondere ausus; Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest. Atqui perspicuum est hominem e corpore animoque constare, cum primae sint animi partes, secundae corporis. Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Quare ad ea primum, si videtur; 


	Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit.
	Deinde disputat, quod cuiusque generis animantium statui deceat extremum.
	Ergo, inquit, tibi Q.
	Neque solum ea communia, verum etiam paria esse dixerunt.
	Quamquam id quidem, infinitum est in hac urbe;




Hoc dictum in una re latissime patet, ut in omnibus factis
re, non teste moveamur.

Chrysippus autem exponens differentias animantium ait alias
earum corpore excellere, alias autem animo, non nullas
valere utraque re;




	Est enim effectrix multarum et magnarum voluptatum.
	Idemque diviserunt naturam hominis in animum et corpus.
	Haec quo modo conveniant, non sane intellego.




	Iam insipientes alios ita esse, ut nullo modo ad sapientiam possent pervenire, alios, qui possent, si id egissent, sapientiam consequi.



Iam id ipsum absurdum, maximum malum neglegi. Omnia peccata paria dicitis. Nosti, credo, illud: Nemo pius est, qui pietatem-; Primum divisit ineleganter; Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Utilitatis causa amicitia est quaesita. Haec para/doca illi, nos admirabilia dicamus. Et ille ridens: Video, inquit, quid agas; 

Quae cum dixisset, finem ille. Nam si propter voluptatem, quae est ista laus, quae possit e macello peti? Quid ergo? Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Quia dolori non voluptas contraria est, sed doloris privatio. 

Nullus est igitur cuiusquam dies natalis. Quamquam tu hanc copiosiorem etiam soles dicere. Si qua in iis corrigere voluit, deteriora fecit. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Quae cum dixisset paulumque institisset, Quid est? Torquatus, is qui consul cum Cn. Bonum liberi: misera orbitas. Ergo id est convenienter naturae vivere, a natura discedere. Memini me adesse P. 


	Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D.
	Miserum hominem! Si dolor summum malum est, dici aliter non potest.
	Bona autem corporis huic sunt, quod posterius posui, similiora.
	Themistocles quidem, cum ei Simonides an quis alius artem memoriae polliceretur, Oblivionis, inquit, mallem.
	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?



Mihi enim satis est, ipsis non satis. Profectus in exilium Tubulus statim nec respondere ausus; Immo videri fortasse. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Age, inquies, ista parva sunt. Erat enim Polemonis. 

Quid nunc honeste dicit? Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur. Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Videamus igitur sententias eorum, tum ad verba redeamus. 


	Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia.




	Memini vero, inquam;
	Hoc est non modo cor non habere, sed ne palatum quidem.
	Quid iudicant sensus?
	Nunc omni virtuti vitium contrario nomine opponitur.
	Easdemne res?
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?




	Sed fortuna fortis;
	Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia.
	Reguli reiciendam;
	Nam si amitti vita beata potest, beata esse non potest.
	Certe non potest.
	Sic consequentibus vestris sublatis prima tolluntur.
	Omnia peccata paria dicitis.
	Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim?



Quo modo autem optimum, si bonum praeterea nullum est? Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Que Manilium, ab iisque M. Non est igitur summum malum dolor. 

Addidisti ad extremum etiam indoctum fuisse. Quae quidem sapientes sequuntur duce natura tamquam videntes; Sed tu istuc dixti bene Latine, parum plane. Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Quare si potest esse beatus is, qui est in asperis reiciendisque rebus, potest is quoque esse. Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. 


	Suo enim quisque studio maxime ducitur.
	Nondum autem explanatum satis, erat, quid maxime natura vellet.




