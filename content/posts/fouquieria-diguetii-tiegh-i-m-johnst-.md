Title: Fouquieria diguetii (Tiegh.) I.M. Johnst.
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?ruby
Date: 22/8/2021
Keywords: work
Category: Vagram
Author: Yacero
Tags: design
Published: true
Background: #08e12b
Color: #839ab8
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed haec quidem liberius ab eo dicuntur et saepius. Ratio quidem vestra sic cogit. Quos nisi redarguimus, omnis virtus, omne decus, omnis vera laus deserenda est. Tanta vis admonitionis inest in locis; Illis videtur, qui illud non dubitant bonum dicere -; Sed ad illum redeo. Duo Reges: constructio interrete. 


	Nam sunt et in animo praecipua quaedam et in corpore, quae cum leviter agnovit, tum discernere incipit, ut ea, quae prima data sunt natura, appetat asperneturque contraria.



Qua tu etiam inprudens utebare non numquam. Quis istud possit, inquit, negare? Quae est igitur causa istarum angustiarum? Nemo nostrum istius generis asotos iucunde putat vivere. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Eam stabilem appellas. Itaque contra est, ac dicitis; 


	Et quis a Stoicis et quem ad modum diceretur, tamen ego quoque exponam, ut perspiciamus, si potuerimus, quidnam a Zenone novi sit allatum.



Si longus, levis. Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere. Cur post Tarentum ad Archytam? Id enim natura desiderat. Videamus igitur sententias eorum, tum ad verba redeamus. Omnes enim iucundum motum, quo sensus hilaretur. Si autem id non concedatur, non continuo vita beata tollitur. Quam ob rem tandem, inquit, non satisfacit? Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia. Longum est enim ad omnia respondere, quae a te dicta sunt. Sapiens autem semper beatus est et est aliquando in dolore; 


	Nam si amitti vita beata potest, beata esse non potest.
	Tuo vero id quidem, inquam, arbitratu.



Equidem e Cn. Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit. Quare conare, quaeso. Sin aliud quid voles, postea. Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus. Quid sequatur, quid repugnet, vident. Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. Et ille ridens: Video, inquit, quid agas; Sed haec nihil sane ad rem; 

Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Multoque hoc melius nos veriusque quam Stoici. Apparet statim, quae sint officia, quae actiones. Ut in geometria, prima si dederis, danda sunt omnia. Universa enim illorum ratione cum tota vestra confligendum puto. Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Sumenda potius quam expetenda. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Gloriosa ostentatio in constituendo summo bono. Quid ait Aristoteles reliquique Platonis alumni? 

Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Obsecro, inquit, Torquate, haec dicit Epicurus? Nam si amitti vita beata potest, beata esse non potest. Quae autem natura suae primae institutionis oblita est? Aliter enim explicari, quod quaeritur, non potest. Quae sequuntur igitur? Hoc est non dividere, sed frangere. Cui Tubuli nomen odio non est? Immo istud quidem, inquam, quo loco quidque, nisi iniquum postulo, arbitratu meo. Item de contrariis, a quibus ad genera formasque generum venerunt. 

Non risu potius quam oratione eiciendum? Nam bonum ex quo appellatum sit, nescio, praepositum ex eo credo, quod praeponatur aliis. Post enim Chrysippum eum non sane est disputatum. At iste non dolendi status non vocatur voluptas. Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet; Beatus autem esse in maximarum rerum timore nemo potest. Nam bonum ex quo appellatum sit, nescio, praepositum ex eo credo, quod praeponatur aliis. Quod autem principium officii quaerunt, melius quam Pyrrho; Hic ambiguo ludimur. 

Cur igitur, cum de re conveniat, non malumus usitate loqui? Bonum integritas corporis: misera debilitas. Omnia contraria, quos etiam insanos esse vultis. Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. Sed haec quidem liberius ab eo dicuntur et saepius. Isto modo, ne si avia quidem eius nata non esset. His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. 


	Quid igitur dubitamus in tota eius natura quaerere quid sit effectum?
	Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat Epicurus voluptatem.
	Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur.
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.
	Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.
	Ego vero isti, inquam, permitto.
	Itaque si aut requietem natura non quaereret aut eam posset alia quadam ratione consequi.




	Tum Lucius: Mihi vero ista valde probata sunt, quod item fratri puto.
	Iubet igitur nos Pythius Apollo noscere nosmet ipsos.
	Sed ne, dum huic obsequor, vobis molestus sim.




	At ille pellit, qui permulcet sensum voluptate.
	Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse.
	Videsne quam sit magna dissensio?
	Mihi quidem Antiochum, quem audis, satis belle videris attendere.
	Sed quid minus probandum quam esse aliquem beatum nec satis beatum?




	Tenent mordicus.
	Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis.
	Quis enim redargueret?
	Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur.




	Perge porro;
	Cum salvum esse flentes sui respondissent, rogavit essentne fusi hostes.
	Quid ergo?
	Restincta enim sitis stabilitatem voluptatis habet, inquit, illa autem voluptas ipsius restinctionis in motu est.
	Quo tandem modo?
	Sit enim idem caecus, debilis.
	Qui convenit?
	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.
	Perge porro;
	Quid est, quod ab ea absolvi et perfici debeat?
	Si quae forte-possumus.
	Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia?
	Idem adhuc;
	Nec enim ignoras his istud honestum non summum modo, sed etiam, ut tu vis, solum bonum videri.



Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Sed ad bona praeterita redeamus. Nos cum te, M. Suo genere perveniant ad extremum; 

Primum in nostrane potestate est, quid meminerimus? Quid est igitur, cur ita semper deum appellet Epicurus beatum et aeternum? Quae duo sunt, unum facit. Summum en�m bonum exposuit vacuitatem doloris; Tu vero, inquam, ducas licet, si sequetur; Quamquam tu hanc copiosiorem etiam soles dicere. Suam denique cuique naturam esse ad vivendum ducem. Quodsi ipsam honestatem undique pertectam atque absolutam. Contineo me ab exemplis. Theophrastus mediocriterne delectat, cum tractat locos ab Aristotele ante tractatos? 


Ut optime, secundum naturam affectum esse possit.

Nam quicquid quaeritur, id habet aut generis ipsius sine
personis temporibusque aut his adiunctis facti aut iuris aut
nominis controversiam.




Illo enim addito iuste fit recte factum, per se autem hoc
ipsum reddere in officio ponitur.

Res tota, Torquate, non doctorum hominum, velle post mortem
epulis celebrari memoriam sui nominis.




