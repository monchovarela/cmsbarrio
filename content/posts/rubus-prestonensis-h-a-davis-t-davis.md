Title: Rubus prestonensis H.A. Davis & T. Davis
Description: In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
Image: https://source.unsplash.com/random/?design
Date: 24/10/2020
Keywords: web
Category: Span
Author: Blogspan
Tags: portfolio
Published: true
Background: #71f5b1
Color: #cac83c
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et quod est munus, quod opus sapientiae? Duo Reges: constructio interrete. Cur iustitia laudatur? Beatus autem esse in maximarum rerum timore nemo potest. An eiusdem modi? Quae duo sunt, unum facit. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; 


	Haec para/doca illi, nos admirabilia dicamus.
	Nam si beatus umquam fuisset, beatam vitam usque ad illum a Cyro extructum rogum pertulisset.
	Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem.
	Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem?



Est, ut dicis, inquam. Ergo omni animali illud, quod appetiti positum est in eo, quod naturae est accommodatum. Sed quae tandem ista ratio est? Eademne, quae restincta siti? Quod cum dixissent, ille contra. Quid est igitur, cur ita semper deum appellet Epicurus beatum et aeternum? 

In qua quid est boni praeter summam voluptatem, et eam sempiternam? An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Et harum quidem rerum facilis est et expedita distinctio. Nihil minus, contraque illa hereditate dives ob eamque rem laetus. Quare attende, quaeso. 

Tu quidem reddes; Cyrenaici quidem non recusant; Paria sunt igitur. Qui ita affectus, beatum esse numquam probabis; An, partus ancillae sitne in fructu habendus, disseretur inter principes civitatis, P. Quo modo autem philosophus loquitur? 

Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Animum autem reliquis rebus ita perfecit, ut corpus; Teneo, inquit, finem illi videri nihil dolere. Summus dolor plures dies manere non potest? Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? Si longus, levis dictata sunt. Quae diligentissime contra Aristonem dicuntur a Chryippo. 


	Bonum incolumis acies: misera caecitas.




	Primum quid tu dicis breve?
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.
	Cur fortior sit, si illud, quod tute concedis, asperum et vix ferendum putabit?




	Mihi enim satis est, ipsis non satis.
	Quod autem ratione actum est, id officium appellamus.
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?
	Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem.
	Quamquam tu hanc copiosiorem etiam soles dicere.
	In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt.



Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Iam id ipsum absurdum, maximum malum neglegi. Quid ergo? Ita prorsus, inquam; Videamus animi partes, quarum est conspectus illustrior; 


Ex quo magnitudo quoque animi existebat, qua facile posset
repugnari obsistique fortunae, quod maximae res essent in
potestate sapientis.

Si est nihil nisi corpus, summa erunt illa: valitudo,
vacuitas doloris, pulchritudo, cetera.




Qualis est igitur omnis haec, quam dico, conspiratio
consensusque virtutum, tale est illud ipsum honestum,
quandoquidem honestum aut ipsa virtus est aut res gesta
virtute;

Quodsi Graeci leguntur a Graecis isdem de rebus alia ratione
compositis, quid est, cur nostri a nostris non legantur?




	Quid enim?
	At iam decimum annum in spelunca iacet.
	Negare non possum.
	Audeo dicere, inquit.
	Perge porro;
	Sed potestne rerum maior esse dissensio?




	Est autem a te semper dictum nec gaudere quemquam nisi propter corpus nec dolere.



Istam voluptatem, inquit, Epicurus ignorat? Ita graviter et severe voluptatem secrevit a bono. Estne, quaeso, inquam, sitienti in bibendo voluptas? Quodsi, ne quo incommodo afficiare, non relinques amicum, tamen, ne sine fructu alligatus sis, ut moriatur optabis. Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. Semovenda est igitur voluptas, non solum ut recta sequamini, sed etiam ut loqui deceat frugaliter. Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim? Et nemo nimium beatus est; 

Videamus igitur sententias eorum, tum ad verba redeamus. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Sed nimis multa. Nos paucis ad haec additis finem faciamus aliquando; In contemplatione et cognitione posita rerum, quae quia deorum erat vitae simillima, sapiente visa est dignissima. 

Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Quam illa ardentis amores excitaret sui! Cur tandem? Sed fac ista esse non inportuna; Non semper, inquam; Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Sed quanta sit alias, nunc tantum possitne esse tanta. Non potes, nisi retexueris illa. Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Tecum optime, deinde etiam cum mediocri amico. Nam illud quidem adduci vix possum, ut ea, quae senserit ille, tibi non vera videantur. Zenonis est, inquam, hoc Stoici. 

Illa enim, quae prosunt aut quae nocent, aut bona sunt aut mala, quae sint paria necesse est. Et non ex maxima parte de tota iudicabis? 


	Sed videbimus.
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?
	Beatum, inquit.
	De ingenio eius in his disputationibus, non de moribus quaeritur.
	Explanetur igitur.
	Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria.
	Stoicos roga.
	Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest.




	Nec hoc ille non vidit, sed verborum magnificentia est et gloria delectatus.
	Qui non moveatur et offensione turpitudinis et comprobatione honestatis?
	Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L.




