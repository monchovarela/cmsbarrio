Title: Navarretia myersii Allen & Day ssp. deminuta Day
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?city
Date: 24/2/2021
Keywords: cms
Category: Tres-Zap
Author: Youtags
Tags: design
Published: true
Background: #93ae66
Color: #17e83d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. At ille pellit, qui permulcet sensum voluptate. Aliud igitur esse censet gaudere, aliud non dolere. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Hoc est non dividere, sed frangere. Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem illam Aristippi in prima commendatione poneret. Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Cenasti in vita numquam bene, cum omnia in ista Consumis squilla atque acupensere cum decimano. Duo Reges: constructio interrete. Summum a vobis bonum voluptas dicitur. 


	Quo tandem modo?
	Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum.
	Nos commodius agimus.
	Sed non sunt in eo genere tantae commoditates corporis tamque productae temporibus tamque multae.
	Venit ad extremum;
	Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit;
	ALIO MODO.
	Ac tamen hic mallet non dolere.
	Efficiens dici potest.
	Gracchum patrem non beatiorem fuisse quam fillum, cum alter stabilire rem publicam studuerit, alter evertere.



At certe gravius. Est, ut dicis, inquit; Quid nunc honeste dicit? 

Sed ego in hoc resisto; Illa argumenta propria videamus, cur omnia sint paria peccata. Ita multo sanguine profuso in laetitia et in victoria est mortuus. Ille enim occurrentia nescio quae comminiscebatur; Videamus igitur sententias eorum, tum ad verba redeamus. Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam esse oportebit; 


	Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus?



Dici enim nihil potest verius. Summum a vobis bonum voluptas dicitur. Urgent tamen et nihil remittunt. Primum cur ista res digna odio est, nisi quod est turpis? Nec mihi illud dixeris: Haec enim ipsa mihi sunt voluptati, et erant illa Torquatis. Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus. Egone quaeris, inquit, quid sentiam? Equidem, sed audistine modo de Carneade? 

Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Quid Zeno? Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Claudii libidini, qui tum erat summo ne imperio, dederetur. Non enim quaero quid verum, sed quid cuique dicendum sit. Ait enim se, si uratur, Quam hoc suave! dicturum. Quamquam tu hanc copiosiorem etiam soles dicere. Prave, nequiter, turpiter cenabat; 

Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Totum autem id externum est, et quod externum, id in casu est. Ita relinquet duas, de quibus etiam atque etiam consideret. Audeo dicere, inquit. Beatus sibi videtur esse moriens. Utrum igitur tibi litteram videor an totas paginas commovere? Quae quo sunt excelsiores, eo dant clariora indicia naturae. 


Quod idem Peripatetici non tenent, quibus dicendum est, quae
et honesta actio sit et sine dolore, eam magis esse
expetendam, quam si esset eadem actio cum dolore.

Quae cum essent dicta, discessimus.




	Quid autem habent admirationis, cum prope accesseris?
	Qui autem esse poteris, nisi te amor ipse ceperit?
	At, illa, ut vobis placet, partem quandam tuetur, reliquam deserit.
	Quod autem satis est, eo quicquid accessit, nimium est;
	Sed erat aequius Triarium aliquid de dissensione nostra iudicare.
	Quacumque enim ingredimur, in aliqua historia vestigium ponimus.




	Nam interdum nimis etiam novit, quippe qui testificetur ne intellegere quidem se posse ubi sit aut quod sit ullum bonum praeter illud, quod cibo et potione et aurium delectatione et obscena voluptate capiatur.




	Qui cum praetor quaestionem inter sicarios exercuisset, ita aperte cepit pecunias ob rem iudicandam, ut anno proximo P.
	Tanti autem aderant vesicae et torminum morbi, ut nihil ad eorum magnitudinem posset accedere.
	Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri.



Si quidem, inquit, tollerem, sed relinquo. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? Odium autem et invidiam facile vitabis. Odium autem et invidiam facile vitabis. Quid adiuvas? 

Verba tu fingas et ea dicas, quae non sentias? Quid enim de amicitia statueris utilitatis causa expetenda vides. Teneo, inquit, finem illi videri nihil dolere. Qui est in parvis malis. Memini vero, inquam; Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? 


	Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat;
	Quid enim necesse est, tamquam meretricem in matronarum coetum, sic voluptatem in virtutum concilium adducere?
	Omnes enim iucundum motum, quo sensus hilaretur.
	Tum Quintus: Est plane, Piso, ut dicis, inquit.
	Quae in controversiam veniunt, de iis, si placet, disseramus.
	Iam in altera philosophiae parte.




	Quod ea non occurrentia fingunt, vincunt Aristonem;
	Rhetorice igitur, inquam, nos mavis quam dialectice disputare?



Proclivi currit oratio. An vero, inquit, quisquam potest probare, quod perceptfum, quod. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Ratio enim nostra consentit, pugnat oratio. 

Honesta oratio, Socratica, Platonis etiam. Sint modo partes vitae beatae. Quae cum essent dicta, discessimus. Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? Nullus est igitur cuiusquam dies natalis. Illud mihi a te nimium festinanter dictum videtur, sapientis omnis esse semper beatos; Hic nihil fuit, quod quaereremus. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Hoc non est positum in nostra actione. Quamquam te quidem video minime esse deterritum. Te enim iudicem aequum puto, modo quae dicat ille bene noris. Sed nunc, quod agimus; 


	Dicimus aliquem hilare vivere;
	Non modo carum sibi quemque, verum etiam vehementer carum esse?
	Audeo dicere, inquit.
	Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;
	Audeo dicere, inquit.
	Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers?




Etiam inchoatum, ut, si iuste depositum reddere in recte
factis sit, in officiis ponatur depositum reddere;

Utrum igitur tibi litteram videor an totas paginas
commovere?




