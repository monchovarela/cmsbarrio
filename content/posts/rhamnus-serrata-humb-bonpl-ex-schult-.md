Title: Rhamnus serrata Humb. & Bonpl. ex Schult.
Description: Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.
Image: https://source.unsplash.com/random/?blue
Date: 27/5/2021
Keywords: portfolio
Category: Overhold
Author: Topdrive
Tags: article
Published: true
Background: #8e3eee
Color: #a48c4d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. 

Ut id aliis narrare gestiant? Quod autem meum munus dicis non equidem recuso, sed te adiungo socium. Sed ad illum redeo. Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium? 


	Stoici scilicet.
	Iam quae corporis sunt, ea nec auctoritatem cum animi partibus, comparandam et cognitionem habent faciliorem.
	Quonam modo?
	Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus.
	Quonam, inquit, modo?
	Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene.



Quod totum contra est. Dolere malum est: in crucem qui agitur, beatus esse non potest. Primum in nostrane potestate est, quid meminerimus? Sed potestne rerum maior esse dissensio? Sic consequentibus vestris sublatis prima tolluntur. Est enim effectrix multarum et magnarum voluptatum. Tum Quintus: Est plane, Piso, ut dicis, inquit. An hoc usque quaque, aliter in vita? Cur id non ita fit? 

Omnia peccata paria dicitis. Ut non sine causa ex iis memoriae ducta sit disciplina. Quae duo sunt, unum facit. Polycratem Samium felicem appellabant. Egone quaeris, inquit, quid sentiam? Confecta res esset. Ne in odium veniam, si amicum destitero tueri. 


	Etenim semper illud extra est, quod arte comprehenditur.
	Quo invento omnis ab eo quasi capite de summo bono et malo disputatio ducitur.



Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Poterat autem inpune; Conferam tecum, quam cuique verso rem subicias; Aliter enim explicari, quod quaeritur, non potest. 


	Quibus autem in rebus tanta obscuratio non fit, fieri tamen potest, ut id ipsum, quod interest, non sit magnum.




	Ait enim se, si uratur, Quam hoc suave! dicturum.
	Hanc in motu voluptatem -sic enim has suaves et quasi dulces voluptates appellat-interdum ita extenuat, ut M.
	Hoc est dicere: Non reprehenderem asotos, si non essent asoti.
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.




	Et nemo nimium beatus est;
	Ita ne hoc quidem modo paria peccata sunt.
	Quae autem natura suae primae institutionis oblita est?
	Traditur, inquit, ab Epicuro ratio neglegendi doloris.



Tum mihi Piso: Quid ergo? Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. At coluit ipse amicitias. Nonne igitur tibi videntur, inquit, mala? Prioris generis est docilitas, memoria; Aliter enim explicari, quod quaeritur, non potest. Scrupulum, inquam, abeunti; Utilitatis causa amicitia est quaesita. 


Alii rursum isdem a principiis omne officium referent aut ad
voluptatem aut ad non dolendum aut ad prima illa secundum
naturam optinenda.

Si sapiens, ne tum quidem miser, cum ab Oroete, praetore
Darei, in crucem actus est.



Non autem hoc: igitur ne illud quidem. An potest cupiditas finiri? Scrupulum, inquam, abeunti; Nam quid possumus facere melius? Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat. 


	Qui cum praetor quaestionem inter sicarios exercuisset, ita aperte cepit pecunias ob rem iudicandam, ut anno proximo P.



Quae autem natura suae primae institutionis oblita est? Gracchum patrem non beatiorem fuisse quam fillum, cum alter stabilire rem publicam studuerit, alter evertere. Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Re mihi non aeque satisfacit, et quidem locis pluribus. Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; Quae sequuntur igitur? Si quicquam extra virtutem habeatur in bonis. Praeteritis, inquit, gaudeo. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt. 

Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat. Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. Confecta res esset. Sed vos squalidius, illorum vides quam niteat oratio. Huius ego nunc auctoritatem sequens idem faciam. Primum quid tu dicis breve? Collige omnia, quae soletis: Praesidium amicorum. 

Quorum sine causa fieri nihil putandum est. Quid enim de amicitia statueris utilitatis causa expetenda vides. At enim sequor utilitatem. Quare ad ea primum, si videtur; Gerendus est mos, modo recte sentiat. Duo Reges: constructio interrete. Gloriosa ostentatio in constituendo summo bono. Et quod est munus, quod opus sapientiae? Quid autem habent admirationis, cum prope accesseris? 


	Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo;
	Rhetorice igitur, inquam, nos mavis quam dialectice disputare?
	Color egregius, integra valitudo, summa gratia, vita denique conferta voluptatum omnium varietate.
	Verum hoc idem saepe faciamus.




Quid turpius quam sapientis vitam ex insipientium sermone
pendere?

Ergo infelix una molestia, fellx rursus, cum is ipse anulus
in praecordiis piscis inventus est?




	In schola desinis.
	Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi.
	Ita prorsus, inquam;
	Multoque hoc melius nos veriusque quam Stoici.




