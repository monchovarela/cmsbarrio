Title: Liatris aspera Michx. var. intermedia (Lunell) Gaiser
Description: Sed ante. Vivamus tortor. Duis mattis egestas metus.
Image: https://source.unsplash.com/random/?modern
Date: 17/12/2020
Keywords: design
Category: Rank
Author: Skyndu
Tags: website
Published: true
Background: #6e26f9
Color: #bc4ac8
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Optime, inquam. Illa argumenta propria videamus, cur omnia sint paria peccata. Philosophi autem in suis lectulis plerumque moriuntur. 

Quodsi ipsam honestatem undique pertectam atque absolutam. Duo Reges: constructio interrete. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Videsne quam sit magna dissensio? Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Iam in altera philosophiae parte. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. 

Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Sumenda potius quam expetenda. Nondum autem explanatum satis, erat, quid maxime natura vellet. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Atque his de rebus et splendida est eorum et illustris oratio. 


	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;
	ALIO MODO.
	Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt.
	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;




	Stoicos roga.
	Quia dolori non voluptas contraria est, sed doloris privatio.
	Haec dicuntur inconstantissime.
	Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum.
	Si longus, levis;
	Deinde concludebas summum malum esse dolorem, summum bonum voluptatem! Lucius Thorius Balbus fuit, Lanuvinus, quem meminisse tu non potes.




	Equidem etiam Epicurum, in physicis quidem, Democriteum puto.
	Se dicere inter honestum et turpe nimium quantum, nescio quid inmensum, inter ceteras res nihil omnino interesse.




	Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem.




	Immo alio genere;
	Itaque in rebus minime obscuris non multus est apud eos disserendi labor.
	Sullae consulatum?
	An me, inquam, nisi te audire vellem, censes haec dicturum fuisse?
	A mene tu?
	Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est.
	Contineo me ab exemplis.
	Quamquam haec quidem praeposita recte et reiecta dicere licebit.
	Tenent mordicus.
	Sed erat aequius Triarium aliquid de dissensione nostra iudicare.



Sed tamen intellego quid velit. Quo igitur, inquit, modo? Negat enim summo bono afferre incrementum diem. Si verbum sequimur, primum longius verbum praepositum quam bonum. Ut optime, secundum naturam affectum esse possit. Sit enim idem caecus, debilis. 

Quia dolori non voluptas contraria est, sed doloris privatio. De illis, cum volemus. Minime vero istorum quidem, inquit. Idemne potest esse dies saepius, qui semel fuit? Ratio quidem vestra sic cogit. Dolere malum est: in crucem qui agitur, beatus esse non potest. Respondeat totidem verbis. 


Quid tibi, Torquate, quid huic Triario litterae, quid
historiae cognitioque rerum, quid poetarum evolutio, quid
tanta tot versuum memoria voluptatis affert?

Quaero igitur, quo modo hae tantae commendationes a natura
profectae subito a sapientia relictae sint.




	Quoniamque non dubium est quin in iis, quae media dicimus, sit aliud sumendum, aliud reiciendum, quicquid ita fit aut dicitur, omne officio continetur.




	Occultum facinus esse potuerit, gaudebit;
	Vitae autem degendae ratio maxime quidem illis placuit quieta.
	Potius inflammat, ut coercendi magis quam dedocendi esse videantur.
	Eaedem enim utilitates poterunt eas labefactare atque pervertere.
	Praeterea et appetendi et refugiendi et omnino rerum gerendarum initia proficiscuntur aut a voluptate aut a dolore.



Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Quis enim redargueret? Sed quot homines, tot sententiae; Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. 


Hanc ergo intuens debet institutum illud quasi signum
absolvere.

Sed ad rem redeamus;



Restinguet citius, si ardentem acceperit. Quasi ego id curem, quid ille aiat aut neget. Atqui reperies, inquit, in hoc quidem pertinacem; Aeque enim contingit omnibus fidibus, ut incontentae sint. 

Sed haec omittamus; Sed potestne rerum maior esse dissensio? Quid me istud rogas? Quamquam tu hanc copiosiorem etiam soles dicere. Non quam nostram quidem, inquit Pomponius iocans; Quid est igitur, inquit, quod requiras? Nemo nostrum istius generis asotos iucunde putat vivere. Quae sequuntur igitur? Atque his de rebus et splendida est eorum et illustris oratio. Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? 

A primo, ut opinor, animantium ortu petitur origo summi boni. A mene tu? Ita enim vivunt quidam, ut eorum vita refellatur oratio. Haec et tu ita posuisti, et verba vestra sunt. Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore. Eademne, quae restincta siti? 

Praeclare hoc quidem. Facillimum id quidem est, inquam. Quod si ita se habeat, non possit beatam praestare vitam sapientia. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Quod cum ille dixisset et satis disputatum videretur, in oppidum ad Pomponium perreximus omnes. Quid enim tanto opus est instrumento in optimis artibus comparandis? Quicquid porro animo cernimus, id omne oritur a sensibus; Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. 


	Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.
	Utrum igitur percurri omnem Epicuri disciplinam placet an de una voluptate quaeri, de qua omne certamen est?
	Deinde concludebas summum malum esse dolorem, summum bonum voluptatem! Lucius Thorius Balbus fuit, Lanuvinus, quem meminisse tu non potes.
	Recte, inquit, intellegis.
	At habetur! Et ego id scilicet nesciebam! Sed ut sit, etiamne post mortem coletur?
	Si autem id non concedatur, non continuo vita beata tollitur.




