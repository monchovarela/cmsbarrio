Title: Dichaetophora campestris A. Gray
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?red
Date: 12/6/2021
Keywords: web
Category: It
Author: Meevee
Tags: post
Published: true
Background: #9d3d93
Color: #52d154
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ille enim occurrentia nescio quae comminiscebatur; Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio. Egone quaeris, inquit, quid sentiam? Duo Reges: constructio interrete. Quod vestri non item. Confecta res esset. Quae si potest singula consolando levare, universa quo modo sustinebit? 

Quid turpius quam sapientis vitam ex insipientium sermone pendere? Quamquam te quidem video minime esse deterritum. Cur post Tarentum ad Archytam? Tanta vis admonitionis inest in locis; Tum ille timide vel potius verecunde: Facio, inquit. 

Cur iustitia laudatur? Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem. Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene. Reguli reiciendam; Urgent tamen et nihil remittunt. Bonum patria: miserum exilium. Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius. 


	Hoc uno captus Erillus scientiam summum bonum esse defendit nec rem ullam aliam per se expetendam.




	Quonam modo?
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;
	Ita prorsus, inquam;
	Coniunctio autem cum honestate vel voluptatis vel non dolendi id ipsum honestum, quod amplecti vult, id efficit turpe.
	Sint ista Graecorum;
	Atque his de rebus et splendida est eorum et illustris oratio.
	Quod vestri non item.
	Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est.




	Nondum autem explanatum satis, erat, quid maxime natura vellet.
	Aliter enim nosmet ipsos nosse non possumus.
	Sed potestne rerum maior esse dissensio?



Quis istum dolorem timet? Nihil enim hoc differt. Iam enim adesse poterit. Isto modo ne improbos quidem, si essent boni viri. Nemo nostrum istius generis asotos iucunde putat vivere. Respondent extrema primis, media utrisque, omnia omnibus. Sed in rebus apertissimis nimium longi sumus. Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit. Estne, quaeso, inquam, sitienti in bibendo voluptas? 

Nemo nostrum istius generis asotos iucunde putat vivere. At hoc in eo M. Ergo id est convenienter naturae vivere, a natura discedere. Nam ista vestra: Si gravis, brevis; Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? At multis se probavit. Quo tandem modo? 

Haec dicuntur fortasse ieiunius; Ita graviter et severe voluptatem secrevit a bono. Ut optime, secundum naturam affectum esse possit. Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere. Hoc mihi cum tuo fratre convenit. Eadem fortitudinis ratio reperietur. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Beatus sibi videtur esse moriens. 

Recte, inquit, intellegis. Gloriosa ostentatio in constituendo summo bono. Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur. Aliud igitur esse censet gaudere, aliud non dolere. Istic sum, inquit. Qui ita affectus, beatum esse numquam probabis; 


	Bonum incolumis acies: misera caecitas.
	Non ego tecum iam ita iocabor, ut isdem his de rebus, cum L.




	An vero, inquit, quisquam potest probare, quod perceptfum, quod.



Quid de Pythagora? Age, inquies, ista parva sunt. Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit. Sed potestne rerum maior esse dissensio? 


Pomponius Luciusque Cicero, frater noster cognatione
patruelis, amore germanus, constituimus inter nos ut
ambulationem postmeridianam conficeremus in Academia, maxime
quod is locus ab omni turba id temporis vacuus esset.

Semper enim ex eo, quod maximas partes continet latissimeque
funditur, tota res appellatur.




	Tubulo putas dicere?
	Summus dolor plures dies manere non potest?
	Quid de Pythagora?
	Quis tibi ergo istud dabit praeter Pyrrhonem, Aristonem eorumve similes, quos tu non probas?




	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.
	Maximus dolor, inquit, brevis est.
	Qualis ista philosophia est, quae non interitum afferat pravitatis, sed sit contenta mediocritate vitiorum?
	Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem.
	Qua ex cognitione facilior facta est investigatio rerum occultissimarum.




	Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat;
	Memini vero, inquam;
	Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere.
	Illa argumenta propria videamus, cur omnia sint paria peccata.



Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Sed residamus, inquit, si placet. In schola desinis. Aliena dixit in physicis nec ea ipsa, quae tibi probarentur; Etsi qui potest intellegi aut cogitari esse aliquod animal, quod se oderit? Sin laboramus, quis est, qui alienae modum statuat industriae? 

Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Sit sane ista voluptas. Certe, nisi voluptatem tanti aestimaretis. Cur iustitia laudatur? Quam ob rem tandem, inquit, non satisfacit? Sumenda potius quam expetenda. Sapiens autem semper beatus est et est aliquando in dolore; Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. 


Nam quicquid quaeritur, id habet aut generis ipsius sine
personis temporibusque aut his adiunctis facti aut iuris aut
nominis controversiam.

De malis autem et bonis ab iis animalibus, quae nondum
depravata sint, ait optime iudicari.




