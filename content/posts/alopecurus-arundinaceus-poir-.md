Title: Alopecurus arundinaceus Poir.
Description: Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.
Image: https://source.unsplash.com/random/?city
Date: 17/3/2021
Keywords: design
Category: Konklux
Author: Topicware
Tags: work
Published: true
Background: #023c67
Color: #ce4e1b
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? Videamus igitur sententias eorum, tum ad verba redeamus. Isto modo, ne si avia quidem eius nata non esset. Non est igitur voluptas bonum. 

Quae cum essent dicta, discessimus. Cur iustitia laudatur? Quid enim est a Chrysippo praetermissum in Stoicis? A mene tu? Non autem hoc: igitur ne illud quidem. Qui bonum omne in virtute ponit, is potest dicere perfici beatam vitam perfectione virtutis; Hoc enim constituto in philosophia constituta sunt omnia. 


	Animum autem reliquis rebus ita perfecit, ut corpus;
	Eam tum adesse, cum dolor omnis absit;



Hoc loco tenere se Triarius non potuit. Sed haec nihil sane ad rem; Nullus est igitur cuiusquam dies natalis. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Urgent tamen et nihil remittunt. Immo alio genere; Sed quot homines, tot sententiae; 


	In eo enim positum est id, quod dicimus esse expetendum.
	Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset.
	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.



Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat? Sin autem eos non probabat, quid attinuit cum iis, quibuscum re concinebat, verbis discrepare? Qualem igitur hominem natura inchoavit? Facit enim ille duo seiuncta ultima bonorum, quae ut essent vera, coniungi debuerunt; Falli igitur possumus. An vero, inquit, quisquam potest probare, quod perceptfum, quod. 

Omnia peccata paria dicitis. Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Videamus animi partes, quarum est conspectus illustrior; Duo Reges: constructio interrete. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium? At hoc in eo M. Progredientibus autem aetatibus sensim tardeve potius quasi nosmet ipsos cognoscimus. 


	Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere.




	Scripta sane et multa et polita, sed nescio quo pacto auctoritatem oratio non habet.
	Sed non sunt in eo genere tantae commoditates corporis tamque productae temporibus tamque multae.
	In qua quid est boni praeter summam voluptatem, et eam sempiternam?
	Ut proverbia non nulla veriora sint quam vestra dogmata.




Teneamus enim illud necesse est, cum consequens aliquod
falsum sit, illud, cuius id consequens sit, non posse esse
verum.

Bonum negas esse divitias, praeposėtum esse dicis?




Stoici restant, ei quidem non unam aliquam aut alteram rem a
nobis, sed totam ad se nostram philosophiam transtulerunt;

Tria genera cupiditatum, naturales et necessariae, naturales
et non necessariae, nec naturales nec necessariae.



In qua quid est boni praeter summam voluptatem, et eam sempiternam? Certe non potest. Quid enim necesse est, tamquam meretricem in matronarum coetum, sic voluptatem in virtutum concilium adducere? Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Ita fit cum gravior, tum etiam splendidior oratio. Hoc ipsum elegantius poni meliusque potuit. 

In qua quid est boni praeter summam voluptatem, et eam sempiternam? Quamquam tu hanc copiosiorem etiam soles dicere. Huius ego nunc auctoritatem sequens idem faciam. Sed quae tandem ista ratio est? Quae similitudo in genere etiam humano apparet. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. 


	Quare teneamus Aristotelem et eius filium Nicomachum, cuius accurate scripti de moribus libri dicuntur illi quidem esse Aristoteli, sed non video, cur non potuerit patri similis esse filius.




	Miserum hominem! Si dolor summum malum est, dici aliter non potest.
	Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus;
	Sed haec quidem liberius ab eo dicuntur et saepius.
	Etenim si delectamur, cum scribimus, quis est tam invidus, qui ab eo nos abducat?




	Praeclare hoc quidem.
	Egone quaeris, inquit, quid sentiam?
	Ille incendat?
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.
	Haeret in salebra.
	Ut optime, secundum naturam affectum esse possit.
	Erat enim Polemonis.
	Egone non intellego, quid sit don Graece, Latine voluptas?



Quasi ego id curem, quid ille aiat aut neget. Odium autem et invidiam facile vitabis. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Si quicquam extra virtutem habeatur in bonis. Certe non potest. Quonam, inquit, modo? 

Verum esto: verbum ipsum voluptatis non habet dignitatem, nec nos fortasse intellegimus. Quae cum magnifice primo dici viderentur, considerata minus probabantur. Item de contrariis, a quibus ad genera formasque generum venerunt. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Quae sunt igitur communia vobis cum antiquis, iis sic utamur quasi concessis; Nisi autem rerum natura perspecta erit, nullo modo poterimus sensuum iudicia defendere. Quamvis enim depravatae non sint, pravae tamen esse possunt. Primum Theophrasti, Strato, physicum se voluit; 

Quis hoc dicit? Hoc non est positum in nostra actione. Iam illud quale tandem est, bona praeterita non effluere sapienti, mala meminisse non oportere? Ita graviter et severe voluptatem secrevit a bono. Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse. Cur deinde Metrodori liberos commendas? Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur. 


	In schola desinis.
	Quis istud possit, inquit, negare?
	Si quae forte-possumus.
	Unum est sine dolore esse, alterum cum voluptate.
	Immo videri fortasse.
	Non ego tecum iam ita iocabor, ut isdem his de rebus, cum L.
	Tenent mordicus.
	Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest.
	Recte, inquit, intellegis.
	Videamus animi partes, quarum est conspectus illustrior;




