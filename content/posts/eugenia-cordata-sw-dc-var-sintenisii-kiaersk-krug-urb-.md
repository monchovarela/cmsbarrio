Title: Eugenia cordata (Sw.) DC. var. sintenisii (Kiaersk.) Krug & Urb.
Description: Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.
Image: https://source.unsplash.com/random/?developer
Date: 18/4/2021
Keywords: cms
Category: Treeflex
Author: Vidoo
Tags: cms
Published: true
Background: #98774e
Color: #2dbe30
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Quis enim potest ea, quae probabilia videantur ei, non probare? Id et fieri posse et saepe esse factum et ad voluptates percipiendas maxime pertinere. Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam; In eo enim positum est id, quod dicimus esse expetendum. Duo Reges: constructio interrete. Si qua in iis corrigere voluit, deteriora fecit. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. 

Primum in nostrane potestate est, quid meminerimus? Quid turpius quam sapientis vitam ex insipientium sermone pendere? Quare hoc videndum est, possitne nobis hoc ratio philosophorum dare. Etiam inchoatum, ut, si iuste depositum reddere in recte factis sit, in officiis ponatur depositum reddere; Universa enim illorum ratione cum tota vestra confligendum puto. Quis est tam dissimile homini. Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus. Facile est hoc cernere in primis puerorum aetatulis. Age sane, inquam. 


	Itaque fecimus.
	Qui enim existimabit posse se miserum esse beatus non erit.
	Paria sunt igitur.
	Duo enim genera quae erant, fecit tria.
	Ita prorsus, inquam;
	Familiares nostros, credo, Sironem dicis et Philodemum, cum optimos viros, tum homines doctissimos.
	Audeo dicere, inquit.
	Sunt enim prima elementa naturae, quibus auctis v�rtutis quasi germen efficitur.
	Ita credo.
	Praeteritis, inquit, gaudeo.
	At certe gravius.
	Sed ad bona praeterita redeamus.




	Quo modo autem philosophus loquitur?



Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis. Quid censes in Latino fore? Aliter homines, aliter philosophos loqui putas oportere? Is es profecto tu. Restinguet citius, si ardentem acceperit. Ait enim se, si uratur, Quam hoc suave! dicturum. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Innumerabilia dici possunt in hanc sententiam, sed non necesse est. 

Sed haec omittamus; Quid ad utilitatem tantae pecuniae? Quamquam id quidem, infinitum est in hac urbe; Potius inflammat, ut coercendi magis quam dedocendi esse videantur. 

Nos commodius agimus. Maximus dolor, inquit, brevis est. Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse. Est enim effectrix multarum et magnarum voluptatum. Quodsi ipsam honestatem undique pertectam atque absolutam. Haeret in salebra. Quae cum dixisset paulumque institisset, Quid est? 


	Iudicia rerum in sensibus ponit, quibus si semel aliquid falsi pro vero probatum sit, sublatum esse omne iudicium veri et falsi putat.




	Ut proverbia non nulla veriora sint quam vestra dogmata.
	Suam denique cuique naturam esse ad vivendum ducem.
	Quacumque enim ingredimur, in aliqua historia vestigium ponimus.
	Quid enim necesse est, tamquam meretricem in matronarum coetum, sic voluptatem in virtutum concilium adducere?
	Ita finis bonorum existit secundum naturam vivere sic affectum, ut optime is affici possit ad naturamque accommodatissime.




	Sed quid attinet de rebus tam apertis plura requirere?
	Qui non moveatur et offensione turpitudinis et comprobatione honestatis?
	Hoc unum Aristo tenuit: praeter vitia atque virtutes negavit rem esse ullam aut fugiendam aut expetendam.
	An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit?
	Quid enim de amicitia statueris utilitatis causa expetenda vides.




	Tubulo putas dicere?
	Quod vestri quidem vel optime disputant, nihil opus esse eum, qui philosophus futurus sit, scire litteras.
	Scaevolam M.
	Ita nemo beato beatior.
	Primum divisit ineleganter;
	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.
	Explanetur igitur.
	Quamquam id quidem licebit iis existimare, qui legerint.
	Quibusnam praeteritis?
	Si mala non sunt, iacet omnis ratio Peripateticorum.



Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Hoc loco tenere se Triarius non potuit. Philosophi autem in suis lectulis plerumque moriuntur. Summae mihi videtur inscitiae. Isto modo, ne si avia quidem eius nata non esset. Quid de Pythagora? 


At enim hic etiam dolore.

Licet hic rursus ea commemores, quae optimis verbis ab
Epicuro de laude amicitiae dicta sunt.



Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. Si quicquam extra virtutem habeatur in bonis. Sed ne, dum huic obsequor, vobis molestus sim. Ut pulsi recurrant? 


Idque quo magis quidam ita faciunt, ut iure etiam
reprehendantur, hoc magis intellegendum est haec ipsa nimia
in quibusdam futura non fuisse, nisi quaedam essent modica
natura.

Aristoteles, Xenocrates, tota illa familia non dabit, quippe
qui valitudinem, vires, divitias, gloriam, multa alia bona
esse dicant, laudabilia non dicant.



Quam si explicavisset, non tam haesitaret. Nec hoc ille non vidit, sed verborum magnificentia est et gloria delectatus. Consequentia exquirere, quoad sit id, quod volumus, effectum. Scisse enim te quis coarguere possit? Sequitur disserendi ratio cognitioque naturae; Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. Sequitur disserendi ratio cognitioque naturae; Cur iustitia laudatur? 


	Hoc Hieronymus summum bonum esse dixit.
	Nam, ut sint illa vendibiliora, haec uberiora certe sunt.
	Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers?
	Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt.
	Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?




	Ut optime, secundum naturam affectum esse possit.
	Quis non odit sordidos, vanos, leves, futtiles?
	Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum.
	Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio.



Hic ambiguo ludimur. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Murenam te accusante defenderem. Itaque ab his ordiamur. Non quam nostram quidem, inquit Pomponius iocans; Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Hanc quoque iucunditatem, si vis, transfer in animum; Hoc non est positum in nostra actione. 

Huius ego nunc auctoritatem sequens idem faciam. Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Eaedem enim utilitates poterunt eas labefactare atque pervertere. 


