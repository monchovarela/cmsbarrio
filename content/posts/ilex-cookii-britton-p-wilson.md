Title: Ilex cookii Britton & P. Wilson
Description: Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.
Image: https://source.unsplash.com/random/?strage
Date: 11/7/2021
Keywords: website
Category: Gembucket
Author: Rhynyx
Tags: work
Published: true
Background: #703668
Color: #99847c
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Inquit, an parum disserui non verbis Stoicos a Peripateticis, sed universa re et tota sententia dissidere? Sed residamus, inquit, si placet. 

Non laboro, inquit, de nomine. Quid vero? Hoc non est positum in nostra actione. Quae duo sunt, unum facit. An hoc usque quaque, aliter in vita? Ita multo sanguine profuso in laetitia et in victoria est mortuus. Negat enim summo bono afferre incrementum diem. Si autem id non concedatur, non continuo vita beata tollitur. Minime vero istorum quidem, inquit. 


	Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur.
	Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam;
	Quod quidem iam fit etiam in Academia.
	A quibus propter discendi cupiditatem videmus ultimas terras esse peragratas.
	Cave putes quicquam esse verius.
	Sit hoc ultimum bonorum, quod nunc a me defenditur;



Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere. Immo istud quidem, inquam, quo loco quidque, nisi iniquum postulo, arbitratu meo. Sed ea mala virtuti magnitudine obruebantur. Tanta vis admonitionis inest in locis; Longum est enim ad omnia respondere, quae a te dicta sunt. Quid autem habent admirationis, cum prope accesseris? Dicimus aliquem hilare vivere; 


	Quod, inquit, quamquam voluptatibus quibusdam est saepe iucundius, tamen expetitur propter voluptatem.
	A primo, ut opinor, animantium ortu petitur origo summi boni.
	Negat enim summo bono afferre incrementum diem.
	Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat?




	Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat.
	Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
	Qui est in parvis malis.
	Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem.
	Quid est, quod ab ea absolvi et perfici debeat?



Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene. Hoc non est positum in nostra actione. Eaedem res maneant alio modo. Omnia peccata paria dicitis. 

Deinde dolorem quem maximum? Duo Reges: constructio interrete. Duarum enim vitarum nobis erunt instituta capienda. Quae in controversiam veniunt, de iis, si placet, disseramus. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Quare attende, quaeso. Et harum quidem rerum facilis est et expedita distinctio. Cum audissem Antiochum, Brute, ut solebam, cum M. 


	Ne amores quidem sanctos a sapiente alienos esse arbitrantur.




	Ut iam liceat una comprehensione omnia complecti non dubitantemque dicere omnem naturam esse servatricem sui idque habere propositum quasi finem et extremum, se ut custodiat quam in optimo sui generis statu;




Quam vellem, inquit, te ad Stoicos inclinavisses! erat enim,
si cuiusquam, certe tuum nihil praeter virtutem in bonis
ducere.

Tum ille: Finem, inquit, interrogandi, si videtur, quod
quidem ego a principio ita me malle dixeram hoc ipsum
providens, dialecticas captiones.




	Tu quidem reddes;
	Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer.
	Haec dicuntur inconstantissime.
	Beatus autem esse in maximarum rerum timore nemo potest.
	In schola desinis.
	Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur?
	Sed haec omittamus;
	Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.




Conveniret, pluribus praeterea conscripsisset qui esset
optimus rei publicae status, hoc amplius Theophrastus: quae
essent in re publica rerum inclinationes et momenta
temporum, quibus esset moderandum, utcumque res postularet.

Fortasse id optimum, sed ubi illud: Plus semper voluptatis?



Haec dicuntur fortasse ieiunius; Si enim ita est, vide ne facinus facias, cum mori suadeas. Haec igitur Epicuri non probo, inquam. Age sane, inquam. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. In motu et in statu corporis nihil inest, quod animadvertendum esse ipsa natura iudicet? 

Iubet igitur nos Pythius Apollo noscere nosmet ipsos. Quae cum dixisset paulumque institisset, Quid est? Ita ne hoc quidem modo paria peccata sunt. Sed ad illum redeo. Ergo, si semel tristior effectus est, hilara vita amissa est? Hic ambiguo ludimur. Ergo ita: non posse honeste vivi, nisi honeste vivatur? Ratio enim nostra consentit, pugnat oratio. Videamus animi partes, quarum est conspectus illustrior; Quo modo autem optimum, si bonum praeterea nullum est? 

Nihil opus est exemplis hoc facere longius. Inde sermone vario sex illa a Dipylo stadia confecimus. Cui Tubuli nomen odio non est? In motu et in statu corporis nihil inest, quod animadvertendum esse ipsa natura iudicet? 


	Ita prorsus, inquam;
	Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?
	Nos cum te, M.
	An eiusdem modi?
	Sed videbimus.
	Nec enim figura corporis nec ratio excellens ingenii humani significat ad unam hanc rem natum hominem, ut frueretur voluptatibus.
	Quo modo?
	Faceres tu quidem, Torquate, haec omnia;
	Pollicetur certe.
	Sed haec quidem liberius ab eo dicuntur et saepius.




	Te enim iudicem aequum puto, modo quae dicat ille bene noris.
	At enim hic etiam dolore.
	Videamus igitur sententias eorum, tum ad verba redeamus.
	Verum hoc idem saepe faciamus.



Ut non sine causa ex iis memoriae ducta sit disciplina. Quid me istud rogas? Non autem hoc: igitur ne illud quidem. Quae iam oratio non a philosopho aliquo, sed a censore opprimenda est. Quo tandem modo? Quicquid porro animo cernimus, id omne oritur a sensibus; Non risu potius quam oratione eiciendum? Nec vero intermittunt aut admirationem earum rerum, quae sunt ab antiquis repertae, aut investigationem novarum. Si qua in iis corrigere voluit, deteriora fecit. Non enim solum Torquatus dixit quid sentiret, sed etiam cur. 

Propter nos enim illam, non propter eam nosmet ipsos diligimus. Quasi ego id curem, quid ille aiat aut neget. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Avaritiamne minuis? 


