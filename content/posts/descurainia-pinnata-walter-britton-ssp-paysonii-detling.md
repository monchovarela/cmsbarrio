Title: Descurainia pinnata (Walter) Britton ssp. paysonii Detling
Description: Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.
Image: https://source.unsplash.com/random/?beauty
Date: 16/8/2021
Keywords: work
Category: Sonsing
Author: Gabvine
Tags: design
Published: true
Background: #c8b2cb
Color: #d5c0c8
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui convenit? Propter nos enim illam, non propter eam nosmet ipsos diligimus. Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt. Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest. 

At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Nisi autem rerum natura perspecta erit, nullo modo poterimus sensuum iudicia defendere. Audeo dicere, inquit. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. 

Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Quae similitudo in genere etiam humano apparet. Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Sed fortuna fortis; Si autem id non concedatur, non continuo vita beata tollitur. 

Est, ut dicis, inquam. Qua igitur re ab deo vincitur, si aeternitate non vincitur? Non laboro, inquit, de nomine. Quid, si reviviscant Platonis illi et deinceps qui eorum auditores fuerunt, et tecum ita loquantur? Sed haec quidem liberius ab eo dicuntur et saepius. Primum cur ista res digna odio est, nisi quod est turpis? Sin laboramus, quis est, qui alienae modum statuat industriae? Quamquam tu hanc copiosiorem etiam soles dicere. Primum Theophrasti, Strato, physicum se voluit; Primum quid tu dicis breve? Quamquam te quidem video minime esse deterritum. 


Scripsit enim et multis saepe verbis et breviter arteque in
eo libro, quem modo nominavi, mortem nihil ad nos pertinere.

Restant Stoici, qui cum a Peripateticis et Academicis omnia
transtulissent, nominibus aliis easdem res secuti sunt.



Ita relinquet duas, de quibus etiam atque etiam consideret. Apparet statim, quae sint officia, quae actiones. Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. Philosophi autem in suis lectulis plerumque moriuntur. Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? Quae cum essent dicta, finem fecimus et ambulandi et disputandi. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Nihil opus est exemplis hoc facere longius. 

Duo Reges: constructio interrete. Haeret in salebra. Et quod est munus, quod opus sapientiae? Gerendus est mos, modo recte sentiat. Hoc mihi cum tuo fratre convenit. 


	Quid ergo?
	Tuo vero id quidem, inquam, arbitratu.
	Optime, inquam.
	Vide igitur ne non debeas verbis nostris uti, sententiis tuis.
	Easdemne res?
	Et quidem, inquit, vehementer errat;




	Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer.
	Summus dolor plures dies manere non potest?
	Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum.
	Quod praeceptum quia maius erat, quam ut ab homine videretur, idcirco assignatum est deo.




Atqui, inquit, si Stoicis concedis ut virtus sola, si adsit
vitam efficiat beatam, concedis etiam Peripateticis.

Tum ille timide vel potius verecunde: Facio, inquit.




	Immo alio genere;
	Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.
	Si longus, levis;
	Totum autem id externum est, et quod externum, id in casu est.
	Ille incendat?
	Mihi quidem Antiochum, quem audis, satis belle videris attendere.
	Quid enim?
	Dicam, inquam, et quidem discendi causa magis, quam quo te aut Epicurum reprehensum velim.
	Nihil sane.
	Conferam tecum, quam cuique verso rem subicias;




	Sed plane dicit quod intellegit.
	Num igitur dubium est, quin, si in re ipsa nihil peccatur a superioribus, verbis illi commodius utantur?
	Quod si ita se habeat, non possit beatam praestare vitam sapientia.




	Quod autem ratione actum est, id officium appellamus.



Utrum igitur tibi litteram videor an totas paginas commovere? Sin laboramus, quis est, qui alienae modum statuat industriae? At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Negare non possum. Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Si longus, levis; Sed ad haec, nisi molestum est, habeo quae velim. Istam voluptatem perpetuam quis potest praestare sapienti? Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Sit sane ista voluptas. 

Cur id non ita fit? Eam stabilem appellas. Summus dolor plures dies manere non potest? Beatus sibi videtur esse moriens. Idem adhuc; An haec ab eo non dicuntur? Quare ad ea primum, si videtur; 

Ita multo sanguine profuso in laetitia et in victoria est mortuus. Ut in geometria, prima si dederis, danda sunt omnia. Quam illa ardentis amores excitaret sui! Cur tandem? Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Estne, quaeso, inquam, sitienti in bibendo voluptas? Cur, nisi quod turpis oratio est? 


	Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore.
	Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.
	Hoc est non dividere, sed frangere.
	At negat Epicurus-hoc enim vestrum lumen estquemquam, qui honeste non vivat, iucunde posse vivere.




	Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus;
	Cuius similitudine perspecta in formarum specie ac dignitate transitum est ad honestatem dictorum atque factorum.
	Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum.
	Cum praesertim illa perdiscere ludus esset.




	Non enim hilaritate nec lascivia nec risu aut ioco, comite levitatis, saepe etiam tristes firmitate et constantia sunt beati.



Idemne, quod iucunde? Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Minime vero, inquit ille, consentit. Nosti, credo, illud: Nemo pius est, qui pietatem-; Sit, inquam, tam facilis, quam vultis, comparatio voluptatis, quid de dolore dicemus? Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. Duo enim genera quae erant, fecit tria. 


