Title: Usnea filipendula Stirt.
Description: Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.
Image: https://source.unsplash.com/random/?css
Date: 22/2/2021
Keywords: cms
Category: Cookley
Author: Realbridge
Tags: article
Published: true
Background: #7f054b
Color: #f2cee3
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae est igitur causa istarum angustiarum? Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Egone quaeris, inquit, quid sentiam? Tubulo putas dicere? Duo Reges: constructio interrete. Tum Quintus: Est plane, Piso, ut dicis, inquit. Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. Dicam, inquam, et quidem discendi causa magis, quam quo te aut Epicurum reprehensum velim. Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Non autem hoc: igitur ne illud quidem. In schola desinis. 

Quippe: habes enim a rhetoribus; Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. At coluit ipse amicitias. Dat enim intervalla et relaxat. 


	In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret.
	Quod quidem iam fit etiam in Academia.
	Quid turpius quam sapientis vitam ex insipientium sermone pendere?
	Portenta haec esse dicit, neque ea ratione ullo modo posse vivi;
	Memini vero, inquam;



Sed ad haec, nisi molestum est, habeo quae velim. Sine ea igitur iucunde negat posse se vivere? Hoc enim constituto in philosophia constituta sunt omnia. Videsne quam sit magna dissensio? 

Zenonis est, inquam, hoc Stoici. Sumenda potius quam expetenda. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Multoque hoc melius nos veriusque quam Stoici. Beatus sibi videtur esse moriens. Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Itaque ad tempus ad Pisonem omnes. At ille pellit, qui permulcet sensum voluptate. Negat enim summo bono afferre incrementum diem. 


	Aut etiam, ut vestitum, sic sententiam habeas aliam domesticam, aliam forensem, ut in fronte ostentatio sit, intus veritas occultetur?



Prioris generis est docilitas, memoria; Suam denique cuique naturam esse ad vivendum ducem. Certe, nisi voluptatem tanti aestimaretis. Atqui reperies, inquit, in hoc quidem pertinacem; Quod iam a me expectare noli. Gloriosa ostentatio in constituendo summo bono. At tu eadem ista dic in iudicio aut, si coronam times, dic in senatu. Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor; Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Si enim non fuit eorum iudicii, nihilo magis hoc non addito illud est iudicatum-. Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. 

Nunc de hominis summo bono quaeritur; Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. 


	-, sed ut hoc iudicaremus, non esse in iis partem maximam positam beate aut secus vivendi.
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?
	Sed quid attinet de rebus tam apertis plura requirere?
	Quae quo sunt excelsiores, eo dant clariora indicia naturae.




	Quae in controversiam veniunt, de iis, si placet, disseramus.
	At hoc in eo M.
	Cum salvum esse flentes sui respondissent, rogavit essentne fusi hostes.




Aut pertinacissimus fueris, si in eo perstiteris ad corpus
ea, quae dixi, referri, aut deserueris totam Epicuri
voluptatem, si negaveris.

An potest cupiditas finiri?




	Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit;




Cum autem progrediens confirmatur animus, agnoscit ille
quidem naturae vim, sed ita, ut progredi possit longius, per
se sit tantum inchoata.

Nam si quae sunt aliae, falsum est omnis animi voluptates
esse e corporis societate.



Ratio quidem vestra sic cogit. Oratio me istius philosophi non offendit; Nobis Heracleotes ille Dionysius flagitiose descivisse videtur a Stoicis propter oculorum dolorem. Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus. Non est ista, inquam, Piso, magna dissensio. Polycratem Samium felicem appellabant. 

Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Tum Triarius: Posthac quidem, inquit, audacius. Deinceps videndum est, quoniam satis apertum est sibi quemque natura esse carum, quae sit hominis natura. Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Idemne, quod iucunde? Contemnit enim disserendi elegantiam, confuse loquitur. 


	Sed ad rem redeamus;
	Efficiens dici potest.
	Nihilo magis.
	Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum.
	Quae sequuntur igitur?
	Sit ista in Graecorum levitate perversitas, qui maledictis insectantur eos, a quibus de veritate dissentiunt.
	Quis enim redargueret?
	Huius, Lyco, oratione locuples, rebus ipsis ielunior.



Quicquid porro animo cernimus, id omne oritur a sensibus; Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Est enim effectrix multarum et magnarum voluptatum. Est, ut dicis, inquit; Si autem id non concedatur, non continuo vita beata tollitur. Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Aliter homines, aliter philosophos loqui putas oportere? 


	Perge porro;
	Itaque hic ipse iam pridem est reiectus;
	Stoici scilicet.
	Neque enim civitas in seditione beata esse potest nec in discordia dominorum domus;
	Recte dicis;
	Nam si propter voluptatem, quae est ista laus, quae possit e macello peti?




	Sin ea non neglegemus neque tamen ad finem summi boni referemus, non multum ab Erilli levitate aberrabimus.
	Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt.
	Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere.
	Quo plebiscito decreta a senatu est consuli quaestio Cn.



At modo dixeras nihil in istis rebus esse, quod interesset. Dempta enim aeternitate nihilo beatior Iuppiter quam Epicurus; Ut optime, secundum naturam affectum esse possit. Traditur, inquit, ab Epicuro ratio neglegendi doloris. 


