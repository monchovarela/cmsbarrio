Title: Ceratopteris pteridoides (Hook.) Hieron.
Description: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.
Image: https://source.unsplash.com/random/?developer
Date: 22/11/2020
Keywords: post
Category: Bitwolf
Author: Feedfish
Tags: article
Published: true
Background: #7d50c9
Color: #0bd386
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid de Pythagora? Omnis enim est natura diligens sui. Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare; Duo Reges: constructio interrete. Immo istud quidem, inquam, quo loco quidque, nisi iniquum postulo, arbitratu meo. Summus dolor plures dies manere non potest? 


	At eum nihili facit;
	Fortasse id optimum, sed ubi illud: Plus semper voluptatis?
	In primo enim ortu inest teneritas ac mollitia quaedam, ut nec res videre optimas nec agere possint.
	Sed tamen est aliquid, quod nobis non liceat, liceat illis.
	Qualem igitur hominem natura inchoavit?
	Amicitiae vero locus ubi esse potest aut quis amicus esse cuiquam, quem non ipsum amet propter ipsum?



Sine ea igitur iucunde negat posse se vivere? Quippe: habes enim a rhetoribus; Nemo igitur esse beatus potest. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Apparet statim, quae sint officia, quae actiones. Non est ista, inquam, Piso, magna dissensio. Beatus sibi videtur esse moriens. Mihi, inquam, qui te id ipsum rogavi? 


	Certe non potest.
	Non enim solum Torquatus dixit quid sentiret, sed etiam cur.
	Pollicetur certe.
	Et adhuc quidem ita nobis progresso ratio est, ut ea duceretur omnis a prima commendatione naturae.
	Stoici scilicet.
	Hos contra singulos dici est melius.




	Iam id ipsum absurdum, maximum malum neglegi.
	Quid enim est a Chrysippo praetermissum in Stoicis?
	Tu enim ista lenius, hic Stoicorum more nos vexat.
	Quid, si non sensus modo ei sit datus, verum etiam animus hominis?



Curium putes loqui, interdum ita laudat, ut quid praeterea sit bonum neget se posse ne suspicari quidem. Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Quantum Aristoxeni ingenium consumptum videmus in musicis? Et quidem illud ipsum non nimium probo et tantum patior, philosophum loqui de cupiditatibus finiendis. 


	Quam si explicavisset, non tam haesitaret.
	Quorum sine causa fieri nihil putandum est.



Quia nec honesto quic quam honestius nec turpi turpius. Huius ego nunc auctoritatem sequens idem faciam. Dat enim intervalla et relaxat. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Id Sextilius factum negabat. Ita nemo beato beatior. Sed ad illum redeo. Theophrastum tamen adhibeamus ad pleraque, dum modo plus in virtute teneamus, quam ille tenuit, firmitatis et roboris. Quae enim adhuc protulisti, popularia sunt, ego autem a te elegantiora desidero. 

Minime vero, inquit ille, consentit. Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Nulla erit controversia. Quid enim de amicitia statueris utilitatis causa expetenda vides. Vide, quantum, inquam, fallare, Torquate. Nam ante Aristippus, et ille melius. 

Duae sunt enim res quoque, ne tu verba solum putes. Iubet igitur nos Pythius Apollo noscere nosmet ipsos. Is es profecto tu. Isto modo, ne si avia quidem eius nata non esset. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Eam tum adesse, cum dolor omnis absit; Sed ne, dum huic obsequor, vobis molestus sim. Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Idem iste, inquam, de voluptate quid sentit? Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? 


	Immo alio genere;
	In quo etsi est magnus, tamen nova pleraque et perpauca de moribus.
	Itaque fecimus.
	Hoc ne statuam quidem dicturam pater aiebat, si loqui posset.
	Quonam, inquit, modo?
	Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur.
	Sed nimis multa.
	Quid, si reviviscant Platonis illi et deinceps qui eorum auditores fuerunt, et tecum ita loquantur?




Qui autem voluptate vitam effici beatam putabit, qui sibi is
conveniet, si negabit voluptatem crescere longinquitate?

Ergo in utroque exercebantur, eaque disciplina effecit
tantam illorum utroque in genere dicendi copiam.



Si verbum sequimur, primum longius verbum praepositum quam bonum. Erat enim Polemonis. Cave putes quicquam esse verius. Nam cui proposito sit conservatio sui, necesse est huic partes quoque sui caras suo genere laudabiles. Itaque contra est, ac dicitis; Quoniam, si dis placet, ab Epicuro loqui discimus. 


Honestum igitur id intellegimus, quod tale est, ut detracta
omni utilitate sine ullis praemiis fructibusve per se ipsum
possit iure laudari.

Sed ad rem redeamus;




	Pudebit te, inquam, illius tabulae, quam Cleanthes sane commode verbis depingere solebat.




	Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est.
	Nam his libris eum malo quam reliquo ornatu villae delectari.
	Quis non odit sordidos, vanos, leves, futtiles?
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis.
	Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est.



Praeterea et appetendi et refugiendi et omnino rerum gerendarum initia proficiscuntur aut a voluptate aut a dolore. Quis hoc dicit? Quod cum dixissent, ille contra. Quicquid porro animo cernimus, id omne oritur a sensibus; Cuius similitudine perspecta in formarum specie ac dignitate transitum est ad honestatem dictorum atque factorum. Quis est tam dissimile homini. 

Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum. Minime vero istorum quidem, inquit. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Vide igitur ne non debeas verbis nostris uti, sententiis tuis. Quo studio Aristophanem putamus aetatem in litteris duxisse? Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Qui ita affectus, beatum esse numquam probabis; Dolere malum est: in crucem qui agitur, beatus esse non potest. Et hercule-fatendum est enim, quod sentio -mirabilis est apud illos contextus rerum. At iam decimum annum in spelunca iacet. Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Negat esse eam, inquit, propter se expetendam. 


	Una voluptas e multis obscuratur in illa vita voluptaria, sed tamen ea, quamvis parva sit, pars est eius vitae, quae posita est in voluptate.



Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Tamen a proposito, inquam, aberramus. Itaque si aut requietem natura non quaereret aut eam posset alia quadam ratione consequi. Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Sed vos squalidius, illorum vides quam niteat oratio. Stoici scilicet. Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Quae quidem vel cum periculo est quaerenda vobis; 


