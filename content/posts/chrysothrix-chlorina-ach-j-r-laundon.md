Title: Chrysothrix chlorina (Ach.) J.R. Laundon
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?iphone
Date: 6/8/2021
Keywords: work
Category: Tempsoft
Author: Avavee
Tags: portfolio
Published: true
Background: #a9d453
Color: #80b365
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tuo vero id quidem, inquam, arbitratu. Restinguet citius, si ardentem acceperit. Duo Reges: constructio interrete. Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Primum in nostrane potestate est, quid meminerimus? Qui est in parvis malis. Si longus, levis dictata sunt. 


Verum enim diceret, idque Socratem, qui voluptatem nullo
loco numerat, audio dicentem, cibi condimentum esse famem,
potionis sitim.

Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam
esse oportebit;




Nobis enim ista quaesita, a nobis descripta, notata,
praecepta sunt, omniumque rerum publicarum reetionis genera,
status, mutationes, leges etiam et instituta ac mores
civitatum perscripsimus.

Nullum inveniri verbum potest quod magis idem declaret
Latine, quod Graece, quam declarat voluptas.




	Cumque ipsa virtus efficiat ita beatam vitam, ut beatior esse non possit, tamen quaedam deesse sapientibus tum, cum sint beatissimi;



Indicant pueri, in quibus ut in speculis natura cernitur. Quae cum dixisset, finem ille. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Sine ea igitur iucunde negat posse se vivere? Conferam tecum, quam cuique verso rem subicias; An tu me de L. 

Non dolere, inquam, istud quam vim habeat postea videro; Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Hoc loco discipulos quaerere videtur, ut, qui asoti esse velint, philosophi ante fiant. Quo modo? Urgent tamen et nihil remittunt. Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium? Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem. Quod non faceret, si in voluptate summum bonum poneret. 


	Recte, inquit, intellegis.
	Quis non odit sordidos, vanos, leves, futtiles?
	Sed videbimus.
	Comprehensum, quod cognitum non habet?
	In schola desinis.
	Etenim si delectamur, cum scribimus, quis est tam invidus, qui ab eo nos abducat?
	Quid adiuvas?
	Non est enim vitium in oratione solum, sed etiam in moribus.
	Pollicetur certe.
	Omnibus enim artibus volumus attributam esse eam, quae communis appellatur prudentia, quam omnes, qui cuique artificio praesunt, debent habere.
	Peccata paria.
	Quae cum dixisset paulumque institisset, Quid est?



Huius, Lyco, oratione locuples, rebus ipsis ielunior. Non autem hoc: igitur ne illud quidem. Efficiens dici potest. Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. Quae cum dixisset paulumque institisset, Quid est? Iam id ipsum absurdum, maximum malum neglegi. Nondum autem explanatum satis, erat, quid maxime natura vellet. At enim, qua in vita est aliquid mali, ea beata esse non potest. 


	Quid enim est tam repugnans quam eundem dicere, quod honestum sit, solum id bonum esse, qui dicat appetitionem rerum ad vivendum accommodatarum natura profectam?




	Nec vero sum nescius esse utilitatem in historia, non modo voluptatem.
	Itaque eos id agere, ut a se dolores, morbos, debilitates repellant.
	Voluptatem cum summum bonum diceret, primum in eo ipso parum vidit, deinde hoc quoque alienum;
	Quae sequuntur igitur?




	Respondeat totidem verbis.
	Zenonis est, inquam, hoc Stoici.
	Audeo dicere, inquit.
	Non pugnem cum homine, cur tantum habeat in natura boni;
	Cur iustitia laudatur?
	Id enim volumus, id contendimus, ut officii fructus sit ipsum officium.
	Easdemne res?
	Nescio quo modo praetervolavit oratio.




	Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere.
	Ergo illi intellegunt quid Epicurus dicat, ego non intellego?
	Quae cum dixisset paulumque institisset, Quid est?
	Sin laboramus, quis est, qui alienae modum statuat industriae?



Nihil opus est exemplis hoc facere longius. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Estne, quaeso, inquam, sitienti in bibendo voluptas? Eam tum adesse, cum dolor omnis absit; Deque his rebus satis multa in nostris de re publica libris sunt dicta a Laelio. 

Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Summus dolor plures dies manere non potest? Quicquid porro animo cernimus, id omne oritur a sensibus; Quippe: habes enim a rhetoribus; Tum ille: Ain tandem? An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit? Indicant pueri, in quibus ut in speculis natura cernitur. 

Bestiarum vero nullum iudicium puto. Duae sunt enim res quoque, ne tu verba solum putes. Sed ad haec, nisi molestum est, habeo quae velim. Polycratem Samium felicem appellabant. Quid de Pythagora? Sed ad rem redeamus; Quae animi affectio suum cuique tribuens atque hanc, quam dico. Quae similitudo in genere etiam humano apparet. Sed quot homines, tot sententiae; 

Quod ea non occurrentia fingunt, vincunt Aristonem; Quis enim redargueret? Quamquam te quidem video minime esse deterritum. Et quod est munus, quod opus sapientiae? Ergo in utroque exercebantur, eaque disciplina effecit tantam illorum utroque in genere dicendi copiam. Miserum hominem! Si dolor summum malum est, dici aliter non potest. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Sed ad bona praeterita redeamus. Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? Nunc omni virtuti vitium contrario nomine opponitur. Erat enim Polemonis. 

Illi enim inter se dissentiunt. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Ea possunt paria non esse. Etenim semper illud extra est, quod arte comprehenditur. Certe nihil nisi quod possit ipsum propter se iure laudari. Re mihi non aeque satisfacit, et quidem locis pluribus. 

Cum salvum esse flentes sui respondissent, rogavit essentne fusi hostes. Sit hoc ultimum bonorum, quod nunc a me defenditur; At, illa, ut vobis placet, partem quandam tuetur, reliquam deserit. Odium autem et invidiam facile vitabis. Non est enim vitium in oratione solum, sed etiam in moribus. Hoc loco tenere se Triarius non potuit. Fortemne possumus dicere eundem illum Torquatum? Multoque hoc melius nos veriusque quam Stoici. 


	Quid igitur dubitamus in tota eius natura quaerere quid sit effectum?
	Ergo, si semel tristior effectus est, hilara vita amissa est?
	Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus;
	Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero.




	Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur?
	Nunc omni virtuti vitium contrario nomine opponitur.
	Scio enim esse quosdam, qui quavis lingua philosophari possint;
	Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest.




