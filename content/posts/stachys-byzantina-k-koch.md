Title: Stachys byzantina K. Koch
Description: Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.
Image: https://source.unsplash.com/random/?city
Date: 11/1/2021
Keywords: web
Category: Holdlamis
Author: Voomm
Tags: work
Published: true
Background: #38ff5f
Color: #42b286
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quonam, inquit, modo? Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Cur iustitia laudatur? 

Sed tu istuc dixti bene Latine, parum plane. Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Quorum sine causa fieri nihil putandum est. Duo Reges: constructio interrete. Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia; Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Ita relinquet duas, de quibus etiam atque etiam consideret. Utrum igitur tibi litteram videor an totas paginas commovere? Illum mallem levares, quo optimum atque humanissimum virum, Cn. Theophrasti igitur, inquit, tibi liber ille placet de beata vita? 

Quamvis enim depravatae non sint, pravae tamen esse possunt. Respondent extrema primis, media utrisque, omnia omnibus. Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Haec igitur Epicuri non probo, inquam. Idemne potest esse dies saepius, qui semel fuit? 


	De qua Epicurus quidem ita dicit, omnium rerum, quas ad beate vivendum sapientia comparaverit, nihil esse maius amicitia, nihil uberius, nihil iucundius.



Nemo nostrum istius generis asotos iucunde putat vivere. At iste non dolendi status non vocatur voluptas. Hoc non est positum in nostra actione. Itaque hic ipse iam pridem est reiectus; Estne, quaeso, inquam, sitienti in bibendo voluptas? In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. 


Sit hoc ultimum bonorum, quod nunc a me defenditur;

Nam si +omnino nos+ neglegemus, in Aristonea vitia incidemus
et peccata obliviscemurque quae virtuti ipsi principia
dederimus;




	Nam memini etiam quae nolo, oblivisci non possum quae volo.
	Si autem id non concedatur, non continuo vita beata tollitur.
	Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum.
	Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.



Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem. Vides igitur te aut ea sumere, quae non concedantur, aut ea, quae etiam concessa te nihil iuvent. Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere. Ut pulsi recurrant? Ego vero volo in virtute vim esse quam maximam; 


Effluit igitur voluptas corporis et prima quaeque avolat
saepiusque relinquit causam paenitendi quam recordandi.

Illa videamus, quae a te de amicitia dicta sunt.



Hic ambiguo ludimur. Quid igitur, inquit, eos responsuros putas? Atque his de rebus et splendida est eorum et illustris oratio. Nemo nostrum istius generis asotos iucunde putat vivere. Qui ita affectus, beatum esse numquam probabis; Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. 

Sed vobis voluptatum perceptarum recordatio vitam beatam facit, et quidem corpore perceptarum. Quodsi ipsam honestatem undique pertectam atque absolutam. Est, ut dicis, inquam. Sed quid sentiat, non videtis. Illa tamen simplicia, vestra versuta. Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Ille enim occurrentia nescio quae comminiscebatur; Quod autem principium officii quaerunt, melius quam Pyrrho; 


	Non semper, inquam;
	Quos quidem tibi studiose et diligenter tractandos magnopere censeo.
	Hic ambiguo ludimur.
	Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.
	Quis enim redargueret?
	Non dolere, inquam, istud quam vim habeat postea videro;




	Miserum hominem! Si dolor summum malum est, dici aliter non potest.
	In enumerandis autem corporis commodis si quis praetermissam a nobis voluptatem putabit, in aliud tempus ea quaestio differatur.
	Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus;




	Uterque enim summo bono fruitur, id est voluptate.
	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.
	Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat?
	Propter nos enim illam, non propter eam nosmet ipsos diligimus.
	Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris.
	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?



Prodest, inquit, mihi eo esse animo. Idem iste, inquam, de voluptate quid sentit? Quid vero? Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia. Qui est in parvis malis. In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Mihi, inquam, qui te id ipsum rogavi? 

Theophrastus mediocriterne delectat, cum tractat locos ab Aristotele ante tractatos? Non enim iam stirpis bonum quaeret, sed animalis. Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est. Utilitatis causa amicitia est quaesita. 


	Ergo id est convenienter naturae vivere, a natura discedere.
	Conferam tecum, quam cuique verso rem subicias;



Quod autem satis est, eo quicquid accessit, nimium est; Traditur, inquit, ab Epicuro ratio neglegendi doloris. Omnia contraria, quos etiam insanos esse vultis. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Indicant pueri, in quibus ut in speculis natura cernitur. Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio. Quia nec honesto quic quam honestius nec turpi turpius. Putabam equidem satis, inquit, me dixisse. 


	Quod enim dissolutum sit, id esse sine sensu, quod autem sine sensu sit, id nihil ad nos pertinere omnino.




	Nos commodius agimus.
	Optime, inquam.
	In schola desinis.
	Sine ea igitur iucunde negat posse se vivere?
	An eiusdem modi?
	Memini vero, inquam;
	Stoici scilicet.
	Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt.
	Poterat autem inpune;
	Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est?
	Quare attende, quaeso.
	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.




