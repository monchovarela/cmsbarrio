Title: Fritillaria glauca Greene
Description: Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.
Image: https://source.unsplash.com/random/?ruby
Date: 12/2/2021
Keywords: design
Category: Voltsillam
Author: Oyondu
Tags: portfolio
Published: true
Background: #6231bd
Color: #41ae76
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Quid, si reviviscant Platonis illi et deinceps qui eorum auditores fuerunt, et tecum ita loquantur? Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Nescio quo modo praetervolavit oratio. Sed quia studebat laudi et dignitati, multum in virtute processerat. Videsne quam sit magna dissensio? 


	Quod enim dissolutum sit, id esse sine sensu, quod autem sine sensu sit, id nihil ad nos pertinere omnino.



Duo Reges: constructio interrete. Obsecro, inquit, Torquate, haec dicit Epicurus? Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Sedulo, inquam, faciam. Invidiosum nomen est, infame, suspectum. Cur id non ita fit? Huius ego nunc auctoritatem sequens idem faciam. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Sed quod proximum fuit non vidit. Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. 

Tum Torquatus: Prorsus, inquit, assentior; Sed fortuna fortis; Nosti, credo, illud: Nemo pius est, qui pietatem-; Omnes enim iucundum motum, quo sensus hilaretur. 


	Stoicos roga.
	Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri.
	Primum divisit ineleganter;
	Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia.
	Explanetur igitur.
	Duo enim genera quae erant, fecit tria.
	Age sane, inquam.
	Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat.
	Nos commodius agimus.
	Multoque hoc melius nos veriusque quam Stoici.



Sit hoc ultimum bonorum, quod nunc a me defenditur; Quae similitudo in genere etiam humano apparet. Nunc vides, quid faciat. Quod praeceptum quia maius erat, quam ut ab homine videretur, idcirco assignatum est deo. His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. Tamen a proposito, inquam, aberramus. 


	Quae cum magnifice primo dici viderentur, considerata minus probabantur.
	Itaque si aut requietem natura non quaereret aut eam posset alia quadam ratione consequi.
	Videmus in quodam volucrium genere non nulla indicia pietatis, cognitionem, memoriam, in multis etiam desideria videmus.
	Quo modo autem optimum, si bonum praeterea nullum est?
	At eum nihili facit;




	At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit;
	Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur?
	Quamquam tu hanc copiosiorem etiam soles dicere.
	Ita est quoddam commune officium sapientis et insipientis, ex quo efficitur versari in iis, quae media dicamus.
	Sed virtutem ipsam inchoavit, nihil amplius.



Nam Pyrrho, Aristo, Erillus iam diu abiecti. Quam si explicavisset, non tam haesitaret. Nunc omni virtuti vitium contrario nomine opponitur. Non potes, nisi retexueris illa. Hoc enim constituto in philosophia constituta sunt omnia. 


	Eam stabilem appellas.
	Nosti, credo, illud: Nemo pius est, qui pietatem-;
	Videsne, ut haec concinant?
	Est autem officium, quod ita factum est, ut eius facti probabilis ratio reddi possit.
	Quis enim redargueret?
	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Idemne, quod iucunde?
	Egone quaeris, inquit, quid sentiam?
	Idem adhuc;
	Facit enim ille duo seiuncta ultima bonorum, quae ut essent vera, coniungi debuerunt;



Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Si quicquam extra virtutem habeatur in bonis. Suo genere perveniant ad extremum; Esse enim, nisi eris, non potes. Quam ob rem tandem, inquit, non satisfacit? Sed haec nihil sane ad rem; Hoc est non dividere, sed frangere. Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q. Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Quod autem ratione actum est, id officium appellamus. 


Haec mirabilia videri intellego, sed cum certe superiora
firma ac vera sint, his autem ea consentanea et
consequentia, ne de horum quidem est veritate dubitandum.

Sit hoc ultimum bonorum, quod nunc a me defenditur;



Teneo, inquit, finem illi videri nihil dolere. Ille enim occurrentia nescio quae comminiscebatur; Eam stabilem appellas. Iam id ipsum absurdum, maximum malum neglegi. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Si enim ad populum me vocas, eum. An haec ab eo non dicuntur? Et quod est munus, quod opus sapientiae? Universa enim illorum ratione cum tota vestra confligendum puto. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? 


	Brutus dissentiet quod et acutum genus est et ad usus civium non inutile, nosque ea scripta reliquaque eiusdem generis et legimus libenter et legemus, haec, quae vitam omnem continent, neglegentur?




Quocirca eodem modo sapiens erit affectus erga amicum, quo
in se ipsum, quosque labores propter suam voluptatem
susciperet, eosdem suscipiet propter amici voluptatem.

Ea, quae dialectici nunc tradunt et docent, nonne ab illis
instituta sunt aut inventa sunt?



Maximus dolor, inquit, brevis est. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Respondent extrema primis, media utrisque, omnia omnibus. Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest. Facit enim ille duo seiuncta ultima bonorum, quae ut essent vera, coniungi debuerunt; Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; 

Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor; Ratio quidem vestra sic cogit. Utilitatis causa amicitia est quaesita. Iam id ipsum absurdum, maximum malum neglegi. 

Sed haec in pueris; Beatus autem esse in maximarum rerum timore nemo potest. Recte, inquit, intellegis. Erat enim res aperta. Qui est in parvis malis. Nondum autem explanatum satis, erat, quid maxime natura vellet. 


	Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex.
	Tum mihi Piso: Quid ergo?
	Verum hoc idem saepe faciamus.




	Ne in odium veniam, si amicum destitero tueri.
	Nam memini etiam quae nolo, oblivisci non possum quae volo.
	Eam tum adesse, cum dolor omnis absit;
	Si longus, levis;
	Quodsi ipsam honestatem undique pertectam atque absolutam.
	Dolere malum est: in crucem qui agitur, beatus esse non potest.




