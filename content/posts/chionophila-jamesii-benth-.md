Title: Chionophila jamesii Benth.
Description: Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.
Image: https://source.unsplash.com/random/?cat
Date: 9/7/2021
Keywords: portfolio
Category: It
Author: Kwimbee
Tags: work
Published: true
Background: #5fa986
Color: #0a4001
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quonam, inquit, modo? Duarum enim vitarum nobis erunt instituta capienda. Proclivi currit oratio. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Nec enim, omnes avaritias si aeque avaritias esse dixerimus, sequetur ut etiam aequas esse dicamus. 

Quo igitur, inquit, modo? An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quid ergo attinet gloriose loqui, nisi constanter loquare? Indicant pueri, in quibus ut in speculis natura cernitur. Quid iudicant sensus? Illa argumenta propria videamus, cur omnia sint paria peccata. Scripta sane et multa et polita, sed nescio quo pacto auctoritatem oratio non habet. Tibi hoc incredibile, quod beatissimum. 


	Quonam, inquit, modo?
	Sed ego in hoc resisto;
	Sedulo, inquam, faciam.
	Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti.
	Certe non potest.
	Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est.
	Sullae consulatum?
	Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile.




	Cum audissem Antiochum, Brute, ut solebam, cum M.
	In qua si nihil est praeter rationem, sit in una virtute finis bonorum;
	Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam.
	Addidisti ad extremum etiam indoctum fuisse.
	Sed ad illum redeo.




Fadio Gallo, cuius in testamento scriptum esset se ab eo
rogatum ut omnis hereditas ad filiam perveniret.

Nam e quibus locis quasi thesauris argumenta depromerentur,
vestri ne suspicati quidem sunt, superiores autem artificio
et via tradiderunt.



Recte dicis; Aeque enim contingit omnibus fidibus, ut incontentae sint. Ergo id est convenienter naturae vivere, a natura discedere. Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. 

Eaedem res maneant alio modo. Ergo hoc quidem apparet, nos ad agendum esse natos. Laboro autem non sine causa; Si enim ita est, vide ne facinus facias, cum mori suadeas. Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum. Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat. Quae duo sunt, unum facit. Quis negat? Sin aliud quid voles, postea. Quare hoc videndum est, possitne nobis hoc ratio philosophorum dare. 


	Quae sequuntur igitur?
	Quid turpius quam sapientis vitam ex insipientium sermone pendere?
	Istic sum, inquit.
	Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam.



Multoque hoc melius nos veriusque quam Stoici. Duo Reges: constructio interrete. Est, ut dicis, inquit; Propter nos enim illam, non propter eam nosmet ipsos diligimus. Quod iam a me expectare noli. Quae duo sunt, unum facit. Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est. Si quicquam extra virtutem habeatur in bonis. Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit. Bonum liberi: misera orbitas. Negare non possum. 

Haec quo modo conveniant, non sane intellego. Quid autem habent admirationis, cum prope accesseris? Ex rebus enim timiditas, non ex vocabulis nascitur. Praeclarae mortes sunt imperatoriae; Inquit, respondet: Quia, nisi quod honestum est, nullum est aliud bonum! Non quaero iam verumne sit; Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Color egregius, integra valitudo, summa gratia, vita denique conferta voluptatum omnium varietate. Nam quid possumus facere melius? 


Ista ipsa, quae tu breviter: regem, dictatorem, divitem
solum esse sapientem, a te quidem apte ac rotunde;

Teneo, inquit, finem illi videri nihil dolere.



Ac tamen hic mallet non dolere. Theophrasti igitur, inquit, tibi liber ille placet de beata vita? Teneo, inquit, finem illi videri nihil dolere. Respondeat totidem verbis. Haec dicuntur inconstantissime. Qualem igitur hominem natura inchoavit? At enim, qua in vita est aliquid mali, ea beata esse non potest. Dolere malum est: in crucem qui agitur, beatus esse non potest. 


	Videamus animi partes, quarum est conspectus illustrior;
	At modo dixeras nihil in istis rebus esse, quod interesset.
	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?
	Bona autem corporis huic sunt, quod posterius posui, similiora.
	Immo sit sane nihil melius, inquam-nondum enim id quaero-, num propterea idem voluptas est, quod, ut ita dicam, indolentia?
	Itaque sensibus rationem adiunxit et ratione effecta sensus non reliquit.




	Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit;
	Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret.
	Quicquid porro animo cernimus, id omne oritur a sensibus;
	Quamquam id quidem licebit iis existimare, qui legerint.
	In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret.
	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.




	Quis est tam dissimile homini.




	Concinnus deinde et elegans huius, Aristo, sed ea, quae desideratur, a magno philosopho, gravitas, in eo non fuit;



Quod ea non occurrentia fingunt, vincunt Aristonem; Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur. Ego vero isti, inquam, permitto. Istam voluptatem, inquit, Epicurus ignorat? Philosophi autem in suis lectulis plerumque moriuntur. Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur. At hoc in eo M. At cum de plurimis eadem dicit, tum certe de maximis. 

Tubulo putas dicere? Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis. Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Illis videtur, qui illud non dubitant bonum dicere -; Illud dico, ea, quae dicat, praeclare inter se cohaerere. Respondent extrema primis, media utrisque, omnia omnibus. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? 


	Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?



Primum cur ista res digna odio est, nisi quod est turpis? Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Aut, Pylades cum sis, dices te esse Orestem, ut moriare pro amico? Uterque enim summo bono fruitur, id est voluptate. Etiam beatissimum? Dat enim intervalla et relaxat. Iam id ipsum absurdum, maximum malum neglegi. Quodsi, ne quo incommodo afficiare, non relinques amicum, tamen, ne sine fructu alligatus sis, ut moriatur optabis. 


