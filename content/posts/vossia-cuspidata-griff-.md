Title: Vossia cuspidata Griff.
Description: Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.
Image: https://source.unsplash.com/random/?ruby
Date: 22/1/2021
Keywords: article
Category: Y-find
Author: Reallinks
Tags: web
Published: true
Background: #9543c6
Color: #489f09
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Audeo dicere, inquit. Apparet statim, quae sint officia, quae actiones. Praeclarae mortes sunt imperatoriae; Duo Reges: constructio interrete. Sed ad haec, nisi molestum est, habeo quae velim. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? 

Odium autem et invidiam facile vitabis. Immo alio genere; Minime vero istorum quidem, inquit. Ita enim vivunt quidam, ut eorum vita refellatur oratio. Hoc ne statuam quidem dicturam pater aiebat, si loqui posset. 


Non enim, quod facit in frugibus, ut, cum ad spicam
perduxerit ab herba, relinquat et pro nihilo habeat herbam,
idem facit in homine, cum eum ad rationis habitum perduxit.

Superiores tres erant, quae esse possent, quarum est una
sola defensa, eaque vehementer.




	Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere.
	Hoc ipsum elegantius poni meliusque potuit.
	Suo enim quisque studio maxime ducitur.
	Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium.



Efficiens dici potest. Vitae autem degendae ratio maxime quidem illis placuit quieta. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Quodsi ipsam honestatem undique pertectam atque absolutam. Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat; Quae tamen a te agetur non melior, quam illae sunt, quas interdum optines. 


Sic est igitur locutus: Quantus ornatus in Peripateticorum
disciplina sit satis est a me, ut brevissime potuit, paulo
ante dictum.

Hic ego: Pomponius quidem, inquam, noster iocari videtur, et
fortasse suo iure.



Qui potest igitur habitare in beata vita summi mali metus? Compensabatur, inquit, cum summis doloribus laetitia. Quid enim de amicitia statueris utilitatis causa expetenda vides. Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Satisne ergo pudori consulat, si quis sine teste libidini pareat? Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Quonam, inquit, modo? Ut aliquid scire se gaudeant? 


	Scrupulum, inquam, abeunti;
	At vero illa, quae Peripatetici, quae Stoici dicunt, semper tibi in ore sunt in iudiciis, in senatu.
	Quid de Pythagora?
	Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest.




	Ergo in gubernando nihil, in officio plurimum interest, quo in genere peccetur.
	Itaque his sapiens semper vacabit.
	Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt.
	Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q.



Aliter enim explicari, quod quaeritur, non potest. Quid autem habent admirationis, cum prope accesseris? An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? Verum hoc idem saepe faciamus. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Quam ob rem tandem, inquit, non satisfacit? 

Nos paucis ad haec additis finem faciamus aliquando; Istam voluptatem, inquit, Epicurus ignorat? Falli igitur possumus. Terram, mihi crede, ea lanx et maria deprimet. Quod autem in homine praestantissimum atque optimum est, id deseruit. 

Stoici scilicet. Sine ea igitur iucunde negat posse se vivere? Si enim ad populum me vocas, eum. Sed quanta sit alias, nunc tantum possitne esse tanta. Dempta enim aeternitate nihilo beatior Iuppiter quam Epicurus; Itaque dicunt nec dubitant: mihi sic usus est, tibi ut opus est facto, fac. 


	Certe non potest.
	Nisi autem rerum natura perspecta erit, nullo modo poterimus sensuum iudicia defendere.
	Hic ambiguo ludimur.
	Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus;
	Sullae consulatum?
	Nam illud quidem adduci vix possum, ut ea, quae senserit ille, tibi non vera videantur.
	Erat enim Polemonis.
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?
	Nos commodius agimus.
	Ita ne hoc quidem modo paria peccata sunt.



Quam multa vitiosa! summum enim bonum et malum vagiens puer utra voluptate diiudicabit, stante an movente? Sed hoc sane concedamus. Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Neque solum ea communia, verum etiam paria esse dixerunt. Non laboro, inquit, de nomine. Estne, quaeso, inquam, sitienti in bibendo voluptas? Respondent extrema primis, media utrisque, omnia omnibus. 


	Quasi vero hoc didicisset a Zenone, non dolere, cum doleret! Illud audierat nec tamen didicerat, malum illud non esse, quia turpe non esset, et esse ferendum viro.



Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Aeque enim contingit omnibus fidibus, ut incontentae sint. Tu quidem reddes; Laboro autem non sine causa; Habent enim et bene longam et satis litigiosam disputationem. 


	At vero Epicurus una in domo, et ea quidem angusta, quam magnos quantaque amoris conspiratione consentientis tenuit amicorum greges! quod fit etiam nunc ab Epicureis.




	Hunc vos beatum;
	Propter nos enim illam, non propter eam nosmet ipsos diligimus.
	Quis enim redargueret?
	Ita fit cum gravior, tum etiam splendidior oratio.
	An quod ita callida est, ut optime possit architectari voluptates?
	Tenesne igitur, inquam, Hieronymus Rhodius quid dicat esse summum bonum, quo putet omnia referri oportere?




	Ista ipsa, quae tu breviter: regem, dictatorem, divitem solum esse sapientem, a te quidem apte ac rotunde;
	Quo studio Aristophanem putamus aetatem in litteris duxisse?
	Huius ego nunc auctoritatem sequens idem faciam.



Mihi enim satis est, ipsis non satis. Si enim ita est, vide ne facinus facias, cum mori suadeas. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; Sed tamen intellego quid velit. Sed ille, ut dixi, vitiose. Et quidem, inquit, vehementer errat; Velut ego nunc moveor. 


