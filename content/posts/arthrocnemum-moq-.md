Title: Arthrocnemum Moq.
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?ruby
Date: 1/10/2021
Keywords: design
Category: Solarbreeze
Author: Tagopia
Tags: article
Published: true
Background: #53ef90
Color: #008742
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mihi, inquam, qui te id ipsum rogavi? Age sane, inquam. Istic sum, inquit. Quid de Platone aut de Democrito loquar? Pauca mutat vel plura sane; Quam ob rem tandem, inquit, non satisfacit? Duo Reges: constructio interrete. Quae in controversiam veniunt, de iis, si placet, disseramus. 

Amicitiae vero locus ubi esse potest aut quis amicus esse cuiquam, quem non ipsum amet propter ipsum? Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat. Nam memini etiam quae nolo, oblivisci non possum quae volo. Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Obsecro, inquit, Torquate, haec dicit Epicurus? ALIO MODO. Primum divisit ineleganter; Re mihi non aeque satisfacit, et quidem locis pluribus. 


	Constituto autem illo, de quo ante diximus, quod honestum esset, id esse solum bonum, intellegi necesse est pluris id, quod honestum sit, aestimandum esse quam illa media, quae ex eo comparentur.



Cur deinde Metrodori liberos commendas? Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Si id dicis, vicimus. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. 


	Portenta haec esse dicit, neque ea ratione ullo modo posse vivi;
	Theophrasti igitur, inquit, tibi liber ille placet de beata vita?
	Maximus dolor, inquit, brevis est.
	Sine ea igitur iucunde negat posse se vivere?




	Ita prorsus, inquam;
	Tecum optime, deinde etiam cum mediocri amico.
	Optime, inquam.
	Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur.
	Tubulo putas dicere?
	Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret?
	Magna laus.
	Ita cum ea volunt retinere, quae superiori sententiae conveniunt, in Aristonem incidunt;
	Negare non possum.
	Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus;



Inde igitur, inquit, ordiendum est. Sit sane ista voluptas. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Utrum igitur tibi litteram videor an totas paginas commovere? 

Sic enim maiores nostri labores non fugiendos tristissimo tamen verbo aerumnas etiam in deo nominaverunt. Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? Bonum valitudo: miser morbus. Quamquam tu hanc copiosiorem etiam soles dicere. Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur. At enim hic etiam dolore. Suam denique cuique naturam esse ad vivendum ducem. Cur id non ita fit? 


	Utrum igitur tibi litteram videor an totas paginas commovere?
	Vulgo enim dicitur: Iucundi acti labores, nec male Euripidesconcludam, si potero, Latine;
	Addidisti ad extremum etiam indoctum fuisse.
	Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim?
	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.
	Deinde qui fit, ut ego nesciam, sciant omnes, quicumque Epicurei esse voluerunt?



Bonum liberi: misera orbitas. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Transfer idem ad modestiam vel temperantiam, quae est moderatio cupiditatum rationi oboediens. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Nam quid possumus facere melius? Nec mihi illud dixeris: Haec enim ipsa mihi sunt voluptati, et erant illa Torquatis. 


	In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret.
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?
	Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat.
	Mihi enim erit isdem istis fortasse iam utendum.




	Quam multa vero iniuste fieri possunt, quae nemo possit reprehendere! si te amicus tuus moriens rogaverit, ut hereditatem reddas suae filiae, nec usquam id scripserit, ut scripsit Fadius, nec cuiquam dixerit, quid facies?




	Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit;
	Quod etsi ingeniis magnis praediti quidam dicendi copiam sine ratione consequuntur, ars tamen est dux certior quam natura.
	Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est;



Age, inquies, ista parva sunt. Theophrastum tamen adhibeamus ad pleraque, dum modo plus in virtute teneamus, quam ille tenuit, firmitatis et roboris. Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Sed potestne rerum maior esse dissensio? 

Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Aufert enim sensus actionemque tollit omnem. Hoc est non dividere, sed frangere. Magna laus. Ita multa dicunt, quae vix intellegam. Quid de Pythagora? 


Cum efficere non possit ut cuiquam, qui ipse sibi notus sit,
hoc est qui suam naturam sensumque perspexerit, vacuitas
doloris et voluptas idem esse videatur.

Ita est quoddam commune officium sapientis et insipientis,
ex quo efficitur versari in iis, quae media dicamus.




	Hunc vos beatum;
	Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus.
	Sed nimis multa.
	Voluptatem cum summum bonum diceret, primum in eo ipso parum vidit, deinde hoc quoque alienum;
	Moriatur, inquit.
	Cur fortior sit, si illud, quod tute concedis, asperum et vix ferendum putabit?
	Praeclare hoc quidem.
	Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus;




Tum enim eam ipsam vis, quam modo ego dixi, et nomen
inponis, in motu ut sit et faciat aliquam varietatem, tum
aliam quandam summam voluptatem, quo addi nihil possit;

Is ita vivebat, ut nulla tam exquisita posset inveniri
voluptas, qua non abundaret.



Bonum negas esse divitias, praeposėtum esse dicis? Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Tum Triarius: Posthac quidem, inquit, audacius. Quae cum magnifice primo dici viderentur, considerata minus probabantur. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat. Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Non est igitur voluptas bonum. Nam ista vestra: Si gravis, brevis; 

Quid enim de amicitia statueris utilitatis causa expetenda vides. De vacuitate doloris eadem sententia erit. Sequitur disserendi ratio cognitioque naturae; 


