Title: Miconia foveolata Cogn.
Description: Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.
Image: https://source.unsplash.com/random/?minimal
Date: 8/4/2021
Keywords: work
Category: Quo Lux
Author: Wordify
Tags: cms
Published: true
Background: #281c74
Color: #a22278
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Ita prorsus, inquam; Duo Reges: constructio interrete. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Sedulo, inquam, faciam. Ita multo sanguine profuso in laetitia et in victoria est mortuus. Quae duo sunt, unum facit. Cur, nisi quod turpis oratio est? 


Nam ceteris in rebus s�ve praetermissum sive ignoratum est
quippiam, non plus incommodi est, quam quanti quaeque earum
rerum est, in quibus neglectum est aliqu�d.

Cupit enim d�cere nihil posse ad beatam vitam deesse
sapienti.




	Nec vero ut voluptatem expetat, natura movet infantem, sed tantum ut se ipse diligat, ut integrum se salvumque velit.



Ego vero isti, inquam, permitto. Videamus igitur sententias eorum, tum ad verba redeamus. Quid ad utilitatem tantae pecuniae? Hoc mihi cum tuo fratre convenit. Mihi, inquam, qui te id ipsum rogavi? Praeclare hoc quidem. Primum cur ista res digna odio est, nisi quod est turpis? Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. 

Quis est, qui non oderit libidinosam, protervam adolescentiam? De quibus cupio scire quid sentias. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Sic enim censent, oportunitatis esse beate vivere. Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere. Nosti, credo, illud: Nemo pius est, qui pietatem-; Quamquam haec quidem praeposita recte et reiecta dicere licebit. Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? Quasi ego id curem, quid ille aiat aut neget. Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. 


	Sed non alienum est, quo facilius vis verbi intellegatur, rationem huius verbi faciendi Zenonis exponere.




	Non igitur de improbo, sed de callido improbo quaerimus, qualis Q.
	Est enim tanti philosophi tamque nobilis audacter sua decreta defendere.
	Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia.
	Quo modo autem philosophus loquitur?



Claudii libidini, qui tum erat summo ne imperio, dederetur. Sin laboramus, quis est, qui alienae modum statuat industriae? Sin ea non neglegemus neque tamen ad finem summi boni referemus, non multum ab Erilli levitate aberrabimus. Memini me adesse P. Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Duo enim genera quae erant, fecit tria. Et non ex maxima parte de tota iudicabis? Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus. Neque solum ea communia, verum etiam paria esse dixerunt. 


	Quare conare, quaeso.
	Quid paulo ante, inquit, dixerim nonne meministi, cum omnis dolor detractus esset, variari, non augeri voluptatem?




In omni enim arte vel studio vel quavis scientia vel in ipsa
virtute optimum quidque rarissimum est.

Quare attendo te studiose et, quaecumque rebus iis, de
quibus hic sermo est, nomina inponis, memoriae mando;




	Nihil sane.
	Sedulo, inquam, faciam.
	Certe non potest.
	Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex.
	Memini vero, inquam;
	De illis, cum volemus.
	Sint ista Graecorum;
	Suo genere perveniant ad extremum;



Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. Partim cursu et peragratione laetantur, congregatione aliae coetum quodam modo civitatis imitantur; Sed quanta sit alias, nunc tantum possitne esse tanta. Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D. Illa argumenta propria videamus, cur omnia sint paria peccata. Nam, ut sint illa vendibiliora, haec uberiora certe sunt. 

Comprehensum, quod cognitum non habet? Sint ista Graecorum; Hoc loco tenere se Triarius non potuit. Beatus sibi videtur esse moriens. 


	Pudebit te, inquam, illius tabulae, quam Cleanthes sane commode verbis depingere solebat.
	Hoc unum Aristo tenuit: praeter vitia atque virtutes negavit rem esse ullam aut fugiendam aut expetendam.
	Quod autem meum munus dicis non equidem recuso, sed te adiungo socium.



Contineo me ab exemplis. Nam, ut paulo ante docui, augendae voluptatis finis est doloris omnis amotio. Videamus animi partes, quarum est conspectus illustrior; Et nemo nimium beatus est; Quae quidem sapientes sequuntur duce natura tamquam videntes; Universa enim illorum ratione cum tota vestra confligendum puto. Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. 

Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Id Sextilius factum negabat. Aliena dixit in physicis nec ea ipsa, quae tibi probarentur; Cur post Tarentum ad Archytam? 

Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. Ex rebus enim timiditas, non ex vocabulis nascitur. Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam esse oportebit; Minime vero, inquit ille, consentit. 


	Tenent mordicus.
	Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse;
	Non semper, inquam;
	Qui autem de summo bono dissentit de tota philosophiae ratione dissentit.
	Audeo dicere, inquit.
	Ergo id est convenienter naturae vivere, a natura discedere.
	Istic sum, inquit.
	Hic nihil fuit, quod quaereremus.



Ergo, si semel tristior effectus est, hilara vita amissa est? Primum in nostrane potestate est, quid meminerimus? Memini me adesse P. Hoc est dicere: Non reprehenderem asotos, si non essent asoti. Sed virtutem ipsam inchoavit, nihil amplius. Consequens enim est et post oritur, ut dixi. Res enim concurrent contrariae. Quis istud possit, inquit, negare? Ut optime, secundum naturam affectum esse possit. Poterat autem inpune; 


	Isto modo ne improbos quidem, si essent boni viri.
	Satisne igitur videor vim verborum tenere, an sum etiam nunc vel Graece loqui vel Latine docendus?
	Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur.
	Non enim solum Torquatus dixit quid sentiret, sed etiam cur.
	Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit.




