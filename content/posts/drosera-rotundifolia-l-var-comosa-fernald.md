Title: Drosera rotundifolia L. var. comosa Fernald
Description: Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.
Image: https://source.unsplash.com/random/?css
Date: 4/7/2021
Keywords: web
Category: Prodder
Author: Cogidoo
Tags: post
Published: true
Background: #1545db
Color: #ea60aa
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Sed ego in hoc resisto; Nihil enim iam habes, quod ad corpus referas; Sed plane dicit quod intellegit. Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Duo Reges: constructio interrete. In eo enim positum est id, quod dicimus esse expetendum. 

Age, inquies, ista parva sunt. Videamus animi partes, quarum est conspectus illustrior; Et homini, qui ceteris animantibus plurimum praestat, praecipue a natura nihil datum esse dicemus? At ego quem huic anteponam non audeo dicere; Sed ne, dum huic obsequor, vobis molestus sim. Nemo nostrum istius generis asotos iucunde putat vivere. Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. 


	Quis, quaeso, illum negat et bonum virum et comem et humanum fuisse?
	Quae quidem sapientes sequuntur duce natura tamquam videntes;
	Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus.



Quo tandem modo? Bonum integritas corporis: misera debilitas. An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Satis est ad hoc responsum. Et quod est munus, quod opus sapientiae? Quod vestri non item. Haeret in salebra. Satisne ergo pudori consulat, si quis sine teste libidini pareat? Sed virtutem ipsam inchoavit, nihil amplius. 

Sed tamen est aliquid, quod nobis non liceat, liceat illis. Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat. Itaque hic ipse iam pridem est reiectus; 


	Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile.



Tum ille timide vel potius verecunde: Facio, inquit. Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. Ita graviter et severe voluptatem secrevit a bono. Cur igitur, cum de re conveniat, non malumus usitate loqui? Est enim effectrix multarum et magnarum voluptatum. Et harum quidem rerum facilis est et expedita distinctio. 


	Quid iudicant sensus?
	Qua ex cognitione facilior facta est investigatio rerum occultissimarum.
	Nunc vides, quid faciat.
	Quid enim de amicitia statueris utilitatis causa expetenda vides.




In qua si nihil est praeter rationem, sit in una virtute
finis bonorum;

Vide igitur ne non debeas verbis nostris uti, sententiis
tuis.



Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Comprehensum, quod cognitum non habet? Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Immo videri fortasse. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Hoc est non dividere, sed frangere. Aliter enim explicari, quod quaeritur, non potest. Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est; Non autem hoc: igitur ne illud quidem. 

De malis autem et bonis ab iis animalibus, quae nondum depravata sint, ait optime iudicari. A mene tu? Vitiosum est enim in dividendo partem in genere numerare. Sed quid sentiat, non videtis. Age nunc isti doceant, vel tu potius quis enim ista melius? Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus; 


	Ita credo.
	Iam doloris medicamenta illa Epicurea tamquam de narthecio proment: Si gravis, brevis;
	Non igitur bene.
	Aeque enim contingit omnibus fidibus, ut incontentae sint.
	Stoicos roga.
	Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit?
	Quonam modo?
	Utrum igitur tibi non placet, inquit, virtutisne tantam esse vim, ut ad beate vivendum se ipsa contenta sit?




	Omne enim animal, simul et ortum est, se ipsum et omnes partes suas diligit duasque, quae maximae sunt, in primis amplectitur, animum et corpus, deinde utriusque partes.




Ista ipsa, quae tu breviter: regem, dictatorem, divitem
solum esse sapientem, a te quidem apte ac rotunde;

Transfer idem ad modestiam vel temperantiam, quae est
moderatio cupiditatum rationi oboediens.



Illa tamen simplicia, vestra versuta. Quorum sine causa fieri nihil putandum est. Hoc sic expositum dissimile est superiori. Stoici scilicet. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Bonum negas esse divitias, praepos�tum esse dicis? 


	Quis Aristidem non mortuum diligit?
	Vadem te ad mortem tyranno dabis pro amico, ut Pythagoreus ille Siculo fecit tyranno?
	Stulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant.
	Sit ista in Graecorum levitate perversitas, qui maledictis insectantur eos, a quibus de veritate dissentiunt.




	Quid ad utilitatem tantae pecuniae?
	Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem.
	At negat Epicurus-hoc enim vestrum lumen estquemquam, qui honeste non vivat, iucunde posse vivere.
	Ea, quae dialectici nunc tradunt et docent, nonne ab illis instituta sunt aut inventa sunt?



At enim, qua in vita est aliquid mali, ea beata esse non potest. Quae si potest singula consolando levare, universa quo modo sustinebit? Itaque hic ipse iam pridem est reiectus; Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest. Illi enim inter se dissentiunt. 

Inquit, dasne adolescenti veniam? Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat. Re mihi non aeque satisfacit, et quidem locis pluribus. Primum quid tu dicis breve? Qui ita affectus, beatum esse numquam probabis; Haec para/doca illi, nos admirabilia dicamus. Omnes enim iucundum motum, quo sensus hilaretur. Inde sermone vario sex illa a Dipylo stadia confecimus. Tum Quintus: Est plane, Piso, ut dicis, inquit. Itaque ab his ordiamur. Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Scrupulum, inquam, abeunti; 


	Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur.
	Satis est tibi in te, satis in legibus, satis in mediocribus amicitiis praesidii.




