Title: Atriplex nuttallii S. Watson
Description: Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.
Image: https://source.unsplash.com/random/?night
Date: 29/3/2021
Keywords: post
Category: Aerified
Author: Yata
Tags: blog
Published: true
Background: #74df29
Color: #a86be2
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non igitur bene. Neque solum ea communia, verum etiam paria esse dixerunt. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Duo Reges: constructio interrete. Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur. Quid est enim aliud esse versutum? Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Et quod est munus, quod opus sapientiae? Quorum altera prosunt, nocent altera. Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. 


	Ergo in iis adolescentibus bonam spem esse dicemus et magnam indolem, quos suis commodis inservituros et quicquid ipsis expediat facturos arbitrabimur?



Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum. Immo alio genere; Iam id ipsum absurdum, maximum malum neglegi. Mihi enim satis est, ipsis non satis. Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat. Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis. Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri. 

Quamquam tu hanc copiosiorem etiam soles dicere. Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Ne discipulum abducam, times. Non potes, nisi retexueris illa. Ergo hoc quidem apparet, nos ad agendum esse natos. Collatio igitur ista te nihil iuvat. 


	Restincta enim sitis stabilitatem voluptatis habet, inquit, illa autem voluptas ipsius restinctionis in motu est.
	Maximus dolor, inquit, brevis est.
	Tecum optime, deinde etiam cum mediocri amico.
	Mihi enim erit isdem istis fortasse iam utendum.
	Re mihi non aeque satisfacit, et quidem locis pluribus.
	Quia nec honesto quic quam honestius nec turpi turpius.



Idem adhuc; Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Urgent tamen et nihil remittunt. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem. Vestri haec verecundius, illi fortasse constantius. Primum divisit ineleganter; 


	Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur.
	Quodsi, ne quo incommodo afficiare, non relinques amicum, tamen, ne sine fructu alligatus sis, ut moriatur optabis.
	Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur?
	Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret.




	Quae sequuntur igitur?
	Quid ergo hoc loco intellegit honestum?
	Quid me istud rogas?
	Sed vos squalidius, illorum vides quam niteat oratio.
	Equidem e Cn.
	Quae diligentissime contra Aristonem dicuntur a Chryippo.
	In schola desinis.
	Minime vero istorum quidem, inquit.
	Efficiens dici potest.
	Ut non sine causa ex iis memoriae ducta sit disciplina.
	Si quae forte-possumus.
	Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;




	Atque adhuc ea dixi, causa cur Zenoni non fuisset, quam ob rem a superiorum auctoritate discederet.




Haec et tu ita posuisti, et verba vestra sunt.

Cum autem venissemus in Academiae non sine causa nobilitata
spatia, solitudo erat ea, quam volueramus.




	Quid vero?
	Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit.
	Venit ad extremum;
	Theophrastus mediocriterne delectat, cum tractat locos ab Aristotele ante tractatos?
	Beatum, inquit.
	Satisne vobis videor pro meo iure in vestris auribus commentatus?
	Quis negat?
	Teneo, inquit, finem illi videri nihil dolere.



Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Nunc omni virtuti vitium contrario nomine opponitur. Sin aliud quid voles, postea. Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat; Certe nihil nisi quod possit ipsum propter se iure laudari. In schola desinis. 

Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Hoc loco discipulos quaerere videtur, ut, qui asoti esse velint, philosophi ante fiant. Hoc loco tenere se Triarius non potuit. Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Miserum hominem! Si dolor summum malum est, dici aliter non potest. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. At multis se probavit. Profectus in exilium Tubulus statim nec respondere ausus; Nihil illinc huc pervenit. Vitiosum est enim in dividendo partem in genere numerare. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore. 

Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest. Sed haec omittamus; An potest cupiditas finiri? Cur iustitia laudatur? Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Uterque enim summo bono fruitur, id est voluptate. Est autem a te semper dictum nec gaudere quemquam nisi propter corpus nec dolere. Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? 

Facile est hoc cernere in primis puerorum aetatulis. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Mihi enim satis est, ipsis non satis. Negare non possum. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Dici enim nihil potest verius. 

Nihil illinc huc pervenit. Ergo ita: non posse honeste vivi, nisi honeste vivatur? Sin laboramus, quis est, qui alienae modum statuat industriae? Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. Facete M. Solum praeterea formosum, solum liberum, solum civem, stultost; Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Tu vero, inquam, ducas licet, si sequetur; Suo genere perveniant ad extremum; 


In omni enim animante est summum aliquid atque optimum, ut
in equis, in canibus, quibus tamen et dolore vacare opus est
et valere;

Nam cum in Graeco sermone haec ipsa quondam rerum nomina
novarum * * non videbantur, quae nunc consuetudo diuturna
trivit;




	Illo enim addito iuste fit recte factum, per se autem hoc ipsum reddere in officio ponitur.
	Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere.




	Quod quidem iam fit etiam in Academia.
	Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere.
	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Ego vero volo in virtute vim esse quam maximam;
	Dicet pro me ipsa virtus nec dubitabit isti vestro beato M.
	A primo, ut opinor, animantium ortu petitur origo summi boni.
	Quid, quod res alia tota est?



Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Suo enim quisque studio maxime ducitur. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Huius ego nunc auctoritatem sequens idem faciam. Quis Aristidem non mortuum diligit? Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Quis enim redargueret? Satisne ergo pudori consulat, si quis sine teste libidini pareat? Immo alio genere; 


