Title: Beilschmiedia Nees
Description: Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.
Image: https://source.unsplash.com/random/?design
Date: 7/7/2021
Keywords: design
Category: Sonsing
Author: Abata
Tags: post
Published: true
Background: #bd0cf9
Color: #4ec3ce
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mihi enim satis est, ipsis non satis. Hoc est non modo cor non habere, sed ne palatum quidem. Occultum facinus esse potuerit, gaudebit; Nam si propter voluptatem, quae est ista laus, quae possit e macello peti? In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Non est ista, inquam, Piso, magna dissensio. Itaque dicunt nec dubitant: mihi sic usus est, tibi ut opus est facto, fac. Duo Reges: constructio interrete. 


Ea denique omni vita, quae in una virtute consisteret, illam
vitam, quae etiam ceteris rebus, quae essent secundum
naturam, abundaret, magis expetendam non esse, sed magis
sumendam.

Et hunc idem dico, inquieta sed ad virtutes et ad vitia
nihil interesse.



Videamus animi partes, quarum est conspectus illustrior; Cur deinde Metrodori liberos commendas? Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri. Sed quid ages tandem, si utilitas ab amicitia, ut fit saepe, defecerit? Primum divisit ineleganter; Paria sunt igitur. Age, inquies, ista parva sunt. Quis negat? Quid ad utilitatem tantae pecuniae? 

Negare non possum. Sed ad illum redeo. Illud dico, ea, quae dicat, praeclare inter se cohaerere. Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus. Qui ita affectus, beatum esse numquam probabis; Satisne vobis videor pro meo iure in vestris auribus commentatus? 

Polycratem Samium felicem appellabant. Addidisti ad extremum etiam indoctum fuisse. Post enim Chrysippum eum non sane est disputatum. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. 


	Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;
	Illis videtur, qui illud non dubitant bonum dicere -;



At ille pellit, qui permulcet sensum voluptate. Qui ita affectus, beatum esse numquam probabis; Sic consequentibus vestris sublatis prima tolluntur. Ut proverbia non nulla veriora sint quam vestra dogmata. Is es profecto tu. Vitae autem degendae ratio maxime quidem illis placuit quieta. Sed nimis multa. Etiam beatissimum? 


	In schola desinis.
	Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster?
	Frater et T.
	Fortasse id optimum, sed ubi illud: Plus semper voluptatis?
	Non igitur bene.
	Qui cum praetor quaestionem inter sicarios exercuisset, ita aperte cepit pecunias ob rem iudicandam, ut anno proximo P.
	Tu quidem reddes;
	Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia.




	Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant.
	Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret.
	Expectoque quid ad id, quod quaerebam, respondeas.
	Istam voluptatem perpetuam quis potest praestare sapienti?




	Perge porro;
	Quid enim necesse est, tamquam meretricem in matronarum coetum, sic voluptatem in virtutum concilium adducere?
	Peccata paria.
	Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego;
	Moriatur, inquit.
	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.
	Tria genera bonorum;
	Quid, si etiam iucunda memoria est praeteritorum malorum?
	Easdemne res?
	Aufidio, praetorio, erudito homine, oculis capto, saepe audiebam, cum se lucis magis quam utilitatis desiderio moveri diceret.




Tum ego: Non mehercule, inquam, soleo temere contra Stoicos,
non quo illis admodum assentiar, sed pudore impedior;

Itaque illa non dico me expetere, sed legere, nec optare,
sed sumere, contraria autem non fugere, sed quasi secernere.



Consequens enim est et post oritur, ut dixi. Tecum optime, deinde etiam cum mediocri amico. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Sic enim censent, oportunitatis esse beate vivere. Nulla erit controversia. 


	Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum.



Aliter homines, aliter philosophos loqui putas oportere? Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere. Non autem hoc: igitur ne illud quidem. Videsne quam sit magna dissensio? Prodest, inquit, mihi eo esse animo. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? 


	Pudebit te, inquam, illius tabulae, quam Cleanthes sane commode verbis depingere solebat.
	Quod cum ita sit, perspicuum est omnis rectas res atque laudabilis eo referri, ut cum voluptate vivatur.
	In qua quid est boni praeter summam voluptatem, et eam sempiternam?
	Claudii libidini, qui tum erat summo ne imperio, dederetur.
	Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.
	Quo modo autem philosophus loquitur?



Hoc loco discipulos quaerere videtur, ut, qui asoti esse velint, philosophi ante fiant. Videamus animi partes, quarum est conspectus illustrior; Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos pertinere? Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Praeterea sublata cognitione et scientia tollitur omnis ratio et vitae degendae et rerum gerendarum. Ea possunt paria non esse. Cenasti in vita numquam bene, cum omnia in ista Consumis squilla atque acupensere cum decimano. Antiquorum autem sententiam Antiochus noster mihi videtur persequi diligentissime, quam eandem Aristoteli fuisse et Polemonis docet. Quicquid porro animo cernimus, id omne oritur a sensibus; 


	Num igitur dubium est, quin, si in re ipsa nihil peccatur a superioribus, verbis illi commodius utantur?



Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. Si quicquam extra virtutem habeatur in bonis. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. Teneo, inquit, finem illi videri nihil dolere. At hoc in eo M. Utram tandem linguam nescio? 


	Urgent tamen et nihil remittunt.
	Quis istud, quaeso, nesciebat?
	Illud urgueam, non intellegere eum quid sibi dicendum sit, cum dolorem summum malum esse dixerit.
	Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis;



Sic enim censent, oportunitatis esse beate vivere. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Putabam equidem satis, inquit, me dixisse. Quid ad utilitatem tantae pecuniae? Isto modo, ne si avia quidem eius nata non esset. In motu et in statu corporis nihil inest, quod animadvertendum esse ipsa natura iudicet? Huius, Lyco, oratione locuples, rebus ipsis ielunior. 


