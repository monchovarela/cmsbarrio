Title: Gayophytum ramosissimum Torr. & A. Gray
Description: Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.
Image: https://source.unsplash.com/random/?blue
Date: 29/11/2020
Keywords: website
Category: Holdlamis
Author: Twitterworks
Tags: blog
Published: true
Background: #c872fb
Color: #de9b87
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis istud possit, inquit, negare? Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum. Haec quo modo conveniant, non sane intellego. Duo Reges: constructio interrete. An eum locum libenter invisit, ubi Demosthenes et Aeschines inter se decertare soliti sunt? Nosti, credo, illud: Nemo pius est, qui pietatem-; Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Cur deinde Metrodori liberos commendas? Suo enim quisque studio maxime ducitur. 


	Immo alio genere;
	Omnes enim iucundum motum, quo sensus hilaretur.
	Sed nimis multa.
	Rapior illuc, revocat autem Antiochus, nec est praeterea, quem audiamus.
	Quid adiuvas?
	Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit;
	Sed nimis multa.
	In eo autem voluptas omnium Latine loquentium more ponitur, cum percipitur ea, quae sensum aliquem moveat, iucunditas.
	Sed fortuna fortis;
	Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera.




Quod quoniam in quo sit magna dissensio est, Carneadea nobis
adhibenda divisio est, qua noster Antiochus libenter uti
solet.

Beatus autem esse in maximarum rerum timore nemo potest.




	Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus.
	Sed tamen omne, quod de re bona dilucide dicitur, mihi praeclare dici videtur.




	Sed cum, quod honestum sit, id solum bonum esse dicamus, consentaneum tamen est fungi officio, cum id officium nec in bonis ponamus nec in malis.



Ratio quidem vestra sic cogit. Videamus animi partes, quarum est conspectus illustrior; Nihilo magis. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Dic in quovis conventu te omnia facere, ne doleas. 


	Pollicetur certe.
	Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum.
	Nulla erit controversia.
	Quaeque de virtutibus dicta sunt, quem ad modum eae semper voluptatibus inhaererent, eadem de amicitia dicenda sunt.
	Quo tandem modo?
	Te enim iudicem aequum puto, modo quae dicat ille bene noris.
	Easdemne res?
	Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus?
	Poterat autem inpune;
	Atque ita re simpliciter primo collocata reliqua subtilius persequentes corporis bona facilem quandam rationem habere censebant;
	Nihil sane.
	Sed virtutem ipsam inchoavit, nihil amplius.




	At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit;
	Age nunc isti doceant, vel tu potius quis enim ista melius?
	Non enim in selectione virtus ponenda erat, ut id ipsum, quod erat bonorum ultimum, aliud aliquid adquireret.



Quorum sine causa fieri nihil putandum est. Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere. Qui autem esse poteris, nisi te amor ipse ceperit? Quae diligentissime contra Aristonem dicuntur a Chryippo. Egone quaeris, inquit, quid sentiam? Faceres tu quidem, Torquate, haec omnia; Et non ex maxima parte de tota iudicabis? Maximas vero virtutes iacere omnis necesse est voluptate dominante. Sed non sunt in eo genere tantae commoditates corporis tamque productae temporibus tamque multae. 


	Potius inflammat, ut coercendi magis quam dedocendi esse videantur.
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Res enim se praeclare habebat, et quidem in utraque parte.
	Qui est in parvis malis.



Non quam nostram quidem, inquit Pomponius iocans; Atque ut ceteri dicere existimantur melius quam facere, sic hi mihi videntur facere melius quam dicere. An hoc usque quaque, aliter in vita? Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Universa enim illorum ratione cum tota vestra confligendum puto. Illud non continuo, ut aeque incontentae. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. 

Idem iste, inquam, de voluptate quid sentit? De maximma autem re eodem modo, divina mente atque natura mundum universum et eius maxima partis administrari. Quo modo autem optimum, si bonum praeterea nullum est? Quae hic rei publicae vulnera inponebat, eadem ille sanabat. An tu me de L. Esse enim quam vellet iniquus iustus poterat inpune. Non prorsus, inquit, omnisque, qui sine dolore sint, in voluptate, et ea quidem summa, esse dico. Quid nunc honeste dicit? Quam illa ardentis amores excitaret sui! Cur tandem? Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. 


	Nam voluptatis causa facere omnia, cum, etiamsi nihil consequamur, tamen ipsum illud consilium ita faciendi per se expetendum et honestum et solum bonum sit, nemo dixit.



Quamvis enim depravatae non sint, pravae tamen esse possunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Eadem fortitudinis ratio reperietur. Quis non odit sordidos, vanos, leves, futtiles? Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Haec dicuntur inconstantissime. 

Quid est, quod ab ea absolvi et perfici debeat? Negat enim summo bono afferre incrementum diem. Sed haec quidem liberius ab eo dicuntur et saepius. Prioris generis est docilitas, memoria; Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere. Praeclarae mortes sunt imperatoriae; 

Idem fecisset Epicurus, si sententiam hanc, quae nunc Hieronymi est, coniunxisset cum Aristippi vetere sententia. Idem iste, inquam, de voluptate quid sentit? Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest. Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Nosti, credo, illud: Nemo pius est, qui pietatem-; Mihi, inquam, qui te id ipsum rogavi? 

Quonam modo? An eiusdem modi? Tria genera bonorum; Sed haec omittamus; Tu vero, inquam, ducas licet, si sequetur; Et quod est munus, quod opus sapientiae? Sin laboramus, quis est, qui alienae modum statuat industriae? Suam denique cuique naturam esse ad vivendum ducem. Quis non odit sordidos, vanos, leves, futtiles? Quid iudicant sensus? 

Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Scrupulum, inquam, abeunti; Hoc est non dividere, sed frangere. Suo genere perveniant ad extremum; Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Quis non odit sordidos, vanos, leves, futtiles? Nihil opus est exemplis hoc facere longius. Ita multa dicunt, quae vix intellegam. Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. Urgent tamen et nihil remittunt. 


Sed utrum hortandus es nobis, Luci, inquit, an etiam tua
sponte propensus es?

Somnum denique nobis, nisi requietem corporibus et is
medicinam quandam laboris afferret, contra naturam putaremus
datum;




	Esse enim, nisi eris, non potes.
	Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat.
	In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt.
	Suam denique cuique naturam esse ad vivendum ducem.




