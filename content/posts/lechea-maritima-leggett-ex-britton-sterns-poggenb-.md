Title: Lechea maritima Leggett ex Britton, Sterns & Poggenb.
Description: Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.
Image: https://source.unsplash.com/random/?cat
Date: 18/7/2021
Keywords: cms
Category: Tres-Zap
Author: Latz
Tags: cms
Published: true
Background: #57f50c
Color: #28b51b
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus. Itaque in rebus minime obscuris non multus est apud eos disserendi labor. Non dolere, inquam, istud quam vim habeat postea videro; 


	Ergo et avarus erit, sed finite, et adulter, verum habebit modum, et luxuriosus eodem modo.



Nunc vides, quid faciat. Ut id aliis narrare gestiant? Illa videamus, quae a te de amicitia dicta sunt. Sed quot homines, tot sententiae; 

Urgent tamen et nihil remittunt. In schola desinis. Quamquam tu hanc copiosiorem etiam soles dicere. Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. Cui Tubuli nomen odio non est? An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? At iam decimum annum in spelunca iacet. Ego vero isti, inquam, permitto. 


Istud quidem, inquam, optime dicis, sed quaero nonne tibi
faciendum idem sit nihil dicenti bonum, quod non rectum
honestumque sit, reliquarum rerum discrimen omne tollenti.

Animi enim quoque dolores percipiet omnibus partibus maiores
quam corporis.



At enim sequor utilitatem. Graece donan, Latine voluptatem vocant. Videsne, ut haec concinant? Quae quo sunt excelsiores, eo dant clariora indicia naturae. Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur. Sine ea igitur iucunde negat posse se vivere? Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? At coluit ipse amicitias. 


	Ratio ista, quam defendis, praecepta, quae didicisti, quae probas, funditus evertunt amicitiam, quamvis eam Epicurus, ut facit, in caelum efferat laudibus.



Egone quaeris, inquit, quid sentiam? Duo Reges: constructio interrete. Ne vitationem quidem doloris ipsam per se quisquam in rebus expetendis putavit, nisi etiam evitare posset. Sed ad bona praeterita redeamus. Hoc loco tenere se Triarius non potuit. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? 

Cur iustitia laudatur? Iam id ipsum absurdum, maximum malum neglegi. Illum mallem levares, quo optimum atque humanissimum virum, Cn. Servari enim iustitia nisi a forti viro, nisi a sapiente non potest. Solum praeterea formosum, solum liberum, solum civem, stultost; Egone quaeris, inquit, quid sentiam? Profectus in exilium Tubulus statim nec respondere ausus; Idemne, quod iucunde? Aperiendum est igitur, quid sit voluptas; Audeo dicere, inquit. 


	Cum autem in quo sapienter dicimus, id a primo rectissime dicitur.
	Transfer idem ad modestiam vel temperantiam, quae est moderatio cupiditatum rationi oboediens.
	Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere.
	Sit hoc ultimum bonorum, quod nunc a me defenditur;



Sint modo partes vitae beatae. Nemo igitur esse beatus potest. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Et ille ridens: Video, inquit, quid agas; Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Stoici scilicet. 

Quae sunt igitur communia vobis cum antiquis, iis sic utamur quasi concessis; Age nunc isti doceant, vel tu potius quis enim ista melius? Quae ista amicitia est? Ait enim se, si uratur, Quam hoc suave! dicturum. Urgent tamen et nihil remittunt. Quid, quod res alia tota est? Quis non odit sordidos, vanos, leves, futtiles? Nihil opus est exemplis hoc facere longius. Nihil opus est exemplis hoc facere longius. Et hercule-fatendum est enim, quod sentio -mirabilis est apud illos contextus rerum. Ergo infelix una molestia, fellx rursus, cum is ipse anulus in praecordiis piscis inventus est? Omnes enim iucundum motum, quo sensus hilaretur. 


Peccata autem partim esse tolerabilia, partim nullo modo,
propterea quod alia peccata plures, alia pauciores quasi
numeros officii praeterirent.

Nihil est enim, de quo aliter tu sentias atque ego, modo
commutatis verbis ipsas res conferamus.



Facillimum id quidem est, inquam. Haec dicuntur inconstantissime. Quod autem principium officii quaerunt, melius quam Pyrrho; Non quam nostram quidem, inquit Pomponius iocans; Quod ea non occurrentia fingunt, vincunt Aristonem; Teneo, inquit, finem illi videri nihil dolere. 

Quis non odit sordidos, vanos, leves, futtiles? Vides igitur, si amicitiam sua caritate metiare, nihil esse praestantius, sin emolumento, summas familiaritates praediorum fructuosorum mercede superari. Idem adhuc; 


	-delector enim, quamquam te non possum, ut ais, corrumpere, delector, inquam, et familia vestra et nomine.
	Aliter enim nosmet ipsos nosse non possumus.
	Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus.
	Animum autem reliquis rebus ita perfecit, ut corpus;
	Longum est enim ad omnia respondere, quae a te dicta sunt.
	Qui ita affectus, beatum esse numquam probabis;




	Si ista mala sunt, in quae potest incidere sapiens, sapientem esse non esse ad beate vivendum satis.
	Negat enim summo bono afferre incrementum diem.
	Quamquam id quidem, infinitum est in hac urbe;
	Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.




	Proclivi currit oratio.
	Scrupulum, inquam, abeunti;
	Audeo dicere, inquit.
	Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis.
	Quid enim?
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?
	Quid vero?
	Portenta haec esse dicit, neque ea ratione ullo modo posse vivi;




	Et quod est munus, quod opus sapientiae?
	Quid de Platone aut de Democrito loquar?
	Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris.
	Ut pulsi recurrant?
	Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali.
	Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet;




	Summae mihi videtur inscitiae.
	Sed tamen omne, quod de re bona dilucide dicitur, mihi praeclare dici videtur.
	Avaritiamne minuis?
	Tu autem negas fortem esse quemquam posse, qui dolorem malum putet.
	Beatum, inquit.
	Ita relinquet duas, de quibus etiam atque etiam consideret.
	Ita credo.
	At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit;




