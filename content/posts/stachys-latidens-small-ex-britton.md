Title: Stachys latidens Small ex Britton
Description: Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.
Image: https://source.unsplash.com/random/?php
Date: 15/9/2021
Keywords: work
Category: Zoolab
Author: Yoveo
Tags: website
Published: true
Background: #534f25
Color: #de3499
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Paria sunt igitur. Pauca mutat vel plura sane; Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Duo Reges: constructio interrete. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem illam Aristippi in prima commendatione poneret. Te enim iudicem aequum puto, modo quae dicat ille bene noris. Itaque ab his ordiamur. Vide, quantum, inquam, fallare, Torquate. Non enim solum Torquatus dixit quid sentiret, sed etiam cur. Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Tum mihi Piso: Quid ergo? 


	Sic, quod est extremum omnium appetendorum atque ductum a prima commendatione naturae, multis gradibus adscendit, ut ad summum perveniret, quod cumulatur ex integritate corporis et ex mentis ratione perfecta.



Dolere malum est: in crucem qui agitur, beatus esse non potest. Prodest, inquit, mihi eo esse animo. Est, ut dicis, inquam. Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. Quae cum dixisset, finem ille. 

Beatus autem esse in maximarum rerum timore nemo potest. Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt. Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. Scaevolam M. 

Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Illa tamen simplicia, vestra versuta. Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur. 

Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Antiquorum autem sententiam Antiochus noster mihi videtur persequi diligentissime, quam eandem Aristoteli fuisse et Polemonis docet. Non pugnem cum homine, cur tantum habeat in natura boni; Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? 


	Peccata paria.
	Ita graviter et severe voluptatem secrevit a bono.
	Poterat autem inpune;
	Hoc est non modo cor non habere, sed ne palatum quidem.
	Quo tandem modo?
	Ergo id est convenienter naturae vivere, a natura discedere.
	Nihil sane.
	Qui autem esse poteris, nisi te amor ipse ceperit?



Nunc haec primum fortasse audientis servire debemus. Paria sunt igitur. Aliter homines, aliter philosophos loqui putas oportere? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Quid enim possumus hoc agere divinius? In qua si nihil est praeter rationem, sit in una virtute finis bonorum; Tum Quintus: Est plane, Piso, ut dicis, inquit. Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest. 


	Corporis autem voluptas si etiam praeterita delectat, non intellego, cur Aristoteles Sardanapalli epigramma tantopere derideat, in quo ille rex Syriae glorietur se omnis secum libidinum voluptates abstulisse.



Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Satis est ad hoc responsum. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Neutrum vero, inquit ille. Odium autem et invidiam facile vitabis. At eum nihili facit; Quis non odit sordidos, vanos, leves, futtiles? Ut id aliis narrare gestiant? 


	Primum divisit ineleganter;
	Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex.
	Quare attende, quaeso.
	Istam voluptatem, inquit, Epicurus ignorat?
	Ita credo.
	Itaque hic ipse iam pridem est reiectus;



Hoc est non modo cor non habere, sed ne palatum quidem. Collatio igitur ista te nihil iuvat. Quid autem habent admirationis, cum prope accesseris? Quis tibi ergo istud dabit praeter Pyrrhonem, Aristonem eorumve similes, quos tu non probas? Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest. 


	Quippe: habes enim a rhetoribus;
	Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest.
	Atqui reperies, inquit, in hoc quidem pertinacem;
	Materiam vero rerum et copiam apud hos exilem, apud illos uberrimam reperiemus.




Sic exclusis sententiis reliquorum cum praeterea nulla esse
possit, haec antiquorum valeat necesse est.

Equidem in omnibus istis conclusionibus hoc putarem
philosophia nobisque dignum, et maxime, cum summum bonum
quaereremus, vitam nostram, consilia, voluntates, non verba
corrigi.




	Et non ex maxima parte de tota iudicabis?
	Itaque et vivere vitem et mori dicimus arboremque et novellan et vetulam et vigere et senescere.
	Primum Theophrasti, Strato, physicum se voluit;
	Atque ab isto capite fluere necesse est omnem rationem bonorum et malorum.




Ergo opifex plus sibi proponet ad formarum quam civis
excellens ad factorum pulchritudinem?

Itaque nostrum est-quod nostrum dico, artis est-ad ea
principia, quae accepimus.




	Putabam equidem satis, inquit, me dixisse.
	Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere.
	Si longus, levis dictata sunt.



Quamquam tu hanc copiosiorem etiam soles dicere. Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere. Duarum enim vitarum nobis erunt instituta capienda. Quamquam tu hanc copiosiorem etiam soles dicere. Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Minime vero, inquit ille, consentit. Nam illud quidem adduci vix possum, ut ea, quae senserit ille, tibi non vera videantur. Quid censes in Latino fore? Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Pollicetur certe. 


	Quid turpius quam sapientis vitam ex insipientium sermone pendere?
	Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur.
	Quodsi ipsam honestatem undique pertectam atque absolutam.
	An, partus ancillae sitne in fructu habendus, disseretur inter principes civitatis, P.
	A primo, ut opinor, animantium ortu petitur origo summi boni.



Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Vitae autem degendae ratio maxime quidem illis placuit quieta. Etenim semper illud extra est, quod arte comprehenditur. Quod si ita se habeat, non possit beatam praestare vitam sapientia. Expectoque quid ad id, quod quaerebam, respondeas. Sed potestne rerum maior esse dissensio? Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. Quod quidem nobis non saepe contingit. Haec para/doca illi, nos admirabilia dicamus. 


