Title: Eucalyptus gillii Maiden
Description: Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.
Image: https://source.unsplash.com/random/?yellow
Date: 17/6/2021
Keywords: post
Category: Konklux
Author: Shuffletag
Tags: blog
Published: true
Background: #ae754b
Color: #1f2ac6
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Iam in altera philosophiae parte. Egone non intellego, quid sit don Graece, Latine voluptas? Tamen a proposito, inquam, aberramus. Morbo gravissimo affectus, exul, orbus, egens, torqueatur eculeo: quem hunc appellas, Zeno? Duo Reges: constructio interrete. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. 

Torquatus, is qui consul cum Cn. Primum Theophrasti, Strato, physicum se voluit; A quibus propter discendi cupiditatem videmus ultimas terras esse peragratas. At tu eadem ista dic in iudicio aut, si coronam times, dic in senatu. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; Igitur neque stultorum quisquam beatus neque sapientium non beatus. At multis malis affectus. Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Quae cum dixisset paulumque institisset, Quid est? Quae hic rei publicae vulnera inponebat, eadem ille sanabat. 

Sin aliud quid voles, postea. At modo dixeras nihil in istis rebus esse, quod interesset. Sed in rebus apertissimis nimium longi sumus. Praeclare hoc quidem. Erat enim Polemonis. An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? Egone quaeris, inquit, quid sentiam? Huius ego nunc auctoritatem sequens idem faciam. Egone non intellego, quid sit don Graece, Latine voluptas? Vestri haec verecundius, illi fortasse constantius. Facit igitur Lucius noster prudenter, qui audire de summo bono potissimum velit; Quod quidem iam fit etiam in Academia. 

Mihi quidem Antiochum, quem audis, satis belle videris attendere. Invidiosum nomen est, infame, suspectum. Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Ita graviter et severe voluptatem secrevit a bono. Nam, ut sint illa vendibiliora, haec uberiora certe sunt. Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Haeret in salebra. Nos commodius agimus. Disserendi artem nullam habuit. 


	Sed hoc summum bonum, quod tertia significatione intellegitur, eaque vita, quae ex summo bono degitur, quia coniuncta ei virtus est.




	Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum.
	Quicquid porro animo cernimus, id omne oritur a sensibus;
	Quo invento omnis ab eo quasi capite de summo bono et malo disputatio ducitur.
	Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus.



Ergo et avarus erit, sed finite, et adulter, verum habebit modum, et luxuriosus eodem modo. Confecta res esset. Nemo nostrum istius generis asotos iucunde putat vivere. Estne, quaeso, inquam, sitienti in bibendo voluptas? Partim cursu et peragratione laetantur, congregatione aliae coetum quodam modo civitatis imitantur; Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Cur haec eadem Democritus? 


	Sed nimis multa.
	Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur?
	Quid de Pythagora?
	Quia nec honesto quic quam honestius nec turpi turpius.
	Idemne, quod iucunde?
	Si longus, levis dictata sunt.
	Scaevolam M.
	Illa tamen simplicia, vestra versuta.




	Hic ambiguo ludimur.
	At eum nihili facit;
	Etiam beatissimum?
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Cyrenaici quidem non recusant;
	Nec enim ignoras his istud honestum non summum modo, sed etiam, ut tu vis, solum bonum videri.




Ipse negat, ut ante dixi, luxuriosorum vitam reprehendendam,
nisi plane fatui sint, id est nisi aut cupiant aut metuant.

Et si turpitudinem fugimus in statu et motu corporis, quid
est cur pulchritudinem non sequamur?



Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Nam diligi et carum esse iucundum est propterea, quia tutiorem vitam et voluptatem pleniorem efficit. Tum Lucius: Mihi vero ista valde probata sunt, quod item fratri puto. Sed ad haec, nisi molestum est, habeo quae velim. Cur igitur, cum de re conveniat, non malumus usitate loqui? Utrum igitur tibi litteram videor an totas paginas commovere? Ac ne plura complectar-sunt enim innumerabilia-, bene laudata virtus voluptatis aditus intercludat necesse est. Verba tu fingas et ea dicas, quae non sentias? 


	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Huius ego nunc auctoritatem sequens idem faciam.
	Multoque hoc melius nos veriusque quam Stoici.
	Nam, ut sint illa vendibiliora, haec uberiora certe sunt.




	Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia;
	Mene ergo et Triarium dignos existimas, apud quos turpiter loquare?
	Ita fit cum gravior, tum etiam splendidior oratio.
	Vitae autem degendae ratio maxime quidem illis placuit quieta.
	Ille vero, si insipiens-quo certe, quoniam tyrannus -, numquam beatus;
	Respondeat totidem verbis.
	At enim sequor utilitatem.



Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Quod autem in homine praestantissimum atque optimum est, id deseruit. Vitae autem degendae ratio maxime quidem illis placuit quieta. Sed quanta sit alias, nunc tantum possitne esse tanta. Satisne ergo pudori consulat, si quis sine teste libidini pareat? At Zeno eum non beatum modo, sed etiam divitem dicere ausus est. 


	Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia?
	Quod autem satis est, eo quicquid accessit, nimium est;
	Verum enim diceret, idque Socratem, qui voluptatem nullo loco numerat, audio dicentem, cibi condimentum esse famem, potionis sitim.




Si vero id etiam explanare velles apertiusque diceres nihil
eum fecisse nisi voluptatis causa, quo modo eum tandem
laturum fuisse existimas?

Hoc est non modo cor non habere, sed ne palatum quidem.



Sumenda potius quam expetenda. Hoc loco tenere se Triarius non potuit. Aufert enim sensus actionemque tollit omnem. Nonne igitur tibi videntur, inquit, mala? Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest. Illud non continuo, ut aeque incontentae. 

Eaedem res maneant alio modo. Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? Re mihi non aeque satisfacit, et quidem locis pluribus. Consequentia exquirere, quoad sit id, quod volumus, effectum. 


	Qui cum cruciaretur non ferendis doloribus, propagabat tamen vitam aucupio, sagittarum ictu configebat tardus celeres, stans volantis, ut apud Accium est, pennarumque contextu corpori tegumenta faciebat.



Apparet statim, quae sint officia, quae actiones. Vide, quantum, inquam, fallare, Torquate. Bonum negas esse divitias, praeposėtum esse dicis? Quid, si non sensus modo ei sit datus, verum etiam animus hominis? Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Eadem nunc mea adversum te oratio est. Huius ego nunc auctoritatem sequens idem faciam. 


