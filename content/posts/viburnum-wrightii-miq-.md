Title: Viburnum wrightii Miq.
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?yellow
Date: 9/4/2021
Keywords: cms
Category: Aerified
Author: Voomm
Tags: post
Published: true
Background: #6fbabe
Color: #7e4c61
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam beatissimum? O magnam vim ingenii causamque iustam, cur nova existeret disciplina! Perge porro. Iam id ipsum absurdum, maximum malum neglegi. Verum hoc idem saepe faciamus. Ea possunt paria non esse. Cum praesertim illa perdiscere ludus esset. Quis hoc dicit? Duo Reges: constructio interrete. Vitae autem degendae ratio maxime quidem illis placuit quieta. Si enim ad populum me vocas, eum. Quos nisi redarguimus, omnis virtus, omne decus, omnis vera laus deserenda est. 

Quae duo sunt, unum facit. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Sumenda potius quam expetenda. Honesta oratio, Socratica, Platonis etiam. Illa tamen simplicia, vestra versuta. Quae autem natura suae primae institutionis oblita est? 

Immo vero, inquit, ad beatissime vivendum parum est, ad beate vero satis. Idem iste, inquam, de voluptate quid sentit? Eaedem enim utilitates poterunt eas labefactare atque pervertere. Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit. Sullae consulatum? 


Et hunc idem dico, inquieta sed ad virtutes et ad vitia
nihil interesse.

Atque ut reliqui fures earum rerum, quas ceperunt, signa
commutant, sic illi, ut sententiis nostris pro suis
uterentur, nomina tamquam rerum notas mutaverunt.




	Nihil minus, contraque illa hereditate dives ob eamque rem laetus.
	Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.
	Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;
	Duae sunt enim res quoque, ne tu verba solum putes.
	Se omnia, quae secundum naturam sint, b o n a appellare, quae autem contra, m a l a.



Non pugnem cum homine, cur tantum habeat in natura boni; Sed quanta sit alias, nunc tantum possitne esse tanta. Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. Moriatur, inquit. Conferam tecum, quam cuique verso rem subicias; Ut optime, secundum naturam affectum esse possit. Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit. Inde sermone vario sex illa a Dipylo stadia confecimus. 

In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret. Ille enim occurrentia nescio quae comminiscebatur; Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Traditur, inquit, ab Epicuro ratio neglegendi doloris. Quid, si etiam iucunda memoria est praeteritorum malorum? Sullae consulatum? Nam quid possumus facere melius? 


	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;
	Hanc ergo intuens debet institutum illud quasi signum absolvere.
	Et ille ridens: Video, inquit, quid agas;
	An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit?
	Sint modo partes vitae beatae.
	At certe gravius.




Quia, cum a Zenone, inquam, hoc magnifice tamquam ex oraculo
editur: Virtus ad beate vivendum se ipsa contenta est, et
Quare?

Itaque ne iustitiam quidem recte quis dixerit per se ipsam
optabilem, sed quia iucunditatis vel plurimum afferat.



Habent enim et bene longam et satis litigiosam disputationem. Graecum enim hunc versum nostis omnes-: Suavis laborum est praeteritorum memoria. Et ille ridens: Video, inquit, quid agas; Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Quam ob rem tandem, inquit, non satisfacit? Expectoque quid ad id, quod quaerebam, respondeas. 


	Paria sunt igitur.
	Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret?
	Equidem e Cn.
	Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.
	Nihilo magis.
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?
	Hic ambiguo ludimur.
	Hoc ne statuam quidem dicturam pater aiebat, si loqui posset.




	Ex quo intellegitur idem illud, solum bonum esse, quod honestum sit, idque esse beate vivere: honeste, id est cum virtute, vivere.




	Idem iste, inquam, de voluptate quid sentit?
	An vero, inquit, quisquam potest probare, quod perceptfum, quod.
	Sensibus enim ornavit ad res percipiendas idoneis, ut nihil aut non multum adiumento ullo ad suam confirmationem indigerent;
	Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam;
	Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere?




	Quacumque enim ingredimur, in aliqua historia vestigium ponimus.
	Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria.
	Numquam hoc ita defendit Epicurus neque Metrodorus aut quisquam eorum, qui aut saperet aliquid aut ista didicisset.



Sunt enim prima elementa naturae, quibus auctis v�rtutis quasi germen efficitur. Hoc est dicere: Non reprehenderem asotos, si non essent asoti. Solum praeterea formosum, solum liberum, solum civem, stultost; Quod autem principium officii quaerunt, melius quam Pyrrho; Primum cur ista res digna odio est, nisi quod est turpis? Ergo infelix una molestia, fellx rursus, cum is ipse anulus in praecordiis piscis inventus est? Miserum hominem! Si dolor summum malum est, dici aliter non potest. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. 

Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. Sed fortuna fortis; Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? Videamus animi partes, quarum est conspectus illustrior; Sed quot homines, tot sententiae; Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? 

Tollenda est atque extrahenda radicitus. Illa videamus, quae a te de amicitia dicta sunt. Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. Memini me adesse P. Graccho, eius fere, aequal�? Vitae autem degendae ratio maxime quidem illis placuit quieta. Haec para/doca illi, nos admirabilia dicamus. 


	Alteri negant quicquam esse bonum, nisi quod honestum sit, alteri plurimum se et longe longeque plurimum tribuere honestati, sed tamen et in corpore et extra esse quaedam bona.



Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Si enim ad populum me vocas, eum. Negat enim summo bono afferre incrementum diem. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. 


	Haeret in salebra.
	Servari enim iustitia nisi a forti viro, nisi a sapiente non potest.
	Avaritiamne minuis?
	Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi;
	Venit ad extremum;
	Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat Epicurus voluptatem.
	Moriatur, inquit.
	Restincta enim sitis stabilitatem voluptatis habet, inquit, illa autem voluptas ipsius restinctionis in motu est.
	Sullae consulatum?
	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;
	Confecta res esset.
	Compensabatur, inquit, cum summis doloribus laetitia.




