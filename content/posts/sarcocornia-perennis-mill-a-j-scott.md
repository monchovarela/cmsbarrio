Title: Sarcocornia perennis (Mill.) A.J. Scott
Description: Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.
Image: https://source.unsplash.com/random/?css
Date: 1/10/2021
Keywords: design
Category: Bitwolf
Author: Abata
Tags: cms
Published: true
Background: #4e18a6
Color: #0d567d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non enim, si malum est dolor, carere eo malo satis est ad bene vivendum. Quid enim de amicitia statueris utilitatis causa expetenda vides. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Quasi ego id curem, quid ille aiat aut neget. Quo igitur, inquit, modo? Dolere malum est: in crucem qui agitur, beatus esse non potest. Duo Reges: constructio interrete. Erat enim Polemonis. 

Oratio me istius philosophi non offendit; Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Illud dico, ea, quae dicat, praeclare inter se cohaerere. At Zeno eum non beatum modo, sed etiam divitem dicere ausus est. Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Scisse enim te quis coarguere possit? 

Bonum valitudo: miser morbus. At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Multoque hoc melius nos veriusque quam Stoici. Collatio igitur ista te nihil iuvat. Scaevolam M. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Hoc sic expositum dissimile est superiori. 

Erat enim Polemonis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Sint ista Graecorum; Sed nonne merninisti licere mihi ista probare, quae sunt a te dicta? 


Si quicquam extra virtutem habeatur in bonis.

Hoc dixerit potius Ennius: Nimium boni est, cui nihil est
mali.



Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat; Consequentia exquirere, quoad sit id, quod volumus, effectum. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Gloriosa ostentatio in constituendo summo bono. Quamquam te quidem video minime esse deterritum. Sed quot homines, tot sententiae; 


	Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur.



Illis videtur, qui illud non dubitant bonum dicere -; Non est enim vitium in oratione solum, sed etiam in moribus. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas? Illud non continuo, ut aeque incontentae. Dolere malum est: in crucem qui agitur, beatus esse non potest. Nam, ut sint illa vendibiliora, haec uberiora certe sunt. Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset. Totum genus hoc Zeno et qui ab eo sunt aut non potuerunt aut noluerunt, certe reliquerunt. 

Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam; Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Nunc agendum est subtilius. Sed plane dicit quod intellegit. Si enim ita est, vide ne facinus facias, cum mori suadeas. Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q. 

Quam ob rem tandem, inquit, non satisfacit? Tecum optime, deinde etiam cum mediocri amico. Tubulo putas dicere? Et nemo nimium beatus est; Cur deinde Metrodori liberos commendas? Peccata paria. Aut unde est hoc contritum vetustate proverbium: quicum in tenebris? Et harum quidem rerum facilis est et expedita distinctio. 


	Estne, quaeso, inquam, sitienti in bibendo voluptas?
	Quid autem habent admirationis, cum prope accesseris?
	Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est.
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;
	Sin laboramus, quis est, qui alienae modum statuat industriae?




Ita fit beatae vitae domina fortuna, quam Epicurus ait
exiguam intervenire sapienti.

Eorum enim omnium multa praetermittentium, dum eligant
aliquid, quod sequantur, quasi curta sententia;




	Quare si potest esse beatus is, qui est in asperis reiciendisque rebus, potest is quoque esse.
	Non enim, si malum est dolor, carere eo malo satis est ad bene vivendum.
	Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus;
	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.
	Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.
	Quid, si etiam iucunda memoria est praeteritorum malorum?




	Ut pulsi recurrant?
	An obliviscimur, quantopere in audiendo in legendoque moveamur, cum pie, cum amice, cum magno animo aliquid factum cognoscimus?
	Non igitur bene.
	Quae autem natura suae primae institutionis oblita est?
	Magna laus.
	Bona autem corporis huic sunt, quod posterius posui, similiora.
	Quis negat?
	De quibus cupio scire quid sentias.
	Quonam modo?
	Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam;
	ALIO MODO.
	Sed quia studebat laudi et dignitati, multum in virtute processerat.
	An eiusdem modi?
	Sed virtutem ipsam inchoavit, nihil amplius.




	Ex rebus enim timiditas, non ex vocabulis nascitur.
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?
	Verum hoc idem saepe faciamus.
	Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur?
	Cur id non ita fit?




	Quo igitur, inquit, modo?
	Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur.
	Quid ergo?
	Uterque enim summo bono fruitur, id est voluptate.
	Sullae consulatum?
	Maximas vero virtutes iacere omnis necesse est voluptate dominante.
	Stoicos roga.
	Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere.
	Quo modo?
	Tubulum fuisse, qua illum, cuius is condemnatus est rogatione, P.
	Eam stabilem appellas.
	Sed potestne rerum maior esse dissensio?
	Sullae consulatum?
	Theophrastum tamen adhibeamus ad pleraque, dum modo plus in virtute teneamus, quam ille tenuit, firmitatis et roboris.




	Omnesque eae sunt genere quattuor, partibus plures, aegritudo, formido, libido, quamque Stoici communi nomine corporis et animi �don�n appellant, ego malo laetitiam appellare, quasi gestientis animi elationem voluptariam.




	Quo plebiscito decreta a senatu est consuli quaestio Cn.
	Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.
	Illud dico, ea, quae dicat, praeclare inter se cohaerere.
	Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem.



Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Quod si ita sit, cur opera philosophiae sit danda nescio. Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit? An vero, inquit, quisquam potest probare, quod perceptfum, quod. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Quod ea non occurrentia fingunt, vincunt Aristonem; Idem iste, inquam, de voluptate quid sentit? Quis, quaeso, illum negat et bonum virum et comem et humanum fuisse? Universa enim illorum ratione cum tota vestra confligendum puto. 

Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. In qua si nihil est praeter rationem, sit in una virtute finis bonorum; Expressa vero in iis aetatibus, quae iam confirmatae sunt. Ne discipulum abducam, times. Compensabatur, inquit, cum summis doloribus laetitia. 


