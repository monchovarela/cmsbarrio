Title: Liriope muscari (Decne.) L.H. Bailey
Description: Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.
Image: https://source.unsplash.com/random/?css
Date: 11/4/2021
Keywords: blog
Category: It
Author: Eidel
Tags: post
Published: true
Background: #54cc18
Color: #1b67ba
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed haec omittamus; Quare hoc videndum est, possitne nobis hoc ratio philosophorum dare. Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Est enim effectrix multarum et magnarum voluptatum. Faceres tu quidem, Torquate, haec omnia; Quod si ita se habeat, non possit beatam praestare vitam sapientia. Videamus animi partes, quarum est conspectus illustrior; Duo Reges: constructio interrete. Servari enim iustitia nisi a forti viro, nisi a sapiente non potest. 

Age sane, inquam. Somnum denique nobis, nisi requietem corporibus et is medicinam quandam laboris afferret, contra naturam putaremus datum; Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Non igitur bene. Quam illa ardentis amores excitaret sui! Cur tandem? Erat enim Polemonis. Nam de isto magna dissensio est. Itaque hic ipse iam pridem est reiectus; 


	Est enim aliquid in his rebus probabile, et quidem ita, ut eius ratio reddi possit, ergo ut etiam probabiliter acti ratio reddi possit.




	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?
	Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis?
	Sed est forma eius disciplinae, sicut fere ceterarum, triplex: una pars est naturae, disserendi altera, vivendi tertia.
	Hoc enim constituto in philosophia constituta sunt omnia.




	Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur.
	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;
	Qui autem diffidet perpetuitati bonorum suorum, timeat necesse est, ne aliquando amissis illis sit miser.
	De hominibus dici non necesse est.
	Miserum hominem! Si dolor summum malum est, dici aliter non potest.



Summum a vobis bonum voluptas dicitur. Bonum integritas corporis: misera debilitas. Teneo, inquit, finem illi videri nihil dolere. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Quod ea non occurrentia fingunt, vincunt Aristonem; Id est enim, de quo quaerimus. Dat enim intervalla et relaxat. Nulla erit controversia. Id mihi magnum videtur. Et nemo nimium beatus est; 

Facillimum id quidem est, inquam. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Neque solum ea communia, verum etiam paria esse dixerunt. Ego vero volo in virtute vim esse quam maximam; Nec vero hoc oratione solum, sed multo magis vita et factis et moribus comprobavit. Quid autem habent admirationis, cum prope accesseris? Quae similitudo in genere etiam humano apparet. Quid est enim aliud esse versutum? 


	Proclivi currit oratio.
	Suam denique cuique naturam esse ad vivendum ducem.
	Praeclare hoc quidem.
	Apud ceteros autem philosophos, qui quaesivit aliquid, tacet;
	Venit ad extremum;
	Ratio enim nostra consentit, pugnat oratio.
	An eiusdem modi?
	Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur.
	Scaevolam M.
	Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret?
	Quid de Pythagora?
	Est autem officium, quod ita factum est, ut eius facti probabilis ratio reddi possit.




	Itaque hic ipse iam pridem est reiectus;
	Quod non faceret, si in voluptate summum bonum poneret.
	In qua si nihil est praeter rationem, sit in una virtute finis bonorum;
	Si verbum sequimur, primum longius verbum praepositum quam bonum.



Facillimum id quidem est, inquam. Ut id aliis narrare gestiant? Quis Aristidem non mortuum diligit? Tum ille timide vel potius verecunde: Facio, inquit. Neutrum vero, inquit ille. Eiuro, inquit adridens, iniquum, hac quidem de re; 


	Itaque prima illa commendatio, quae a natura nostri facta est nobis, incerta et obscura est, primusque appetitus ille animi tantum agit, ut salvi atque integri esse possimus.



Quem ad modum quis ambulet, sedeat, qui ductus oris, qui vultus in quoque sit? Sed tamen est aliquid, quod nobis non liceat, liceat illis. Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Quod quidem iam fit etiam in Academia. Ad eas enim res ab Epicuro praecepta dantur. Quid est igitur, inquit, quod requiras? 

Facit enim ille duo seiuncta ultima bonorum, quae ut essent vera, coniungi debuerunt; Illa videamus, quae a te de amicitia dicta sunt. Mihi enim satis est, ipsis non satis. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Quod si ita se habeat, non possit beatam praestare vitam sapientia. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Quod non faceret, si in voluptate summum bonum poneret. Pugnant Stoici cum Peripateticis. Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; 

Itaque rursus eadem ratione, qua sum paulo ante usus, haerebitis. Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt; Tum ille timide vel potius verecunde: Facio, inquit. Ac ne plura complectar-sunt enim innumerabilia-, bene laudata virtus voluptatis aditus intercludat necesse est. Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? Faceres tu quidem, Torquate, haec omnia; 

Itaque a sapientia praecipitur se ipsam, si usus sit, sapiens ut relinquat. Stoici autem, quod finem bonorum in una virtute ponunt, similes sunt illorum; Idemne potest esse dies saepius, qui semel fuit? Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Primum quid tu dicis breve? 


Sed non alienum est, quo facilius vis verbi intellegatur,
rationem huius verbi faciendi Zenonis exponere.

Nihil opus est exemplis hoc facere longius.




	Scrupulum, inquam, abeunti;
	Primum cur ista res digna odio est, nisi quod est turpis?
	Quibusnam praeteritis?
	Quid enim dicis omne animal, simul atque sit ortum, applicatum esse ad se diligendum esseque in se conservando occupatum?




	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.
	Quamquam tu hanc copiosiorem etiam soles dicere.
	Quid sequatur, quid repugnet, vident.
	Immo alio genere;
	Itaque hic ipse iam pridem est reiectus;
	Ita multo sanguine profuso in laetitia et in victoria est mortuus.




Quid, si non sensus modo ei sit datus, verum etiam animus
hominis?

Tu autem, si tibi illa probabantur, cur non propriis verbis
ea tenebas?



Ea possunt paria non esse. Nosti, credo, illud: Nemo pius est, qui pietatem-; Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Sed residamus, inquit, si placet. Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Nos autem non solum beatae vitae istam esse oblectationem videmus, sed etiam levamentum miseriarum. Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Est enim effectrix multarum et magnarum voluptatum. 


