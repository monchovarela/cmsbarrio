Title: Isocoma acradenia (Greene) Greene
Description: Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.
Image: https://source.unsplash.com/random/?iphone
Date: 24/2/2021
Keywords: design
Category: Fintone
Author: Photobug
Tags: website
Published: true
Background: #addf32
Color: #32a18d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Memini me adesse P. Quis non odit sordidos, vanos, leves, futtiles? Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? Duo Reges: constructio interrete. Duo enim genera quae erant, fecit tria. Velut ego nunc moveor. Non autem hoc: igitur ne illud quidem. Torquatus, is qui consul cum Cn. Ubi ut eam caperet aut quando? Cupiditates non Epicuri divisione finiebat, sed sua satietate. 

Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; Quis negat? Quid, si etiam iucunda memoria est praeteritorum malorum? Si quidem, inquit, tollerem, sed relinquo. Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. 


	Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus.
	Primum in nostrane potestate est, quid meminerimus?
	Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis.
	Sine ea igitur iucunde negat posse se vivere?
	Quod autem meum munus dicis non equidem recuso, sed te adiungo socium.
	Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem illam Aristippi in prima commendatione poneret.




	Ecce aliud simile dissimile.
	Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium.
	Multoque hoc melius nos veriusque quam Stoici.



Me igitur ipsum ames oportet, non mea, si veri amici futuri sumus. Ut id aliis narrare gestiant? Igitur neque stultorum quisquam beatus neque sapientium non beatus. Mihi quidem Antiochum, quem audis, satis belle videris attendere. Nunc omni virtuti vitium contrario nomine opponitur. Urgent tamen et nihil remittunt. Quo studio Aristophanem putamus aetatem in litteris duxisse? 


	Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius.
	Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus;
	Universa enim illorum ratione cum tota vestra confligendum puto.
	An me, inquam, nisi te audire vellem, censes haec dicturum fuisse?
	Primum quid tu dicis breve?



Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Sed fortuna fortis; Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. 

Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Non est igitur voluptas bonum. Tu autem, si tibi illa probabantur, cur non propriis verbis ea tenebas? Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere. 


	Restincta enim sitis stabilitatem voluptatis habet, inquit, illa autem voluptas ipsius restinctionis in motu est.
	Amicitiam autem adhibendam esse censent, quia sit ex eo genere, quae prosunt.
	Atque his de rebus et splendida est eorum et illustris oratio.
	Quid, cum volumus nomina eorum, qui quid gesserint, nota nobis esse, parentes, patriam, multa praeterea minime necessaria?
	Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere.
	Duae sunt enim res quoque, ne tu verba solum putes.




	Qui convenit?
	Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest.
	Sed ad rem redeamus;
	Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus.
	Magna laus.
	Etenim si delectamur, cum scribimus, quis est tam invidus, qui ab eo nos abducat?
	Sullae consulatum?
	Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur.




Quod, inquit, quamquam voluptatibus quibusdam est saepe
iucundius, tamen expetitur propter voluptatem.

Ego vero volo in virtute vim esse quam maximam;




Physicae quoque non sine causa tributus idem est honos,
propterea quod, qui convenienter naturae victurus sit, ei
proficiscendum est ab omni mundo atque ab eius procuratione.

Sunt enim quasi prima elementa naturae, quibus ubertas
orationis adhiberi vix potest, nec equidem eam cogito
consectari.



Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Itaque hic ipse iam pridem est reiectus; Bonum integritas corporis: misera debilitas. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer. 

Philosophi autem in suis lectulis plerumque moriuntur. Quam tu ponis in verbis, ego positam in re putabam. Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur. Primum Theophrasti, Strato, physicum se voluit; Beatus autem esse in maximarum rerum timore nemo potest. Optime, inquam. Non est ista, inquam, Piso, magna dissensio. Primum cur ista res digna odio est, nisi quod est turpis? Partim cursu et peragratione laetantur, congregatione aliae coetum quodam modo civitatis imitantur; Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? Profectus in exilium Tubulus statim nec respondere ausus; 

Tria genera bonorum; Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet; Non enim iam stirpis bonum quaeret, sed animalis. Nos commodius agimus. Ratio enim nostra consentit, pugnat oratio. Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Laboro autem non sine causa; Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Quod autem satis est, eo quicquid accessit, nimium est; Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. 

Quamquam te quidem video minime esse deterritum. Facile pateremur, qui etiam nunc agendi aliquid discendique causa prope contra naturam v�gillas suscipere soleamus. Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. Haec para/doca illi, nos admirabilia dicamus. Qua tu etiam inprudens utebare non numquam. Tollenda est atque extrahenda radicitus. 

Homines optimi non intellegunt totam rationem everti, si ita res se habeat. Bona autem corporis huic sunt, quod posterius posui, similiora. Nihilo beatiorem esse Metellum quam Regulum. Id est enim, de quo quaerimus. Erat enim Polemonis. Nulla erit controversia. 


	Audeo dicere, inquit.
	Multoque hoc melius nos veriusque quam Stoici.
	Primum divisit ineleganter;
	Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit?
	Tu quidem reddes;
	Sin laboramus, quis est, qui alienae modum statuat industriae?
	Quid de Pythagora?
	Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere.




	Sed nonne merninisti licere mihi ista probare, quae sunt a te dicta?




	Cognitis autem rerum finibus, cum intellegitur, quid sit et bonorum extremum et malorum, inventa vitae via est conformatioque omnium officiorum, cum quaeritur, quo quodque referatur;




