Title: Gaura coccinea Nutt. ex Pursh
Description: Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.
Image: https://source.unsplash.com/random/?ruby
Date: 11/10/2021
Keywords: design
Category: Cookley
Author: Pixope
Tags: post
Published: true
Background: #3c14c5
Color: #ab4459
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis istud possit, inquit, negare? Bonum patria: miserum exilium. Duo Reges: constructio interrete. Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Omnia contraria, quos etiam insanos esse vultis. Quae cum dixisset, finem ille. An hoc usque quaque, aliter in vita? Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Omnia contraria, quos etiam insanos esse vultis. 


	Easdemne res?
	Ego vero isti, inquam, permitto.
	Quis negat?
	Quod autem satis est, eo quicquid accessit, nimium est;
	Non igitur bene.
	Quae quo sunt excelsiores, eo dant clariora indicia naturae.
	Frater et T.
	Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q.



Nam de isto magna dissensio est. Quae similitudo in genere etiam humano apparet. Urgent tamen et nihil remittunt. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Quamquam tu hanc copiosiorem etiam soles dicere. Bonum integritas corporis: misera debilitas. At enim hic etiam dolore. Sequitur disserendi ratio cognitioque naturae; 


	Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore.




	Recte, inquit, intellegis.
	Sed ea mala virtuti magnitudine obruebantur.
	Stoici scilicet.
	Qui non moveatur et offensione turpitudinis et comprobatione honestatis?
	Erat enim Polemonis.
	Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile.
	Certe non potest.
	Non est ista, inquam, Piso, magna dissensio.
	Recte dicis;
	Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria.



Maximus dolor, inquit, brevis est. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Omnia contraria, quos etiam insanos esse vultis. An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit? Egone non intellego, quid sit don Graece, Latine voluptas? At, illa, ut vobis placet, partem quandam tuetur, reliquam deserit. Quae duo sunt, unum facit. 


	An me, inquis, tam amentem putas, ut apud imperitos isto modo loquar?
	Ego vero volo in virtute vim esse quam maximam;
	Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.
	Non enim quaero quid verum, sed quid cuique dicendum sit.




Quodsi esset in voluptate summum bonum, ut dicitis, optabile
esset maxima in voluptate nullo intervallo interiecto dies
noctesque versari, cum omnes sensus dulcedine omni quasi
perfusi moverentur.

Reperiam multos, vel innumerabilis potius, non tam curiosos
nec tam molestos, quam vos estis, quibus, quid velim, facile
persuadeam.




	Haec et tu ita posuisti, et verba vestra sunt.
	Duarum enim vitarum nobis erunt instituta capienda.
	Quid de Platone aut de Democrito loquar?
	Non quam nostram quidem, inquit Pomponius iocans;
	Indicant pueri, in quibus ut in speculis natura cernitur.
	Teneo, inquit, finem illi videri nihil dolere.



Piso igitur hoc modo, vir optimus tuique, ut scis, amantissimus. In schola desinis. Si longus, levis; At hoc in eo M. Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Confecta res esset. 

Minime vero probatur huic disciplinae, de qua loquor, aut iustitiam aut amicitiam propter utilitates adscisci aut probari. Sed in rebus apertissimis nimium longi sumus. 


	An nisi populari fama?
	Quis istud, quaeso, nesciebat?
	Est enim effectrix multarum et magnarum voluptatum.
	Non est enim vitium in oratione solum, sed etiam in moribus.
	Et quod est munus, quod opus sapientiae?
	Dicet pro me ipsa virtus nec dubitabit isti vestro beato M.




De malis autem et bonis ab iis animalibus, quae nondum
depravata sint, ait optime iudicari.

Bona autem corporis huic sunt, quod posterius posui,
similiora.




	Nam constitui virtus nullo modo potesti nisi ea, quae sunt prima naturae, ut ad summam pertinentia tenebit.



Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? Hoc tu nunc in illo probas. Consequentia exquirere, quoad sit id, quod volumus, effectum. Quid igitur, inquit, eos responsuros putas? Recte, inquit, intellegis. Illum mallem levares, quo optimum atque humanissimum virum, Cn. 

Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Tu vero, inquam, ducas licet, si sequetur; Sed plane dicit quod intellegit. Quare obscurentur etiam haec, quae secundum naturam esse dicimus, in vita beata; Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. Atque adhuc ea dixi, causa cur Zenoni non fuisset, quam ob rem a superiorum auctoritate discederet. Age nunc isti doceant, vel tu potius quis enim ista melius? Sin laboramus, quis est, qui alienae modum statuat industriae? 

Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Iam illud quale tandem est, bona praeterita non effluere sapienti, mala meminisse non oportere? Non enim iam stirpis bonum quaeret, sed animalis. Isto modo, ne si avia quidem eius nata non esset. 

At enim hic etiam dolore. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Nobis Heracleotes ille Dionysius flagitiose descivisse videtur a Stoicis propter oculorum dolorem. Inde sermone vario sex illa a Dipylo stadia confecimus. Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; Inde sermone vario sex illa a Dipylo stadia confecimus. Quis enim potest ea, quae probabilia videantur ei, non probare? Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. 


	Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere.
	Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
	Tuo vero id quidem, inquam, arbitratu.
	Inde igitur, inquit, ordiendum est.
	Si enim ita est, vide ne facinus facias, cum mori suadeas.
	Certe non potest.



Quid autem habent admirationis, cum prope accesseris? Quo invento omnis ab eo quasi capite de summo bono et malo disputatio ducitur. Pugnant Stoici cum Peripateticis. Erat enim Polemonis. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Quo modo autem optimum, si bonum praeterea nullum est? Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium? Ne in odium veniam, si amicum destitero tueri. 


