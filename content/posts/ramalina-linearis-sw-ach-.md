Title: Ramalina linearis (Sw.) Ach.
Description: Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.
Image: https://source.unsplash.com/random/?night
Date: 2/4/2021
Keywords: design
Category: Otcom
Author: Gigashots
Tags: portfolio
Published: true
Background: #83c6b7
Color: #7f2283
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis enim potest ea, quae probabilia videantur ei, non probare? Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Non enim quaero quid verum, sed quid cuique dicendum sit. 


	Qui autem esse poteris, nisi te amor ipse ceperit?
	Ut optime, secundum naturam affectum esse possit.




	Peccata paria.
	Nam quid possumus facere melius?
	Moriatur, inquit.
	Cur deinde Metrodori liberos commendas?
	Quis enim redargueret?
	Videamus animi partes, quarum est conspectus illustrior;




	Tu quidem reddes;
	Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum.
	Non semper, inquam;
	Haec mihi videtur delicatior, ut ita dicam, molliorque ratio, quam virtutis vis gravitasque postulat.
	ALIO MODO.
	Sed et illum, quem nominavi, et ceteros sophistas, ut e Platone intellegi potest, lusos videmus a Socrate.



An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. Hoc non est positum in nostra actione. Addidisti ad extremum etiam indoctum fuisse. Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest. Estne, quaeso, inquam, sitienti in bibendo voluptas? Habent enim et bene longam et satis litigiosam disputationem. Multoque hoc melius nos veriusque quam Stoici. Morbo gravissimo affectus, exul, orbus, egens, torqueatur eculeo: quem hunc appellas, Zeno? 


	Si enim ad populum me vocas, eum.
	Ista ipsa, quae tu breviter: regem, dictatorem, divitem solum esse sapientem, a te quidem apte ac rotunde;
	Tenesne igitur, inquam, Hieronymus Rhodius quid dicat esse summum bonum, quo putet omnia referri oportere?
	His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent.



Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Quod autem ratione actum est, id officium appellamus. Amicitiam autem adhibendam esse censent, quia sit ex eo genere, quae prosunt. Teneo, inquit, finem illi videri nihil dolere. Dolere malum est: in crucem qui agitur, beatus esse non potest. Sullae consulatum? Animum autem reliquis rebus ita perfecit, ut corpus; An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? 

Idemne, quod iucunde? Haeret in salebra. Omnia contraria, quos etiam insanos esse vultis. Non risu potius quam oratione eiciendum? 

Res enim fortasse verae, certe graves, non ita tractantur, ut debent, sed aliquanto minutius. Quae diligentissime contra Aristonem dicuntur a Chryippo. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Sullae consulatum? Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? Est, ut dicis, inquit; Quo tandem modo? An est aliquid, quod te sua sponte delectet? 


Illud autem ipsum qui optineri potest, quod dicitis, omnis
animi et voluptates et dolores ad corporis voluptates ac
dolores pertinere?

Nam si quae sunt aliae, falsum est omnis animi voluptates
esse e corporis societate.




Quamquam tu hanc copiosiorem etiam soles dicere.

Neque solum ea communia, verum etiam paria esse dixerunt.



Quae cum magnifice primo dici viderentur, considerata minus probabantur. Nullum inveniri verbum potest quod magis idem declaret Latine, quod Graece, quam declarat voluptas. Unum est sine dolore esse, alterum cum voluptate. Consequentia exquirere, quoad sit id, quod volumus, effectum. Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Ut id aliis narrare gestiant? Quae autem natura suae primae institutionis oblita est? Cum audissem Antiochum, Brute, ut solebam, cum M. 


	Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam.



Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Verba tu fingas et ea dicas, quae non sentias? Non est ista, inquam, Piso, magna dissensio. Satis est ad hoc responsum. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? Maximus dolor, inquit, brevis est. Summus dolor plures dies manere non potest? 


	Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.




	Eaedem enim utilitates poterunt eas labefactare atque pervertere.
	Negat enim summo bono afferre incrementum diem.
	Qui enim existimabit posse se miserum esse beatus non erit.
	Sed non alienum est, quo facilius vis verbi intellegatur, rationem huius verbi faciendi Zenonis exponere.



Aperiendum est igitur, quid sit voluptas; Duo Reges: constructio interrete. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Quantum Aristoxeni ingenium consumptum videmus in musicis? Verba tu fingas et ea dicas, quae non sentias? Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. 


	Ut proverbia non nulla veriora sint quam vestra dogmata.
	Verum hoc loco sumo verbis his eandem certe vim voluptatis Epicurum nosse quam ceteros.
	Dic in quovis conventu te omnia facere, ne doleas.
	Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit?
	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.
	Conferam tecum, quam cuique verso rem subicias;



Hoc non est positum in nostra actione. Ubi ut eam caperet aut quando? Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Egone quaeris, inquit, quid sentiam? At ille pellit, qui permulcet sensum voluptate. Haec quo modo conveniant, non sane intellego. 

Primum quid tu dicis breve? Tu vero, inquam, ducas licet, si sequetur; Inde sermone vario sex illa a Dipylo stadia confecimus. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. 


