Title: Halesia carolina L.
Description: Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.
Image: https://source.unsplash.com/random/?developer
Date: 13/6/2021
Keywords: portfolio
Category: Aerified
Author: Gevee
Tags: cms
Published: true
Background: #3cc746
Color: #9b8a92
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sic consequentibus vestris sublatis prima tolluntur. Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Erat enim res aperta. Etenim si delectamur, cum scribimus, quis est tam invidus, qui ab eo nos abducat? Quid est, quod ab ea absolvi et perfici debeat? Si quidem, inquit, tollerem, sed relinquo. Duo Reges: constructio interrete. Omnia contraria, quos etiam insanos esse vultis. 

Scio enim esse quosdam, qui quavis lingua philosophari possint; Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Videamus igitur sententias eorum, tum ad verba redeamus. Expectoque quid ad id, quod quaerebam, respondeas. Si enim ad populum me vocas, eum. Quae duo sunt, unum facit. Deinde qui fit, ut ego nesciam, sciant omnes, quicumque Epicurei esse voluerunt? Vitiosum est enim in dividendo partem in genere numerare. Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere. Ut in geometria, prima si dederis, danda sunt omnia. 

Aeque enim contingit omnibus fidibus, ut incontentae sint. Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam esse oportebit; Quis est, qui non oderit libidinosam, protervam adolescentiam? Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Ille incendat? Qualem igitur hominem natura inchoavit? At iam decimum annum in spelunca iacet. Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; 


	Nihilne est in his rebus, quod dignum libero aut indignum esse ducamus?
	Quodsi ipsam honestatem undique pertectam atque absolutam.
	An hoc usque quaque, aliter in vita?
	Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim.
	At habetur! Et ego id scilicet nesciebam! Sed ut sit, etiamne post mortem coletur?




	Tubulo putas dicere?
	Miserum hominem! Si dolor summum malum est, dici aliter non potest.
	Istic sum, inquit.
	Ita multo sanguine profuso in laetitia et in victoria est mortuus.
	Negare non possum.
	Quid de Platone aut de Democrito loquar?
	Si longus, levis.
	Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit;



Oratio me istius philosophi non offendit; Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Quid de Pythagora? Sapiens autem semper beatus est et est aliquando in dolore; An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? 


	Primum divisit ineleganter;
	Et hercule-fatendum est enim, quod sentio -mirabilis est apud illos contextus rerum.
	Restatis igitur vos;
	Sed in rebus apertissimis nimium longi sumus.
	Idem adhuc;
	Sin est etiam corpus, ista explanatio naturae nempe hoc effecerit, ut ea, quae ante explanationem tenebamus, relinquamus.
	Frater et T.
	Non laboro, inquit, de nomine.
	Haeret in salebra.
	Sed erat aequius Triarium aliquid de dissensione nostra iudicare.
	Sullae consulatum?
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;




	Miserum hominem! Si dolor summum malum est, dici aliter non potest.




	Cum audissem Antiochum, Brute, ut solebam, cum M.
	Age, inquies, ista parva sunt.



Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? Teneo, inquit, finem illi videri nihil dolere. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; Claudii libidini, qui tum erat summo ne imperio, dederetur. Si qua in iis corrigere voluit, deteriora fecit. Sed nimis multa. Inde sermone vario sex illa a Dipylo stadia confecimus. Ita prorsus, inquam; Quid turpius quam sapientis vitam ex insipientium sermone pendere? 


	Septem autem illi non suo, sed populorum suffragio omnium nominati sunt.
	Quae cum dixisset paulumque institisset, Quid est?




Atqui pugnantibus et contrariis studiis consiliisque semper
utens nihil quieti videre, nihil tranquilli potest.

Positum est a nostris in iis esse rebus, quae secundum
naturam essent, non dolere;



Erit enim mecum, si tecum erit. Quid censes in Latino fore? Conferam tecum, quam cuique verso rem subicias; At iam decimum annum in spelunca iacet. Hunc vos beatum; An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. 

Quorum sine causa fieri nihil putandum est. Tecum optime, deinde etiam cum mediocri amico. Honesta oratio, Socratica, Platonis etiam. Hanc ergo intuens debet institutum illud quasi signum absolvere. 


	Erillus autem ad scientiam omnia revocans unum quoddam bonum vidit, sed nec optimum nec quo vita gubernari possit.



Minime vero probatur huic disciplinae, de qua loquor, aut iustitiam aut amicitiam propter utilitates adscisci aut probari. Nonne igitur tibi videntur, inquit, mala? Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Cur igitur, cum de re conveniat, non malumus usitate loqui? Comprehensum, quod cognitum non habet? Inde igitur, inquit, ordiendum est. Sed vos squalidius, illorum vides quam niteat oratio. 

Nihil opus est exemplis hoc facere longius. Itaque in rebus minime obscuris non multus est apud eos disserendi labor. Est autem officium, quod ita factum est, ut eius facti probabilis ratio reddi possit. Quid est, quod ab ea absolvi et perfici debeat? Laboro autem non sine causa; 

Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Istam voluptatem perpetuam quis potest praestare sapienti? Pugnant Stoici cum Peripateticis. Quid est igitur, inquit, quod requiras? Quid sequatur, quid repugnet, vident. Hoc est non dividere, sed frangere. Claudii libidini, qui tum erat summo ne imperio, dederetur. 


	Bonum integritas corporis: misera debilitas.
	Expectoque quid ad id, quod quaerebam, respondeas.
	Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum.
	Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile.
	An vero, inquit, quisquam potest probare, quod perceptfum, quod.




Quam ob rem tandem, inquit, non satisfacit?

At enim sequor utilitatem.




