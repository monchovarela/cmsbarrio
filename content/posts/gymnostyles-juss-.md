Title: Gymnostyles Juss.
Description: In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.
Image: https://source.unsplash.com/random/?design
Date: 24/12/2020
Keywords: web
Category: Pannier
Author: Gabvine
Tags: website
Published: true
Background: #5be1ae
Color: #13f779
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quorum altera prosunt, nocent altera. Quaerimus enim finem bonorum. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Aufidio, praetorio, erudito homine, oculis capto, saepe audiebam, cum se lucis magis quam utilitatis desiderio moveri diceret. An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? Duo Reges: constructio interrete. 


Aequam igitur pronuntiabit sententiam ratio adhibita primum
divinarum humanarumque rerum scientia, quae potest appellari
rite sapientia, deinde adiunctis virtutibus, quas ratio
rerum omnium dominas, tu voluptatum satellites et ministras
esse voluisti.

Itaque et vivere vitem et mori dicimus arboremque et
novellan et vetulam et vigere et senescere.




Quin etiam ferae, inquit Pacuvius, qu�bus abest, ad
pra�cavendum int�llegendi ast�tia, iniecto terrore mortis
horrescunt.

Virtutis, magnitudinis animi, patientiae, fortitudinis
fomentis dolor mitigari solet.



Dat enim intervalla et relaxat. Quod ea non occurrentia fingunt, vincunt Aristonem; Negat esse eam, inquit, propter se expetendam. Quia voluptatem hanc esse sentiunt omnes, quam sensus accipiens movetur et iucunditate quadam perfunditur. Quod iam a me expectare noli. Addebat etiam se in legem Voconiam iuratum contra eam facere non audere, nisi aliter amicis videretur. Illud dico, ea, quae dicat, praeclare inter se cohaerere. Quid enim tanto opus est instrumento in optimis artibus comparandis? Quicquid porro animo cernimus, id omne oritur a sensibus; 


	Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti.
	Conferam tecum, quam cuique verso rem subicias;
	Teneo, inquit, finem illi videri nihil dolere.



Bonum integritas corporis: misera debilitas. Magna laus. Sin kakan malitiam dixisses, ad aliud nos unum certum vitium consuetudo Latina traduceret. Et nunc quidem quod eam tuetur, ut de vite potissimum loquar, est id extrinsecus; Ergo, inquit, tibi Q. Nunc haec primum fortasse audientis servire debemus. Illum mallem levares, quo optimum atque humanissimum virum, Cn. Eaedem res maneant alio modo. 

Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Haec et tu ita posuisti, et verba vestra sunt. Tamen a proposito, inquam, aberramus. Primum quid tu dicis breve? Quare attende, quaeso. Idemque diviserunt naturam hominis in animum et corpus. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Omnes enim iucundum motum, quo sensus hilaretur. 


	Idem adhuc;
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Sint ista Graecorum;
	Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere?
	Quis enim redargueret?
	Ergo illi intellegunt quid Epicurus dicat, ego non intellego?
	Proclivi currit oratio.
	Nihil minus, contraque illa hereditate dives ob eamque rem laetus.



Disserendi artem nullam habuit. Sed fortuna fortis; Maximas vero virtutes iacere omnis necesse est voluptate dominante. 

Ac tamen hic mallet non dolere. Idemne, quod iucunde? Longum est enim ad omnia respondere, quae a te dicta sunt. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Sed residamus, inquit, si placet. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. 

Zenonis est, inquam, hoc Stoici. Eadem fortitudinis ratio reperietur. At enim hic etiam dolore. Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur. Graece donan, Latine voluptatem vocant. Urgent tamen et nihil remittunt. Sit sane ista voluptas. 


	Portenta haec esse dicit, neque ea ratione ullo modo posse vivi;
	Non potes ergo ista tueri, Torquate, mihi crede, si te ipse et tuas cogitationes et studia perspexeris;
	Ita cum ea volunt retinere, quae superiori sententiae conveniunt, in Aristonem incidunt;




	Sullae consulatum?
	Quod ea non occurrentia fingunt, vincunt Aristonem;
	Avaritiamne minuis?
	Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo;
	Quis enim redargueret?
	Et adhuc quidem ita nobis progresso ratio est, ut ea duceretur omnis a prima commendatione naturae.
	Quo tandem modo?
	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.




	Quid, si non modo utilitatem tibi nullam afferet, sed iacturae rei familiaris erunt faciendae, labores suscipiendi, adeundum vitae periculum?



Quod ea non occurrentia fingunt, vincunt Aristonem; Praeclare hoc quidem. Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Quod quidem iam fit etiam in Academia. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Istam voluptatem perpetuam quis potest praestare sapienti? 


	Aliter homines, aliter philosophos loqui putas oportere?



Aeque enim contingit omnibus fidibus, ut incontentae sint. Illud dico, ea, quae dicat, praeclare inter se cohaerere. Respondent extrema primis, media utrisque, omnia omnibus. Quis est tam dissimile homini. Inde sermone vario sex illa a Dipylo stadia confecimus. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Quis est, qui non oderit libidinosam, protervam adolescentiam? Atqui iste locus est, Piso, tibi etiam atque etiam confirmandus, inquam; 

Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos pertinere? Uterque enim summo bono fruitur, id est voluptate. Atqui iste locus est, Piso, tibi etiam atque etiam confirmandus, inquam; Respondeat totidem verbis. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Progredientibus autem aetatibus sensim tardeve potius quasi nosmet ipsos cognoscimus. Quid iudicant sensus? Quamquam te quidem video minime esse deterritum. 


	Nobis Heracleotes ille Dionysius flagitiose descivisse videtur a Stoicis propter oculorum dolorem.
	Beatus autem esse in maximarum rerum timore nemo potest.
	Vitae autem degendae ratio maxime quidem illis placuit quieta.
	Nisi autem rerum natura perspecta erit, nullo modo poterimus sensuum iudicia defendere.
	Sequitur disserendi ratio cognitioque naturae;
	Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt.




	Facile pateremur, qui etiam nunc agendi aliquid discendique causa prope contra naturam v�gillas suscipere soleamus.
	Nemo nostrum istius generis asotos iucunde putat vivere.
	Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit.
	Hoc loco discipulos quaerere videtur, ut, qui asoti esse velint, philosophi ante fiant.




