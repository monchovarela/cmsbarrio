Title: Cheirodendron dominii Krajina
Description: Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.
Image: https://source.unsplash.com/random/?design
Date: 11/4/2021
Keywords: article
Category: Alphazap
Author: Cogibox
Tags: post
Published: true
Background: #66a38d
Color: #e55a35
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Quae cum essent dicta, discessimus. Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. Duo Reges: constructio interrete. Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. 


	Ut aliquid scire se gaudeant?
	Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego;
	Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt.
	Quae contraria sunt his, malane?




Res tota, Torquate, non doctorum hominum, velle post mortem
epulis celebrari memoriam sui nominis.

Non potes ergo ista tueri, Torquate, mihi crede, si te ipse
et tuas cogitationes et studia perspexeris;



Illum mallem levares, quo optimum atque humanissimum virum, Cn. Estne, quaeso, inquam, sitienti in bibendo voluptas? 


	Moriatur, inquit.
	Ego vero volo in virtute vim esse quam maximam;
	Si quae forte-possumus.
	Facile est hoc cernere in primis puerorum aetatulis.



Praeclare hoc quidem. Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Qui ita affectus, beatum esse numquam probabis; Explanetur igitur. 

Quodsi ipsam honestatem undique pertectam atque absolutam. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Eam stabilem appellas. Praeterea sublata cognitione et scientia tollitur omnis ratio et vitae degendae et rerum gerendarum. Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti. Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. 


	Quae sunt igitur communia vobis cum antiquis, iis sic utamur quasi concessis;




	Aliter enim nosmet ipsos nosse non possumus.
	Aliter homines, aliter philosophos loqui putas oportere?
	Quid, si reviviscant Platonis illi et deinceps qui eorum auditores fuerunt, et tecum ita loquantur?
	Illis videtur, qui illud non dubitant bonum dicere -;
	Tu enim ista lenius, hic Stoicorum more nos vexat.



Nam Metrodorum non puto ipsum professum, sed, cum appellaretur ab Epicuro, repudiare tantum beneficium noluisse; Quid autem habent admirationis, cum prope accesseris? Utrum igitur tibi litteram videor an totas paginas commovere? An vero, inquit, quisquam potest probare, quod perceptfum, quod. Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Nihil enim iam habes, quod ad corpus referas; An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Tecum optime, deinde etiam cum mediocri amico. 

At vero illa, quae Peripatetici, quae Stoici dicunt, semper tibi in ore sunt in iudiciis, in senatu. Quid ergo hoc loco intellegit honestum? Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. An nisi populari fama? Dat enim intervalla et relaxat. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Si longus, levis dictata sunt. 


Praeclare enim Epicurus his paene verbis: Eadem, inquit,
scientia confirmavit animum, ne quod aut sempiternum aut
diuturnum timeret malum, quae perspexit in hoc ipso vitae
spatio amicitiae praesidium esse firmissimum.

Cur, nisi quod turpis oratio est?



Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Ut alios omittam, hunc appello, quem ille unum secutus est. Re mihi non aeque satisfacit, et quidem locis pluribus. Respondent extrema primis, media utrisque, omnia omnibus. Equidem, sed audistine modo de Carneade? 


	Nihil est enim, de quo minus dubitari possit, quam et honesta expetenda per se et eodem modo turpia per se esse fugienda.




	An eiusdem modi?
	Quid igitur dubitamus in tota eius natura quaerere quid sit effectum?
	Easdemne res?
	Si enim ita est, vide ne facinus facias, cum mori suadeas.
	Itaque fecimus.
	Antiquorum autem sententiam Antiochus noster mihi videtur persequi diligentissime, quam eandem Aristoteli fuisse et Polemonis docet.



Aliter homines, aliter philosophos loqui putas oportere? Haec mihi videtur delicatior, ut ita dicam, molliorque ratio, quam virtutis vis gravitasque postulat. Sic consequentibus vestris sublatis prima tolluntur. Sed fortuna fortis; Quae animi affectio suum cuique tribuens atque hanc, quam dico. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; 


	Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam;
	Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia;



In schola desinis. Torquatus, is qui consul cum Cn. Murenam te accusante defenderem. Cui Tubuli nomen odio non est? Et quod est munus, quod opus sapientiae? Sic vester sapiens magno aliquo emolumento commotus cicuta, si opus erit, dimicabit. Restinguet citius, si ardentem acceperit. 

Paria sunt igitur. Istam voluptatem, inquit, Epicurus ignorat? Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Hoc est non modo cor non habere, sed ne palatum quidem. Ratio quidem vestra sic cogit. At enim sequor utilitatem. 


	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti.
	Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest.
	Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?




