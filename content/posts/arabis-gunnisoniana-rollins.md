Title: Arabis gunnisoniana Rollins
Description: Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.
Image: https://source.unsplash.com/random/?night
Date: 18/7/2021
Keywords: design
Category: Daltfresh
Author: Shuffletag
Tags: post
Published: true
Background: #5f2371
Color: #733c21
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed potestne rerum maior esse dissensio? At miser, si in flagitiosa et vitiosa vita afflueret voluptatibus. Duo Reges: constructio interrete. Quaerimus enim finem bonorum. Nam ante Aristippus, et ille melius. Hoc Hieronymus summum bonum esse dixit. Nemo igitur esse beatus potest. 

Summum a vobis bonum voluptas dicitur. Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Tum Quintus: Est plane, Piso, ut dicis, inquit. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret. Sed plane dicit quod intellegit. Et harum quidem rerum facilis est et expedita distinctio. Que Manilium, ab iisque M. 


	Tum ille: Finem, inquit, interrogandi, si videtur, quod quidem ego a principio ita me malle dixeram hoc ipsum providens, dialecticas captiones.



Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? Sed fortuna fortis; Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur. Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. An eum locum libenter invisit, ubi Demosthenes et Aeschines inter se decertare soliti sunt? Istam voluptatem perpetuam quis potest praestare sapienti? Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Igitur neque stultorum quisquam beatus neque sapientium non beatus. Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit. 

Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Sin aliud quid voles, postea. Praetereo multos, in bis doctum hominem et suavem, Hieronymum, quem iam cur Peripateticum appellem nescio. Neutrum vero, inquit ille. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. 

Qui autem esse poteris, nisi te amor ipse ceperit? At, si voluptas esset bonum, desideraret. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Hic ambiguo ludimur. Apparet statim, quae sint officia, quae actiones. Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Itaque his sapiens semper vacabit. 

Quid sequatur, quid repugnet, vident. Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Duarum enim vitarum nobis erunt instituta capienda. Nulla erit controversia. Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt. Nam ante Aristippus, et ille melius. Invidiosum nomen est, infame, suspectum. 


	Quod totum contra est.
	At enim hic etiam dolore.
	Hoc est non modo cor non habere, sed ne palatum quidem.
	Quid enim de amicitia statueris utilitatis causa expetenda vides.




	Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse.
	Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit;
	Minime vero, inquit ille, consentit.
	A villa enim, credo, et: Si ibi te esse scissem, ad te ipse venissem.




	Num igitur dubium est, quin, si in re ipsa nihil peccatur a superioribus, verbis illi commodius utantur?




Quid loquor de nobis, qui ad laudem et ad decus nati,
suscepti, instituti sumus?

Nec vero hoc oratione solum, sed multo magis vita et factis
et moribus comprobavit.




	Istic sum, inquit.
	An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia?
	Sed videbimus.
	Dicimus aliquem hilare vivere;
	Efficiens dici potest.
	Deinde disputat, quod cuiusque generis animantium statui deceat extremum.



Quod autem meum munus dicis non equidem recuso, sed te adiungo socium. Immo videri fortasse. Collatio igitur ista te nihil iuvat. Explanetur igitur. Pauca mutat vel plura sane; Quid de Pythagora? 

Et hercule-fatendum est enim, quod sentio -mirabilis est apud illos contextus rerum. Non igitur bene. Quid autem habent admirationis, cum prope accesseris? Minime vero, inquit ille, consentit. Non laboro, inquit, de nomine. Quid enim possumus hoc agere divinius? Illum mallem levares, quo optimum atque humanissimum virum, Cn. Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Quis istum dolorem timet? Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. 


	Comprehensum, quod cognitum non habet?
	Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est.
	Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret?
	Quo studio Aristophanem putamus aetatem in litteris duxisse?
	Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant.
	Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius.




	Oratio me istius philosophi non offendit;
	Equidem e Cn.
	Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse.
	Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem.
	Dicam, inquam, et quidem discendi causa magis, quam quo te aut Epicurum reprehensum velim.



Stoicos roga. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? 


	Nulla erit controversia.
	Cur deinde Metrodori liberos commendas?
	Pollicetur certe.
	Et quidem, inquit, vehementer errat;
	Nihil sane.
	Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?



Hic nihil fuit, quod quaereremus. Sed vos squalidius, illorum vides quam niteat oratio. Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Quae duo sunt, unum facit. Dici enim nihil potest verius. 


Attica pubes reliquique Graeci, qui hoc anapaesto citantur,
hoc non dolere solum voluptatis nomine appellaret, illud
Aristippeum contemneret, aut, si utrumque probaret, ut
probat, coniungeret doloris vacuitatem cum voluptate et
duobus ultimis uteretur.

Quid est, quod ab ea absolvi et perfici debeat?




