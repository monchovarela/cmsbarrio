Title: Isoetes lithophila N.E. Pfeiffer
Description: Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.
Image: https://source.unsplash.com/random/?iphone
Date: 16/7/2021
Keywords: website
Category: Quo Lux
Author: Wordify
Tags: portfolio
Published: true
Background: #a3beec
Color: #6b43ae
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hanc ergo intuens debet institutum illud quasi signum absolvere. Se dicere inter honestum et turpe nimium quantum, nescio quid inmensum, inter ceteras res nihil omnino interesse. 


	Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae.
	Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.
	Res tota, Torquate, non doctorum hominum, velle post mortem epulis celebrari memoriam sui nominis.
	Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus;
	Cur ipse Pythagoras et Aegyptum lustravit et Persarum magos adiit?




Is enim percontando atque interrogando elicere solebat eorum
opiniones, quibuscum disserebat, ut ad ea, quae ii
respondissent, si quid videretur, diceret.

Alia quaedam dicent, credo, magna antiquorum esse peccata,
quae ille veri investigandi cupidus nullo modo ferre
potuerit.



Et non ex maxima parte de tota iudicabis? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Cur ipse Pythagoras et Aegyptum lustravit et Persarum magos adiit? Sed quanta sit alias, nunc tantum possitne esse tanta. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Quia dolori non voluptas contraria est, sed doloris privatio. 


Itaque illa non dico me expetere, sed legere, nec optare,
sed sumere, contraria autem non fugere, sed quasi secernere.

Non ergo Epicurus ineruditus, sed ii indocti, qui, quae
pueros non didicisse turpe est, ea putant usque ad
senectutem esse discenda.




	Unum est sine dolore esse, alterum cum voluptate.
	Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.




	Itaque ad tempus ad Pisonem omnes.
	Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum.
	Quae cum essent dicta, finem fecimus et ambulandi et disputandi.
	Sin autem eos non probabat, quid attinuit cum iis, quibuscum re concinebat, verbis discrepare?
	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?
	Haec igitur Epicuri non probo, inquam.




	Atqui perspicuum est hominem e corpore animoque constare, cum primae sint animi partes, secundae corporis.



Quae est igitur causa istarum angustiarum? Sic enim censent, oportunitatis esse beate vivere. Quonam, inquit, modo? His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. Quia nec honesto quic quam honestius nec turpi turpius. Aliter enim explicari, quod quaeritur, non potest. Nulla profecto est, quin suam vim retineat a primo ad extremum. Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. 

Quod autem principium officii quaerunt, melius quam Pyrrho; Quo studio Aristophanem putamus aetatem in litteris duxisse? Eam tum adesse, cum dolor omnis absit; Itaque his sapiens semper vacabit. Simus igitur contenti his. Ergo hoc quidem apparet, nos ad agendum esse natos. 

Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Mene ergo et Triarium dignos existimas, apud quos turpiter loquare? A villa enim, credo, et: Si ibi te esse scissem, ad te ipse venissem. Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur. Igitur neque stultorum quisquam beatus neque sapientium non beatus. Sed haec quidem liberius ab eo dicuntur et saepius. Quae diligentissime contra Aristonem dicuntur a Chryippo. Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. 

Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Quae in controversiam veniunt, de iis, si placet, disseramus. Nos paucis ad haec additis finem faciamus aliquando; Certe non potest. Nescio quo modo praetervolavit oratio. Cur haec eadem Democritus? An haec ab eo non dicuntur? 

Sed nonne merninisti licere mihi ista probare, quae sunt a te dicta? Quae in controversiam veniunt, de iis, si placet, disseramus. Duo Reges: constructio interrete. Num quid tale Democritus? Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? 

Idem adhuc; Si enim ita est, vide ne facinus facias, cum mori suadeas. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Hoc ille tuus non vult omnibusque ex rebus voluptatem quasi mercedem exigit. Si autem id non concedatur, non continuo vita beata tollitur. Dat enim intervalla et relaxat. Consequatur summas voluptates non modo parvo, sed per me nihilo, si potest; Ea, quae dialectici nunc tradunt et docent, nonne ab illis instituta sunt aut inventa sunt? Quam ob rem tandem, inquit, non satisfacit? Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt. 


	Proclivi currit oratio.
	Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim.
	Recte dicis;
	An me, inquis, tam amentem putas, ut apud imperitos isto modo loquar?
	Ostendit pedes et pectus.
	Quid turpius quam sapientis vitam ex insipientium sermone pendere?
	Quid de Pythagora?
	Nam memini etiam quae nolo, oblivisci non possum quae volo.
	Ita prorsus, inquam;
	Traditur, inquit, ab Epicuro ratio neglegendi doloris.
	Quo tandem modo?
	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;




	Aliena dixit in physicis nec ea ipsa, quae tibi probarentur;
	Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit.
	Philosophi autem in suis lectulis plerumque moriuntur.



Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Iam enim adesse poterit. De vacuitate doloris eadem sententia erit. Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. Pugnant Stoici cum Peripateticis. Aliter autem vobis placet. 


	Beatum, inquit.
	Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem illam Aristippi in prima commendatione poneret.
	Peccata paria.
	Quid ergo hoc loco intellegit honestum?
	Etiam beatissimum?
	Ita enim vivunt quidam, ut eorum vita refellatur oratio.
	Quid de Pythagora?
	Certe non potest.




	Id enim ille summum bonum eu)qumi/an et saepe a)qambi/an appellat, id est animum terrore liberum.



Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius. Sed vobis voluptatum perceptarum recordatio vitam beatam facit, et quidem corpore perceptarum. Ratio quidem vestra sic cogit. Ergo hoc quidem apparet, nos ad agendum esse natos. Quid enim de amicitia statueris utilitatis causa expetenda vides. Quare conare, quaeso. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. An quod ita callida est, ut optime possit architectari voluptates? 


