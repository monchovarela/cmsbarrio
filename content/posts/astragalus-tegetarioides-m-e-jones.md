Title: Astragalus tegetarioides M.E. Jones
Description: Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.
Image: https://source.unsplash.com/random/?night
Date: 17/6/2021
Keywords: article
Category: Otcom
Author: Devify
Tags: web
Published: true
Background: #d650e6
Color: #90977c
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut scias me intellegere, primum idem esse dico voluptatem, quod ille don. Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam. Ut id aliis narrare gestiant? Primum Theophrasti, Strato, physicum se voluit; Quid nunc honeste dicit? Duo Reges: constructio interrete. Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. Idemne, quod iucunde? Istam voluptatem perpetuam quis potest praestare sapienti? Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor; Traditur, inquit, ab Epicuro ratio neglegendi doloris. 

Non est igitur voluptas bonum. Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. In contemplatione et cognitione posita rerum, quae quia deorum erat vitae simillima, sapiente visa est dignissima. Sic enim censent, oportunitatis esse beate vivere. An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit? Cupiditates non Epicuri divisione finiebat, sed sua satietate. Quid nunc honeste dicit? Tum ille timide vel potius verecunde: Facio, inquit. 

Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere. Nescio quo modo praetervolavit oratio. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Haec igitur Epicuri non probo, inquam. Quid loquor de nobis, qui ad laudem et ad decus nati, suscepti, instituti sumus? Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? Quae similitudo in genere etiam humano apparet. 

Quid, quod res alia tota est? Ait enim se, si uratur, Quam hoc suave! dicturum. Hic ambiguo ludimur. Quo tandem modo? 


	Oratio me istius philosophi non offendit;
	Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum.
	Summum en�m bonum exposuit vacuitatem doloris;
	Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis;




	Aliam vero vim voluptatis esse, aliam nihil dolendi, nisi valde pertinax fueris, concedas necesse est.



Pauca mutat vel plura sane; Si verbum sequimur, primum longius verbum praepositum quam bonum. Haec dicuntur inconstantissime. Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Esse enim, nisi eris, non potes. Confecta res esset. Certe nihil nisi quod possit ipsum propter se iure laudari. Quod non faceret, si in voluptate summum bonum poneret. Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est. Atqui reperies, inquit, in hoc quidem pertinacem; 


	Ille incendat?
	Non quam nostram quidem, inquit Pomponius iocans;
	Perge porro;
	Quod autem magnum dolorem brevem, longinquum levem esse dicitis, id non intellego quale sit.



Quid de Platone aut de Democrito loquar? Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem. Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret? Quorum sine causa fieri nihil putandum est. Et nemo nimium beatus est; Eaedem enim utilitates poterunt eas labefactare atque pervertere. His enim rebus detractis negat se reperire in asotorum vita quod reprehendat. Sed quid attinet de rebus tam apertis plura requirere? Quorum sine causa fieri nihil putandum est. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. 


	Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum.
	Hoc Hieronymus summum bonum esse dixit.
	Quibus rebus vita consentiens virtutibusque respondens recta et honesta et constans et naturae congruens existimari potest.
	Quamquam tu hanc copiosiorem etiam soles dicere.
	Quod quidem iam fit etiam in Academia.
	Scisse enim te quis coarguere possit?




Illa sunt similia: hebes acies est cuipiam oculorum, corpore
alius senescit;

Quae animi affectio suum cuique tribuens atque hanc, quam
dico.




Qui ita affectus, beatum esse numquam probabis;

Expectoque quid ad id, quod quaerebam, respondeas.




	Moriatur, inquit.
	Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur.
	Memini vero, inquam;
	Stoici autem, quod finem bonorum in una virtute ponunt, similes sunt illorum;
	Quare conare, quaeso.
	Satis est tibi in te, satis in legibus, satis in mediocribus amicitiis praesidii.
	An eiusdem modi?
	Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit.



Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Si enim ad populum me vocas, eum. Sed fortuna fortis; Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Nemo igitur esse beatus potest. Ut in geometria, prima si dederis, danda sunt omnia. Tu autem, si tibi illa probabantur, cur non propriis verbis ea tenebas? Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Quis istum dolorem timet? Comprehensum, quod cognitum non habet? At, si voluptas esset bonum, desideraret. 


	Sin laboramus, quis est, qui alienae modum statuat industriae?
	Ego vero volo in virtute vim esse quam maximam;
	Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.
	Et ille ridens: Age, age, inquit,-satis enim scite me nostri sermonis principium esse voluisti-exponamus adolescenti,.



Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam. Itaque quantum adiit periculum! ad honestatem enim illum omnem conatum suum referebat, non ad voluptatem. Idem fecisset Epicurus, si sententiam hanc, quae nunc Hieronymi est, coniunxisset cum Aristippi vetere sententia. Quae est igitur causa istarum angustiarum? 

Quid nunc honeste dicit? Longum est enim ad omnia respondere, quae a te dicta sunt. Quo plebiscito decreta a senatu est consuli quaestio Cn. Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q. Facile est hoc cernere in primis puerorum aetatulis. 


	Haec para/doca illi, nos admirabilia dicamus.
	Hic nihil fuit, quod quaereremus.
	Venit enim mihi Platonis in mentem, quem accepimus primum hic disputare solitum;
	Aliter homines, aliter philosophos loqui putas oportere?



Sed fortuna fortis; Quid affers, cur Thorius, cur Caius Postumius, cur omnium horum magister, Orata, non iucundissime vixerit? Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Neutrum vero, inquit ille. Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Illud quaero, quid ei, qui in voluptate summum bonum ponat, consentaneum sit dicere. Ita fit cum gravior, tum etiam splendidior oratio. Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Quid autem habent admirationis, cum prope accesseris? 


	Rem videlicet difficilem et obscuram! individua cum dicitis et intermundia, quae nec sunt ulla nec possunt esse, intellegimus, voluptas, quae passeribus omnibus nota est, a nobis intellegi non potest?




