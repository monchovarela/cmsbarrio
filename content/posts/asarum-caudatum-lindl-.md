Title: Asarum caudatum Lindl.
Description: Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.
Image: https://source.unsplash.com/random/?developer
Date: 1/2/2021
Keywords: work
Category: Biodex
Author: Innojam
Tags: portfolio
Published: true
Background: #833f51
Color: #8d3d3e
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non est igitur voluptas bonum. Quae cum dixisset paulumque institisset, Quid est? Addidisti ad extremum etiam indoctum fuisse. Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim. Haec quo modo conveniant, non sane intellego. Duo Reges: constructio interrete. Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia. Tecum optime, deinde etiam cum mediocri amico. Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Ita ne hoc quidem modo paria peccata sunt. 


	Sumenda potius quam expetenda.
	Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt.
	Nihil sane.
	Certe nihil nisi quod possit ipsum propter se iure laudari.
	Restatis igitur vos;
	Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere.
	Quonam modo?
	Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum.
	Ita credo.
	In motu et in statu corporis nihil inest, quod animadvertendum esse ipsa natura iudicet?



Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Non autem hoc: igitur ne illud quidem. Sed quid sentiat, non videtis. Sed ea mala virtuti magnitudine obruebantur. Zenonis est, inquam, hoc Stoici. Compensabatur, inquit, cum summis doloribus laetitia. Nullus est igitur cuiusquam dies natalis. Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? 


Nam cum suscepta semel est beata vita, tam permanet quam
ipsa illa effectrix beatae vitae sapientia neque expectat
ultimum tempus aetatis, quod Croeso scribit Herodotus
praeceptum a Solone.

Atqui, inquit, si Stoicis concedis ut virtus sola, si adsit
vitam efficiat beatam, concedis etiam Peripateticis.




	Sullae consulatum?



Et ille ridens: Video, inquit, quid agas; Si quae forte-possumus. Vide igitur ne non debeas verbis nostris uti, sententiis tuis. Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat; Atqui reperies, inquit, in hoc quidem pertinacem; Est enim effectrix multarum et magnarum voluptatum. Tu enim ista lenius, hic Stoicorum more nos vexat. Quae contraria sunt his, malane? Quid Zeno? Aliter homines, aliter philosophos loqui putas oportere? Tum, Quintus et Pomponius cum idem se velle dixissent, Piso exorsus est. 

An vero, inquit, quisquam potest probare, quod perceptfum, quod. Expectoque quid ad id, quod quaerebam, respondeas. Quod ea non occurrentia fingunt, vincunt Aristonem; Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Sed vos squalidius, illorum vides quam niteat oratio. Quis est, qui non oderit libidinosam, protervam adolescentiam? Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. 

Id est enim, de quo quaerimus. Itaque ad tempus ad Pisonem omnes. Vides igitur te aut ea sumere, quae non concedantur, aut ea, quae etiam concessa te nihil iuvent. Teneo, inquit, finem illi videri nihil dolere. At iam decimum annum in spelunca iacet. Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. Isto modo, ne si avia quidem eius nata non esset. An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? 


	Nam de summo mox, ut dixi, videbimus et ad id explicandum disputationem omnem conferemus.




Quod et posse fieri intellegimus et saepe etiam videmus, et
perspicuum est nihil ad iucunde vivendum reperiri posse,
quod coniunctione tali sit aptius.

Pungunt quasi aculeis interrogatiunculis angustis, quibus
etiam qui assentiuntur nihil commutantur animo et idem
abeunt, qui venerant.




	Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi;
	Si qua in iis corrigere voluit, deteriora fecit.
	Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene.
	Sed audiamus ipsum: Compensabatur, inquit, tamen cum his omnibus animi laetitia, quam capiebam memoria rationum inventorumque nostrorum.
	Indicant pueri, in quibus ut in speculis natura cernitur.




	Quae iam oratio non a philosopho aliquo, sed a censore opprimenda est.
	Scripta sane et multa et polita, sed nescio quo pacto auctoritatem oratio non habet.
	Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae.
	Nec vero hoc oratione solum, sed multo magis vita et factis et moribus comprobavit.
	Sed quid minus probandum quam esse aliquem beatum nec satis beatum?




	ALIO MODO.
	Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit.
	Equidem e Cn.
	Iubet igitur nos Pythius Apollo noscere nosmet ipsos.
	Quis hoc dicit?
	Cave putes quicquam esse verius.
	Nos commodius agimus.
	An est aliquid, quod te sua sponte delectet?



Sed ego in hoc resisto; Idemque diviserunt naturam hominis in animum et corpus. Respondeat totidem verbis. Prioris generis est docilitas, memoria; Scaevolam M. Occultum facinus esse potuerit, gaudebit; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Hoc loco discipulos quaerere videtur, ut, qui asoti esse velint, philosophi ante fiant. 

Piso igitur hoc modo, vir optimus tuique, ut scis, amantissimus. Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Sed tamen intellego quid velit. Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Ut aliquid scire se gaudeant? 

Nam cui proposito sit conservatio sui, necesse est huic partes quoque sui caras suo genere laudabiles. Eam stabilem appellas. Non quam nostram quidem, inquit Pomponius iocans; Mihi enim satis est, ipsis non satis. Hoc ne statuam quidem dicturam pater aiebat, si loqui posset. Dic in quovis conventu te omnia facere, ne doleas. Aliter enim explicari, quod quaeritur, non potest. Est enim effectrix multarum et magnarum voluptatum. Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Ne discipulum abducam, times. 

De quibus cupio scire quid sentias. Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est; Equidem, sed audistine modo de Carneade? Quo modo autem philosophus loquitur? Quae in controversiam veniunt, de iis, si placet, disseramus. Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. Nam ante Aristippus, et ille melius. Nobis aliter videtur, recte secusne, postea; Esse enim, nisi eris, non potes. Si qua in iis corrigere voluit, deteriora fecit. Teneo, inquit, finem illi videri nihil dolere. Occultum facinus esse potuerit, gaudebit; 


	Qua tu etiam inprudens utebare non numquam.
	Tu vero, inquam, ducas licet, si sequetur;



Res enim se praeclare habebat, et quidem in utraque parte. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Multoque hoc melius nos veriusque quam Stoici. Si quidem, inquit, tollerem, sed relinquo. Id est enim, de quo quaerimus. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Quamquam tu hanc copiosiorem etiam soles dicere. 


	Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat;
	Illud mihi a te nimium festinanter dictum videtur, sapientis omnis esse semper beatos;
	De quibus cupio scire quid sentias.
	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.
	Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus.
	Portenta haec esse dicit, neque ea ratione ullo modo posse vivi;




