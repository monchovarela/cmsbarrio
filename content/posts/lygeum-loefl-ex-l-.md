Title: Lygeum Loefl. ex L.
Description: Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.
Image: https://source.unsplash.com/random/?javascript
Date: 19/1/2021
Keywords: portfolio
Category: Transcof
Author: Skimia
Tags: web
Published: true
Background: #9801f8
Color: #c3f5db
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam de summo mox, ut dixi, videbimus et ad id explicandum disputationem omnem conferemus. Hoc est non dividere, sed frangere. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Duo Reges: constructio interrete. Aliter homines, aliter philosophos loqui putas oportere? Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est. Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Tu autem, si tibi illa probabantur, cur non propriis verbis ea tenebas? 


	Hunc vos beatum;
	Pauca mutat vel plura sane;
	Quid Zeno?
	Certe non potest.
	Etiam beatissimum?
	Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur.
	Tenent mordicus.
	Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus;



Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Scio enim esse quosdam, qui quavis lingua philosophari possint; Sint modo partes vitae beatae. Quid ergo attinet gloriose loqui, nisi constanter loquare? Sed ille, ut dixi, vitiose. 


Callipho ad virtutem nihil adiunxit nisi voluptatem,
Diodorus vacuitatem doloris.

Atqui haec patefactio quasi rerum opertarum, cum quid
quidque sit aperitur, definitio est.




	Odium autem et invidiam facile vitabis.



Quare attende, quaeso. Est enim effectrix multarum et magnarum voluptatum. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Sic enim censent, oportunitatis esse beate vivere. Quae similitudo in genere etiam humano apparet. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est. An quod ita callida est, ut optime possit architectari voluptates? Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. 

Multoque hoc melius nos veriusque quam Stoici. Sed nimis multa. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. At certe gravius. Vadem te ad mortem tyranno dabis pro amico, ut Pythagoreus ille Siculo fecit tyranno? 


Equidem etiam curiam nostram-Hostiliam dico, non hanc novam,
quae minor mihi esse videtur, posteaquam est maior-solebam
intuens Scipionem, Catonem, Laelium, nostrum vero in primis
avum cogitare;

Quod autem magnum dolorem brevem, longinquum levem esse
dicitis, id non intellego quale sit.



Hoc non est positum in nostra actione. Et quod est munus, quod opus sapientiae? Et summatim quidem haec erant de corpore animoque dicenda, quibus quasi informatum est quid hominis natura postulet. Quod equidem non reprehendo; Suo genere perveniant ad extremum; Suam denique cuique naturam esse ad vivendum ducem. Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. 


	Sed haec omittamus;
	Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos pertinere?
	Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster?
	At quanta conantur! Mundum hunc omnem oppidum esse nostrum! Incendi igitur eos, qui audiunt, vides.
	Nam de isto magna dissensio est.



Expectoque quid ad id, quod quaerebam, respondeas. Pisone in eo gymnasio, quod Ptolomaeum vocatur, unaque nobiscum Q. Numquam hoc ita defendit Epicurus neque Metrodorus aut quisquam eorum, qui aut saperet aliquid aut ista didicisset. Consequentia exquirere, quoad sit id, quod volumus, effectum. Sed quid sentiat, non videtis. Id quaeris, inquam, in quo, utrum respondero, verses te huc atque illuc necesse est. Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. Quae cum dixisset, finem ille. Quo igitur, inquit, modo? Quae autem natura suae primae institutionis oblita est? 

Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Age sane, inquam. Nam Metrodorum non puto ipsum professum, sed, cum appellaretur ab Epicuro, repudiare tantum beneficium noluisse; Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est; Hic ambiguo ludimur. Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia. Non laboro, inquit, de nomine. Illa argumenta propria videamus, cur omnia sint paria peccata. 

Duo enim genera quae erant, fecit tria. Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Iam in altera philosophiae parte. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Summum en�m bonum exposuit vacuitatem doloris; Tum ille timide vel potius verecunde: Facio, inquit. 

Non quam nostram quidem, inquit Pomponius iocans; Quod autem ratione actum est, id officium appellamus. At enim sequor utilitatem. Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur. Murenam te accusante defenderem. Hoc est non dividere, sed frangere. 


	Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam.
	Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est.
	Quo modo autem optimum, si bonum praeterea nullum est?
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Ita ceterorum sententiis semotis relinquitur non mihi cum Torquato, sed virtuti cum voluptate certatio.




	Tum Lucius: Mihi vero ista valde probata sunt, quod item fratri puto.



Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus; In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret. Suam denique cuique naturam esse ad vivendum ducem. Videsne quam sit magna dissensio? Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Hoc simile tandem est? 


	Magna laus.
	Addidisti ad extremum etiam indoctum fuisse.
	Quid enim?
	Mihi, inquam, qui te id ipsum rogavi?




	Quid de Pythagora?
	Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum.
	Indicant pueri, in quibus ut in speculis natura cernitur.
	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.
	Sed quot homines, tot sententiae;




	Tu autem, si tibi illa probabantur, cur non propriis verbis ea tenebas?
	Totum genus hoc Zeno et qui ab eo sunt aut non potuerunt aut noluerunt, certe reliquerunt.
	Sed quid attinet de rebus tam apertis plura requirere?
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?




