Title: Carex corrugata Fernald
Description: Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.
Image: https://source.unsplash.com/random/?css
Date: 17/11/2020
Keywords: cms
Category: Veribet
Author: Vimbo
Tags: website
Published: true
Background: #5bf458
Color: #a37bb8
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Haec et tu ita posuisti, et verba vestra sunt. Duo Reges: constructio interrete. Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit. Ego vero volo in virtute vim esse quam maximam; Quae contraria sunt his, malane? Nam quid possumus facere melius? Ex quo, id quod omnes expetunt, beate vivendi ratio inveniri et comparari potest. 

Vestri haec verecundius, illi fortasse constantius. Disserendi artem nullam habuit. A primo, ut opinor, animantium ortu petitur origo summi boni. Licet hic rursus ea commemores, quae optimis verbis ab Epicuro de laude amicitiae dicta sunt. Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. In his igitur partibus duabus nihil erat, quod Zeno commutare gestiret. 


	Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.
	Iubet igitur nos Pythius Apollo noscere nosmet ipsos.
	Primum cur ista res digna odio est, nisi quod est turpis?
	Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur?
	Quantum Aristoxeni ingenium consumptum videmus in musicis?
	Quid ergo attinet gloriose loqui, nisi constanter loquare?




	Atqui perspicuum est hominem e corpore animoque constare, cum primae sint animi partes, secundae corporis.
	Facillimum id quidem est, inquam.
	Quis hoc dicit?
	Color egregius, integra valitudo, summa gratia, vita denique conferta voluptatum omnium varietate.
	Quid est, quod ab ea absolvi et perfici debeat?




Non metuet autem, sive celare poterit, sive opibus magnis
quicquid fecerit optinere, certeque malet existimari bonus
vir, ut non sit, quam esse, ut non putetur.

Nemo est igitur, quin hanc affectionem animi probet atque
laudet, qua non modo utilitas nulla quaeritur, sed contra
utilitatem etiam conservatur fides.




	Quid iudicant sensus?
	Illis videtur, qui illud non dubitant bonum dicere -;
	Si longus, levis.
	Ita relinquet duas, de quibus etiam atque etiam consideret.
	Itaque fecimus.
	Non igitur de improbo, sed de callido improbo quaerimus, qualis Q.
	Pollicetur certe.
	Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.
	Recte, inquit, intellegis.
	Omnes enim iucundum motum, quo sensus hilaretur.
	Scrupulum, inquam, abeunti;
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?



Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Hoc ipsum elegantius poni meliusque potuit. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Restinguet citius, si ardentem acceperit. Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? Quis enim redargueret? Huius, Lyco, oratione locuples, rebus ipsis ielunior. 


	Ad hanc rem aiunt artis quoque requisitas, quae naturam adiuvarent in quibus ea numeretur in primis, quae est est vivendi ars, ut tueatur, quod a natura datum sit, quod desit, adquirat.



Quod autem ratione actum est, id officium appellamus. Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat. Non dolere, inquam, istud quam vim habeat postea videro; Quae cum essent dicta, finem fecimus et ambulandi et disputandi. Quare attende, quaeso. Nescio quo modo praetervolavit oratio. Non enim iam stirpis bonum quaeret, sed animalis. Sequitur disserendi ratio cognitioque naturae; Quae similitudo in genere etiam humano apparet. Tamen a proposito, inquam, aberramus. 


Is enim, qui occultus et tectus dicitur, tantum abest ut se
indicet, perficiet etiam ut dolere alterius improbe facto
videatur.

Omnes, qui non sint sapientes, aeque miseros esse, sapientes
omnes summe beatos, recte facta omnia aequalia, omnia
peccata paria;



Philosophi autem in suis lectulis plerumque moriuntur. Omnes enim iucundum motum, quo sensus hilaretur. 


	Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri.
	Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata.
	Theophrastus mediocriterne delectat, cum tractat locos ab Aristotele ante tractatos?
	Et ille ridens: Age, age, inquit,-satis enim scite me nostri sermonis principium esse voluisti-exponamus adolescenti,.
	Stoici scilicet.
	Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum.



Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Hanc quoque iucunditatem, si vis, transfer in animum; Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; Si quidem, inquit, tollerem, sed relinquo. Nos paucis ad haec additis finem faciamus aliquando; An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? Itaque contra est, ac dicitis; Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. Sed quid attinet de rebus tam apertis plura requirere? Obsecro, inquit, Torquate, haec dicit Epicurus? 

Ergo id est convenienter naturae vivere, a natura discedere. Ita credo. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Dicimus aliquem hilare vivere; Sed quid ages tandem, si utilitas ab amicitia, ut fit saepe, defecerit? Tu enim ista lenius, hic Stoicorum more nos vexat. Non ego tecum iam ita iocabor, ut isdem his de rebus, cum L. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Quae sunt igitur communia vobis cum antiquis, iis sic utamur quasi concessis; 

Non enim solum Torquatus dixit quid sentiret, sed etiam cur. Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera. Nos commodius agimus. Quis est tam dissimile homini. Praeterea sublata cognitione et scientia tollitur omnis ratio et vitae degendae et rerum gerendarum. Quo modo autem optimum, si bonum praeterea nullum est? Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis. Hunc vos beatum; 

Expectoque quid ad id, quod quaerebam, respondeas. Suam denique cuique naturam esse ad vivendum ducem. Tecum optime, deinde etiam cum mediocri amico. Quid vero? Antiquorum autem sententiam Antiochus noster mihi videtur persequi diligentissime, quam eandem Aristoteli fuisse et Polemonis docet. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Stoici scilicet. An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit? Beatus sibi videtur esse moriens. Itaque his sapiens semper vacabit. Sed tu istuc dixti bene Latine, parum plane. 


	Et tamen quid attinet luxuriosis ullam exceptionem dari aut fingere aliquos, qui, cum luxuriose viverent, a summo philosopho non reprehenderentur eo nomine dumtaxat, cetera caverent?



Si qua in iis corrigere voluit, deteriora fecit. Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Prodest, inquit, mihi eo esse animo. An haec ab eo non dicuntur? Expectoque quid ad id, quod quaerebam, respondeas. Aliter homines, aliter philosophos loqui putas oportere? At, illa, ut vobis placet, partem quandam tuetur, reliquam deserit. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? 


	Graece donan, Latine voluptatem vocant.
	Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
	Haec igitur Epicuri non probo, inquam.
	An hoc usque quaque, aliter in vita?




	Immo videri fortasse.
	Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus.
	Erat enim Polemonis.
	Ille igitur vidit, non modo quot fuissent adhuc philosophorum de summo bono, sed quot omnino esse possent sententiae.
	Nihil sane.
	Tuo vero id quidem, inquam, arbitratu.
	Memini vero, inquam;
	Et summatim quidem haec erant de corpore animoque dicenda, quibus quasi informatum est quid hominis natura postulet.
	Facete M.
	Hic ambiguo ludimur.




