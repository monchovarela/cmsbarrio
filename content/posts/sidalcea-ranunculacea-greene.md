Title: Sidalcea ranunculacea Greene
Description: Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.
Image: https://source.unsplash.com/random/?night
Date: 15/11/2020
Keywords: design
Category: Fixflex
Author: Wikizz
Tags: work
Published: true
Background: #65f8ed
Color: #73bc79
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nec vero hoc oratione solum, sed multo magis vita et factis et moribus comprobavit. Vitae autem degendae ratio maxime quidem illis placuit quieta. Quos quidem tibi studiose et diligenter tractandos magnopere censeo. Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Iam id ipsum absurdum, maximum malum neglegi. Duo Reges: constructio interrete. At ille pellit, qui permulcet sensum voluptate. Quis istud possit, inquit, negare? 


	Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere.



At ille pellit, qui permulcet sensum voluptate. Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Sin autem eos non probabat, quid attinuit cum iis, quibuscum re concinebat, verbis discrepare? Quorum sine causa fieri nihil putandum est. Ita fit cum gravior, tum etiam splendidior oratio. Animi enim quoque dolores percipiet omnibus partibus maiores quam corporis. Addidisti ad extremum etiam indoctum fuisse. Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur. 


	Atque ab isto capite fluere necesse est omnem rationem bonorum et malorum.
	Expressa vero in iis aetatibus, quae iam confirmatae sunt.
	Confecta res esset.
	Si de re disceptari oportet, nulla mihi tecum, Cato, potest esse dissensio.
	Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur?
	Age nunc isti doceant, vel tu potius quis enim ista melius?



In contemplatione et cognitione posita rerum, quae quia deorum erat vitae simillima, sapiente visa est dignissima. Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Conferam avum tuum Drusum cum C. Dicimus aliquem hilare vivere; Ut optime, secundum naturam affectum esse possit. Unum est sine dolore esse, alterum cum voluptate. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Nemo igitur esse beatus potest. Nam adhuc, meo fortasse vitio, quid ego quaeram non perspicis. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Paupertas si malum est, mendicus beatus esse nemo potest, quamvis sit sapiens. 


Cognitis autem rerum finibus, cum intellegitur, quid sit et
bonorum extremum et malorum, inventa vitae via est
conformatioque omnium officiorum, cum quaeritur, quo quodque
referatur;

Omnes enim iucundum motum, quo sensus hilaretur.




	Avaritiamne minuis?
	Ita multo sanguine profuso in laetitia et in victoria est mortuus.
	In schola desinis.
	Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur.
	Immo alio genere;
	An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia?
	Quid vero?
	Maximas vero virtutes iacere omnis necesse est voluptate dominante.
	Pollicetur certe.
	Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere?




	Est enim effectrix multarum et magnarum voluptatum.
	Ita multo sanguine profuso in laetitia et in victoria est mortuus.
	Sed plane dicit quod intellegit.
	Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia;
	Tecum optime, deinde etiam cum mediocri amico.



An potest, inquit ille, quicquam esse suavius quam nihil dolere? Venit enim mihi Platonis in mentem, quem accepimus primum hic disputare solitum; An vero, inquit, quisquam potest probare, quod perceptfum, quod. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Immo alio genere; 

Est autem a te semper dictum nec gaudere quemquam nisi propter corpus nec dolere. Te enim iudicem aequum puto, modo quae dicat ille bene noris. Torquatus, is qui consul cum Cn. Quod cum dixissent, ille contra. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. 

Hoc simile tandem est? Equidem e Cn. Non potes, nisi retexueris illa. Igitur ne dolorem quidem. Beatum, inquit. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Id quaeris, inquam, in quo, utrum respondero, verses te huc atque illuc necesse est. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. 


	Potius inflammat, ut coercendi magis quam dedocendi esse videantur.
	Quo studio Aristophanem putamus aetatem in litteris duxisse?



Ut aliquid scire se gaudeant? Quicquid porro animo cernimus, id omne oritur a sensibus; Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Itaque fecimus. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? 

Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile. Estne, quaeso, inquam, sitienti in bibendo voluptas? Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est. Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Omnis enim est natura diligens sui. Quae cum dixisset paulumque institisset, Quid est? Sit enim idem caecus, debilis. Solum praeterea formosum, solum liberum, solum civem, stultost; 


	Hunc vos beatum;
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.
	Stoicos roga.
	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;



Suo genere perveniant ad extremum; Non potes, nisi retexueris illa. Longum est enim ad omnia respondere, quae a te dicta sunt. Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. 


	Iam argumenti ratione conclusi caput esse faciunt ea, quae perspicua dicunt, deinde ordinem sequuntur, tum, quid verum sit in singulis, extrema conclusio est.



Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Non est igitur voluptas bonum. Deinde dolorem quem maximum? Torquatus, is qui consul cum Cn. Quod autem magnum dolorem brevem, longinquum levem esse dicitis, id non intellego quale sit. Invidiosum nomen est, infame, suspectum. Illi enim inter se dissentiunt. Pugnant Stoici cum Peripateticis. 


Theophrasti igitur, inquit, tibi liber ille placet de beata
vita?

Ita relinquitur sola haec disciplina digna studiosis
ingenuarum artium, digna eruditis, digna claris viris, digna
principibus, digna regibus.




	Hoc ne statuam quidem dicturam pater aiebat, si loqui posset.
	Ego vero isti, inquam, permitto.




