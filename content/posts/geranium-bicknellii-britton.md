Title: Geranium bicknellii Britton
Description: In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
Image: https://source.unsplash.com/random/?green
Date: 28/8/2021
Keywords: blog
Category: Greenlam
Author: Gabspot
Tags: cms
Published: true
Background: #a36fd8
Color: #eda96b
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quasi ego id curem, quid ille aiat aut neget. Esse enim quam vellet iniquus iustus poterat inpune. Negare non possum. Recte, inquit, intellegis. Sit ista in Graecorum levitate perversitas, qui maledictis insectantur eos, a quibus de veritate dissentiunt. Duo Reges: constructio interrete. Claudii libidini, qui tum erat summo ne imperio, dederetur. Illa videamus, quae a te de amicitia dicta sunt. 


	Virtutis enim beataeque vitae, quae duo maxime expetenda sunt, serius lumen apparet, multo etiam serius, ut plane qualia sint intellegantur.




	Ubi ut eam caperet aut quando?
	Quo modo autem optimum, si bonum praeterea nullum est?
	Sed ut iis bonis erigimur, quae expectamus, sic laetamur iis, quae recordamur.
	Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse.
	Quis enim redargueret?
	Laelius clamores sof�w ille so lebat Edere compellans gumias ex ordine nostros.



Utilitatis causa amicitia est quaesita. Memini me adesse P. Itaque e contrario moderati aequabilesque habitus, affectiones ususque corporis apti esse ad naturam videntur. Utrum igitur tibi litteram videor an totas paginas commovere? Ut aliquid scire se gaudeant? Hic nihil fuit, quod quaereremus. Ut in geometria, prima si dederis, danda sunt omnia. Duarum enim vitarum nobis erunt instituta capienda. Iam id ipsum absurdum, maximum malum neglegi. Beatum, inquit. 

Quis hoc dicit? Vide, quantum, inquam, fallare, Torquate. Scaevolam M. Falli igitur possumus. Itaque hic ipse iam pridem est reiectus; Non est enim vitium in oratione solum, sed etiam in moribus. 

Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Minime id quidem, inquam, alienum, multumque ad ea, quae quaerimus, explicatio tua ista profecerit. Nunc haec primum fortasse audientis servire debemus. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est. Illa argumenta propria videamus, cur omnia sint paria peccata. 

Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. Nihil sane. An tu me de L. Aliud igitur esse censet gaudere, aliud non dolere. At iam decimum annum in spelunca iacet. Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Videsne quam sit magna dissensio? Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere. 


	Satisne vobis videor pro meo iure in vestris auribus commentatus?
	Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego;
	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.
	An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia?




	Quae sequuntur igitur?
	Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos pertinere?
	Scaevolam M.
	Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest.
	Pollicetur certe.
	Sed quamvis comis in amicis tuendis fuerit, tamen, si haec vera sunt-nihil enim affirmo-, non satis acutus fuit.
	Quo tandem modo?
	Nescio quo modo praetervolavit oratio.




Aliis esse maiora, illud dubium, ad id, quod summum bonum
dicitis, ecquaenam possit fieri accessio.

Sin autem eos non probabat, quid attinuit cum iis, quibuscum
re concinebat, verbis discrepare?



Immo alio genere; Illum mallem levares, quo optimum atque humanissimum virum, Cn. Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. Sed quod proximum fuit non vidit. Iam enim adesse poterit. Gerendus est mos, modo recte sentiat. 


	Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium.



Beatus autem esse in maximarum rerum timore nemo potest. Tibi hoc incredibile, quod beatissimum. Restinguet citius, si ardentem acceperit. Ita nemo beato beatior. Sit hoc ultimum bonorum, quod nunc a me defenditur; Sedulo, inquam, faciam. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. 


	Itaque rursus eadem ratione, qua sum paulo ante usus, haerebitis.
	Num igitur eum postea censes anxio animo aut sollicito fuisse?
	Egone quaeris, inquit, quid sentiam?
	Eodem modo is enim tibi nemo dabit, quod, expetendum sit, id esse laudabile.
	Paulum, cum regem Persem captum adduceret, eodem flumine invectio?
	Utram tandem linguam nescio?



Sed ego in hoc resisto; Non est ista, inquam, Piso, magna dissensio. Bestiarum vero nullum iudicium puto. Illud urgueam, non intellegere eum quid sibi dicendum sit, cum dolorem summum malum esse dixerit. Hic nihil fuit, quod quaereremus. Vestri haec verecundius, illi fortasse constantius. 

Vestri haec verecundius, illi fortasse constantius. Ergo omni animali illud, quod appetiti positum est in eo, quod naturae est accommodatum. Non enim ipsa genuit hominem, sed accepit a natura inchoatum. At enim, qua in vita est aliquid mali, ea beata esse non potest. Illis videtur, qui illud non dubitant bonum dicere -; Id est enim, de quo quaerimus. Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. 

Quis hoc dicit? Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Quid ait Aristoteles reliquique Platonis alumni? -, sed ut hoc iudicaremus, non esse in iis partem maximam positam beate aut secus vivendi. Pauca mutat vel plura sane; Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. At certe gravius. 


	Quamquam haec quidem praeposita recte et reiecta dicere licebit.
	Qua ex cognitione facilior facta est investigatio rerum occultissimarum.
	Non modo carum sibi quemque, verum etiam vehementer carum esse?
	Nam memini etiam quae nolo, oblivisci non possum quae volo.
	Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres.
	Paulum, cum regem Persem captum adduceret, eodem flumine invectio?




	Quare attende, quaeso.
	Cupiditates non Epicuri divisione finiebat, sed sua satietate.
	Quo tandem modo?
	Quis istud, quaeso, nesciebat?
	Quid de Pythagora?
	Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi;
	Stoici scilicet.
	Paupertas si malum est, mendicus beatus esse nemo potest, quamvis sit sapiens.
	Explanetur igitur.
	Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus;
	Quid adiuvas?
	Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest.




Ab hoc autem quaedam non melius quam veteres, quaedam omnino
relicta.

Hoc foedus facere si potuerunt, faciant etiam illud, ut
aequitatem, modestiam, virtutes omnes per se ipsas gratis
diligant.




