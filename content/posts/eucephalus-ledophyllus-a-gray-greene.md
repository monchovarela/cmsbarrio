Title: Eucephalus ledophyllus (A. Gray) Greene
Description: Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.
Image: https://source.unsplash.com/random/?ruby
Date: 14/7/2021
Keywords: cms
Category: Holdlamis
Author: Jayo
Tags: cms
Published: true
Background: #7074c1
Color: #24af51
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat Epicurus voluptatem. Si enim ad populum me vocas, eum. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Duo Reges: constructio interrete. 

Non est igitur voluptas bonum. Minime vero, inquit ille, consentit. An vero, inquit, quisquam potest probare, quod perceptfum, quod. Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Ecce aliud simile dissimile. 

Cave putes quicquam esse verius. An est aliquid, quod te sua sponte delectet? Nihil enim iam habes, quod ad corpus referas; Non igitur bene. Illud urgueam, non intellegere eum quid sibi dicendum sit, cum dolorem summum malum esse dixerit. Ne discipulum abducam, times. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Quae autem natura suae primae institutionis oblita est? 


	Tanta vis admonitionis inest in locis;
	Omnia contraria, quos etiam insanos esse vultis.
	Non laboro, inquit, de nomine.



Solum praeterea formosum, solum liberum, solum civem, stultost; Nam si amitti vita beata potest, beata esse non potest. Ergo, inquit, tibi Q. Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet; Tuo vero id quidem, inquam, arbitratu. Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Dat enim intervalla et relaxat. 


	Deque his rebus satis multa in nostris de re publica libris sunt dicta a Laelio.
	Apud ceteros autem philosophos, qui quaesivit aliquid, tacet;
	Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.
	Et ille ridens: Video, inquit, quid agas;
	Dolere malum est: in crucem qui agitur, beatus esse non potest.
	Itaque sensibus rationem adiunxit et ratione effecta sensus non reliquit.




Unum, cum in voluptate sumus, alterum, cum in dolore,
tertium hoc, in quo nunc equidem sum, credo item vos, nec in
dolore nec in voluptate;

Ergo in iis adolescentibus bonam spem esse dicemus et magnam
indolem, quos suis commodis inservituros et quicquid ipsis
expediat facturos arbitrabimur?



Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Si enim ita est, vide ne facinus facias, cum mori suadeas. Satis est ad hoc responsum. Quid est, quod ab ea absolvi et perfici debeat? Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Haec non erant eius, qui innumerabilis mundos infinitasque regiones, quarum nulla esset ora, nulla extremitas, mente peragravisset. Tuo vero id quidem, inquam, arbitratu. Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur. Aut, Pylades cum sis, dices te esse Orestem, ut moriare pro amico? An hoc usque quaque, aliter in vita? 


	At, illa, ut vobis placet, partem quandam tuetur, reliquam deserit.
	Bonum patria: miserum exilium.




	Idemne, quod iucunde?
	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.
	Et ille ridens: Video, inquit, quid agas;
	Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim?




	Quia, cum a Zenone, inquam, hoc magnifice tamquam ex oraculo editur: Virtus ad beate vivendum se ipsa contenta est, et Quare?



Effluit igitur voluptas corporis et prima quaeque avolat saepiusque relinquit causam paenitendi quam recordandi. Nam de isto magna dissensio est. Immo alio genere; Utram tandem linguam nescio? 

Aliter enim nosmet ipsos nosse non possumus. Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Eaedem res maneant alio modo. Quamquam te quidem video minime esse deterritum. De quibus cupio scire quid sentias. Esse enim quam vellet iniquus iustus poterat inpune. Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt. Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos pertinere? Peccata paria. Theophrasti igitur, inquit, tibi liber ille placet de beata vita? 

Nam ante Aristippus, et ille melius. Nos commodius agimus. Vadem te ad mortem tyranno dabis pro amico, ut Pythagoreus ille Siculo fecit tyranno? Utilitatis causa amicitia est quaesita. Unum est sine dolore esse, alterum cum voluptate. Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Philosophi autem in suis lectulis plerumque moriuntur. Quid de Platone aut de Democrito loquar? 

Illo enim addito iuste fit recte factum, per se autem hoc ipsum reddere in officio ponitur. Quam ob rem tandem, inquit, non satisfacit? Apparet statim, quae sint officia, quae actiones. Facile est hoc cernere in primis puerorum aetatulis. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; 


	Mihi quidem etiam lautius videtur, quod eligitur, et ad quod dilectus adhibetur -, sed, cum ego ista omnia bona dixero, tantum refert quam magna dicam, cum expetenda, quam valde.




	Itaque fecimus.
	Igitur neque stultorum quisquam beatus neque sapientium non beatus.
	Haec dicuntur inconstantissime.
	Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia?
	Cyrenaici quidem non recusant;
	Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum.




Cui vero in voluptate summum bonum est, huic omnia sensu,
non ratione sunt iudicanda, eaque dicenda optima, quae sint
suavissima.

Huic mori optimum esse propter desperationem sapientiae,
illi propter spem vivere.




	Ut pulsi recurrant?
	At enim sequor utilitatem.
	Peccata paria.
	Et ille ridens: Video, inquit, quid agas;
	Omnia peccata paria dicitis.
	Qui autem de summo bono dissentit de tota philosophiae ratione dissentit.
	Tria genera bonorum;
	Illud mihi a te nimium festinanter dictum videtur, sapientis omnis esse semper beatos;



Scrupulum, inquam, abeunti; A quibus propter discendi cupiditatem videmus ultimas terras esse peragratas. At ille pellit, qui permulcet sensum voluptate. Tu quidem reddes; Nihilne te delectat umquam -video, quicum loquar-, te igitur, Torquate, ipsum per se nihil delectat? Teneo, inquit, finem illi videri nihil dolere. Invidiosum nomen est, infame, suspectum. Et harum quidem rerum facilis est et expedita distinctio. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Sed vobis voluptatum perceptarum recordatio vitam beatam facit, et quidem corpore perceptarum. 


