Title: Trifolium subterraneum L.
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?cat
Date: 29/7/2021
Keywords: web
Category: Cardguard
Author: Aivee
Tags: design
Published: true
Background: #150e95
Color: #9759b7
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo Reges: constructio interrete. Quamquam tu hanc copiosiorem etiam soles dicere. Unum est sine dolore esse, alterum cum voluptate. Si id dicis, vicimus. Universa enim illorum ratione cum tota vestra confligendum puto. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Egone quaeris, inquit, quid sentiam? Quamquam tu hanc copiosiorem etiam soles dicere. Nam adhuc, meo fortasse vitio, quid ego quaeram non perspicis. 

Venit ad extremum; Utilitatis causa amicitia est quaesita. Quid est igitur, inquit, quod requiras? Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus. Sit sane ista voluptas. Venit ad extremum; Sed ad bona praeterita redeamus. Et nemo nimium beatus est; Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. 

Qui enim existimabit posse se miserum esse beatus non erit. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Quo tandem modo? Sed quia studebat laudi et dignitati, multum in virtute processerat. Sed quod proximum fuit non vidit. Stuprata per vim Lucretia a regis filio testata civis se ipsa interemit. 

Sin aliud quid voles, postea. Nos commodius agimus. Quis Aristidem non mortuum diligit? A primo, ut opinor, animantium ortu petitur origo summi boni. Itaque hic ipse iam pridem est reiectus; Sed tamen intellego quid velit. Idemne, quod iucunde? Fortasse id optimum, sed ubi illud: Plus semper voluptatis? An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Quamquam tu hanc copiosiorem etiam soles dicere. 

Ut proverbia non nulla veriora sint quam vestra dogmata. Quod si ita se habeat, non possit beatam praestare vitam sapientia. Cur deinde Metrodori liberos commendas? Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. 


	Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus;
	Quae cum dixisset, finem ille.



Quippe: habes enim a rhetoribus; Sed potestne rerum maior esse dissensio? Totum genus hoc Zeno et qui ab eo sunt aut non potuerunt aut noluerunt, certe reliquerunt. Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? 


Hoc est dicere: Non reprehenderem asotos, si non essent
asoti.

Nec enim, omnes avaritias si aeque avaritias esse dixerimus,
sequetur ut etiam aequas esse dicamus.




	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.
	Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D.
	Paulum, cum regem Persem captum adduceret, eodem flumine invectio?
	Inde sermone vario sex illa a Dipylo stadia confecimus.
	Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.



Restinguet citius, si ardentem acceperit. An potest cupiditas finiri? Sullae consulatum? Aliter enim nosmet ipsos nosse non possumus. Verba tu fingas et ea dicas, quae non sentias? Quid est, quod ab ea absolvi et perfici debeat? 

Cur igitur, inquam, res tam dissimiles eodem nomine appellas? De ingenio eius in his disputationibus, non de moribus quaeritur. De hominibus dici non necesse est. Primum in nostrane potestate est, quid meminerimus? 


	Memini me adesse P.
	Istam voluptatem, inquit, Epicurus ignorat?
	Stoicos roga.
	Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus;
	Etiam beatissimum?
	Quid est, quod ab ea absolvi et perfici debeat?
	Beatum, inquit.
	Si enim non fuit eorum iudicii, nihilo magis hoc non addito illud est iudicatum-.



Quae sequuntur igitur? Sine ea igitur iucunde negat posse se vivere? Quid enim? Omnes enim iucundum motum, quo sensus hilaretur. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; Cyrenaici quidem non recusant; Huius, Lyco, oratione locuples, rebus ipsis ielunior. Igitur ne dolorem quidem. Omnes enim iucundum motum, quo sensus hilaretur. Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. 


	Iam id ipsum absurdum, maximum malum neglegi.
	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Nihil ad rem! Ne sit sane;
	Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero.




	Quodsi esset in voluptate summum bonum, ut dicitis, optabile esset maxima in voluptate nullo intervallo interiecto dies noctesque versari, cum omnes sensus dulcedine omni quasi perfusi moverentur.



Tu enim ista lenius, hic Stoicorum more nos vexat. Etsi qui potest intellegi aut cogitari esse aliquod animal, quod se oderit? Si qua in iis corrigere voluit, deteriora fecit. Pugnant Stoici cum Peripateticis. 


	Sedulo, inquam, faciam.
	Suo genere perveniant ad extremum;
	Quonam modo?
	An est aliquid, quod te sua sponte delectet?
	Tu quidem reddes;
	Eorum enim est haec querela, qui sibi cari sunt seseque diligunt.
	Pollicetur certe.
	Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit.




	Aderamus nos quidem adolescentes, sed multi amplissimi viri, quorum nemo censuit plus Fadiae dandum, quam posset ad eam lege Voconia pervenire.




	Isto modo ne improbos quidem, si essent boni viri.
	Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Atqui perspicuum est hominem e corpore animoque constare, cum primae sint animi partes, secundae corporis.
	Consequentia exquirere, quoad sit id, quod volumus, effectum.




Quid enim necesse est, tamquam meretricem in matronarum
coetum, sic voluptatem in virtutum concilium adducere?

Sed non alienum est, quo facilius vis verbi intellegatur,
rationem huius verbi faciendi Zenonis exponere.




