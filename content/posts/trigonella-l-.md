Title: Trigonella L.
Description: Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.
Image: https://source.unsplash.com/random/?blue
Date: 23/8/2021
Keywords: portfolio
Category: Voyatouch
Author: Oloo
Tags: blog
Published: true
Background: #39b7ea
Color: #bf1160
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae cum dixisset paulumque institisset, Quid est? Quae cum magnifice primo dici viderentur, considerata minus probabantur. Cetera illa adhibebat, quibus demptis negat se Epicurus intellegere quid sit bonum. Quis non odit sordidos, vanos, leves, futtiles? Duo Reges: constructio interrete. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. 

Nihil ad rem! Ne sit sane; Tu vero, inquam, ducas licet, si sequetur; Ego vero volo in virtute vim esse quam maximam; Res enim se praeclare habebat, et quidem in utraque parte. Verba tu fingas et ea dicas, quae non sentias? Beatus autem esse in maximarum rerum timore nemo potest. Vitae autem degendae ratio maxime quidem illis placuit quieta. Falli igitur possumus. 

Quae similitudo in genere etiam humano apparet. Itaque in rebus minime obscuris non multus est apud eos disserendi labor. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Ut non sine causa ex iis memoriae ducta sit disciplina. Cave putes quicquam esse verius. 


	Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere.
	Quia dolori non voluptas contraria est, sed doloris privatio.
	Quamquam non negatis nos intellegere quid sit voluptas, sed quid ille dicat.
	Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur;
	Inde sermone vario sex illa a Dipylo stadia confecimus.



Dolere malum est: in crucem qui agitur, beatus esse non potest. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Omnes enim iucundum motum, quo sensus hilaretur. Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Occultum facinus esse potuerit, gaudebit; Si autem id non concedatur, non continuo vita beata tollitur. Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. Nam, ut paulo ante docui, augendae voluptatis finis est doloris omnis amotio. 


De quibus cupio scire quid sentias.

Nos autem non solum beatae vitae istam esse oblectationem
videmus, sed etiam levamentum miseriarum.




	Itaque non ob ea solum incommoda, quae eveniunt inprobis, fugiendam inprobitatem putamus, sed multo etiam magis, quod, cuius in animo versatur, numquam sinit eum respirare, numquam adquiescere.




	Esse enim quam vellet iniquus iustus poterat inpune.
	Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare?




	Scaevolam M.
	Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L.
	Sullae consulatum?
	Ergo et avarus erit, sed finite, et adulter, verum habebit modum, et luxuriosus eodem modo.
	Eam stabilem appellas.
	Inde sermone vario sex illa a Dipylo stadia confecimus.



Non quam nostram quidem, inquit Pomponius iocans; Istam voluptatem perpetuam quis potest praestare sapienti? Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Scaevolam M. 


	Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.
	Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem?
	Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi.
	An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit?
	Atqui iste locus est, Piso, tibi etiam atque etiam confirmandus, inquam;




	Perfecto enim et concluso neque virtutibus neque amicitiis usquam locum esse, si ad voluptatem omnia referantur, nihil praeterea est magnopere dicendum.



Videsne, ut haec concinant? Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore. Quid, si etiam iucunda memoria est praeteritorum malorum? Quam si explicavisset, non tam haesitaret. De quibus etsi a Chrysippo maxime est elaboratum, tamen a Zenone minus multo quam ab antiquis; Verum hoc idem saepe faciamus. Tum Triarius: Posthac quidem, inquit, audacius. Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem. Ergo, si semel tristior effectus est, hilara vita amissa est? Utrum igitur tibi litteram videor an totas paginas commovere? 


Tu autem inter haec tantam multitudinem hominum interiectam
non vides nec laetantium nec dolentium?

Sed non alienum est, quo facilius vis verbi intellegatur,
rationem huius verbi faciendi Zenonis exponere.




	Hic ambiguo ludimur.
	Sed potestne rerum maior esse dissensio?
	Etiam beatissimum?
	Uterque enim summo bono fruitur, id est voluptate.



Dic in quovis conventu te omnia facere, ne doleas. Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Idem iste, inquam, de voluptate quid sentit? Quo igitur, inquit, modo? Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Inscite autem medicinae et gubernationis ultimum cum ultimo sapientiae comparatur. Neque enim civitas in seditione beata esse potest nec in discordia dominorum domus; Ne amores quidem sanctos a sapiente alienos esse arbitrantur. Sed ego in hoc resisto; Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. Quis enim est, qui non videat haec esse in natura rerum tria? 

Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Ille vero, si insipiens-quo certe, quoniam tyrannus -, numquam beatus; Cur id non ita fit? Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Quasi ego id curem, quid ille aiat aut neget. Suam denique cuique naturam esse ad vivendum ducem. Idemne, quod iucunde? Recte, inquit, intellegis. Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Hoc loco tenere se Triarius non potuit. 


	Vidit Homerus probari fabulam non posse, si cantiunculis tantus irretitus vir teneretur;
	Tu enim ista lenius, hic Stoicorum more nos vexat.
	Aut unde est hoc contritum vetustate proverbium: quicum in tenebris?
	Nam his libris eum malo quam reliquo ornatu villae delectari.



Que Manilium, ab iisque M. Nos paucis ad haec additis finem faciamus aliquando; Ait enim se, si uratur, Quam hoc suave! dicturum. Si enim non fuit eorum iudicii, nihilo magis hoc non addito illud est iudicatum-. Ampulla enim sit necne sit, quis non iure optimo irrideatur, si laboret? 

Sed ad haec, nisi molestum est, habeo quae velim. Duarum enim vitarum nobis erunt instituta capienda. Quae quidem vel cum periculo est quaerenda vobis; Qui ita affectus, beatum esse numquam probabis; Nam si propter voluptatem, quae est ista laus, quae possit e macello peti? 


