Title: Verbena urticifolia L. var. leiocarpa L.M. Perry & Fernald
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?girl
Date: 19/9/2021
Keywords: blog
Category: Viva
Author: Devify
Tags: cms
Published: true
Background: #1b57d6
Color: #6cf1d5
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Videamus animi partes, quarum est conspectus illustrior; Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Zenonis est, inquam, hoc Stoici. Duo Reges: constructio interrete. Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest. Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim? 


Aliis esse maiora, illud dubium, ad id, quod summum bonum
dicitis, ecquaenam possit fieri accessio.

A quibus propter discendi cupiditatem videmus ultimas terras
esse peragratas.




	Recte, inquit, intellegis.
	Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?
	Non igitur bene.
	Aufert enim sensus actionemque tollit omnem.
	Audeo dicere, inquit.
	Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate.




Estne, quaeso, inquam, sitienti in bibendo voluptas?

Ergo instituto veterum, quo etiam Stoici utuntur, hinc
capiamus exordium.




	Negare non possum.
	Claudii libidini, qui tum erat summo ne imperio, dederetur.
	Quonam, inquit, modo?
	Epicurus autem cum in prima commendatione voluptatem dixisset, si eam, quam Aristippus, idem tenere debuit ultimum bonorum, quod ille;
	Praeclare hoc quidem.
	Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare?
	Etiam beatissimum?
	Aeque enim contingit omnibus fidibus, ut incontentae sint.




	Nam bonum ex quo appellatum sit, nescio, praepositum ex eo credo, quod praeponatur aliis.



Nihilo beatiorem esse Metellum quam Regulum. Sed haec quidem liberius ab eo dicuntur et saepius. Suam denique cuique naturam esse ad vivendum ducem. Tanta vis admonitionis inest in locis; Contemnit enim disserendi elegantiam, confuse loquitur. Et nemo nimium beatus est; Praeclare hoc quidem. An vero, inquit, quisquam potest probare, quod perceptfum, quod. Quo modo autem optimum, si bonum praeterea nullum est? 

Mihi enim erit isdem istis fortasse iam utendum. Proclivi currit oratio. Nam, ut paulo ante docui, augendae voluptatis finis est doloris omnis amotio. Quae animi affectio suum cuique tribuens atque hanc, quam dico. Quae cum essent dicta, finem fecimus et ambulandi et disputandi. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Equidem soleo etiam quod uno Graeci, si aliter non possum, idem pluribus verbis exponere. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Ista ipsa, quae tu breviter: regem, dictatorem, divitem solum esse sapientem, a te quidem apte ac rotunde; 


	Itaque et manendi in vita et migrandi ratio omnis iis rebus, quas supra dixi, metienda.
	Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus.
	At Zeno eum non beatum modo, sed etiam divitem dicere ausus est.
	Negabat igitur ullam esse artem, quae ipsa a se proficisceretur;
	Cum audissem Antiochum, Brute, ut solebam, cum M.
	Quae dici eadem de ceteris virtutibus possunt, quarum omnium fundamenta vos in voluptate tamquam in aqua ponitis.



Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Quo modo autem philosophus loquitur? Estne, quaeso, inquam, sitienti in bibendo voluptas? Primum quid tu dicis breve? Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Sed haec omittamus; Facile est hoc cernere in primis puerorum aetatulis. Et quod est munus, quod opus sapientiae? Sed non alienum est, quo facilius vis verbi intellegatur, rationem huius verbi faciendi Zenonis exponere. Gracchum patrem non beatiorem fuisse quam fillum, cum alter stabilire rem publicam studuerit, alter evertere. Sed hoc sane concedamus. 


	Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium.
	Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est.




	Et tamen puto concedi nobis oportere ut Graeco verbo utamur, si quando minus occurret Latinum, ne hoc ephippiis et acratophoris potius quam proegmenis et apoproegmenis concedatur;




	Non dolere, inquam, istud quam vim habeat postea videro;
	Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem.
	Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam;



Restatis igitur vos; Bona autem corporis huic sunt, quod posterius posui, similiora. Quorum altera prosunt, nocent altera. An tu me de L. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus; Praeclare enim Plato: Beatum, cui etiam in senectute contigerit, ut sapientiam verasque opiniones assequi possit. Nam Metrodorum non puto ipsum professum, sed, cum appellaretur ab Epicuro, repudiare tantum beneficium noluisse; Atqui reperies, inquit, in hoc quidem pertinacem; Pauca mutat vel plura sane; Quia dolori non voluptas contraria est, sed doloris privatio. 


	Nam si amitti vita beata potest, beata esse non potest.
	Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem?
	Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum;
	Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam.
	Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse;



Quae si potest singula consolando levare, universa quo modo sustinebit? Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. Aliud igitur esse censet gaudere, aliud non dolere. Dat enim intervalla et relaxat. Sumenda potius quam expetenda. Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere. 

Quod si ita sit, cur opera philosophiae sit danda nescio. Manebit ergo amicitia tam diu, quam diu sequetur utilitas, et, si utilitas amicitiam constituet, tollet eadem. Si enim ad populum me vocas, eum. Quo modo autem optimum, si bonum praeterea nullum est? Quamquam tu hanc copiosiorem etiam soles dicere. 

Quod iam a me expectare noli. Sed quod proximum fuit non vidit. Ac tamen hic mallet non dolere. Ille enim occurrentia nescio quae comminiscebatur; Equidem, sed audistine modo de Carneade? Quis istud, quaeso, nesciebat? Eadem fortitudinis ratio reperietur. Quare si potest esse beatus is, qui est in asperis reiciendisque rebus, potest is quoque esse. Huius ego nunc auctoritatem sequens idem faciam. 

Quae cum dixisset, finem ille. Quid nunc honeste dicit? Laelius clamores sof�w ille so lebat Edere compellans gumias ex ordine nostros. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Id est enim, de quo quaerimus. 

Si id dicis, vicimus. Nosti, credo, illud: Nemo pius est, qui pietatem-; Utrum igitur tibi litteram videor an totas paginas commovere? Quippe: habes enim a rhetoribus; Maximus dolor, inquit, brevis est. Graece donan, Latine voluptatem vocant. Bonum integritas corporis: misera debilitas. Restatis igitur vos; Ut alios omittam, hunc appello, quem ille unum secutus est. 


