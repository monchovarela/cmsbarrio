Title: Xyris montana Ries
Description: Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.
Image: https://source.unsplash.com/random/?red
Date: 25/1/2021
Keywords: work
Category: Zoolab
Author: Skyndu
Tags: blog
Published: true
Background: #f41db4
Color: #064a54
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Non quam nostram quidem, inquit Pomponius iocans; Quo studio Aristophanem putamus aetatem in litteris duxisse? Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? Universa enim illorum ratione cum tota vestra confligendum puto. Duo Reges: constructio interrete. 

Mihi enim satis est, ipsis non satis. Non risu potius quam oratione eiciendum? Dici enim nihil potest verius. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Ergo in utroque exercebantur, eaque disciplina effecit tantam illorum utroque in genere dicendi copiam. Equidem e Cn. 


Temporibus autem quibusdam et aut officiis debitis aut rerum
necessitatibus saepe eveniet, ut et voluptates repudiandae
sint et molestiae non recusandae.

Addidisti ad extremum etiam indoctum fuisse.



Vitae autem degendae ratio maxime quidem illis placuit quieta. Res enim concurrent contrariae. Iam in altera philosophiae parte. Sed tu istuc dixti bene Latine, parum plane. Eiuro, inquit adridens, iniquum, hac quidem de re; 

Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster? Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit. 

Respondent extrema primis, media utrisque, omnia omnibus. Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Ita relinquet duas, de quibus etiam atque etiam consideret. Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. Quid enim possumus hoc agere divinius? Aliter enim explicari, quod quaeritur, non potest. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Nihil enim iam habes, quod ad corpus referas; Quam nemo umquam voluptatem appellavit, appellat; Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. 


	Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera.
	Aliter enim nosmet ipsos nosse non possumus.
	Maximus dolor, inquit, brevis est.
	Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.
	Ita prorsus, inquam;




	Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere?
	Quis est enim, in quo sit cupiditas, quin recte cupidus dici possit?
	Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare?



Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Si quidem, inquit, tollerem, sed relinquo. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Est, ut dicis, inquit; Graccho, eius fere, aequal�? Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Qui autem esse poteris, nisi te amor ipse ceperit? 


	Istud quidem, inquam, optime dicis, sed quaero nonne tibi faciendum idem sit nihil dicenti bonum, quod non rectum honestumque sit, reliquarum rerum discrimen omne tollenti.




	Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D.
	Sit enim idem caecus, debilis.
	Primum in nostrane potestate est, quid meminerimus?
	An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat?




	Ita credo.
	Earum etiam rerum, quas terra gignit, educatio quaedam et perfectio est non dissimilis animantium.
	Paria sunt igitur.
	Mihi quidem Antiochum, quem audis, satis belle videris attendere.



Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus. Primum quid tu dicis breve? Apparet statim, quae sint officia, quae actiones. Et quod est munus, quod opus sapientiae? 


Hoc vero non videre, maximo argumento esse voluptatem illam,
qua sublata neget se intellegere omnino quid sit bonum-eam
autem ita persequitur: quae palato percipiatur, quae
auribus;

Positum est a nostris in iis esse rebus, quae secundum
naturam essent, non dolere;



Quid loquor de nobis, qui ad laudem et ad decus nati, suscepti, instituti sumus? Qualem igitur hominem natura inchoavit? Iam contemni non poteris. Eam tum adesse, cum dolor omnis absit; 

Cave putes quicquam esse verius. Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? Non modo carum sibi quemque, verum etiam vehementer carum esse? Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere. Mihi enim satis est, ipsis non satis. Quaerimus enim finem bonorum. 

Quod cum ille dixisset et satis disputatum videretur, in oppidum ad Pomponium perreximus omnes. Quam illa ardentis amores excitaret sui! Cur tandem? Philosophi autem in suis lectulis plerumque moriuntur. Sed quanta sit alias, nunc tantum possitne esse tanta. Quid, si etiam iucunda memoria est praeteritorum malorum? Tum ille timide vel potius verecunde: Facio, inquit. Sed tamen intellego quid velit. Immo alio genere; Videsne quam sit magna dissensio? 


	Quia, si mala sunt, is, qui erit in iis, beatus non erit.




	Itaque fecimus.
	Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis.
	Quo tandem modo?
	Age nunc isti doceant, vel tu potius quis enim ista melius?
	Recte, inquit, intellegis.
	Unum est sine dolore esse, alterum cum voluptate.
	At certe gravius.
	Ergo in gubernando nihil, in officio plurimum interest, quo in genere peccetur.




	Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius.
	Itaque contra est, ac dicitis;
	Videmus in quodam volucrium genere non nulla indicia pietatis, cognitionem, memoriam, in multis etiam desideria videmus.
	Nam cui proposito sit conservatio sui, necesse est huic partes quoque sui caras suo genere laudabiles.
	Quid igitur, inquit, eos responsuros putas?




