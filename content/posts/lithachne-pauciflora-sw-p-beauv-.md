Title: Lithachne pauciflora (Sw.) P. Beauv.
Description: Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.
Image: https://source.unsplash.com/random/?yellow
Date: 23/6/2021
Keywords: post
Category: Holdlamis
Author: Kanoodle
Tags: design
Published: true
Background: #ccb81f
Color: #dbd228
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Et nemo nimium beatus est; Iam doloris medicamenta illa Epicurea tamquam de narthecio proment: Si gravis, brevis; Non est igitur summum malum dolor. Duo Reges: constructio interrete. Sic enim censent, oportunitatis esse beate vivere. Si enim ad populum me vocas, eum. 


Hic dolor populi Romani duce et auctore Bruto causa civitati
libertatis fuit, ob eiusque mulieris memoriam primo anno et
vir et pater eius consul est factus.

Etenim nec iustitia nec amicitia esse omnino poterunt, nisi
ipsae per se expetuntur.



Primum quid tu dicis breve? Etsi qui potest intellegi aut cogitari esse aliquod animal, quod se oderit? Expressa vero in iis aetatibus, quae iam confirmatae sunt. 

Non est enim vitium in oratione solum, sed etiam in moribus. Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba. Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia. Illud dico, ea, quae dicat, praeclare inter se cohaerere. Quoniam, si dis placet, ab Epicuro loqui discimus. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Haeret in salebra. 


	At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit;
	Eadem fortitudinis ratio reperietur.
	At vero illa, quae Peripatetici, quae Stoici dicunt, semper tibi in ore sunt in iudiciis, in senatu.
	Quid igitur dubitamus in tota eius natura quaerere quid sit effectum?
	Ita enim vivunt quidam, ut eorum vita refellatur oratio.
	Ait enim se, si uratur, Quam hoc suave! dicturum.




At quanta conantur! Mundum hunc omnem oppidum esse nostrum!
Incendi igitur eos, qui audiunt, vides.

Modo etiam paulum ad dexteram de via declinavi, ut ad
Pericli sepulcrum accederem.



Quis istud possit, inquit, negare? Si verbum sequimur, primum longius verbum praepositum quam bonum. Quod autem principium officii quaerunt, melius quam Pyrrho; Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Sed haec quidem liberius ab eo dicuntur et saepius. Homines optimi non intellegunt totam rationem everti, si ita res se habeat. Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. 

Quia nec honesto quic quam honestius nec turpi turpius. Illi enim inter se dissentiunt. Quae in controversiam veniunt, de iis, si placet, disseramus. Expectoque quid ad id, quod quaerebam, respondeas. Et nemo nimium beatus est; Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. Non quam nostram quidem, inquit Pomponius iocans; 


	Si quicquam extra virtutem habeatur in bonis.
	Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis;
	Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur.
	Qua igitur re ab deo vincitur, si aeternitate non vincitur?
	Sed ad bona praeterita redeamus.




	Et harum quidem rerum facilis est et expedita distinctio.
	Atqui pugnantibus et contrariis studiis consiliisque semper utens nihil quieti videre, nihil tranquilli potest.
	Quae cum magnifice primo dici viderentur, considerata minus probabantur.



Quorum sine causa fieri nihil putandum est. Illa tamen simplicia, vestra versuta. Tum Torquatus: Prorsus, inquit, assentior; Nemo nostrum istius generis asotos iucunde putat vivere. Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Omnia contraria, quos etiam insanos esse vultis. Aperiendum est igitur, quid sit voluptas; Qui est in parvis malis. 


	Quid adiuvas?
	Aliter enim nosmet ipsos nosse non possumus.
	Quibusnam praeteritis?
	Sed quae tandem ista ratio est?
	Quae sequuntur igitur?
	Nam, ut sint illa vendibiliora, haec uberiora certe sunt.
	Quid adiuvas?
	Est tamen ea secundum naturam multoque nos ad se expetendam magis hortatur quam superiora omnia.
	An eiusdem modi?
	Illa videamus, quae a te de amicitia dicta sunt.



Inde sermone vario sex illa a Dipylo stadia confecimus. Nulla erit controversia. Quae cum magnifice primo dici viderentur, considerata minus probabantur. Huius ego nunc auctoritatem sequens idem faciam. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Conferam avum tuum Drusum cum C. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Minime vero, inquit ille, consentit. Quicquid porro animo cernimus, id omne oritur a sensibus; Nam, ut sint illa vendibiliora, haec uberiora certe sunt. 

Itaque ad tempus ad Pisonem omnes. Cupiditates non Epicuri divisione finiebat, sed sua satietate. Quid est, quod ab ea absolvi et perfici debeat? Qua igitur re ab deo vincitur, si aeternitate non vincitur? Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; Atque his de rebus et splendida est eorum et illustris oratio. Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. Atqui reperies, inquit, in hoc quidem pertinacem; 

Id mihi magnum videtur. Quam ob rem tandem, inquit, non satisfacit? Frater et T. Nunc omni virtuti vitium contrario nomine opponitur. 


	Nos cum te, M.
	Recte, inquit, intellegis.
	Praeteritis, inquit, gaudeo.
	Amicitiae vero locus ubi esse potest aut quis amicus esse cuiquam, quem non ipsum amet propter ipsum?
	Falli igitur possumus.
	Dicet pro me ipsa virtus nec dubitabit isti vestro beato M.
	Istic sum, inquit.
	Hoc ille tuus non vult omnibusque ex rebus voluptatem quasi mercedem exigit.



Hunc vos beatum; Istam voluptatem perpetuam quis potest praestare sapienti? Quae animi affectio suum cuique tribuens atque hanc, quam dico. Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? Et harum quidem rerum facilis est et expedita distinctio. Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur. Negat esse eam, inquit, propter se expetendam. 


	Quamquam te quidem video minime esse deterritum.
	Servari enim iustitia nisi a forti viro, nisi a sapiente non potest.
	Idemque diviserunt naturam hominis in animum et corpus.
	Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat;
	Beatum, inquit.




	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;




	Semper enim ex eo, quod maximas partes continet latissimeque funditur, tota res appellatur.




