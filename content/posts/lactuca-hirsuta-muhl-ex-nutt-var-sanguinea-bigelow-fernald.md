Title: Lactuca hirsuta Muhl. ex Nutt. var. sanguinea (Bigelow) Fernald
Description: Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.
Image: https://source.unsplash.com/random/?modern
Date: 20/12/2020
Keywords: post
Category: Tres-Zap
Author: Edgewire
Tags: web
Published: true
Background: #c5ab99
Color: #77f649
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid, quod res alia tota est? An haec ab eo non dicuntur? Tanta vis admonitionis inest in locis; Utram tandem linguam nescio? Ostendit pedes et pectus. Eadem fortitudinis ratio reperietur. Duo Reges: constructio interrete. Itaque ad tempus ad Pisonem omnes. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? 


Quarum adeo omnium sententia pronuntiabit primum de
voluptate nihil esse ei loci, non modo ut sola ponatur in
summi boni sede, quam quaerimus, sed ne illo quidem modo, ut
ad honestatem applicetur.

Manebit ergo amicitia tam diu, quam diu sequetur utilitas,
et, si utilitas amicitiam constituet, tollet eadem.



Sed haec quidem liberius ab eo dicuntur et saepius. Totum autem id externum est, et quod externum, id in casu est. Recte, inquit, intellegis. Post enim Chrysippum eum non sane est disputatum. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Torquatus, is qui consul cum Cn. Idemne, quod iucunde? Idemne, quod iucunde? 


	Duo enim genera quae erant, fecit tria.
	Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus.
	Quid autem habent admirationis, cum prope accesseris?
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?



Vide, quaeso, rectumne sit. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Multoque hoc melius nos veriusque quam Stoici. Ratio quidem vestra sic cogit. Eaedem enim utilitates poterunt eas labefactare atque pervertere. Ista ipsa, quae tu breviter: regem, dictatorem, divitem solum esse sapientem, a te quidem apte ac rotunde; At iam decimum annum in spelunca iacet. Ubi ut eam caperet aut quando? 


	Adsint etiam formosi pueri, qui ministrent, respondeat his vestis, argentum, Corinthium, locus ipse, aedificium-hos ergo asotos bene quidem vivere aut beate numquam dixerim.




	Nihilo magis.
	Etiam beatissimum?
	Cur iustitia laudatur?
	Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q.



Istam voluptatem perpetuam quis potest praestare sapienti? Velut ego nunc moveor. Isto modo ne improbos quidem, si essent boni viri. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Illa enim, quae prosunt aut quae nocent, aut bona sunt aut mala, quae sint paria necesse est. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Beatus autem esse in maximarum rerum timore nemo potest. Erat enim res aperta. 

Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis? Etsi qui potest intellegi aut cogitari esse aliquod animal, quod se oderit? Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Aut unde est hoc contritum vetustate proverbium: quicum in tenebris? Refert tamen, quo modo. 


	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;
	Idem fecisset Epicurus, si sententiam hanc, quae nunc Hieronymi est, coniunxisset cum Aristippi vetere sententia.
	Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere?




	Quamquam id quidem, infinitum est in hac urbe;
	Iam id ipsum absurdum, maximum malum neglegi.
	Sit hoc ultimum bonorum, quod nunc a me defenditur;




	Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia.
	Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim.
	Nunc reliqua videamus, nisi aut ad haec, Cato, dicere aliquid vis aut nos iam longiores sumus.
	Ergo in gubernando nihil, in officio plurimum interest, quo in genere peccetur.
	An me, inquam, nisi te audire vellem, censes haec dicturum fuisse?




	Quibusnam praeteritis?
	Quae duo sunt, unum facit.
	Moriatur, inquit.
	Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat.



Tubulum fuisse, qua illum, cuius is condemnatus est rogatione, P. Hic nihil fuit, quod quaereremus. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Ac tamen hic mallet non dolere. Haec quo modo conveniant, non sane intellego. Est enim effectrix multarum et magnarum voluptatum. Idemne, quod iucunde? Erat enim res aperta. Inscite autem medicinae et gubernationis ultimum cum ultimo sapientiae comparatur. Aliam vero vim voluptatis esse, aliam nihil dolendi, nisi valde pertinax fueris, concedas necesse est. Maximas vero virtutes iacere omnis necesse est voluptate dominante. Sed ad bona praeterita redeamus. 

His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. Tu vero, inquam, ducas licet, si sequetur; Aliter enim explicari, quod quaeritur, non potest. Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Idemque diviserunt naturam hominis in animum et corpus. Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Huius ego nunc auctoritatem sequens idem faciam. 

Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Multoque hoc melius nos veriusque quam Stoici. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; Tamen aberramus a proposito, et, ne longius, prorsus, inquam, Piso, si ista mala sunt, placet. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Putabam equidem satis, inquit, me dixisse. Quid enim est a Chrysippo praetermissum in Stoicis? Aliter homines, aliter philosophos loqui putas oportere? 

Utrum igitur tibi litteram videor an totas paginas commovere? Illi enim inter se dissentiunt. Peccata paria. Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Ergo ita: non posse honeste vivi, nisi honeste vivatur? Mihi, inquam, qui te id ipsum rogavi? 

Equidem etiam Epicurum, in physicis quidem, Democriteum puto. Tecum optime, deinde etiam cum mediocri amico. Non quam nostram quidem, inquit Pomponius iocans; Commoda autem et incommoda in eo genere sunt, quae praeposita et reiecta diximus; Quid censes in Latino fore? Hoc ille tuus non vult omnibusque ex rebus voluptatem quasi mercedem exigit. Haec para/doca illi, nos admirabilia dicamus. Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Negat esse eam, inquit, propter se expetendam. 


	Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit.




Aequam igitur pronuntiabit sententiam ratio adhibita primum
divinarum humanarumque rerum scientia, quae potest appellari
rite sapientia, deinde adiunctis virtutibus, quas ratio
rerum omnium dominas, tu voluptatum satellites et ministras
esse voluisti.

Sed ad rem redeamus;




