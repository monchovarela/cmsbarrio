Title: Ipomopsis longiflora (Torr.) V.E. Grant ssp. australis Fletcher & W.L. Wagner
Description: Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.
Image: https://source.unsplash.com/random/?php
Date: 8/10/2021
Keywords: post
Category: Y-Solowarm
Author: Brightbean
Tags: article
Published: true
Background: #76d9af
Color: #856743
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Huius ego nunc auctoritatem sequens idem faciam. Duo Reges: constructio interrete. Piso igitur hoc modo, vir optimus tuique, ut scis, amantissimus. Sunt autem, qui dicant foedus esse quoddam sapientium, ut ne minus amicos quam se ipsos diligant. Quid est, quod ab ea absolvi et perfici debeat? Nihil ad rem! Ne sit sane; Quid est, quod ab ea absolvi et perfici debeat? Age, inquies, ista parva sunt. Putabam equidem satis, inquit, me dixisse. Cur ipse Pythagoras et Aegyptum lustravit et Persarum magos adiit? Collatio igitur ista te nihil iuvat. 

Quonam, inquit, modo? Quo tandem modo? Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Si quicquam extra virtutem habeatur in bonis. 


	Verum enim diceret, idque Socratem, qui voluptatem nullo loco numerat, audio dicentem, cibi condimentum esse famem, potionis sitim.



Quo tandem modo? Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio. Quod non faceret, si in voluptate summum bonum poneret. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Bonum patria: miserum exilium. Hanc ergo intuens debet institutum illud quasi signum absolvere. Non est ista, inquam, Piso, magna dissensio. Pisone in eo gymnasio, quod Ptolomaeum vocatur, unaque nobiscum Q. 


	Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia?
	Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur?




	Neque enim civitas in seditione beata esse potest nec in discordia dominorum domus;
	Theophrastum tamen adhibeamus ad pleraque, dum modo plus in virtute teneamus, quam ille tenuit, firmitatis et roboris.
	Sed quid attinet de rebus tam apertis plura requirere?
	Respondent extrema primis, media utrisque, omnia omnibus.
	Illa argumenta propria videamus, cur omnia sint paria peccata.
	Prave, nequiter, turpiter cenabat;




	Nam ut peccatum est patriam prodere, parentes violare, fana depeculari, quae sunt in effectu, sic timere, sic maerere, sic in libidine esse peccatum est etiam sine effectu.




	Quis hoc dicit?
	Poterat autem inpune;
	Pollicetur certe.
	Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo;
	Equidem e Cn.
	Haec mihi videtur delicatior, ut ita dicam, molliorque ratio, quam virtutis vis gravitasque postulat.
	Hunc vos beatum;
	Quis est enim, in quo sit cupiditas, quin recte cupidus dici possit?
	Si longus, levis.
	Que Manilium, ab iisque M.




Et quis a Stoicis et quem ad modum diceretur, tamen ego
quoque exponam, ut perspiciamus, si potuerimus, quidnam a
Zenone novi sit allatum.

Audax negotium, dicerem impudens, nisi hoc institutum postea
translatum ad philosophos nostros esset.



Quam nemo umquam voluptatem appellavit, appellat; Consequatur summas voluptates non modo parvo, sed per me nihilo, si potest; Cur deinde Metrodori liberos commendas? Quamquam id quidem, infinitum est in hac urbe; Respondeat totidem verbis. Teneo, inquit, finem illi videri nihil dolere. Plane idem, inquit, et maxima quidem, qua fieri nulla maior potest. Quod si ita sit, cur opera philosophiae sit danda nescio. Quid dubitas igitur mutare principia naturae? 


	Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur.
	Si verbum sequimur, primum longius verbum praepositum quam bonum.
	Inde sermone vario sex illa a Dipylo stadia confecimus.
	Illud dico, ea, quae dicat, praeclare inter se cohaerere.
	Inquit, respondet: Quia, nisi quod honestum est, nullum est aliud bonum! Non quaero iam verumne sit;
	At enim, qua in vita est aliquid mali, ea beata esse non potest.



Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Me igitur ipsum ames oportet, non mea, si veri amici futuri sumus. Non semper, inquam; Quid nunc honeste dicit? Id mihi magnum videtur. 

Nulla erit controversia. Hic ambiguo ludimur. Si longus, levis; Item de contrariis, a quibus ad genera formasque generum venerunt. Expectoque quid ad id, quod quaerebam, respondeas. Quibus ego vehementer assentior. 

Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit? Quid de Pythagora? Audio equidem philosophi vocem, Epicure, sed quid tibi dicendum sit oblitus es. Obsecro, inquit, Torquate, haec dicit Epicurus? Quae quidem vel cum periculo est quaerenda vobis; Hoc non est positum in nostra actione. Te enim iudicem aequum puto, modo quae dicat ille bene noris. Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Mihi, inquam, qui te id ipsum rogavi? Quo igitur, inquit, modo? 


	Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?
	Quod autem meum munus dicis non equidem recuso, sed te adiungo socium.
	Omnes enim iucundum motum, quo sensus hilaretur.
	Quod totum contra est.
	At, illa, ut vobis placet, partem quandam tuetur, reliquam deserit.
	Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est.




	Non igitur bene.
	Quare attendo te studiose et, quaecumque rebus iis, de quibus hic sermo est, nomina inponis, memoriae mando;
	Si longus, levis;
	Ac tamen hic mallet non dolere.



Hanc ergo intuens debet institutum illud quasi signum absolvere. Igitur ne dolorem quidem. Ita prorsus, inquam; Idque testamento cavebit is, qui nobis quasi oraculum ediderit nihil post mortem ad nos pertinere? Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. 

Hoc ne statuam quidem dicturam pater aiebat, si loqui posset. Videmus igitur ut conquiescere ne infantes quidem possint. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Aliter homines, aliter philosophos loqui putas oportere? Quamquam id quidem, infinitum est in hac urbe; Quod ea non occurrentia fingunt, vincunt Aristonem; Tria genera bonorum; Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. 


Hunc igitur finem illi tenuerunt, quodque ego pluribus
verbis, illi brevius secundum naturam vivere, hoc iis
bonorum videbatur extremum.

Quamquam in hac divisione rem ipsam prorsus probo,
elegantiam desidero.



Nam Pyrrho, Aristo, Erillus iam diu abiecti. Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest. An vero, inquit, quisquam potest probare, quod perceptfum, quod. Proclivi currit oratio. Nullus est igitur cuiusquam dies natalis. 


