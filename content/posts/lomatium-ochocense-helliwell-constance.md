Title: Lomatium ochocense Helliwell & Constance
Description: Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.
Image: https://source.unsplash.com/random/?developer
Date: 28/12/2020
Keywords: portfolio
Category: Sub-Ex
Author: Dynava
Tags: blog
Published: true
Background: #ca4f18
Color: #a75369
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non quam nostram quidem, inquit Pomponius iocans; Atqui reperies, inquit, in hoc quidem pertinacem; Nihil opus est exemplis hoc facere longius. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Duo Reges: constructio interrete. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. 


	At eum nihili facit;
	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Nihil sane.
	Fadio Gallo, cuius in testamento scriptum esset se ab eo rogatum ut omnis hereditas ad filiam perveniret.



Ut pompa, ludis atque eius modi spectaculis teneantur ob eamque rem vel famem et sitim perferant? In eo enim positum est id, quod dicimus esse expetendum. Quae cum essent dicta, discessimus. Prioris generis est docilitas, memoria; Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Neque enim civitas in seditione beata esse potest nec in discordia dominorum domus; Sint modo partes vitae beatae. Ergo id est convenienter naturae vivere, a natura discedere. 

Nihil minus, contraque illa hereditate dives ob eamque rem laetus. Sed nimis multa. Ubi ut eam caperet aut quando? Etenim semper illud extra est, quod arte comprehenditur. 


	Qui non moveatur et offensione turpitudinis et comprobatione honestatis?
	Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia;
	Serpere anguiculos, nare anaticulas, evolare merulas, cornibus uti videmus boves, nepas aculeis.
	Consequens enim est et post oritur, ut dixi.



Quid censes in Latino fore? Addidisti ad extremum etiam indoctum fuisse. An haec ab eo non dicuntur? Cum audissem Antiochum, Brute, ut solebam, cum M. Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Bonum valitudo: miser morbus. Equidem e Cn. Simus igitur contenti his. Hoc loco tenere se Triarius non potuit. 

Quod quidem iam fit etiam in Academia. Tanti autem aderant vesicae et torminum morbi, ut nihil ad eorum magnitudinem posset accedere. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Occultum facinus esse potuerit, gaudebit; Illa videamus, quae a te de amicitia dicta sunt. Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Haeret in salebra. Mihi quidem Antiochum, quem audis, satis belle videris attendere. 


	Quonam modo?
	Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.
	Reguli reiciendam;
	Quae cum essent dicta, finem fecimus et ambulandi et disputandi.
	Nihilo magis.
	An quod ita callida est, ut optime possit architectari voluptates?




	Verba tu fingas et ea dicas, quae non sentias?
	Eaedem enim utilitates poterunt eas labefactare atque pervertere.




	Illud urgueam, non intellegere eum quid sibi dicendum sit, cum dolorem summum malum esse dixerit.
	Quia dolori non voluptas contraria est, sed doloris privatio.
	Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate.
	Quod quidem iam fit etiam in Academia.
	At tu eadem ista dic in iudicio aut, si coronam times, dic in senatu.




Illa enim, quae prosunt aut quae nocent, aut bona sunt aut
mala, quae sint paria necesse est.

In quibus doctissimi illi veteres inesse quiddam caeleste et
divinum putaverunt.




	Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum.




	Quasi ego id curem, quid ille aiat aut neget.
	Itaque his sapiens semper vacabit.
	Ego autem existimo, si honestum esse aliquid ostendero, quod sit ipsum vi sua propter seque expetendum, iacere vestra omnia.
	Iam id ipsum absurdum, maximum malum neglegi.




Aliis esse maiora, illud dubium, ad id, quod summum bonum
dicitis, ecquaenam possit fieri accessio.

Equidem soleo etiam quod uno Graeci, si aliter non possum,
idem pluribus verbis exponere.




	Cum autem usus progrediens familiaritatem effecerit, tum amorem efflorescere tantum, ut, etiamsi nulla sit utilitas ex amicitia, tamen ipsi amici propter se ipsos amentur.



Ita enim vivunt quidam, ut eorum vita refellatur oratio. Quod autem satis est, eo quicquid accessit, nimium est; 

Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt; Sed mehercule pergrata mihi oratio tua. Haec quo modo conveniant, non sane intellego. Sic enim censent, oportunitatis esse beate vivere. Nihilne est in his rebus, quod dignum libero aut indignum esse ducamus? Non quam nostram quidem, inquit Pomponius iocans; 

Non est igitur voluptas bonum. Quid de Pythagora? Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Quorum altera prosunt, nocent altera. Age nunc isti doceant, vel tu potius quis enim ista melius? Quod maxime efficit Theophrasti de beata vita liber, in quo multum admodum fortunae datur. Quodsi ipsam honestatem undique pertectam atque absolutam. Estne, quaeso, inquam, sitienti in bibendo voluptas? 

Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Nihilo beatiorem esse Metellum quam Regulum. Satis est ad hoc responsum. Quis hoc dicit? Aliter enim explicari, quod quaeritur, non potest. Estne, quaeso, inquam, sitienti in bibendo voluptas? O magnam vim ingenii causamque iustam, cur nova existeret disciplina! Perge porro. 

Vide igitur ne non debeas verbis nostris uti, sententiis tuis. Nam constitui virtus nullo modo potesti nisi ea, quae sunt prima naturae, ut ad summam pertinentia tenebit. Sed quae tandem ista ratio est? At enim hic etiam dolore. 


