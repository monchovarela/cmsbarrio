Title: Senecio amplectens A. Gray var. holmii (Greene) Harrington
Description: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.
Image: https://source.unsplash.com/random/?minimal
Date: 17/11/2020
Keywords: blog
Category: Otcom
Author: Skimia
Tags: cms
Published: true
Background: #cf9b54
Color: #e86bf1
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mihi enim satis est, ipsis non satis. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Duo Reges: constructio interrete. Quid est, quod ab ea absolvi et perfici debeat? Expectoque quid ad id, quod quaerebam, respondeas. 


	Traditur, inquit, ab Epicuro ratio neglegendi doloris.
	Tu vero, inquam, ducas licet, si sequetur;
	Haec et tu ita posuisti, et verba vestra sunt.




	Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus;
	Itaque haec cum illis est dissensio, cum Peripateticis nulla sane.
	Ita multo sanguine profuso in laetitia et in victoria est mortuus.
	Immo vero, inquit, ad beatissime vivendum parum est, ad beate vero satis.
	Omnia peccata paria dicitis.



Sint modo partes vitae beatae. Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest. Illa argumenta propria videamus, cur omnia sint paria peccata. Duo enim genera quae erant, fecit tria. Idem adhuc; Aliud igitur esse censet gaudere, aliud non dolere. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri. Utrum igitur tibi litteram videor an totas paginas commovere? Sed ad illum redeo. 

Id est enim, de quo quaerimus. Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Isto modo ne improbos quidem, si essent boni viri. Quae similitudo in genere etiam humano apparet. Quis enim redargueret? Quod cum ille dixisset et satis disputatum videretur, in oppidum ad Pomponium perreximus omnes. Profectus in exilium Tubulus statim nec respondere ausus; Iam enim adesse poterit. Introduci enim virtus nullo modo potest, nisi omnia, quae leget quaeque reiciet, unam referentur ad summam. Comprehensum, quod cognitum non habet? 

Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Tum Torquatus: Prorsus, inquit, assentior; Ut in geometria, prima si dederis, danda sunt omnia. Qui est in parvis malis. Quo igitur, inquit, modo? 


	Cave putes quicquam esse verius.
	Non enim, si omnia non sequebatur, idcirco non erat ortus illinc.
	Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse;
	Qui est in parvis malis.
	At iam decimum annum in spelunca iacet.
	Deinde disputat, quod cuiusque generis animantium statui deceat extremum.




	Quid est, quod ab ea absolvi et perfici debeat?
	Sed quid attinet de rebus tam apertis plura requirere?




	Si longus, levis.
	Illum mallem levares, quo optimum atque humanissimum virum, Cn.
	Quare conare, quaeso.
	Venit enim mihi Platonis in mentem, quem accepimus primum hic disputare solitum;
	Peccata paria.
	Cur post Tarentum ad Archytam?



Ita graviter et severe voluptatem secrevit a bono. Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Que Manilium, ab iisque M. Beatus autem esse in maximarum rerum timore nemo potest. Quae si potest singula consolando levare, universa quo modo sustinebit? Quid sequatur, quid repugnet, vident. Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. 

A primo, ut opinor, animantium ortu petitur origo summi boni. Non potes, nisi retexueris illa. Servari enim iustitia nisi a forti viro, nisi a sapiente non potest. Isto modo ne improbos quidem, si essent boni viri. Verba tu fingas et ea dicas, quae non sentias? Ut proverbia non nulla veriora sint quam vestra dogmata. Primum quid tu dicis breve? Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. 

Ne discipulum abducam, times. Gerendus est mos, modo recte sentiat. Equidem e Cn. Primum divisit ineleganter; Et ego: Piso, inquam, si est quisquam, qui acute in causis videre soleat quae res agatur. Sed quid ages tandem, si utilitas ab amicitia, ut fit saepe, defecerit? 

Sed nimis multa. Maximus dolor, inquit, brevis est. Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Eadem nunc mea adversum te oratio est. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. 

Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare? Non enim iam stirpis bonum quaeret, sed animalis. Et ego: Piso, inquam, si est quisquam, qui acute in causis videre soleat quae res agatur. Ad eos igitur converte te, quaeso. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Nunc vides, quid faciat. Quod autem magnum dolorem brevem, longinquum levem esse dicitis, id non intellego quale sit. 

Quae contraria sunt his, malane? Quae cum magnifice primo dici viderentur, considerata minus probabantur. Tu quidem reddes; Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Positum est a nostris in iis esse rebus, quae secundum naturam essent, non dolere; Summus dolor plures dies manere non potest? Sed ego in hoc resisto; Sapiens autem semper beatus est et est aliquando in dolore; 


Multi enim et magni philosophi haec ultima bonorum iuncta
fecerunt, ut Aristoteles virtutis usum cum vitae perfectae
prosperitate coniunxit, Callipho adiunxit ad honestatem
voluptatem, Diodorus ad eandem honestatem addidit vacuitatem
doloris.

Ego vero volo in virtute vim esse quam maximam;




	Pollicetur certe.
	Quibusnam praeteritis?
	Quid Zeno?
	Hoc est non modo cor non habere, sed ne palatum quidem.
	Ita credo.
	Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret?
	Respondeat totidem verbis.
	Comprehensum, quod cognitum non habet?




Nec enim absolvi beata vita sapientis neque ad exitum
perduci poterit, si prima quaeque bene ab eo consulta atque
facta ipsius oblivione obruentur.

Tu autem negas fortem esse quemquam posse, qui dolorem malum
putet.




	Non dolere, inquam, istud quam vim habeat postea videro;




	Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia;




