Title: Astelia waialealae Wawra
Description: Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.
Image: https://source.unsplash.com/random/?code
Date: 2/5/2021
Keywords: portfolio
Category: Otcom
Author: Photobug
Tags: design
Published: true
Background: #b1fae3
Color: #f898a2
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Que Manilium, ab iisque M. Sed virtutem ipsam inchoavit, nihil amplius. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. Sed quot homines, tot sententiae; Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Hoc tu nunc in illo probas. 


Negarine ullo modo possit numquam quemquam stabili et firmo
et magno animo, quem fortem virum dicimus, effici posse,
nisi constitutum sit non esse malum dolorem?

Et quidem iure fortasse, sed tamen non gravissimum est
testimonium multitudinis.



Bonum integritas corporis: misera debilitas. Non pugnem cum homine, cur tantum habeat in natura boni; Non quam nostram quidem, inquit Pomponius iocans; Graece donan, Latine voluptatem vocant. Qua tu etiam inprudens utebare non numquam. Quid loquor de nobis, qui ad laudem et ad decus nati, suscepti, instituti sumus? 

Et harum quidem rerum facilis est et expedita distinctio. Nulla erit controversia. Quorum altera prosunt, nocent altera. Non igitur bene. At quanta conantur! Mundum hunc omnem oppidum esse nostrum! Incendi igitur eos, qui audiunt, vides. Sed erat aequius Triarium aliquid de dissensione nostra iudicare. 


	Mihi enim satis est, ipsis non satis.
	Inscite autem medicinae et gubernationis ultimum cum ultimo sapientiae comparatur.
	Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam.
	Sed quid sentiat, non videtis.
	Qui enim existimabit posse se miserum esse beatus non erit.
	Quamquam non negatis nos intellegere quid sit voluptas, sed quid ille dicat.



Ita multo sanguine profuso in laetitia et in victoria est mortuus. Utram tandem linguam nescio? Non quam nostram quidem, inquit Pomponius iocans; Illis videtur, qui illud non dubitant bonum dicere -; 


	Sed ita falsa sunt ea, quae consequuntur, ut illa, e quibus haec nata sunt, vera esse non possint.



Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Quod praeceptum quia maius erat, quam ut ab homine videretur, idcirco assignatum est deo. An nisi populari fama? Sed in rebus apertissimis nimium longi sumus. Nihilo beatiorem esse Metellum quam Regulum. 

Duo Reges: constructio interrete. Esse enim, nisi eris, non potes. An ea, quae per vinitorem antea consequebatur, per se ipsa curabit? Collige omnia, quae soletis: Praesidium amicorum. Quae autem natura suae primae institutionis oblita est? Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Memini vero, inquam; Quae ista amicitia est? Quis est, qui non oderit libidinosam, protervam adolescentiam? 


	Non igitur bene.
	Sapiens autem semper beatus est et est aliquando in dolore;
	Nulla erit controversia.
	Illo enim addito iuste fit recte factum, per se autem hoc ipsum reddere in officio ponitur.
	Pollicetur certe.
	Avaritiamne minuis?
	Facete M.
	Summus dolor plures dies manere non potest?
	Sed fortuna fortis;
	Et quidem saepe quaerimus verbum Latinum par Graeco et quod idem valeat;
	Haec dicuntur inconstantissime.
	Sed quanta sit alias, nunc tantum possitne esse tanta.



Quid affers, cur Thorius, cur Caius Postumius, cur omnium horum magister, Orata, non iucundissime vixerit? Invidiosum nomen est, infame, suspectum. At modo dixeras nihil in istis rebus esse, quod interesset. Hoc simile tandem est? Deinde prima illa, quae in congressu solemus: Quid tu, inquit, huc? 


	A mene tu?
	Et non ex maxima parte de tota iudicabis?
	Deprehensus omnem poenam contemnet.
	Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem.
	Idemne, quod iucunde?
	Scio enim esse quosdam, qui quavis lingua philosophari possint;
	Qui convenit?
	Praeclare enim Plato: Beatum, cui etiam in senectute contigerit, ut sapientiam verasque opiniones assequi possit.



Restinguet citius, si ardentem acceperit. Et quod est munus, quod opus sapientiae? Haec et tu ita posuisti, et verba vestra sunt. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Nemo igitur esse beatus potest. 

Faceres tu quidem, Torquate, haec omnia; Sit enim idem caecus, debilis. Atqui reperies, inquit, in hoc quidem pertinacem; Illa argumenta propria videamus, cur omnia sint paria peccata. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Nam et a te perfici istam disputationem volo, nec tua mihi oratio longa videri potest. 


	Quantum Aristoxeni ingenium consumptum videmus in musicis?
	Haec igitur Epicuri non probo, inquam.
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Istam voluptatem, inquit, Epicurus ignorat?




	Sin laboramus, quis est, qui alienae modum statuat industriae?
	Quaeque de virtutibus dicta sunt, quem ad modum eae semper voluptatibus inhaererent, eadem de amicitia dicenda sunt.
	Mihi, inquam, qui te id ipsum rogavi?
	Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt.
	Itaque rursus eadem ratione, qua sum paulo ante usus, haerebitis.




	Quid turpius quam sapientis vitam ex insipientium sermone pendere?




Eorum erat iste mos qui tum sophistae nominabantur, quorum e
numero primus est ausus Leontinus Gorgias in conventu
poscere quaestionem, id est iubere dicere, qua de re quis
vellet audire.

Ut proverbia non nulla veriora sint quam vestra dogmata.




	Qui non moveatur et offensione turpitudinis et comprobatione honestatis?
	Quantum Aristoxeni ingenium consumptum videmus in musicis?



Quis est tam dissimile homini. Intrandum est igitur in rerum naturam et penitus quid ea postulet pervidendum; Quonam, inquit, modo? Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Collatio igitur ista te nihil iuvat. Huius ego nunc auctoritatem sequens idem faciam. Quis negat? 


