Title: Lobelia chinensis Lour.
Description: Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.
Image: https://source.unsplash.com/random/?green
Date: 13/11/2020
Keywords: cms
Category: Latlux
Author: Tanoodle
Tags: work
Published: true
Background: #2c58bc
Color: #2e271a
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Si enim ad populum me vocas, eum. Haec quo modo conveniant, non sane intellego. An vero, inquit, quisquam potest probare, quod perceptfum, quod. Duo Reges: constructio interrete. Sed ego in hoc resisto; Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Quae similitudo in genere etiam humano apparet. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Claudii libidini, qui tum erat summo ne imperio, dederetur. Hoc non est positum in nostra actione. 

Quid ait Aristoteles reliquique Platonis alumni? Ut in geometria, prima si dederis, danda sunt omnia. Sed residamus, inquit, si placet. Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Cum audissem Antiochum, Brute, ut solebam, cum M. Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Iam id ipsum absurdum, maximum malum neglegi. 


	Confecta res esset.
	Beatum, inquit.
	Numquam facies.
	Solum praeterea formosum, solum liberum, solum civem, stultost;
	ALIO MODO.
	Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?
	Quid iudicant sensus?
	Contemnit enim disserendi elegantiam, confuse loquitur.
	Quid vero?
	Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet.
	Hic ambiguo ludimur.
	Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.




	Nulla profecto est, quin suam vim retineat a primo ad extremum.
	Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum.
	Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere.
	Habent enim et bene longam et satis litigiosam disputationem.




	At certe gravius.
	Iam in altera philosophiae parte.
	Sullae consulatum?
	Quae similitudo in genere etiam humano apparet.
	Tenent mordicus.
	Nam quid possumus facere melius?
	Scrupulum, inquam, abeunti;
	Si longus, levis;




Si est nihil in eo, quod perficiendum est, praeter motum
ingenii quendam, id est rationem, necesse est huic ultimum
esse virtute agere;

Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam
causam Zenoni desciscendi fuisse.




	Non enim, si omnia non sequebatur, idcirco non erat ortus illinc.
	Et quod est munus, quod opus sapientiae?
	Nam quid possumus facere melius?
	Quamquam te quidem video minime esse deterritum.




	Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore.
	Quia, si mala sunt, is, qui erit in iis, beatus non erit.



Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Immo alio genere; Quid ad utilitatem tantae pecuniae? Tanta vis admonitionis inest in locis; An potest, inquit ille, quicquam esse suavius quam nihil dolere? Quid ergo attinet dicere: Nihil haberem, quod reprehenderem, si finitas cupiditates haberent? 

Odium autem et invidiam facile vitabis. Profectus in exilium Tubulus statim nec respondere ausus; An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Item de contrariis, a quibus ad genera formasque generum venerunt. 

Conferam tecum, quam cuique verso rem subicias; Hoc ne statuam quidem dicturam pater aiebat, si loqui posset. Sed ego in hoc resisto; Omnia contraria, quos etiam insanos esse vultis. Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba. Tum Triarius: Posthac quidem, inquit, audacius. 


	Nemo enim est, qui aliter dixerit quin omnium naturarum simile esset id, ad quod omnia referrentur, quod est ultimum rerum appetendarum.




Mihi quidem etiam lautius videtur, quod eligitur, et ad quod
dilectus adhibetur -, sed, cum ego ista omnia bona dixero,
tantum refert quam magna dicam, cum expetenda, quam valde.

Ego autem tibi, Piso, assentior usu hoc venire, ut acrius
aliquanto et attentius de claris viris locorum admonitu
cogitemus.



Non dolere, inquam, istud quam vim habeat postea videro; Ad eas enim res ab Epicuro praecepta dantur. Igitur neque stultorum quisquam beatus neque sapientium non beatus. Quid enim de amicitia statueris utilitatis causa expetenda vides. Quodsi ipsam honestatem undique pertectam atque absolutam. Tuo vero id quidem, inquam, arbitratu. Istam voluptatem, inquit, Epicurus ignorat? Vulgo enim dicitur: Iucundi acti labores, nec male Euripidesconcludam, si potero, Latine; 

Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Quia nec honesto quic quam honestius nec turpi turpius. Quonam, inquit, modo? Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Profectus in exilium Tubulus statim nec respondere ausus; Sed tamen est aliquid, quod nobis non liceat, liceat illis. Potius ergo illa dicantur: turpe esse, viri non esse debilitari dolore, frangi, succumbere. 

Summum en�m bonum exposuit vacuitatem doloris; Bonum incolumis acies: misera caecitas. Respondeat totidem verbis. An est aliquid, quod te sua sponte delectet? 


	Ut bacillum aliud est inflexum et incurvatum de industria, aliud ita natum, sic ferarum natura non est illa quidem depravata mala disciplina, sed natura sua.



Ea possunt paria non esse. Longum est enim ad omnia respondere, quae a te dicta sunt. Videamus igitur sententias eorum, tum ad verba redeamus. Morbo gravissimo affectus, exul, orbus, egens, torqueatur eculeo: quem hunc appellas, Zeno? Sine ea igitur iucunde negat posse se vivere? Pauca mutat vel plura sane; Ad eas enim res ab Epicuro praecepta dantur. 

Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis; Haec non erant eius, qui innumerabilis mundos infinitasque regiones, quarum nulla esset ora, nulla extremitas, mente peragravisset. Quamquam tu hanc copiosiorem etiam soles dicere. Hoc est non dividere, sed frangere. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Tum Torquatus: Prorsus, inquit, assentior; Cur iustitia laudatur? 


	Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia.
	Illa tamen simplicia, vestra versuta.
	Iam doloris medicamenta illa Epicurea tamquam de narthecio proment: Si gravis, brevis;
	Tubulum fuisse, qua illum, cuius is condemnatus est rogatione, P.
	Ergo illi intellegunt quid Epicurus dicat, ego non intellego?




