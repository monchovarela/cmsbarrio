Title: Huperzia occidentalis (Clute) Kartesz & Gandhi
Description: Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.
Image: https://source.unsplash.com/random/?green
Date: 31/7/2021
Keywords: work
Category: Duobam
Author: Roombo
Tags: post
Published: true
Background: #a02b32
Color: #570dec
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et nemo nimium beatus est; Totum autem id externum est, et quod externum, id in casu est. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Duo Reges: constructio interrete. Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus; 


Quae dici eadem de ceteris virtutibus possunt, quarum omnium
fundamenta vos in voluptate tamquam in aqua ponitis.

Cum sciret confestim esse moriendum eamque mortem ardentiore
studio peteret, quam Epicurus voluptatem petendam putat.



Beatum, inquit. Haec para/doca illi, nos admirabilia dicamus. Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Quid est, quod ab ea absolvi et perfici debeat? Quae cum essent dicta, discessimus. Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. 


	Non enim iam stirpis bonum quaeret, sed animalis.
	Isto modo ne improbos quidem, si essent boni viri.



Sed videbimus. Tum mihi Piso: Quid ergo? Quod si ita se habeat, non possit beatam praestare vitam sapientia. Ergo in gubernando nihil, in officio plurimum interest, quo in genere peccetur. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Indicant pueri, in quibus ut in speculis natura cernitur. Quod autem principium officii quaerunt, melius quam Pyrrho; Cave putes quicquam esse verius. 


	Confecta res esset.
	Nam de summo mox, ut dixi, videbimus et ad id explicandum disputationem omnem conferemus.
	Negare non possum.
	Tu vero, inquam, ducas licet, si sequetur;
	Nihil sane.
	Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur.
	Praeclare hoc quidem.
	At enim, qua in vita est aliquid mali, ea beata esse non potest.
	Scrupulum, inquam, abeunti;
	Sed utrum hortandus es nobis, Luci, inquit, an etiam tua sponte propensus es?




	Quid enim perversius, quid intolerabilius, quid stultius quam bonam valetudinem, quam dolorum omnium vacuitatem, quam integritatem oculorum reliquorumque sensuum ponere in bonis potius, quam dicerent nihil omnino inter eas res iisque contrarias interesse?



Mihi enim satis est, ipsis non satis. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Quam ob rem tandem, inquit, non satisfacit? Quid sequatur, quid repugnet, vident. Isto modo ne improbos quidem, si essent boni viri. Nunc vero a primo quidem mirabiliter occulta natura est nec perspici nec cognosci potest. Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. 


	An est aliquid, quod te sua sponte delectet?
	Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria.
	At iste non dolendi status non vocatur voluptas.
	Quis est enim, in quo sit cupiditas, quin recte cupidus dici possit?




	Dic in quovis conventu te omnia facere, ne doleas.
	Quod etsi ingeniis magnis praediti quidam dicendi copiam sine ratione consequuntur, ars tamen est dux certior quam natura.
	Familiares nostros, credo, Sironem dicis et Philodemum, cum optimos viros, tum homines doctissimos.
	Torquatus, is qui consul cum Cn.




	Praeteritis, inquit, gaudeo.
	Duo enim genera quae erant, fecit tria.
	Sint ista Graecorum;
	Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria.
	Quid vero?
	Non ego tecum iam ita iocabor, ut isdem his de rebus, cum L.
	Ita prorsus, inquam;
	Satisne igitur videor vim verborum tenere, an sum etiam nunc vel Graece loqui vel Latine docendus?




	Quodsi vultum tibi, si incessum fingeres, quo gravior viderere, non esses tui similis;
	Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt.




	Bene facis, inquit, quod me adiuvas, et istis quidem, quae modo dixisti, utar potius Latinis, in ceteris subvenies, si me haerentem videbis.



Re mihi non aeque satisfacit, et quidem locis pluribus. Quare hoc videndum est, possitne nobis hoc ratio philosophorum dare. Hanc quoque iucunditatem, si vis, transfer in animum; Nunc haec primum fortasse audientis servire debemus. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet; Cupiditates non Epicuri divisione finiebat, sed sua satietate. 

De quibus cupio scire quid sentias. Certe, nisi voluptatem tanti aestimaretis. Ita prorsus, inquam; Hoc est non dividere, sed frangere. 

Hoc Hieronymus summum bonum esse dixit. Ratio quidem vestra sic cogit. Cenasti in vita numquam bene, cum omnia in ista Consumis squilla atque acupensere cum decimano. Qualem igitur hominem natura inchoavit? Quamquam te quidem video minime esse deterritum. Ac tamen hic mallet non dolere. Atque ita re simpliciter primo collocata reliqua subtilius persequentes corporis bona facilem quandam rationem habere censebant; 

Quis est tam dissimile homini. Quae est igitur causa istarum angustiarum? Sit hoc ultimum bonorum, quod nunc a me defenditur; Praeclare hoc quidem. Quid enim est a Chrysippo praetermissum in Stoicis? Quod autem principium officii quaerunt, melius quam Pyrrho; 

Sed residamus, inquit, si placet. Quid, de quo nulla dissensio est? Hoc sic expositum dissimile est superiori. Nos commodius agimus. Tu vero, inquam, ducas licet, si sequetur; Mihi, inquam, qui te id ipsum rogavi? 


Laelius clamores sof�w ille so lebat Edere compellans
gumias ex ordine nostros.

Quod autem meum munus dicis non equidem recuso, sed te
adiungo socium.



Non risu potius quam oratione eiciendum? Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Quo modo autem philosophus loquitur? Ergo, si semel tristior effectus est, hilara vita amissa est? Semovenda est igitur voluptas, non solum ut recta sequamini, sed etiam ut loqui deceat frugaliter. Nunc omni virtuti vitium contrario nomine opponitur. 


