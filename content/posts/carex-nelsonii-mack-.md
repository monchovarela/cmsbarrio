Title: Carex nelsonii Mack.
Description: Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.
Image: https://source.unsplash.com/random/?javascript
Date: 13/6/2021
Keywords: cms
Category: Alphazap
Author: Tagpad
Tags: article
Published: true
Background: #f16e62
Color: #c2b81e
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praeclarae mortes sunt imperatoriae; Nobis aliter videtur, recte secusne, postea; Satis est ad hoc responsum. Videamus igitur sententias eorum, tum ad verba redeamus. 

Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Quid igitur, inquit, eos responsuros putas? Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit; Tubulum fuisse, qua illum, cuius is condemnatus est rogatione, P. Iam id ipsum absurdum, maximum malum neglegi. Non autem hoc: igitur ne illud quidem. Contineo me ab exemplis. 


	Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.
	Sed haec in pueris;
	Istam voluptatem perpetuam quis potest praestare sapienti?




	Eadem nunc mea adversum te oratio est.
	Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis.
	Ex rebus enim timiditas, non ex vocabulis nascitur.
	Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae.
	Quid, si etiam iucunda memoria est praeteritorum malorum?




	Appellet haec desideria naturae, cupiditatis nomen servet alio, ut eam, cum de avaritia, cum de intemperantia, cum de maximis vitiis loquetur, tamquam capitis accuset.



Illa argumenta propria videamus, cur omnia sint paria peccata. Aliam vero vim voluptatis esse, aliam nihil dolendi, nisi valde pertinax fueris, concedas necesse est. Dici enim nihil potest verius. Quis hoc dicit? Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Tum ille: Ain tandem? Nos autem non solum beatae vitae istam esse oblectationem videmus, sed etiam levamentum miseriarum. Atque his de rebus et splendida est eorum et illustris oratio. Nam de isto magna dissensio est. 


Critolaus imitari voluit antiquos, et quidem est gravitate
proximus, et redundat oratio, ac tamen is quidem in patriis
institutis manet.

Num igitur eum postea censes anxio animo aut sollicito
fuisse?



Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego; Illi enim inter se dissentiunt. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Qua tu etiam inprudens utebare non numquam. 

Nihilo beatiorem esse Metellum quam Regulum. Duo Reges: constructio interrete. Aufert enim sensus actionemque tollit omnem. Nihilne est in his rebus, quod dignum libero aut indignum esse ducamus? Ita enim vivunt quidam, ut eorum vita refellatur oratio. Non est ista, inquam, Piso, magna dissensio. 

Est autem officium, quod ita factum est, ut eius facti probabilis ratio reddi possit. Eam stabilem appellas. Quasi ego id curem, quid ille aiat aut neget. Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia. Eam stabilem appellas. Hic nihil fuit, quod quaereremus. Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Tu vero, inquam, ducas licet, si sequetur; Hoc est dicere: Non reprehenderem asotos, si non essent asoti. Sit hoc ultimum bonorum, quod nunc a me defenditur; 

Quid ergo hoc loco intellegit honestum? Philosophi autem in suis lectulis plerumque moriuntur. Non est ista, inquam, Piso, magna dissensio. At enim, qua in vita est aliquid mali, ea beata esse non potest. 


Et tamen ego a philosopho, si afferat eloquentiam, non
asperner, si non habeat, non admodum flagitem.

Manebit ergo amicitia tam diu, quam diu sequetur utilitas,
et, si utilitas amicitiam constituet, tollet eadem.




	Equidem e Cn.
	Igitur neque stultorum quisquam beatus neque sapientium non beatus.
	Sed videbimus.
	Non potes, nisi retexueris illa.
	Nihilo magis.
	Sed memento te, quae nos sentiamus, omnia probare, nisi quod verbis aliter utamur, mihi autem vestrorum nihil probari.



A primo, ut opinor, animantium ortu petitur origo summi boni. Cur igitur, cum de re conveniat, non malumus usitate loqui? Iam in altera philosophiae parte. Tum Torquatus: Prorsus, inquit, assentior; An eiusdem modi? Estne, quaeso, inquam, sitienti in bibendo voluptas? Unum est sine dolore esse, alterum cum voluptate. Non modo carum sibi quemque, verum etiam vehementer carum esse? 


	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Rem unam praeclarissimam omnium maximeque laudandam, penitus viderent, quonam gaudio complerentur, cum tantopere eius adumbrata opinione laetentur?
	Mihi enim erit isdem istis fortasse iam utendum.



Nec enim ignoras his istud honestum non summum modo, sed etiam, ut tu vis, solum bonum videri. Cur iustitia laudatur? Erit enim mecum, si tecum erit. Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat? Quod autem meum munus dicis non equidem recuso, sed te adiungo socium. Sed quod proximum fuit non vidit. Sic enim censent, oportunitatis esse beate vivere. 


	Quid iudicant sensus?
	At certe gravius.
	Quid de Pythagora?
	Sed utrum hortandus es nobis, Luci, inquit, an etiam tua sponte propensus es?
	Quid enim?
	Possumusne ergo in vita summum bonum dicere, cum id ne in cena quidem posse videamur?
	Perge porro;
	Oratio me istius philosophi non offendit;
	Pollicetur certe.
	Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem.



Urgent tamen et nihil remittunt. Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere. Ea possunt paria non esse. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Non enim iam stirpis bonum quaeret, sed animalis. Illis videtur, qui illud non dubitant bonum dicere -; Duo enim genera quae erant, fecit tria. Quo tandem modo? Videamus igitur sententias eorum, tum ad verba redeamus. 


	Sed in ceteris artibus cum dicitur artificiose, posterum quodam modo et consequens putandum est, quod illi �pigennhmatik�n appellant;




	Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate.
	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?
	Unum est sine dolore esse, alterum cum voluptate.
	Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum;




