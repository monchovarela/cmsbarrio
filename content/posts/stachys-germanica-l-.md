Title: Stachys germanica L.
Description: Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.
Image: https://source.unsplash.com/random/?aqua
Date: 21/5/2021
Keywords: cms
Category: Pannier
Author: Edgewire
Tags: article
Published: true
Background: #d6d62d
Color: #24818d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quorum sine causa fieri nihil putandum est. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero. Hic nihil fuit, quod quaereremus. Quorum sine causa fieri nihil putandum est. Omnia contraria, quos etiam insanos esse vultis. Duo Reges: constructio interrete. Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. 

Animi enim quoque dolores percipiet omnibus partibus maiores quam corporis. Sed haec omittamus; Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. Facit enim ille duo seiuncta ultima bonorum, quae ut essent vera, coniungi debuerunt; Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. 

Bonum incolumis acies: misera caecitas. Nonne odio multos dignos putamus, qui quodam motu aut statu videntur naturae legem et modum contempsisse? Pisone in eo gymnasio, quod Ptolomaeum vocatur, unaque nobiscum Q. Ut aliquid scire se gaudeant? Si verbum sequimur, primum longius verbum praepositum quam bonum. Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere? Hoc loco tenere se Triarius non potuit. 


	Ita enim vivunt quidam, ut eorum vita refellatur oratio.
	Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera.
	Quantam rem agas, ut Circeis qui habitet totum hunc mundum suum municipium esse existimet?
	Praeterea et appetendi et refugiendi et omnino rerum gerendarum initia proficiscuntur aut a voluptate aut a dolore.



Cur post Tarentum ad Archytam? Septem autem illi non suo, sed populorum suffragio omnium nominati sunt. Haec et tu ita posuisti, et verba vestra sunt. Etenim si delectamur, cum scribimus, quis est tam invidus, qui ab eo nos abducat? Facile est hoc cernere in primis puerorum aetatulis. Non laboro, inquit, de nomine. Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Immo vero, inquit, ad beatissime vivendum parum est, ad beate vero satis. 


	Stoicos roga.
	Quid est igitur, cur ita semper deum appellet Epicurus beatum et aeternum?
	Pollicetur certe.
	At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit;
	Quo modo?
	Audeo dicere, inquit.
	Avaritiamne minuis?
	Ita enim vivunt quidam, ut eorum vita refellatur oratio.
	Explanetur igitur.
	Cum audissem Antiochum, Brute, ut solebam, cum M.
	Omnia peccata paria dicitis.
	Vulgo enim dicitur: Iucundi acti labores, nec male Euripidesconcludam, si potero, Latine;




	Nihil enim iam habes, quod ad corpus referas;
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?




	In homine autem summa omnis animi est et in animo rationis, ex qua virtus est, quae rationis absolutio definitur, quam etiam atque etiam explicandam putant.




Quid enim dicis omne animal, simul atque sit ortum,
applicatum esse ad se diligendum esseque in se conservando
occupatum?

Ergo adhuc, quantum equidem intellego, causa non videtur
fuisse mutandi nominis.




	Tamen aberramus a proposito, et, ne longius, prorsus, inquam, Piso, si ista mala sunt, placet.
	Sed finge non solum callidum eum, qui aliquid improbe faciat, verum etiam praepotentem, ut M.



Quid adiuvas? Stoici scilicet. Quis est enim, in quo sit cupiditas, quin recte cupidus dici possit? Quis non odit sordidos, vanos, leves, futtiles? Sed plane dicit quod intellegit. Hoc simile tandem est? Sed quod proximum fuit non vidit. Tu quidem reddes; 


Nec vero potest quisquam de bonis et malis vere iudicare
nisi omni cognita ratione naturae et vitae etiam deorum, et
utrum conveniat necne natura hominis cum universa.

Reicietur etiam Carneades, nec ulla de summo bono ratio aut
voluptatis non dolendive particeps aut honestatis expers
probabitur.




	Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam esse oportebit;
	Itaque haec cum illis est dissensio, cum Peripateticis nulla sane.
	Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit;
	Hoc sic expositum dissimile est superiori.



Ita prorsus, inquam; Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Quae autem natura suae primae institutionis oblita est? Ita relinquet duas, de quibus etiam atque etiam consideret. Quis est tam dissimile homini. Ut id aliis narrare gestiant? Istam voluptatem perpetuam quis potest praestare sapienti? At iste non dolendi status non vocatur voluptas. Quid, quod res alia tota est? Vide igitur ne non debeas verbis nostris uti, sententiis tuis. 

Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Gerendus est mos, modo recte sentiat. Si longus, levis dictata sunt. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Quod autem magnum dolorem brevem, longinquum levem esse dicitis, id non intellego quale sit. 

Atqui reperies, inquit, in hoc quidem pertinacem; Sed tamen intellego quid velit. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Haec quo modo conveniant, non sane intellego. Iam contemni non poteris. Sed quid attinet de rebus tam apertis plura requirere? Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Videsne quam sit magna dissensio? 

Stoicos roga. Luxuriam non reprehendit, modo sit vacua infinita cupiditate et timore. Quod autem ratione actum est, id officium appellamus. Sin aliud quid voles, postea. An eiusdem modi? An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Si longus, levis dictata sunt. Minime vero istorum quidem, inquit. Tu vero, inquam, ducas licet, si sequetur; Partim cursu et peragratione laetantur, congregatione aliae coetum quodam modo civitatis imitantur; At hoc in eo M. At coluit ipse amicitias. 

Bonum valitudo: miser morbus. Simus igitur contenti his. Quae diligentissime contra Aristonem dicuntur a Chryippo. Sed plane dicit quod intellegit. At habetur! Et ego id scilicet nesciebam! Sed ut sit, etiamne post mortem coletur? Age sane, inquam. 


	Alia quaedam dicent, credo, magna antiquorum esse peccata, quae ille veri investigandi cupidus nullo modo ferre potuerit.




	Quid enim?
	Quo modo autem optimum, si bonum praeterea nullum est?
	Magna laus.
	Idem iste, inquam, de voluptate quid sentit?
	Tria genera bonorum;
	Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis.
	Qui convenit?
	Hoc uno captus Erillus scientiam summum bonum esse defendit nec rem ullam aliam per se expetendam.
	Tria genera bonorum;
	Omnium enim rerum principia parva sunt, sed suis progressionibus usa augentur nec sine causa;
	Quid Zeno?
	Tu quidem reddes;




