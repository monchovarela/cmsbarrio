Title: Trifolium barbigerum Torr. var. barbigerum
Description: Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.
Image: https://source.unsplash.com/random/?iphone
Date: 12/1/2021
Keywords: article
Category: Flexidy
Author: Edgeify
Tags: cms
Published: true
Background: #dcb52d
Color: #e2e66a
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ille vero, si insipiens-quo certe, quoniam tyrannus -, numquam beatus; Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare; Bonum valitudo: miser morbus. Omnis enim est natura diligens sui. Quid sequatur, quid repugnet, vident. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? Quasi ego id curem, quid ille aiat aut neget. 


	At iam decimum annum in spelunca iacet.
	Quamquam tu hanc copiosiorem etiam soles dicere.
	Scio enim esse quosdam, qui quavis lingua philosophari possint;
	Proclivi currit oratio.
	Quod si ita se habeat, non possit beatam praestare vitam sapientia.
	Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit?



Quod est, ut dixi, habere ea, quae secundum naturam sint, vel omnia vel plurima et maxima. Cur post Tarentum ad Archytam? Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? 

Duo Reges: constructio interrete. Equidem e Cn. Dolere malum est: in crucem qui agitur, beatus esse non potest. Non laboro, inquit, de nomine. Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est. Si mala non sunt, iacet omnis ratio Peripateticorum. Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? De vacuitate doloris eadem sententia erit. Sedulo, inquam, faciam. Conferam avum tuum Drusum cum C. Quo studio Aristophanem putamus aetatem in litteris duxisse? 

Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Eam tum adesse, cum dolor omnis absit; Quis non odit sordidos, vanos, leves, futtiles? Age, inquies, ista parva sunt. Suam denique cuique naturam esse ad vivendum ducem. Nam Pyrrho, Aristo, Erillus iam diu abiecti. 


	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;
	Et si in ipsa gubernatione neglegentia est navis eversa, maius est peccatum in auro quam in palea.
	Quae cum dixisset paulumque institisset, Quid est?
	Quid autem habent admirationis, cum prope accesseris?




	Ipse enim Metrodorus, paene alter Epicurus, beatum esse describit his fere verbis: cum corpus bene constitutum sit et sit exploratum ita futurum.




	Si longus, levis;
	Nihil opus est exemplis hoc facere longius.
	Quare attende, quaeso.
	Tibi hoc incredibile, quod beatissimum.
	Istic sum, inquit.
	Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam.
	Memini vero, inquam;
	Velut ego nunc moveor.
	Si quae forte-possumus.
	Cum praesertim illa perdiscere ludus esset.




	Quare istam quoque aggredere tractatam praesertim et ab aliis et a te ipso saepe, ut tibi deesse non possit oratio.



Quid iudicant sensus? Quamquam id quidem, infinitum est in hac urbe; Praeclare hoc quidem. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Sed finge non solum callidum eum, qui aliquid improbe faciat, verum etiam praepotentem, ut M. Sed haec omittamus; 

Aliter enim nosmet ipsos nosse non possumus. Isto modo ne improbos quidem, si essent boni viri. Nos commodius agimus. Ratio quidem vestra sic cogit. Iam in altera philosophiae parte. Ego vero isti, inquam, permitto. 

Sed ad illum redeo. At ego quem huic anteponam non audeo dicere; Nec vero intermittunt aut admirationem earum rerum, quae sunt ab antiquis repertae, aut investigationem novarum. Non autem hoc: igitur ne illud quidem. Cyrenaici quidem non recusant; Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; 

Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Sint ista Graecorum; Facete M. Ita nemo beato beatior. 


	Haeret in salebra.
	Non est enim vitium in oratione solum, sed etiam in moribus.
	Negare non possum.
	Cave putes quicquam esse verius.
	Bonum patria: miserum exilium.
	Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare;
	Quid iudicant sensus?
	Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur.




Falli igitur possumus.

Non autem hoc: igitur ne illud quidem.




Et tamen vide, ne, si ego non intellegam quid Epicurus
loquatur, cum Graece, ut videor, luculenter sciam, sit
aliqua culpa eius, qui ita loquatur, ut non intellegatur.

Qui haec didicerunt, quae ille contemnit, sic solent: Duo
genera cupiditatum, naturales et inanes, naturalium duo,
necessariae et non necessariae.




	Semper enim ita adsumit aliquid, ut ea, quae prima dederit, non deserat.
	Sin laboramus, quis est, qui alienae modum statuat industriae?
	Paria sunt igitur.
	Quasi ego id curem, quid ille aiat aut neget.
	Eorum enim est haec querela, qui sibi cari sunt seseque diligunt.



Introduci enim virtus nullo modo potest, nisi omnia, quae leget quaeque reiciet, unam referentur ad summam. Negare non possum. Ita ne hoc quidem modo paria peccata sunt. Itaque rursus eadem ratione, qua sum paulo ante usus, haerebitis. Nosti, credo, illud: Nemo pius est, qui pietatem-; Quid turpius quam sapientis vitam ex insipientium sermone pendere? Quam nemo umquam voluptatem appellavit, appellat; Multoque hoc melius nos veriusque quam Stoici. 

Primum Theophrasti, Strato, physicum se voluit; Satis est ad hoc responsum. Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. Non est igitur voluptas bonum. Haec quo modo conveniant, non sane intellego. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Utrum igitur tibi litteram videor an totas paginas commovere? 


	An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat?
	Quoniam, si dis placet, ab Epicuro loqui discimus.
	Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.
	A mene tu?
	Omnes enim iucundum motum, quo sensus hilaretur.
	Ergo in utroque exercebantur, eaque disciplina effecit tantam illorum utroque in genere dicendi copiam.
	Certe nihil nisi quod possit ipsum propter se iure laudari.




