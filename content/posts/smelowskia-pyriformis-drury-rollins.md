Title: Smelowskia pyriformis Drury & Rollins
Description: Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.
Image: https://source.unsplash.com/random/?design
Date: 14/8/2021
Keywords: article
Category: Zathin
Author: Rooxo
Tags: work
Published: true
Background: #4de7a0
Color: #41d474
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bonum negas esse divitias, praeposėtum esse dicis? Aufert enim sensus actionemque tollit omnem. Quid, de quo nulla dissensio est? Ita nemo beato beatior. Et non ex maxima parte de tota iudicabis? Nulla profecto est, quin suam vim retineat a primo ad extremum. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Duo Reges: constructio interrete. Erat enim Polemonis. Falli igitur possumus. 


	Nam ista commendatio puerorum, memoria et caritas amicitiae, summorum officiorum in extremo spiritu conservatio indicat innatam esse homini probitatem gratuitam, non invitatam voluptatibus nec praemiorum mercedibus evocatam.




	Nec vero sum nescius esse utilitatem in historia, non modo voluptatem.




	Poterat autem inpune;
	De hominibus dici non necesse est.
	Eademne, quae restincta siti?
	A primo, ut opinor, animantium ortu petitur origo summi boni.



Intrandum est igitur in rerum naturam et penitus quid ea postulet pervidendum; Cave putes quicquam esse verius. Sed quae tandem ista ratio est? Nihil ad rem! Ne sit sane; Vitae autem degendae ratio maxime quidem illis placuit quieta. Quare conare, quaeso. Minime vero istorum quidem, inquit. Quam illa ardentis amores excitaret sui! Cur tandem? Quam si explicavisset, non tam haesitaret. Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium. 

Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Sed quanta sit alias, nunc tantum possitne esse tanta. Quamquam non negatis nos intellegere quid sit voluptas, sed quid ille dicat. Paria sunt igitur. Poterat autem inpune; Te enim iudicem aequum puto, modo quae dicat ille bene noris. Sed quot homines, tot sententiae; Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. 

Et homini, qui ceteris animantibus plurimum praestat, praecipue a natura nihil datum esse dicemus? Ita graviter et severe voluptatem secrevit a bono. Deprehensus omnem poenam contemnet. Quod dicit Epicurus etiam de voluptate, quae minime sint voluptates, eas obscurari saepe et obrui. Praeclarae mortes sunt imperatoriae; Non quaeritur autem quid naturae tuae consentaneum sit, sed quid disciplinae. 


	Nec enim, omnes avaritias si aeque avaritias esse dixerimus, sequetur ut etiam aequas esse dicamus.
	Tum Torquatus: Prorsus, inquit, assentior;



Praeclare enim Plato: Beatum, cui etiam in senectute contigerit, ut sapientiam verasque opiniones assequi possit. Mihi quidem Antiochum, quem audis, satis belle videris attendere. Quo invento omnis ab eo quasi capite de summo bono et malo disputatio ducitur. Quid ait Aristoteles reliquique Platonis alumni? Frater et T. 

Suam denique cuique naturam esse ad vivendum ducem. Negat enim summo bono afferre incrementum diem. Maximus dolor, inquit, brevis est. Bona autem corporis huic sunt, quod posterius posui, similiora. 


Ut enim, inquit, gubernator aeque peccat, si palearum navem
evertit et si auri, item aeque peccat, qui parentem et qui
servum iniuria verberat.

Quae enim cupiditates a natura proficiscuntur, facile
explentur sine ulla iniuria, quae autem inanes sunt, iis
parendum non est.



Non quam nostram quidem, inquit Pomponius iocans; Ego vero isti, inquam, permitto. 


	Ut pulsi recurrant?
	Hanc se tuus Epicurus omnino ignorare dicit quam aut qualem esse velint qui honestate summum bonum metiantur.
	Sed fortuna fortis;
	Atque his de rebus et splendida est eorum et illustris oratio.
	Venit ad extremum;
	Sed ego in hoc resisto;
	Tenent mordicus.
	Bonum incolumis acies: misera caecitas.
	Efficiens dici potest.
	Verum hoc loco sumo verbis his eandem certe vim voluptatis Epicurum nosse quam ceteros.
	Facete M.
	Inde sermone vario sex illa a Dipylo stadia confecimus.



Hoc tu nunc in illo probas. Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere? Aut, Pylades cum sis, dices te esse Orestem, ut moriare pro amico? Quis enim confidit semper sibi illud stabile et firmum permansurum, quod fragile et caducum sit? Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Nihil opus est exemplis hoc facere longius. 


	Te enim iudicem aequum puto, modo quae dicat ille bene noris.
	Quae duo sunt, unum facit.
	In eo enim positum est id, quod dicimus esse expetendum.




	His enim rebus detractis negat se reperire in asotorum vita quod reprehendat.
	Ergo id est convenienter naturae vivere, a natura discedere.



Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum. Hos contra singulos dici est melius. Quamvis enim depravatae non sint, pravae tamen esse possunt. Quippe: habes enim a rhetoribus; Quae cum magnifice primo dici viderentur, considerata minus probabantur. Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur. Istam voluptatem, inquit, Epicurus ignorat? Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta. 

Sed quanta sit alias, nunc tantum possitne esse tanta. Quamvis enim depravatae non sint, pravae tamen esse possunt. Sed quid attinet de rebus tam apertis plura requirere? Duarum enim vitarum nobis erunt instituta capienda. Si verbum sequimur, primum longius verbum praepositum quam bonum. Ut aliquid scire se gaudeant? Quae cum essent dicta, discessimus. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Non est enim vitium in oratione solum, sed etiam in moribus. 


Quodsi, ne quo incommodo afficiare, non relinques amicum,
tamen, ne sine fructu alligatus sis, ut moriatur optabis.

Nam haec ipsa mihi erunt in promptu, quae modo audivi, nec
ante aggrediar, quam te ab istis, quos dicis, instructum
videro.




	Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus;
	Ut alios omittam, hunc appello, quem ille unum secutus est.
	Quamquam id quidem, infinitum est in hac urbe;
	Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat.
	Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum.
	Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt.




