Title: Thellungiella Al-Shehbaz
Description: Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.
Image: https://source.unsplash.com/random/?blue
Date: 28/8/2021
Keywords: design
Category: Bamity
Author: Voolith
Tags: article
Published: true
Background: #5a64b5
Color: #6c6c9e
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nonne igitur tibi videntur, inquit, mala? Nunc haec primum fortasse audientis servire debemus. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Est enim tanti philosophi tamque nobilis audacter sua decreta defendere. Sint modo partes vitae beatae. Duo Reges: constructio interrete. Sin autem est in ea, quod quidam volunt, nihil impedit hanc nostram comprehensionem summi boni. 


	Quid vero?
	Putabam equidem satis, inquit, me dixisse.
	Memini vero, inquam;
	Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego;
	Etiam beatissimum?
	Verum hoc idem saepe faciamus.




	Hic homo severus luxuriam ipsam per se reprehendendam non putat, et hercule, Torquate, ut verum loquamur, si summum bonum voluptas est, rectissime non putat.



Sed vobis voluptatum perceptarum recordatio vitam beatam facit, et quidem corpore perceptarum. At ille pellit, qui permulcet sensum voluptate. Esse enim quam vellet iniquus iustus poterat inpune. Unum est sine dolore esse, alterum cum voluptate. Omnia contraria, quos etiam insanos esse vultis. Haeret in salebra. Hoc est dicere: Non reprehenderem asotos, si non essent asoti. Dici enim nihil potest verius. Multoque hoc melius nos veriusque quam Stoici. Miserum hominem! Si dolor summum malum est, dici aliter non potest. 


	Ita prorsus, inquam;
	Illa videamus, quae a te de amicitia dicta sunt.
	Ecce aliud simile dissimile.
	Uterque enim summo bono fruitur, id est voluptate.
	Eam stabilem appellas.
	Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus.
	Erat enim Polemonis.
	Sed nimis multa.
	Idem adhuc;
	Deque his rebus satis multa in nostris de re publica libris sunt dicta a Laelio.




	Qua tu etiam inprudens utebare non numquam.
	Nulla profecto est, quin suam vim retineat a primo ad extremum.
	Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest.
	Cur, nisi quod turpis oratio est?
	Quia, si mala sunt, is, qui erit in iis, beatus non erit.



Neque solum ea communia, verum etiam paria esse dixerunt. Inquit, dasne adolescenti veniam? Hoc mihi cum tuo fratre convenit. Miserum hominem! Si dolor summum malum est, dici aliter non potest. Negat enim summo bono afferre incrementum diem. Honesta oratio, Socratica, Platonis etiam. 


Simul atque natum animal est, gaudet voluptate et eam
appetit ut bonum, aspernatur dolorem ut malum.

An potest, inquit ille, quicquam esse suavius quam nihil
dolere?



Profectus in exilium Tubulus statim nec respondere ausus; Et harum quidem rerum facilis est et expedita distinctio. Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo; An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Facit enim ille duo seiuncta ultima bonorum, quae ut essent vera, coniungi debuerunt; Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius. Sed ea mala virtuti magnitudine obruebantur. Quacumque enim ingredimur, in aliqua historia vestigium ponimus. Si quae forte-possumus. Non potes, nisi retexueris illa. 

Collatio igitur ista te nihil iuvat. Ita prorsus, inquam; Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Sed vobis voluptatum perceptarum recordatio vitam beatam facit, et quidem corpore perceptarum. 


	Quippe, inquieta cum tam docuerim gradus istam rem non habere quam virtutem, in qua sit ipsum et�am beatum.



Sed quot homines, tot sententiae; Quamquam id quidem, infinitum est in hac urbe; Quasi ego id curem, quid ille aiat aut neget. Scrupulum, inquam, abeunti; 


	Quia nec honesto quic quam honestius nec turpi turpius.
	At cum de plurimis eadem dicit, tum certe de maximis.
	Animi enim quoque dolores percipiet omnibus partibus maiores quam corporis.
	Aliter homines, aliter philosophos loqui putas oportere?
	Satis est tibi in te, satis in legibus, satis in mediocribus amicitiis praesidii.
	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.
	Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem.



Quis non odit sordidos, vanos, leves, futtiles? Si quidem, inquit, tollerem, sed relinquo. Isto modo ne improbos quidem, si essent boni viri. Itaque haec cum illis est dissensio, cum Peripateticis nulla sane. Sed ad haec, nisi molestum est, habeo quae velim. Uterque enim summo bono fruitur, id est voluptate. Et quod est munus, quod opus sapientiae? 

Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Nec vero intermittunt aut admirationem earum rerum, quae sunt ab antiquis repertae, aut investigationem novarum. Quodsi ipsam honestatem undique pertectam atque absolutam. Negat enim tenuissimo victu, id est contemptissimis escis et potionibus, minorem voluptatem percipi quam rebus exquisitissimis ad epulandum. Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset. Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare; Atque ab isto capite fluere necesse est omnem rationem bonorum et malorum. 


	Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem.
	Hanc ergo intuens debet institutum illud quasi signum absolvere.
	Duarum enim vitarum nobis erunt instituta capienda.




	Primum Theophrasti, Strato, physicum se voluit;
	Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est.
	Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur.



Reguli reiciendam; Quamquam te quidem video minime esse deterritum. Tecum optime, deinde etiam cum mediocri amico. Perge porro; Bonum valitudo: miser morbus. Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. In qua quid est boni praeter summam voluptatem, et eam sempiternam? Restatis igitur vos; Me igitur ipsum ames oportet, non mea, si veri amici futuri sumus. Isto modo ne improbos quidem, si essent boni viri. 


Sive hoc difficile est, tamen nec modus est ullus
investigandi veri, nisi inveneris, et quaerendi defatigatio
turpis est, cum id, quod quaeritur, sit pulcherrimum.

Ut in geometria, prima si dederis, danda sunt omnia.



Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Illud urgueam, non intellegere eum quid sibi dicendum sit, cum dolorem summum malum esse dixerit. Sed eum qui audiebant, quoad poterant, defendebant sententiam suam. Quam tu ponis in verbis, ego positam in re putabam. Nullus est igitur cuiusquam dies natalis. Si qua in iis corrigere voluit, deteriora fecit. Propter nos enim illam, non propter eam nosmet ipsos diligimus. Itaque et manendi in vita et migrandi ratio omnis iis rebus, quas supra dixi, metienda. 


