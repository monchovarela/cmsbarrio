Title: Liatris spheroidea Michx.
Description: Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.
Image: https://source.unsplash.com/random/?css3
Date: 6/9/2021
Keywords: cms
Category: Tempsoft
Author: Shuffledrive
Tags: blog
Published: true
Background: #2006a1
Color: #11b4fb
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qualem igitur hominem natura inchoavit? Erit enim mecum, si tecum erit. Nihil opus est exemplis hoc facere longius. Suo genere perveniant ad extremum; Hos contra singulos dici est melius. Eaedem res maneant alio modo. Duo Reges: constructio interrete. Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est. Quid autem habent admirationis, cum prope accesseris? 


	Indicant pueri, in quibus ut in speculis natura cernitur.
	Hoc mihi cum tuo fratre convenit.
	Tecum optime, deinde etiam cum mediocri amico.
	Ergo illi intellegunt quid Epicurus dicat, ego non intellego?
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?




	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Esse enim quam vellet iniquus iustus poterat inpune.
	Ita credo.
	Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D.
	Laboro autem non sine causa;



Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Quid, si etiam iucunda memoria est praeteritorum malorum? Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Iam doloris medicamenta illa Epicurea tamquam de narthecio proment: Si gravis, brevis; Tu quidem reddes; Mene ergo et Triarium dignos existimas, apud quos turpiter loquare? Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Quae sequuntur igitur? 

Quamquam id quidem licebit iis existimare, qui legerint. Aliter enim explicari, quod quaeritur, non potest. Nihilo magis. Mene ergo et Triarium dignos existimas, apud quos turpiter loquare? Si quae forte-possumus. Facete M. Nam, ut sint illa vendibiliora, haec uberiora certe sunt. Nunc haec primum fortasse audientis servire debemus. Polycratem Samium felicem appellabant. 


	An eiusdem modi?
	Illis videtur, qui illud non dubitant bonum dicere -;
	Poterat autem inpune;
	Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus.
	Beatum, inquit.
	Quis enim potest ea, quae probabilia videantur ei, non probare?
	Quid me istud rogas?
	Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia;
	Idem adhuc;
	Conferam avum tuum Drusum cum C.
	Sullae consulatum?
	Qui ita affectus, beatum esse numquam probabis;




	Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest.
	Rationis enim perfectio est virtus;
	Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere.
	Nos commodius agimus.
	Expressa vero in iis aetatibus, quae iam confirmatae sunt.




	Theophrasti igitur, inquit, tibi liber ille placet de beata vita?




Nec tamen argumentum hoc Epicurus a parvis petivit aut etiam
a bestiis, quae putat esse specula naturae, ut diceret ab
iis duce natura hanc voluptatem expeti nihil dolendi.

Quid enim ab antiquis ex eo genere, quod ad disserendum
valet, praetermissum est?



Idemne potest esse dies saepius, qui semel fuit? Longum est enim ad omnia respondere, quae a te dicta sunt. Cur haec eadem Democritus? Ratio enim nostra consentit, pugnat oratio. Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. Iam id ipsum absurdum, maximum malum neglegi. 

Hoc ille tuus non vult omnibusque ex rebus voluptatem quasi mercedem exigit. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Quorum sine causa fieri nihil putandum est. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Sedulo, inquam, faciam. Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur. Sed ego in hoc resisto; Occultum facinus esse potuerit, gaudebit; 


Ita fit, ut duo genera propter se expetendorum reperiantur,
unum, quod est in iis, in quibus completar illud extremum,
quae sunt aut animi aut corporis;

Potius inflammat, ut coercendi magis quam dedocendi esse
videantur.



Non laboro, inquit, de nomine. Putabam equidem satis, inquit, me dixisse. Bonum valitudo: miser morbus. Ita ne hoc quidem modo paria peccata sunt. Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. Hoc non est positum in nostra actione. Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse. Non potes, nisi retexueris illa. Sed quanta sit alias, nunc tantum possitne esse tanta. 

Omnes enim iucundum motum, quo sensus hilaretur. Negat enim summo bono afferre incrementum diem. Nulla profecto est, quin suam vim retineat a primo ad extremum. Itaque ad tempus ad Pisonem omnes. Non est igitur summum malum dolor. Maximus dolor, inquit, brevis est. Tu enim ista lenius, hic Stoicorum more nos vexat. Quid turpius quam sapientis vitam ex insipientium sermone pendere? At multis malis affectus. Quis istud possit, inquit, negare? 


	Ut pulsi recurrant?
	Illud dico, ea, quae dicat, praeclare inter se cohaerere.
	Numquam facies.
	At coluit ipse amicitias.
	Idemne, quod iucunde?
	Eaedem res maneant alio modo.
	Praeclare hoc quidem.
	Qualem igitur hominem natura inchoavit?




	At negat Epicurus-hoc enim vestrum lumen estquemquam, qui honeste non vivat, iucunde posse vivere.



Ecce aliud simile dissimile. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Et nemo nimium beatus est; Recte, inquit, intellegis. Nobis Heracleotes ille Dionysius flagitiose descivisse videtur a Stoicis propter oculorum dolorem. Sed non sunt in eo genere tantae commoditates corporis tamque productae temporibus tamque multae. Sed haec nihil sane ad rem; De ingenio eius in his disputationibus, non de moribus quaeritur. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Ubi ut eam caperet aut quando? 


	Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant.
	A primo, ut opinor, animantium ortu petitur origo summi boni.
	Quae si potest singula consolando levare, universa quo modo sustinebit?
	Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret?



Nihil ad rem! Ne sit sane; Universa enim illorum ratione cum tota vestra confligendum puto. Quod autem principium officii quaerunt, melius quam Pyrrho; Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Istic sum, inquit. Quid autem habent admirationis, cum prope accesseris? 

An tu me de L. Inde sermone vario sex illa a Dipylo stadia confecimus. Quid est igitur, inquit, quod requiras? Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit? Praeclarae mortes sunt imperatoriae; 


