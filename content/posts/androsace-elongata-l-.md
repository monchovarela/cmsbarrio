Title: Androsace elongata L.
Description: Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.
Image: https://source.unsplash.com/random/?design
Date: 7/3/2021
Keywords: website
Category: Y-find
Author: Devpulse
Tags: work
Published: true
Background: #764713
Color: #0db35f
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit; Non quam nostram quidem, inquit Pomponius iocans; Quae cum essent dicta, finem fecimus et ambulandi et disputandi. Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus. Non enim quaero quid verum, sed quid cuique dicendum sit. Quamquam id quidem, infinitum est in hac urbe; Duo Reges: constructio interrete. Quid, si etiam iucunda memoria est praeteritorum malorum? Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Sed ad bona praeterita redeamus. 


	Haec dicuntur inconstantissime.
	Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas.
	Ita prorsus, inquam;
	Si sapiens, ne tum quidem miser, cum ab Oroete, praetore Darei, in crucem actus est.
	Quibusnam praeteritis?
	Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat.
	Quonam modo?
	Aliter homines, aliter philosophos loqui putas oportere?



Inde igitur, inquit, ordiendum est. Beatum, inquit. Duo enim genera quae erant, fecit tria. Age sane, inquam. Teneo, inquit, finem illi videri nihil dolere. Nihil illinc huc pervenit. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Utilitatis causa amicitia est quaesita. 

Quicquid porro animo cernimus, id omne oritur a sensibus; Erat enim Polemonis. Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Propter nos enim illam, non propter eam nosmet ipsos diligimus. Quod autem principium officii quaerunt, melius quam Pyrrho; 

Illa argumenta propria videamus, cur omnia sint paria peccata. Bonum valitudo: miser morbus. Effluit igitur voluptas corporis et prima quaeque avolat saepiusque relinquit causam paenitendi quam recordandi. Tecum optime, deinde etiam cum mediocri amico. Idemne, quod iucunde? 


	Ergo in eadem voluptate eum, qui alteri misceat mulsum ipse non sitiens, et eum, qui illud sitiens bibat?



Nihilo beatiorem esse Metellum quam Regulum. Illum mallem levares, quo optimum atque humanissimum virum, Cn. Sint ista Graecorum; Et harum quidem rerum facilis est et expedita distinctio. Dolere malum est: in crucem qui agitur, beatus esse non potest. Expectoque quid ad id, quod quaerebam, respondeas. Quis, quaeso, illum negat et bonum virum et comem et humanum fuisse? An quod ita callida est, ut optime possit architectari voluptates? Re mihi non aeque satisfacit, et quidem locis pluribus. Quamvis enim depravatae non sint, pravae tamen esse possunt. 


	Cum vero paulum processerunt, lusionibus vel laboriosis delectantur, ut ne verberibus quidem deterreri possint, eaque cupiditas agendi aliquid adolescit una cum aetatibus.




	Ille incendat?
	Illa argumenta propria videamus, cur omnia sint paria peccata.
	Quare attende, quaeso.
	Hoc est dicere: Non reprehenderem asotos, si non essent asoti.
	Numquam facies.
	Est tamen ea secundum naturam multoque nos ad se expetendam magis hortatur quam superiora omnia.
	Quid de Pythagora?
	Quod si ita se habeat, non possit beatam praestare vitam sapientia.



Nam Metrodorum non puto ipsum professum, sed, cum appellaretur ab Epicuro, repudiare tantum beneficium noluisse; Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Dici enim nihil potest verius. 


	Venit enim mihi Platonis in mentem, quem accepimus primum hic disputare solitum;
	Quasi vero, inquit, perpetua oratio rhetorum solum, non etiam philosophorum sit.
	Cur, nisi quod turpis oratio est?
	Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q.
	Nam adhuc, meo fortasse vitio, quid ego quaeram non perspicis.
	Hos contra singulos dici est melius.




Sed audiamus ipsum: Compensabatur, inquit, tamen cum his
omnibus animi laetitia, quam capiebam memoria rationum
inventorumque nostrorum.

Erit enim instructus ad mortem contemnendam, ad exilium, ad
ipsum etiam dolorem.



Sin aliud quid voles, postea. Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Nihilo beatiorem esse Metellum quam Regulum. Huius ego nunc auctoritatem sequens idem faciam. Polycratem Samium felicem appellabant. Itaque ad tempus ad Pisonem omnes. 

Quid, si reviviscant Platonis illi et deinceps qui eorum auditores fuerunt, et tecum ita loquantur? Ergo, si semel tristior effectus est, hilara vita amissa est? Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Inde igitur, inquit, ordiendum est. Consequentia exquirere, quoad sit id, quod volumus, effectum. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? Est, ut dicis, inquam. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? 


	Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam;
	Quid est, quod ab ea absolvi et perfici debeat?
	Quid, si non sensus modo ei sit datus, verum etiam animus hominis?
	Dat enim intervalla et relaxat.



At cum de plurimis eadem dicit, tum certe de maximis. Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Mihi enim satis est, ipsis non satis. Nihil ad rem! Ne sit sane; Nos autem non solum beatae vitae istam esse oblectationem videmus, sed etiam levamentum miseriarum. Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? 


Quin etiam ipsi voluptarii deverticula quaerunt et virtutes
habent in ore totos dies voluptatemque primo dumtaxat expeti
dicunt, deinde consuetudine quasi alteram quandam naturam
effici, qua inpulsi multa faciant nullam quaerentes
voluptatem.

Satisne ergo pudori consulat, si quis sine teste libidini
pareat?




	Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est.
	In qua quid est boni praeter summam voluptatem, et eam sempiternam?




	Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est.
	Sed quanta sit alias, nunc tantum possitne esse tanta.
	Sed ad bona praeterita redeamus.
	Quam illa ardentis amores excitaret sui! Cur tandem?
	Quae cum dixisset paulumque institisset, Quid est?



Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Ita fit beatae vitae domina fortuna, quam Epicurus ait exiguam intervenire sapienti. Age, inquies, ista parva sunt. Primum in nostrane potestate est, quid meminerimus? Beatus autem esse in maximarum rerum timore nemo potest. Idem fecisset Epicurus, si sententiam hanc, quae nunc Hieronymi est, coniunxisset cum Aristippi vetere sententia. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Stoici autem, quod finem bonorum in una virtute ponunt, similes sunt illorum; Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem. Quantum Aristoxeni ingenium consumptum videmus in musicis? Certe non potest. 


