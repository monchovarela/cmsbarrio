Title: Rhynchospora ciliaris (Michx.) C. Mohr
Description: Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.
Image: https://source.unsplash.com/random/?css3
Date: 30/10/2020
Keywords: web
Category: Prodder
Author: Einti
Tags: blog
Published: true
Background: #2482e1
Color: #fe3260
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sint modo partes vitae beatae. Non risu potius quam oratione eiciendum? Sed potestne rerum maior esse dissensio? De illis, cum volemus. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? Scaevolam M. Duo Reges: constructio interrete. At certe gravius. Nemo igitur esse beatus potest. 

Sed tamen est aliquid, quod nobis non liceat, liceat illis. Qui potest igitur habitare in beata vita summi mali metus? Hoc unum Aristo tenuit: praeter vitia atque virtutes negavit rem esse ullam aut fugiendam aut expetendam. Sin laboramus, quis est, qui alienae modum statuat industriae? 

Neque solum ea communia, verum etiam paria esse dixerunt. Sit enim idem caecus, debilis. ALIO MODO. Callipho ad virtutem nihil adiunxit nisi voluptatem, Diodorus vacuitatem doloris. Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. Item de contrariis, a quibus ad genera formasque generum venerunt. Aeque enim contingit omnibus fidibus, ut incontentae sint. Et hercule-fatendum est enim, quod sentio -mirabilis est apud illos contextus rerum. Quantum Aristoxeni ingenium consumptum videmus in musicis? Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Sed tu istuc dixti bene Latine, parum plane. Cur post Tarentum ad Archytam? At ille pellit, qui permulcet sensum voluptate. 


	Iubet igitur nos Pythius Apollo noscere nosmet ipsos.
	Duarum enim vitarum nobis erunt instituta capienda.



Non enim quaero quid verum, sed quid cuique dicendum sit. Bonum liberi: misera orbitas. Quod vestri non item. Sed ego in hoc resisto; Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. Primum quid tu dicis breve? 


	Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum.
	Totum genus hoc Zeno et qui ab eo sunt aut non potuerunt aut noluerunt, certe reliquerunt.
	Nihilne te delectat umquam -video, quicum loquar-, te igitur, Torquate, ipsum per se nihil delectat?
	Audio equidem philosophi vocem, Epicure, sed quid tibi dicendum sit oblitus es.
	Et hunc idem dico, inquieta sed ad virtutes et ad vitia nihil interesse.



Expectoque quid ad id, quod quaerebam, respondeas. Primum Theophrasti, Strato, physicum se voluit; Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis. Verba tu fingas et ea dicas, quae non sentias? Stulti autem malorum memoria torquentur, sapientes bona praeterita grata recordatione renovata delectant. Ergo hoc quidem apparet, nos ad agendum esse natos. Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. Sed tamen est aliquid, quod nobis non liceat, liceat illis. 

Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Istam voluptatem, inquit, Epicurus ignorat? An tu me de L. Que Manilium, ab iisque M. 


Constituto autem illo, de quo ante diximus, quod honestum
esset, id esse solum bonum, intellegi necesse est pluris id,
quod honestum sit, aestimandum esse quam illa media, quae ex
eo comparentur.

Quamquam haec quidem praeposita recte et reiecta dicere
licebit.



Non est igitur summum malum dolor. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? Vide, quantum, inquam, fallare, Torquate. Honesta oratio, Socratica, Platonis etiam. Paupertas si malum est, mendicus beatus esse nemo potest, quamvis sit sapiens. Sed fortuna fortis; Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero. Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem; 


	Quoniamque in iis rebus, quae neque in virtutibus sunt neque in vitiis, est tamen quiddam, quod usui possit esse, tollendum id non est.




Quasi enim emendum eis sit, quod addant ad virtutem, primum
vilissimas res addunt, dein singulas potius, quam omnia,
quae prima natura approbavisset, ea cum honestate
coniungerent.

Quid de Platone aut de Democrito loquar?



Quis enim est, qui non videat haec esse in natura rerum tria? At modo dixeras nihil in istis rebus esse, quod interesset. Ut aliquid scire se gaudeant? Sed tamen intellego quid velit. Et homini, qui ceteris animantibus plurimum praestat, praecipue a natura nihil datum esse dicemus? Age, inquies, ista parva sunt. 


	Si longus, levis.
	Neminem videbis ita laudatum, ut artifex callidus comparandarum voluptatum diceretur.
	At certe gravius.
	Ut in geometria, prima si dederis, danda sunt omnia.
	At certe gravius.
	Quid turpius quam sapientis vitam ex insipientium sermone pendere?
	Praeclare hoc quidem.
	Illa tamen simplicia, vestra versuta.
	Sed haec omittamus;
	Teneo, inquit, finem illi videri nihil dolere.




	Quod autem meum munus dicis non equidem recuso, sed te adiungo socium.
	Huius ego nunc auctoritatem sequens idem faciam.
	Quid ergo hoc loco intellegit honestum?



Quod ea non occurrentia fingunt, vincunt Aristonem; Quid ad utilitatem tantae pecuniae? Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere. Sed mehercule pergrata mihi oratio tua. Quid ergo attinet dicere: Nihil haberem, quod reprehenderem, si finitas cupiditates haberent? Apparet statim, quae sint officia, quae actiones. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Non enim ipsa genuit hominem, sed accepit a natura inchoatum. Non enim ipsa genuit hominem, sed accepit a natura inchoatum. 


	Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus;




	Beatum, inquit.
	Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia.
	Sint ista Graecorum;
	Nos cum te, M.
	Immo videri fortasse.
	Quid censes in Latino fore?
	Sumenda potius quam expetenda.
	Quam ob rem tandem, inquit, non satisfacit?
	Audeo dicere, inquit.
	Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia.
	Quid ergo?
	Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est?




	Age nunc isti doceant, vel tu potius quis enim ista melius?
	Non est igitur summum malum dolor.
	Unum est sine dolore esse, alterum cum voluptate.
	Rhetorice igitur, inquam, nos mavis quam dialectice disputare?
	Bonum negas esse divitias, praepos�tum esse dicis?
	Summum en�m bonum exposuit vacuitatem doloris;



Apparet statim, quae sint officia, quae actiones. Bonum negas esse divitias, praepos�tum esse dicis? Sit hoc ultimum bonorum, quod nunc a me defenditur; Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est; Quantum Aristoxeni ingenium consumptum videmus in musicis? Confecta res esset. Haec dicuntur inconstantissime. Putabam equidem satis, inquit, me dixisse. Quid enim mihi potest esse optatius quam cum Catone, omnium virtutum auctore, de virtutibus disputare? Aliud igitur esse censet gaudere, aliud non dolere. 


