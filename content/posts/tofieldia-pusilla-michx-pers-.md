Title: Tofieldia pusilla (Michx.) Pers.
Description: Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.
Image: https://source.unsplash.com/random/?green
Date: 8/12/2020
Keywords: post
Category: Tresom
Author: Chatterbridge
Tags: design
Published: true
Background: #55f97b
Color: #d434dd
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Si enim ita est, vide ne facinus facias, cum mori suadeas. Ita enim vivunt quidam, ut eorum vita refellatur oratio. Inde igitur, inquit, ordiendum est. Sed quid sentiat, non videtis. Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt. Illa tamen simplicia, vestra versuta. Duo Reges: constructio interrete. Teneo, inquit, finem illi videri nihil dolere. 


	Memini vero, inquam;
	Innumerabilia dici possunt in hanc sententiam, sed non necesse est.
	Magna laus.
	Si mala non sunt, iacet omnis ratio Peripateticorum.
	Haeret in salebra.
	Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim.
	Nulla erit controversia.
	Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego;



Ille enim occurrentia nescio quae comminiscebatur; Et quidem, inquit, vehementer errat; Quae cum dixisset, finem ille. Nam cui proposito sit conservatio sui, necesse est huic partes quoque sui caras suo genere laudabiles. 


	Non igitur bene.
	Eadem nunc mea adversum te oratio est.
	Quo modo?
	Nemo nostrum istius generis asotos iucunde putat vivere.




	Estne, quaeso, inquam, sitienti in bibendo voluptas?
	Apparet statim, quae sint officia, quae actiones.
	Non enim, si omnia non sequebatur, idcirco non erat ortus illinc.
	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.




	Quam haec sunt contraria! hic si definire, si dividere didicisset, si loquendi vim, si denique consuetudinem verborum teneret, numquam in tantas salebras incidisset.



Non modo carum sibi quemque, verum etiam vehementer carum esse? Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Quoniam, si dis placet, ab Epicuro loqui discimus. Nam si propter voluptatem, quae est ista laus, quae possit e macello peti? 

Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Ita relinquet duas, de quibus etiam atque etiam consideret. Illa argumenta propria videamus, cur omnia sint paria peccata. Quia, si mala sunt, is, qui erit in iis, beatus non erit. At enim, qua in vita est aliquid mali, ea beata esse non potest. Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur. Ergo id est convenienter naturae vivere, a natura discedere. Quid est, quod ab ea absolvi et perfici debeat? 


	Sed ne, dum huic obsequor, vobis molestus sim.
	Quid ergo dubitamus, quin, si non dolere voluptas sit summa, non esse in voluptate dolor sit maximus?
	Putabam equidem satis, inquit, me dixisse.
	Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.




	Somnum denique nobis, nisi requietem corporibus et is medicinam quandam laboris afferret, contra naturam putaremus datum;



Si ista mala sunt, in quae potest incidere sapiens, sapientem esse non esse ad beate vivendum satis. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Isto modo, ne si avia quidem eius nata non esset. Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Quicquid porro animo cernimus, id omne oritur a sensibus; Ut id aliis narrare gestiant? Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Summus dolor plures dies manere non potest? An eum locum libenter invisit, ubi Demosthenes et Aeschines inter se decertare soliti sunt? 


	Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus;
	Quae autem natura suae primae institutionis oblita est?
	Itaque in rebus minime obscuris non multus est apud eos disserendi labor.
	Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus;



Cave putes quicquam esse verius. Quamquam haec quidem praeposita recte et reiecta dicere licebit. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Quis Aristidem non mortuum diligit? Sed haec omittamus; Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; At ille pellit, qui permulcet sensum voluptate. Gerendus est mos, modo recte sentiat. 

Proclivi currit oratio. Nos vero, inquit ille; Nunc de hominis summo bono quaeritur; Atque ab isto capite fluere necesse est omnem rationem bonorum et malorum. Tria genera bonorum; Maximas vero virtutes iacere omnis necesse est voluptate dominante. Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Tu vero, inquam, ducas licet, si sequetur; An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Tum Lucius: Mihi vero ista valde probata sunt, quod item fratri puto. Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt. 

Longum est enim ad omnia respondere, quae a te dicta sunt. Ergo ita: non posse honeste vivi, nisi honeste vivatur? Quod autem satis est, eo quicquid accessit, nimium est; Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere? Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Omnia contraria, quos etiam insanos esse vultis. Quae cum magnifice primo dici viderentur, considerata minus probabantur. Recte, inquit, intellegis. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Philosophi autem in suis lectulis plerumque moriuntur. 

Immo alio genere; Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Age nunc isti doceant, vel tu potius quis enim ista melius? Bestiarum vero nullum iudicium puto. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Itaque hic ipse iam pridem est reiectus; Quamquam tu hanc copiosiorem etiam soles dicere. Graecum enim hunc versum nostis omnes-: Suavis laborum est praeteritorum memoria. 

Quamquam tu hanc copiosiorem etiam soles dicere. Illud non continuo, ut aeque incontentae. Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. Si longus, levis. Ut id aliis narrare gestiant? Quare hoc videndum est, possitne nobis hoc ratio philosophorum dare. Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Frater et T. 


	Minime id quidem, inquam, alienum, multumque ad ea, quae quaerimus, explicatio tua ista profecerit.
	Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas?
	Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum.




Quarum adeo omnium sententia pronuntiabit primum de
voluptate nihil esse ei loci, non modo ut sola ponatur in
summi boni sede, quam quaerimus, sed ne illo quidem modo, ut
ad honestatem applicetur.

De malis autem et bonis ab iis animalibus, quae nondum
depravata sint, ait optime iudicari.




Quibus ex omnibus iudicari potest non modo non impediri
rationem amicitiae, si summum bonum in voluptate ponatur,
sed sine hoc institutionem omnino amicitiae non posse
reperiri.

Vos autem cum perspicuis dubia debeatis illustrare, dubiis
perspicua conamini tollere.




