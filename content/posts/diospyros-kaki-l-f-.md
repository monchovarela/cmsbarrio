Title: Diospyros kaki L. f.
Description: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.
Image: https://source.unsplash.com/random/?design
Date: 24/11/2020
Keywords: article
Category: Stringtough
Author: Minyx
Tags: cms
Published: true
Background: #799b6a
Color: #b378e7
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Minime vero, inquit ille, consentit. Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Bonum patria: miserum exilium. Duo Reges: constructio interrete. Modo etiam paulum ad dexteram de via declinavi, ut ad Pericli sepulcrum accederem. Quid, si reviviscant Platonis illi et deinceps qui eorum auditores fuerunt, et tecum ita loquantur? 


	Primum divisit ineleganter;
	Nihil enim desiderabile concupiscunt, plusque in ipsa iniuria detrimenti est quam in iis rebus emolumenti, quae pariuntur iniuria.




	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.
	Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare;
	Et quod est munus, quod opus sapientiae?
	Quam ob rem tandem, inquit, non satisfacit?
	Quorum altera prosunt, nocent altera.




	Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat.
	Saepe ab Aristotele, a Theophrasto mirabiliter est laudata per se ipsa rerum scientia;
	Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.



Torquatus, is qui consul cum Cn. Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia. Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Et tamen ego a philosopho, si afferat eloquentiam, non asperner, si non habeat, non admodum flagitem. Quid ad utilitatem tantae pecuniae? Deinceps videndum est, quoniam satis apertum est sibi quemque natura esse carum, quae sit hominis natura. 

Quid sequatur, quid repugnet, vident. Sed residamus, inquit, si placet. At multis malis affectus. At, si voluptas esset bonum, desideraret. Duo enim genera quae erant, fecit tria. Multoque hoc melius nos veriusque quam Stoici. 

Eaedem res maneant alio modo. Sed in rebus apertissimis nimium longi sumus. Ille incendat? Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? Cur post Tarentum ad Archytam? Putabam equidem satis, inquit, me dixisse. 


	Nunc idem, nisi molestum est, quoniam tibi non omnino displicet definire et id facis, cum vis, velim definias quid sit voluptas, de quo omnis haec quaestio est.




	Istud quidem, inquam, optime dicis, sed quaero nonne tibi faciendum idem sit nihil dicenti bonum, quod non rectum honestumque sit, reliquarum rerum discrimen omne tollenti.




	Quae sequuntur igitur?
	At miser, si in flagitiosa et vitiosa vita afflueret voluptatibus.
	Venit ad extremum;
	Conferam tecum, quam cuique verso rem subicias;
	Sint ista Graecorum;
	Ut aliquid scire se gaudeant?
	Restatis igitur vos;
	De ingenio eius in his disputationibus, non de moribus quaeritur.



At eum nihili facit; Quod si ita sit, cur opera philosophiae sit danda nescio. 

Sed quot homines, tot sententiae; Duae sunt enim res quoque, ne tu verba solum putes. Sed quot homines, tot sententiae; Cur, nisi quod turpis oratio est? Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Bona autem corporis huic sunt, quod posterius posui, similiora. Dolere malum est: in crucem qui agitur, beatus esse non potest. 


Quarum cum una sit, qua mores conformari putantur, differo
eam partem, quae quasi stirps ets huius quaestionis.

Omnium enim rerum principia parva sunt, sed suis
progressionibus usa augentur nec sine causa;



Bonum integritas corporis: misera debilitas. Quae contraria sunt his, malane? Rhetorice igitur, inquam, nos mavis quam dialectice disputare? Recte dicis; At multis se probavit. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Sed plane dicit quod intellegit. Quamquam tu hanc copiosiorem etiam soles dicere. Si enim ad populum me vocas, eum. Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Tum mihi Piso: Quid ergo? 

Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit? Odium autem et invidiam facile vitabis. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Quae hic rei publicae vulnera inponebat, eadem ille sanabat. Hoc loco tenere se Triarius non potuit. Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus. Ergo in utroque exercebantur, eaque disciplina effecit tantam illorum utroque in genere dicendi copiam. Expressa vero in iis aetatibus, quae iam confirmatae sunt. 


	Sed nimis multa.
	Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam.
	Scrupulum, inquam, abeunti;
	Idemne, quod iucunde?
	Negare non possum.
	Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt.
	Tria genera bonorum;
	Itaque primos congressus copulationesque et consuetudinum instituendarum voluntates fieri propter voluptatem;




	Istam voluptatem, inquit, Epicurus ignorat?
	Non est enim vitium in oratione solum, sed etiam in moribus.
	Res tota, Torquate, non doctorum hominum, velle post mortem epulis celebrari memoriam sui nominis.
	Miserum hominem! Si dolor summum malum est, dici aliter non potest.



Nihil opus est exemplis hoc facere longius. Aliter enim nosmet ipsos nosse non possumus. Sic enim censent, oportunitatis esse beate vivere. Et ais, si una littera commota sit, fore tota ut labet disciplina. Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Longum est enim ad omnia respondere, quae a te dicta sunt. 


Cur ad reliquos Pythagoreos, Echecratem, Timaeum, Arionem,
Locros, ut, cum Socratem expressisset, adiungeret
Pythagoreorum disciplinam eaque, quae Socrates repudiabat,
addisceret?

Habes, inquam, Cato, formam eorum, de quibus loquor,
philosophorum.



Quid, si etiam iucunda memoria est praeteritorum malorum? Materiam vero rerum et copiam apud hos exilem, apud illos uberrimam reperiemus. Restatis igitur vos; Philosophi autem in suis lectulis plerumque moriuntur. At iam decimum annum in spelunca iacet. Praeclarae mortes sunt imperatoriae; Sed quia studebat laudi et dignitati, multum in virtute processerat. Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur. Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Aeque enim contingit omnibus fidibus, ut incontentae sint. Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium. 


