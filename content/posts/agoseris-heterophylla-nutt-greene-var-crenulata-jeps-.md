Title: Agoseris heterophylla (Nutt.) Greene var. crenulata Jeps.
Description: Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.
Image: https://source.unsplash.com/random/?developer
Date: 7/5/2021
Keywords: design
Category: Quo Lux
Author: Wikizz
Tags: cms
Published: true
Background: #acbee9
Color: #41ca29
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Idemque diviserunt naturam hominis in animum et corpus. Quid est igitur, inquit, quod requiras? Quid est igitur, inquit, quod requiras? Si longus, levis; Duo Reges: constructio interrete. Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Atqui reperies, inquit, in hoc quidem pertinacem; Quid censes in Latino fore? Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? 


	Suam denique cuique naturam esse ad vivendum ducem.
	Putabam equidem satis, inquit, me dixisse.
	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.
	Mihi, inquam, qui te id ipsum rogavi?
	Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit?




	Stoici scilicet.
	Philosophi autem in suis lectulis plerumque moriuntur.
	Moriatur, inquit.
	Ergo illi intellegunt quid Epicurus dicat, ego non intellego?
	Beatum, inquit.
	Qui enim existimabit posse se miserum esse beatus non erit.
	Deinde dolorem quem maximum?
	Primum divisit ineleganter;




	Quid de Pythagora?
	Ita redarguitur ipse a sese, convincunturque scripta eius probitate ipsius ac moribus.
	Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat?
	Suo enim quisque studio maxime ducitur.
	Est enim tanti philosophi tamque nobilis audacter sua decreta defendere.



Atqui reperies, inquit, in hoc quidem pertinacem; Traditur, inquit, ab Epicuro ratio neglegendi doloris. Quod vestri non item. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Gerendus est mos, modo recte sentiat. 

Ego vero isti, inquam, permitto. Sed id ne cogitari quidem potest quale sit, ut non repugnet ipsum sibi. Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. De quibus cupio scire quid sentias. Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. Ergo hoc quidem apparet, nos ad agendum esse natos. Sin aliud quid voles, postea. Quos nisi redarguimus, omnis virtus, omne decus, omnis vera laus deserenda est. Sed quid minus probandum quam esse aliquem beatum nec satis beatum? Neque enim civitas in seditione beata esse potest nec in discordia dominorum domus; 


Sin ea non neglegemus neque tamen ad finem summi boni
referemus, non multum ab Erilli levitate aberrabimus.

Modo etiam paulum ad dexteram de via declinavi, ut ad
Pericli sepulcrum accederem.



Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Sed quid sentiat, non videtis. Ubi ut eam caperet aut quando? Aeque enim contingit omnibus fidibus, ut incontentae sint. Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. Et ille ridens: Video, inquit, quid agas; 

Non autem hoc: igitur ne illud quidem. Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium. Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Dolere malum est: in crucem qui agitur, beatus esse non potest. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est. 


	Sed quot homines, tot sententiae;
	Vadem te ad mortem tyranno dabis pro amico, ut Pythagoreus ille Siculo fecit tyranno?
	Tum Lucius: Mihi vero ista valde probata sunt, quod item fratri puto.
	Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres.




	Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas?



Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Tu autem, si tibi illa probabantur, cur non propriis verbis ea tenebas? Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Quam ob rem tandem, inquit, non satisfacit? Quae iam oratio non a philosopho aliquo, sed a censore opprimenda est. 


	Verum ut haec non in posteris et in consequentibus, sed in primis continuo peccata sunt, sic ea, quae proficiscuntur a virtute, susceptione prima, non perfectione recta sunt iudicanda.



Nulla erit controversia. Sed ad haec, nisi molestum est, habeo quae velim. Scrupulum, inquam, abeunti; Quamquam tu hanc copiosiorem etiam soles dicere. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. Istam voluptatem, inquit, Epicurus ignorat? Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. 


	Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere.
	Aut, Pylades cum sis, dices te esse Orestem, ut moriare pro amico?
	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.
	Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio.
	Nam, ut sint illa vendibiliora, haec uberiora certe sunt.
	Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis;




	Etiam beatissimum?
	Placet igitur tibi, Cato, cum res sumpseris non concessas, ex illis efficere, quod velis?
	Refert tamen, quo modo.
	Sint ista Graecorum;
	Istic sum, inquit.
	Illa argumenta propria videamus, cur omnia sint paria peccata.
	Cur iustitia laudatur?
	Aliter enim nosmet ipsos nosse non possumus.



Tum Torquatus: Prorsus, inquit, assentior; Ne discipulum abducam, times. At certe gravius. Si longus, levis; Si longus, levis dictata sunt. Quas enim kakaw Graeci appellant, vitia malo quam malitias nominare. 

Vide, quantum, inquam, fallare, Torquate. An haec ab eo non dicuntur? Apparet statim, quae sint officia, quae actiones. Vide igitur ne non debeas verbis nostris uti, sententiis tuis. Tecum optime, deinde etiam cum mediocri amico. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Quare conare, quaeso. 


Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam
causam Zenoni desciscendi fuisse.

Hinc ceteri particulas arripere conati suam quisque videro
voluit afferre sententiam.



Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Estne, quaeso, inquam, sitienti in bibendo voluptas? Ut alios omittam, hunc appello, quem ille unum secutus est. Invidiosum nomen est, infame, suspectum. Sine ea igitur iucunde negat posse se vivere? Nobis aliter videtur, recte secusne, postea; At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Bona autem corporis huic sunt, quod posterius posui, similiora. 


