Title: Senna alata (L.) Roxb.
Description: Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.
Image: https://source.unsplash.com/random/?css3
Date: 13/6/2021
Keywords: article
Category: Lotlux
Author: Kare
Tags: cms
Published: true
Background: #3f4ff1
Color: #fdc4f8
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit. Nihil opus est exemplis hoc facere longius. Quid ait Aristoteles reliquique Platonis alumni? Ad eos igitur converte te, quaeso. Omnes enim iucundum motum, quo sensus hilaretur. Summus dolor plures dies manere non potest? Omnia contraria, quos etiam insanos esse vultis. Duo Reges: constructio interrete. Ex quo, id quod omnes expetunt, beate vivendi ratio inveniri et comparari potest. 

At iam decimum annum in spelunca iacet. Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? Tubulo putas dicere? A mene tu? Quae rursus dum sibi evelli ex ordine nolunt, horridiores evadunt, asperiores, duriores et oratione et moribus. Idem iste, inquam, de voluptate quid sentit? 

Sed quae tandem ista ratio est? Haec para/doca illi, nos admirabilia dicamus. Quis enim redargueret? Immo alio genere; Hanc ergo intuens debet institutum illud quasi signum absolvere. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Immo alio genere; 


	Quae tamen a te agetur non melior, quam illae sunt, quas interdum optines.
	Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius.
	Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus.
	Sed in rebus apertissimis nimium longi sumus.




	Haec dicuntur inconstantissime.
	Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam;
	Idemne, quod iucunde?
	Sed videbimus.
	Tenent mordicus.
	Reguli reiciendam;
	Quo tandem modo?
	Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres.
	Numquam facies.
	Nunc dicam de voluptate, nihil scilicet novi, ea tamen, quae te ipsum probaturum esse confidam.




	Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia.
	Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.
	Nam, ut sint illa vendibiliora, haec uberiora certe sunt.




	Illi autem, quibus summum bonum sine virtute est, non dabunt fortasse vitam beatam habere, in quo iure possit gloriari, etsi illi quidem etiam voluptates faciunt interdum gloriosas.




	Erat enim Polemonis.
	Eiuro, inquit adridens, iniquum, hac quidem de re;
	At multis malis affectus.
	De quibus cupio scire quid sentias.
	Poterat autem inpune;
	Quae cum essent dicta, discessimus.
	Simus igitur contenti his.
	Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant.



Cur id non ita fit? Qui autem diffidet perpetuitati bonorum suorum, timeat necesse est, ne aliquando amissis illis sit miser. Quid de Platone aut de Democrito loquar? Quae autem natura suae primae institutionis oblita est? 


Istud quidem, inquam, optime dicis, sed quaero nonne tibi
faciendum idem sit nihil dicenti bonum, quod non rectum
honestumque sit, reliquarum rerum discrimen omne tollenti.

Causa autem fuit huc veniendi ut quosdam hinc libros
promerem.




	Est enim effectrix multarum et magnarum voluptatum.
	Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam.
	Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere.
	Itaque sensibus rationem adiunxit et ratione effecta sensus non reliquit.



Quid turpius quam sapientis vitam ex insipientium sermone pendere? Re mihi non aeque satisfacit, et quidem locis pluribus. 

Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Qui ita affectus, beatum esse numquam probabis; Quid de Pythagora? Dic in quovis conventu te omnia facere, ne doleas. Quorum sine causa fieri nihil putandum est. Quam si explicavisset, non tam haesitaret. Quod equidem non reprehendo; Quonam, inquit, modo? Nihil sane. 

Estne, quaeso, inquam, sitienti in bibendo voluptas? Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas? Nam, ut paulo ante docui, augendae voluptatis finis est doloris omnis amotio. Si enim ad populum me vocas, eum. Dicimus aliquem hilare vivere; Haec igitur Epicuri non probo, inquam. 


	Atque hoc dabitis, ut opinor, si modo sit aliquid esse beatum, id oportere totum poni in potestate sapientis.




Tantaque est vis talibus in studiis, ut eos etiam, qui sibi
alios proposuerunt fines bonorum, quos utilitate aut
voluptate dirigunt, tamen in rebus quaerendis explicandisque
naturis aetates contenere videamus.

Compensabatur, inquit, cum summis doloribus laetitia.




	Atqui, inquam, Cato, si istud optinueris, traducas me ad te totum licebit.
	Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer.
	Sed quid minus probandum quam esse aliquem beatum nec satis beatum?



Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Aut, Pylades cum sis, dices te esse Orestem, ut moriare pro amico? Sed haec in pueris; Atqui pugnantibus et contrariis studiis consiliisque semper utens nihil quieti videre, nihil tranquilli potest. 

Nescio quo modo praetervolavit oratio. Ut in geometria, prima si dederis, danda sunt omnia. Quorum sine causa fieri nihil putandum est. Sed virtutem ipsam inchoavit, nihil amplius. Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur. 

Quoniam, si dis placet, ab Epicuro loqui discimus. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Summum en�m bonum exposuit vacuitatem doloris; Invidiosum nomen est, infame, suspectum. Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Quid ergo? Illud dico, ea, quae dicat, praeclare inter se cohaerere. Quid autem habent admirationis, cum prope accesseris? Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere. 


