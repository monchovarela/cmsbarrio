Title: Tradescantia bracteata Small
Description: Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.
Image: https://source.unsplash.com/random/?iphone
Date: 22/11/2020
Keywords: web
Category: Viva
Author: Babblestorm
Tags: design
Published: true
Background: #40b788
Color: #6cca1f
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Quod ea non occurrentia fingunt, vincunt Aristonem; Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Quamquam tu hanc copiosiorem etiam soles dicere. Duo Reges: constructio interrete. Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. 


	Ut scias me intellegere, primum idem esse dico voluptatem, quod ille don.
	Conferam tecum, quam cuique verso rem subicias;
	Intrandum est igitur in rerum naturam et penitus quid ea postulet pervidendum;
	Terram, mihi crede, ea lanx et maria deprimet.




	Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt.
	Suo genere perveniant ad extremum;
	Quod vestri quidem vel optime disputant, nihil opus esse eum, qui philosophus futurus sit, scire litteras.
	Quid enim necesse est, tamquam meretricem in matronarum coetum, sic voluptatem in virtutum concilium adducere?



Igitur neque stultorum quisquam beatus neque sapientium non beatus. Dicimus aliquem hilare vivere; Sed ne, dum huic obsequor, vobis molestus sim. 


	Tubulo putas dicere?
	Bona autem corporis huic sunt, quod posterius posui, similiora.
	Quis enim redargueret?
	Illa enim, quae prosunt aut quae nocent, aut bona sunt aut mala, quae sint paria necesse est.
	ALIO MODO.
	Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis.
	Audeo dicere, inquit.
	Illa tamen simplicia, vestra versuta.



Mene ergo et Triarium dignos existimas, apud quos turpiter loquare? Conferam avum tuum Drusum cum C. Satis est ad hoc responsum. Laboro autem non sine causa; Quae dici eadem de ceteris virtutibus possunt, quarum omnium fundamenta vos in voluptate tamquam in aqua ponitis. Quod autem in homine praestantissimum atque optimum est, id deseruit. Nunc reliqua videamus, nisi aut ad haec, Cato, dicere aliquid vis aut nos iam longiores sumus. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri. 


	Sed quae tandem ista ratio est?
	Quod eo liquidius faciet, si perspexerit rerum inter eas verborumne sit controversia.
	Quamquam te quidem video minime esse deterritum.
	Ille vero, si insipiens-quo certe, quoniam tyrannus -, numquam beatus;




Critolaus imitari voluit antiquos, et quidem est gravitate
proximus, et redundat oratio, ac tamen is quidem in patriis
institutis manet.

Quam haec sunt contraria! hic si definire, si dividere
didicisset, si loquendi vim, si denique consuetudinem
verborum teneret, numquam in tantas salebras incidisset.



Nihilo magis. Atqui reperies, inquit, in hoc quidem pertinacem; Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Inde sermone vario sex illa a Dipylo stadia confecimus. 


	Cumque duae sint artes, quibus perfecte ratio et oratio compleatur, una inveniendi, altera disserendi, hanc posteriorem et Stoici et Peripatetici, priorem autem illi egregie tradiderunt, hi omnino ne attigerunt quidem.



Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Mihi, inquam, qui te id ipsum rogavi? Bonum liberi: misera orbitas. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. 

Nemo igitur esse beatus potest. Idem fecisset Epicurus, si sententiam hanc, quae nunc Hieronymi est, coniunxisset cum Aristippi vetere sententia. Bonum liberi: misera orbitas. Est tamen ea secundum naturam multoque nos ad se expetendam magis hortatur quam superiora omnia. 


	Peccata paria.
	Si quicquam extra virtutem habeatur in bonis.
	Quid Zeno?
	Atqui reperies, inquit, in hoc quidem pertinacem;
	Ita credo.
	Itaque fecimus.
	Quis enim redargueret?
	Sed quid minus probandum quam esse aliquem beatum nec satis beatum?
	Restatis igitur vos;
	Animi enim quoque dolores percipiet omnibus partibus maiores quam corporis.
	Quid ergo?
	Tum ille timide vel potius verecunde: Facio, inquit.



Non laboro, inquit, de nomine. An haec ab eo non dicuntur? Si enim ad populum me vocas, eum. Nam memini etiam quae nolo, oblivisci non possum quae volo. O magnam vim ingenii causamque iustam, cur nova existeret disciplina! Perge porro. Sin laboramus, quis est, qui alienae modum statuat industriae? 


	Et ais, si una littera commota sit, fore tota ut labet disciplina.



Ergo illi intellegunt quid Epicurus dicat, ego non intellego? Sed venio ad inconstantiae crimen, ne saepius dicas me aberrare; Primum Theophrasti, Strato, physicum se voluit; At, si voluptas esset bonum, desideraret. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Age nunc isti doceant, vel tu potius quis enim ista melius? 


	At quanta conantur! Mundum hunc omnem oppidum esse nostrum! Incendi igitur eos, qui audiunt, vides.
	Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere.
	Sed quae tandem ista ratio est?
	Ne amores quidem sanctos a sapiente alienos esse arbitrantur.



Atqui eorum nihil est eius generis, ut sit in fine atque extrerno bonorum. Simus igitur contenti his. Longum est enim ad omnia respondere, quae a te dicta sunt. Minime vero, inquit ille, consentit. Tum Torquatus: Prorsus, inquit, assentior; Iubet igitur nos Pythius Apollo noscere nosmet ipsos. Istam voluptatem perpetuam quis potest praestare sapienti? Inde igitur, inquit, ordiendum est. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. 


Quaeque de virtutibus dicta sunt, quem ad modum eae semper
voluptatibus inhaererent, eadem de amicitia dicenda sunt.

Ut placet, inquit, etsi enim illud erat aptius, aequum
cuique concedere.



Quid censes in Latino fore? Atque ego: Scis me, inquam, istud idem sentire, Piso, sed a te opportune facta mentio est. Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Sed ad haec, nisi molestum est, habeo quae velim. Haec dicuntur inconstantissime. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. 


