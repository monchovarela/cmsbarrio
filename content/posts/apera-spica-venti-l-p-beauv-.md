Title: Apera spica-venti (L.) P. Beauv.
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?code
Date: 27/11/2020
Keywords: article
Category: Asoka
Author: Mybuzz
Tags: portfolio
Published: true
Background: #3956cc
Color: #c7cd0d
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas? Eiuro, inquit adridens, iniquum, hac quidem de re; Est igitur officium eius generis, quod nec in bonis ponatur nec in contrariis. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. 


	Haec et tu ita posuisti, et verba vestra sunt.
	Itaque his sapiens semper vacabit.
	Bonum integritas corporis: misera debilitas.
	Dicimus aliquem hilare vivere;



Eademne, quae restincta siti? Quid ad utilitatem tantae pecuniae? Haec mihi videtur delicatior, ut ita dicam, molliorque ratio, quam virtutis vis gravitasque postulat. 

Sed ego in hoc resisto; Traditur, inquit, ab Epicuro ratio neglegendi doloris. Sed residamus, inquit, si placet. An haec ab eo non dicuntur? Quam ob rem tandem, inquit, non satisfacit? Perge porro; Ut alios omittam, hunc appello, quem ille unum secutus est. Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant. 


	Sed quid minus probandum quam esse aliquem beatum nec satis beatum?




	Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus.
	Nam diligi et carum esse iucundum est propterea, quia tutiorem vitam et voluptatem pleniorem efficit.
	Ita cum ea volunt retinere, quae superiori sententiae conveniunt, in Aristonem incidunt;
	Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera.



At iam decimum annum in spelunca iacet. Eam stabilem appellas. 


Cum autem venissemus in Academiae non sine causa nobilitata
spatia, solitudo erat ea, quam volueramus.

Nam prius a se poterit quisque discedere quam appetitum
earum rerum, quae sibi conducant, amittere.




	Erat enim Polemonis.
	Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter.
	Erat enim Polemonis.
	Iam id ipsum absurdum, maximum malum neglegi.
	Sed fortuna fortis;
	Gloriosa ostentatio in constituendo summo bono.
	Peccata paria.
	Cum autem in quo sapienter dicimus, id a primo rectissime dicitur.



Zenonis est, inquam, hoc Stoici. Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus. Ut in voluptate sit, qui epuletur, in dolore, qui torqueatur. Ergo hoc quidem apparet, nos ad agendum esse natos. Praeclarae mortes sunt imperatoriae; Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit? Nos paucis ad haec additis finem faciamus aliquando; Quod praeceptum quia maius erat, quam ut ab homine videretur, idcirco assignatum est deo. Solum praeterea formosum, solum liberum, solum civem, stultost; Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest. Non est igitur summum malum dolor. 

De hominibus dici non necesse est. Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere. Semovenda est igitur voluptas, non solum ut recta sequamini, sed etiam ut loqui deceat frugaliter. Tecum optime, deinde etiam cum mediocri amico. Non enim iam stirpis bonum quaeret, sed animalis. Sed ad bona praeterita redeamus. Haeret in salebra. Bona autem corporis huic sunt, quod posterius posui, similiora. Non pugnem cum homine, cur tantum habeat in natura boni; 


	Nunc vero a primo quidem mirabiliter occulta natura est nec perspici nec cognosci potest.
	Id enim volumus, id contendimus, ut officii fructus sit ipsum officium.
	Dic in quovis conventu te omnia facere, ne doleas.
	Quorum sine causa fieri nihil putandum est.
	Quid enim de amicitia statueris utilitatis causa expetenda vides.



Numquam facies. Diodorus, eius auditor, adiungit ad honestatem vacuitatem doloris. Prave, nequiter, turpiter cenabat; Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. 

Nisi autem rerum natura perspecta erit, nullo modo poterimus sensuum iudicia defendere. Illa argumenta propria videamus, cur omnia sint paria peccata. Hic nihil fuit, quod quaereremus. Duo Reges: constructio interrete. Nam de summo mox, ut dixi, videbimus et ad id explicandum disputationem omnem conferemus. Cum audissem Antiochum, Brute, ut solebam, cum M. 

Esse enim, nisi eris, non potes. Sed ego in hoc resisto; Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur. Nihil ad rem! Ne sit sane; Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. 


	Quia, cum a Zenone, inquam, hoc magnifice tamquam ex oraculo editur: Virtus ad beate vivendum se ipsa contenta est, et Quare?




Hoc vero non videre, maximo argumento esse voluptatem illam,
qua sublata neget se intellegere omnino quid sit bonum-eam
autem ita persequitur: quae palato percipiatur, quae
auribus;

Neque enim disputari sine reprehensione nec cum iracundia
aut pertinacia recte disputari potest.



Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Et harum quidem rerum facilis est et expedita distinctio. Ea possunt paria non esse. Illum mallem levares, quo optimum atque humanissimum virum, Cn. Sed nimis multa. Bestiarum vero nullum iudicium puto. 


	Quonam, inquit, modo?
	Itaque si aut requietem natura non quaereret aut eam posset alia quadam ratione consequi.
	Quae sequuntur igitur?
	Venit enim mihi Platonis in mentem, quem accepimus primum hic disputare solitum;




	Longum est enim ad omnia respondere, quae a te dicta sunt.
	Sed quoniam et advesperascit et mihi ad villam revertendum est, nunc quidem hactenus;
	Habent enim et bene longam et satis litigiosam disputationem.
	Non quaero, quid dicat, sed quid convenienter possit rationi et sententiae suae dicere.
	Nam si propter voluptatem, quae est ista laus, quae possit e macello peti?
	Mihi enim erit isdem istis fortasse iam utendum.
	Quis tibi ergo istud dabit praeter Pyrrhonem, Aristonem eorumve similes, quos tu non probas?




