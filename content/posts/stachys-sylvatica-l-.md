Title: Stachys sylvatica L.
Description: Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.
Image: https://source.unsplash.com/random/?aqua
Date: 7/10/2021
Keywords: work
Category: Quo Lux
Author: Fiveclub
Tags: cms
Published: true
Background: #ad0022
Color: #cf4beb
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Negat esse eam, inquit, propter se expetendam. Gerendus est mos, modo recte sentiat. Quid, si etiam iucunda memoria est praeteritorum malorum? Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Duo Reges: constructio interrete. Nam si amitti vita beata potest, beata esse non potest. Sed est forma eius disciplinae, sicut fere ceterarum, triplex: una pars est naturae, disserendi altera, vivendi tertia. Tecum optime, deinde etiam cum mediocri amico. Idemne potest esse dies saepius, qui semel fuit? Illa videamus, quae a te de amicitia dicta sunt. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. 

Ergo ita: non posse honeste vivi, nisi honeste vivatur? Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Graecum enim hunc versum nostis omnes-: Suavis laborum est praeteritorum memoria. Quos nisi redarguimus, omnis virtus, omne decus, omnis vera laus deserenda est. Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum. 

Mihi, inquam, qui te id ipsum rogavi? Collige omnia, quae soletis: Praesidium amicorum. Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Quos nisi redarguimus, omnis virtus, omne decus, omnis vera laus deserenda est. 


	Qui convenit?
	Sed haec omittamus;
	Immo videri fortasse.
	Rhetorice igitur, inquam, nos mavis quam dialectice disputare?
	Age sane, inquam.
	Sed nonne merninisti licere mihi ista probare, quae sunt a te dicta?
	Recte dicis;
	An est aliquid, quod te sua sponte delectet?




	Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere.
	Quae diligentissime contra Aristonem dicuntur a Chryippo.
	Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis;
	Omnia peccata paria dicitis.
	Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor;




	Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur.



Animum autem reliquis rebus ita perfecit, ut corpus; Quae autem natura suae primae institutionis oblita est? Quam ob rem tandem, inquit, non satisfacit? Ergo hoc quidem apparet, nos ad agendum esse natos. 


	Aliud igitur esse censet gaudere, aliud non dolere.
	Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset.
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;
	Quod non faceret, si in voluptate summum bonum poneret.
	Quo minus animus a se ipse dissidens secumque discordans gustare partem ullam liquidae voluptatis et liberae potest.




	Certe nihil nisi quod possit ipsum propter se iure laudari.
	Eorum enim est haec querela, qui sibi cari sunt seseque diligunt.
	Praeclare hoc quidem.
	Quaerimus enim finem bonorum.




At vero illa, quae Peripatetici, quae Stoici dicunt, semper
tibi in ore sunt in iudiciis, in senatu.

Quid, cum volumus nomina eorum, qui quid gesserint, nota
nobis esse, parentes, patriam, multa praeterea minime
necessaria?




	Non igitur bene.



Cave putes quicquam esse verius. De vacuitate doloris eadem sententia erit. Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. Quamquam in hac divisione rem ipsam prorsus probo, elegantiam desidero. Ut in geometria, prima si dederis, danda sunt omnia. Ratio quidem vestra sic cogit. Quantum Aristoxeni ingenium consumptum videmus in musicis? Nihil ad rem! Ne sit sane; Quid, si etiam iucunda memoria est praeteritorum malorum? 


	Confecta res esset.
	Hoc enim constituto in philosophia constituta sunt omnia.
	Ut pulsi recurrant?
	Sed non sunt in eo genere tantae commoditates corporis tamque productae temporibus tamque multae.
	Facete M.
	Si est nihil nisi corpus, summa erunt illa: valitudo, vacuitas doloris, pulchritudo, cetera.
	Perge porro;
	An tu me de L.
	Ille incendat?
	Etenim semper illud extra est, quod arte comprehenditur.



Tum Triarius: Posthac quidem, inquit, audacius. Vitiosum est enim in dividendo partem in genere numerare. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Quamvis enim depravatae non sint, pravae tamen esse possunt. Esse enim quam vellet iniquus iustus poterat inpune. Ut in geometria, prima si dederis, danda sunt omnia. 


Nam me ipsum huc modo venientem convertebat ad sese Coloneus
ille locus, cuius incola Sophocles ob oculos versabatur,
quem scis quam admirer quemque eo delecter.

Quid ergo dubitamus, quin, si non dolere voluptas sit summa,
non esse in voluptate dolor sit maximus?



Sed quot homines, tot sententiae; Nihil enim arbitror esse magna laude dignum, quod te praetermissurum credam aut mortis aut doloris metu. Qui ita affectus, beatum esse numquam probabis; Utrum igitur tibi litteram videor an totas paginas commovere? Ostendit pedes et pectus. Aufert enim sensus actionemque tollit omnem. Hoc ipsum elegantius poni meliusque potuit. Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat. 

Poterat autem inpune; Qui est in parvis malis. Graecum enim hunc versum nostis omnes-: Suavis laborum est praeteritorum memoria. Haec quo modo conveniant, non sane intellego. Nec vero hoc oratione solum, sed multo magis vita et factis et moribus comprobavit. At eum nihili facit; Atque ab isto capite fluere necesse est omnem rationem bonorum et malorum. Summae mihi videtur inscitiae. 


	Nam ista vestra: Si gravis, brevis;
	Terram, mihi crede, ea lanx et maria deprimet.
	Iam doloris medicamenta illa Epicurea tamquam de narthecio proment: Si gravis, brevis;



Si longus, levis dictata sunt. Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat; Qua igitur re ab deo vincitur, si aeternitate non vincitur? Non quam nostram quidem, inquit Pomponius iocans; Immo alio genere; Teneo, inquit, finem illi videri nihil dolere. Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. 

Causa autem fuit huc veniendi ut quosdam hinc libros promerem. De illis, cum volemus. Prave, nequiter, turpiter cenabat; Quaero igitur, quo modo hae tantae commendationes a natura profectae subito a sapientia relictae sint. Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer. Tu vero, inquam, ducas licet, si sequetur; Cur iustitia laudatur? Tu quidem reddes; Esse enim quam vellet iniquus iustus poterat inpune. 


