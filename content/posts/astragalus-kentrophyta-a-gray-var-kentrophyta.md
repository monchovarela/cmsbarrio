Title: Astragalus kentrophyta A. Gray var. kentrophyta
Description: Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.
Image: https://source.unsplash.com/random/?night
Date: 31/8/2021
Keywords: article
Category: Holdlamis
Author: Yoveo
Tags: post
Published: true
Background: #d6c003
Color: #67be63
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Negat enim summo bono afferre incrementum diem. Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Tecum optime, deinde etiam cum mediocri amico. Verum esto: verbum ipsum voluptatis non habet dignitatem, nec nos fortasse intellegimus. Duo Reges: constructio interrete. Et quod est munus, quod opus sapientiae? 

Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum. Solum praeterea formosum, solum liberum, solum civem, stultost; Ad eas enim res ab Epicuro praecepta dantur. Graccho, eius fere, aequal�? Idne consensisse de Calatino plurimas gentis arbitramur, primarium populi fuisse, quod praestantissimus fuisset in conficiendis voluptatibus? Si longus, levis; Quodsi ipsam honestatem undique pertectam atque absolutam. 

Quid de Pythagora? Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Inquit, an parum disserui non verbis Stoicos a Peripateticis, sed universa re et tota sententia dissidere? Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? 

Tu vero, inquam, ducas licet, si sequetur; At enim, qua in vita est aliquid mali, ea beata esse non potest. At, si voluptas esset bonum, desideraret. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Minime vero, inquit ille, consentit. 

Nihilne est in his rebus, quod dignum libero aut indignum esse ducamus? Tum Triarius: Posthac quidem, inquit, audacius. Bona autem corporis huic sunt, quod posterius posui, similiora. Contineo me ab exemplis. Quare, quoniam de primis naturae commodis satis dietum est nunc de maioribus consequentibusque videamus. Itaque hic ipse iam pridem est reiectus; 


Sed id ne cogitari quidem potest quale sit, ut non repugnet
ipsum sibi.

Nam si propter voluptatem, quae est ista laus, quae possit e
macello peti?



Cur, nisi quod turpis oratio est? Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Inde sermone vario sex illa a Dipylo stadia confecimus. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; Sunt enim prima elementa naturae, quibus auctis v�rtutis quasi germen efficitur. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Illa videamus, quae a te de amicitia dicta sunt. Age sane, inquam. Neutrum vero, inquit ille. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; 


	Et tamen puto concedi nobis oportere ut Graeco verbo utamur, si quando minus occurret Latinum, ne hoc ephippiis et acratophoris potius quam proegmenis et apoproegmenis concedatur;




	Scisse enim te quis coarguere possit?
	Nescio quo modo praetervolavit oratio.




	Hoc positum in Phaedro a Platone probavit Epicurus sensitque in omni disputatione id fieri oportere.




Ita est quoddam commune officium sapientis et insipientis,
ex quo efficitur versari in iis, quae media dicamus.

Si est nihil nisi corpus, summa erunt illa: valitudo,
vacuitas doloris, pulchritudo, cetera.




	Itaque fecimus.
	In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt.
	Quid enim?
	Ratio quidem vestra sic cogit.
	Stoicos roga.
	Quid est igitur, cur ita semper deum appellet Epicurus beatum et aeternum?
	Equidem e Cn.
	Quid enim de amicitia statueris utilitatis causa expetenda vides.




	Non igitur bene.
	Omnes enim iucundum motum, quo sensus hilaretur.
	ALIO MODO.
	Haec quo modo conveniant, non sane intellego.
	Ut pulsi recurrant?
	Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium.
	Facete M.
	Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L.



Sed ad haec, nisi molestum est, habeo quae velim. Verum esto: verbum ipsum voluptatis non habet dignitatem, nec nos fortasse intellegimus. 

Utilitatis causa amicitia est quaesita. Quid autem habent admirationis, cum prope accesseris? Easdemne res? Quid enim est a Chrysippo praetermissum in Stoicis? A mene tu? Ille vero, si insipiens-quo certe, quoniam tyrannus -, numquam beatus; Nemo nostrum istius generis asotos iucunde putat vivere. 


	Aliter enim explicari, quod quaeritur, non potest.
	Stoici autem, quod finem bonorum in una virtute ponunt, similes sunt illorum;
	Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere.
	Hoc est dicere: Non reprehenderem asotos, si non essent asoti.
	Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio.




	Quare obscurentur etiam haec, quae secundum naturam esse dicimus, in vita beata;
	Hoc loco tenere se Triarius non potuit.
	Est autem etiam actio quaedam corporis, quae motus et status naturae congruentis tenet;
	Quicquid porro animo cernimus, id omne oritur a sensibus;




	Legimus tamen Diogenem, Antipatrum, Mnesarchum, Panaetium, multos alios in primisque familiarem nostrum Posidonium.
	Negat esse eam, inquit, propter se expetendam.
	Ex quo intellegitur officium medium quiddam esse, quod neque in bonis ponatur neque in contrariis.
	Cum salvum esse flentes sui respondissent, rogavit essentne fusi hostes.



Non quam nostram quidem, inquit Pomponius iocans; Si autem id non concedatur, non continuo vita beata tollitur. Ut proverbia non nulla veriora sint quam vestra dogmata. Quid sequatur, quid repugnet, vident. Dici enim nihil potest verius. Itaque his sapiens semper vacabit. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. 

Quamquam tu hanc copiosiorem etiam soles dicere. Si quicquam extra virtutem habeatur in bonis. Sed erat aequius Triarium aliquid de dissensione nostra iudicare. Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas? Sed nimis multa. Quid de Pythagora? Quis negat? 


