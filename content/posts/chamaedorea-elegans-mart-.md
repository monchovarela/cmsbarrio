Title: Chamaedorea elegans Mart.
Description: Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.
Image: https://source.unsplash.com/random/?css
Date: 12/3/2021
Keywords: website
Category: Cardguard
Author: Feedbug
Tags: portfolio
Published: true
Background: #98ac65
Color: #e31ca7
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus. Etenim si delectamur, cum scribimus, quis est tam invidus, qui ab eo nos abducat? Summus dolor plures dies manere non potest? Non igitur bene. 

Dici enim nihil potest verius. Teneo, inquit, finem illi videri nihil dolere. Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Tum Torquatus: Prorsus, inquit, assentior; 

Nam Pyrrho, Aristo, Erillus iam diu abiecti. Sed quid attinet de rebus tam apertis plura requirere? Duo Reges: constructio interrete. Non potes, nisi retexueris illa. Satis est ad hoc responsum. Restatis igitur vos; Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Quae cum dixisset paulumque institisset, Quid est? 

Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Quod ea non occurrentia fingunt, vincunt Aristonem; At ego quem huic anteponam non audeo dicere; 

Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Etiam beatissimum? Qui est in parvis malis. Non est igitur summum malum dolor. Iam in altera philosophiae parte. Non quam nostram quidem, inquit Pomponius iocans; 


	Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest.
	Est autem officium, quod ita factum est, ut eius facti probabilis ratio reddi possit.
	Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus.
	Aliud igitur esse censet gaudere, aliud non dolere.
	Non autem hoc: igitur ne illud quidem.
	Aberat omnis dolor, qui si adesset, nec molliter ferret et tamen medicis plus quam philosophis uteretur.



Hosne igitur laudas et hanc eorum, inquam, sententiam sequi nos censes oportere? Totum autem id externum est, et quod externum, id in casu est. Duarum enim vitarum nobis erunt instituta capienda. Negat enim summo bono afferre incrementum diem. Vitae autem degendae ratio maxime quidem illis placuit quieta. Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Nam Pyrrho, Aristo, Erillus iam diu abiecti. Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur? 

Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Et nemo nimium beatus est; Ut id aliis narrare gestiant? Quis est tam dissimile homini. Immo videri fortasse. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Igitur neque stultorum quisquam beatus neque sapientium non beatus. Philosophi autem in suis lectulis plerumque moriuntur. 


	Nam qui valitudinem aestimatione aliqua dignam iudicamus neque eam tamen in bonis ponimus, idem censemus nullam esse tantam aestimationem, ut ea virtuti anteponatur.




	Si longus, levis.
	Aut pertinacissimus fueris, si in eo perstiteris ad corpus ea, quae dixi, referri, aut deserueris totam Epicuri voluptatem, si negaveris.
	An eiusdem modi?
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.
	Si quae forte-possumus.
	Tantum dico, magis fuisse vestrum agere Epicuri diem natalem, quam illius testamento cavere ut ageretur.
	Quare conare, quaeso.
	Cur fortior sit, si illud, quod tute concedis, asperum et vix ferendum putabit?
	Optime, inquam.
	Prave, nequiter, turpiter cenabat;




	At iam decimum annum in spelunca iacet.
	Quis suae urbis conservatorem Codrum, quis Erechthei filias non maxime laudat?
	Totum genus hoc Zeno et qui ab eo sunt aut non potuerunt aut noluerunt, certe reliquerunt.
	Ut alios omittam, hunc appello, quem ille unum secutus est.




Maximeque eos videre possumus res gestas audire et legere
velle, qui a spe gerendi absunt confecti senectute.

Nihil enim desiderabile concupiscunt, plusque in ipsa
iniuria detrimenti est quam in iis rebus emolumenti, quae
pariuntur iniuria.




	Beatum, inquit.
	Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum.
	Sumenda potius quam expetenda.
	Et non ex maxima parte de tota iudicabis?
	Avaritiamne minuis?
	Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit?
	Quid adiuvas?
	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;
	Ut pulsi recurrant?
	De maximma autem re eodem modo, divina mente atque natura mundum universum et eius maxima partis administrari.
	At certe gravius.
	Illud mihi a te nimium festinanter dictum videtur, sapientis omnis esse semper beatos;




Quocirca intellegi necesse est in ipsis rebus, quae
discuntur et cognoscuntur, invitamenta inesse, quibus ad
discendum cognoscendumque moveamur.

Ratio ista, quam defendis, praecepta, quae didicisti, quae
probas, funditus evertunt amicitiam, quamvis eam Epicurus,
ut facit, in caelum efferat laudibus.



Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant? Quod autem ratione actum est, id officium appellamus. 

Quae quo sunt excelsiores, eo dant clariora indicia naturae. Qui autem esse poteris, nisi te amor ipse ceperit? Itaque contra est, ac dicitis; Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset. 

Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Ut id aliis narrare gestiant? Quae duo sunt, unum facit. Quamvis enim depravatae non sint, pravae tamen esse possunt. An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? Quis est tam dissimile homini. Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse. Sit enim idem caecus, debilis. 


	Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia.
	Illud mihi a te nimium festinanter dictum videtur, sapientis omnis esse semper beatos;
	Quodsi ipsam honestatem undique pertectam atque absolutam.
	Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur.
	Sed tamen enitar et, si minus multa mihi occurrent, non fugiam ista popularia.




	Sed non alienum est, quo facilius vis verbi intellegatur, rationem huius verbi faciendi Zenonis exponere.
	Non autem hoc: igitur ne illud quidem.




	Etenim, cum omnes natura totos se expetendos putent, nec id ob aliam rem, sed propter ipsos, necesse est eius etiam partis propter se expeti, quod universum propter se expetatur.




