Title: Mentzelia laevicaulis (Hook.) Torr. & A. Gray var. parviflora (Douglas ex Hook.) C.L. Hitchc.
Description: In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.
Image: https://source.unsplash.com/random/?ruby
Date: 26/11/2020
Keywords: cms
Category: Fintone
Author: Janyx
Tags: web
Published: true
Background: #fb4df2
Color: #237bfe
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Idem iste, inquam, de voluptate quid sentit? Duo Reges: constructio interrete. Sic, et quidem diligentius saepiusque ista loquemur inter nos agemusque communiter. Non enim iam stirpis bonum quaeret, sed animalis. 


	Minime vero probatur huic disciplinae, de qua loquor, aut iustitiam aut amicitiam propter utilitates adscisci aut probari.




	Sed ego in hoc resisto;
	Equidem, sed audistine modo de Carneade?
	An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia?
	Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam;




Atque haec contra Aristippum, qui eam voluptatem non modo
summam, sed solam etiam ducit, quam omnes unam appellamus
voluptatem.

Qui autem diffidet perpetuitati bonorum suorum, timeat
necesse est, ne aliquando amissis illis sit miser.



Hic Speusippus, hic Xenocrates, hic eius auditor Polemo, cuius illa ipsa sessio fuit, quam videmus. Quantum Aristoxeni ingenium consumptum videmus in musicis? Istam voluptatem perpetuam quis potest praestare sapienti? Nos vero, inquit ille; Quid enim? Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Ne tum quidem te respicies et cogitabis sibi quemque natum esse et suis voluptatibus? 


	Stoici scilicet.
	Quae cum essent dicta, finem fecimus et ambulandi et disputandi.
	Sed nimis multa.
	Et nunc quidem quod eam tuetur, ut de vite potissimum loquar, est id extrinsecus;




Idque quo magis quidam ita faciunt, ut iure etiam
reprehendantur, hoc magis intellegendum est haec ipsa nimia
in quibusdam futura non fuisse, nisi quaedam essent modica
natura.

Restatis igitur vos;



Unum est sine dolore esse, alterum cum voluptate. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Philosophi autem in suis lectulis plerumque moriuntur. Quia dolori non voluptas contraria est, sed doloris privatio. Huius ego nunc auctoritatem sequens idem faciam. 

Praeteritis, inquit, gaudeo. Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Sed haec quidem liberius ab eo dicuntur et saepius. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Sed quod proximum fuit non vidit. Animum autem reliquis rebus ita perfecit, ut corpus; Quis istud, quaeso, nesciebat? 

Res enim se praeclare habebat, et quidem in utraque parte. Et certamen honestum et disputatio splendida! omnis est enim de virtutis dignitate contentio. Quamquam ab iis philosophiam et omnes ingenuas disciplinas habemus; Curium putes loqui, interdum ita laudat, ut quid praeterea sit bonum neget se posse ne suspicari quidem. Quae ista amicitia est? Expectoque quid ad id, quod quaerebam, respondeas. Itaque et manendi in vita et migrandi ratio omnis iis rebus, quas supra dixi, metienda. Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. Nam adhuc, meo fortasse vitio, quid ego quaeram non perspicis. Quid iudicant sensus? 


	Paria sunt igitur.
	Quasi ego id curem, quid ille aiat aut neget.
	Efficiens dici potest.
	Pollicetur certe.




	Habent enim et bene longam et satis litigiosam disputationem.
	In ipsa enim parum magna vis inest, ut quam optime se habere possit, si nulla cultura adhibeatur.
	Graecis hoc modicum est: Leonidas, Epaminondas, tres aliqui aut quattuor;
	Septem autem illi non suo, sed populorum suffragio omnium nominati sunt.
	Quid censes in Latino fore?



Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Sed ille, ut dixi, vitiose. Nec mihi illud dixeris: Haec enim ipsa mihi sunt voluptati, et erant illa Torquatis. Apparet statim, quae sint officia, quae actiones. An me, inquam, nisi te audire vellem, censes haec dicturum fuisse? Quod equidem non reprehendo; 

Tu enim ista lenius, hic Stoicorum more nos vexat. Sine ea igitur iucunde negat posse se vivere? Ut alios omittam, hunc appello, quem ille unum secutus est. Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. Quicquid enim a sapientia proficiscitur, id continuo debet expletum esse omnibus suis partibus; Potius ergo illa dicantur: turpe esse, viri non esse debilitari dolore, frangi, succumbere. 


	Si autem id non concedatur, non continuo vita beata tollitur.
	Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore.
	Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant.
	Ne amores quidem sanctos a sapiente alienos esse arbitrantur.



Nam si propter voluptatem, quae est ista laus, quae possit e macello peti? Tanta vis admonitionis inest in locis; Nondum autem explanatum satis, erat, quid maxime natura vellet. Quis hoc dicit? His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent. Tecum optime, deinde etiam cum mediocri amico. Ex quo illud efficitur, qui bene cenent omnis libenter cenare, qui libenter, non continuo bene. Immo videri fortasse. 

Reguli reiciendam; At enim sequor utilitatem. Id Sextilius factum negabat. Maximus dolor, inquit, brevis est. Numquam facies. Quodsi ipsam honestatem undique pertectam atque absolutam. Quid iudicant sensus? Quid turpius quam sapientis vitam ex insipientium sermone pendere? 


	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?
	Apparet statim, quae sint officia, quae actiones.
	Haec bene dicuntur, nec ego repugno, sed inter sese ipsa pugnant.
	His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent.
	Haec quo modo conveniant, non sane intellego.
	Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam.




	At iam decimum annum in spelunca iacet.



Sit hoc ultimum bonorum, quod nunc a me defenditur; Sin te auctoritas commovebat, nobisne omnibus et Platoni ipsi nescio quem illum anteponebas? Eadem nunc mea adversum te oratio est. Haec para/doca illi, nos admirabilia dicamus. Non laboro, inquit, de nomine. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? 


