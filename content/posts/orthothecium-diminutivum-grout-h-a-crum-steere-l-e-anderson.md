Title: Orthothecium diminutivum (Grout) H.A. Crum, Steere & L.E. Anderson
Description: In congue. Etiam justo. Etiam pretium iaculis justo.
Image: https://source.unsplash.com/random/?css
Date: 12/1/2021
Keywords: blog
Category: Voyatouch
Author: Gigazoom
Tags: design
Published: true
Background: #552536
Color: #f59c7a
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis non odit sordidos, vanos, leves, futtiles? Maximeque eos videre possumus res gestas audire et legere velle, qui a spe gerendi absunt confecti senectute. Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Quis istud possit, inquit, negare? 

Ratio enim nostra consentit, pugnat oratio. An hoc usque quaque, aliter in vita? Age, inquies, ista parva sunt. Apud ceteros autem philosophos, qui quaesivit aliquid, tacet; Audio equidem philosophi vocem, Epicure, sed quid tibi dicendum sit oblitus es. Videamus igitur sententias eorum, tum ad verba redeamus. Ergo ita: non posse honeste vivi, nisi honeste vivatur? Sed plane dicit quod intellegit. Expressa vero in iis aetatibus, quae iam confirmatae sunt. 

Universa enim illorum ratione cum tota vestra confligendum puto. Cyrenaici quidem non recusant; Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Confecta res esset. Morbo gravissimo affectus, exul, orbus, egens, torqueatur eculeo: quem hunc appellas, Zeno? Istic sum, inquit. Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Obsecro, inquit, Torquate, haec dicit Epicurus? Itaque hic ipse iam pridem est reiectus; 


Quae cum dixissem, magis ut illum provocarem quam ut ipse
loquerer, tum Triarius leniter arridens: Tu quidem, inquit,
totum Epicurum paene e philosophorum choro sustulisti.

Quem Tiberina descensio festo illo die tanto gaudio affecit,
quanto L.




	Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem.
	Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q.
	Fieri, inquam, Triari, nullo pacto potest, ut non dicas, quid non probes eius, a quo dissentias.
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?
	Collige omnia, quae soletis: Praesidium amicorum.




Quanti quidque sit aliter docti et indocti, sed cum
constiterit inter doctos quanti res quaeque sit-si homines
essent, usitate loquerentur -, dum res maneant, verba
fingant arbitratu suo.

Illis videtur, qui illud non dubitant bonum dicere -;



Quamquam id quidem licebit iis existimare, qui legerint. Nihil enim iam habes, quod ad corpus referas; Huius ego nunc auctoritatem sequens idem faciam. An est aliquid, quod te sua sponte delectet? Respondent extrema primis, media utrisque, omnia omnibus. Quis enim redargueret? Et ille ridens: Video, inquit, quid agas; Primum in nostrane potestate est, quid meminerimus? Erit enim mecum, si tecum erit. Ita multa dicunt, quae vix intellegam. 

Ergo, inquit, tibi Q. Iam id ipsum absurdum, maximum malum neglegi. Nunc vides, quid faciat. At iam decimum annum in spelunca iacet. Videamus animi partes, quarum est conspectus illustrior; At ille pellit, qui permulcet sensum voluptate. Urgent tamen et nihil remittunt. Sed hoc sane concedamus. 


	Quid igitur, inquit, eos responsuros putas?
	Cave putes quicquam esse verius.
	Expressa vero in iis aetatibus, quae iam confirmatae sunt.
	Fatebuntur Stoici haec omnia dicta esse praeclare, neque eam causam Zenoni desciscendi fuisse.
	Sed quid attinet de rebus tam apertis plura requirere?



Quid turpius quam sapientis vitam ex insipientium sermone pendere? Et non ex maxima parte de tota iudicabis? Quod ea non occurrentia fingunt, vincunt Aristonem; Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus. Cum audissem Antiochum, Brute, ut solebam, cum M. Non enim quaero quid verum, sed quid cuique dicendum sit. Explanetur igitur. Est enim effectrix multarum et magnarum voluptatum. Atque haec ita iustitiae propria sunt, ut sint virtutum reliquarum communia. Illud dico, ea, quae dicat, praeclare inter se cohaerere. 


	Magna laus.
	At cum de plurimis eadem dicit, tum certe de maximis.
	Peccata paria.
	Si quae forte-possumus.
	Quid Zeno?
	Est tamen ea secundum naturam multoque nos ad se expetendam magis hortatur quam superiora omnia.
	Quis hoc dicit?
	Quia, si mala sunt, is, qui erit in iis, beatus non erit.



Ita ne hoc quidem modo paria peccata sunt. Cur haec eadem Democritus? Universa enim illorum ratione cum tota vestra confligendum puto. Qua igitur re ab deo vincitur, si aeternitate non vincitur? Quid ergo hoc loco intellegit honestum? Universa enim illorum ratione cum tota vestra confligendum puto. 


	Omnis sermo elegans sumi potest, tum varietas est tanta artium, ut nemo sine eo instrumento ad ullam rem illustriorem satis ornatus possit accedere.



Cum praesertim illa perdiscere ludus esset. Quid ergo hoc loco intellegit honestum? Quae cum dixisset paulumque institisset, Quid est? Primum divisit ineleganter; Duo Reges: constructio interrete. 


	An quae de prudentia, de cognitione rerum, de coniunctione generis humani, quaeque ab eisdem de temperantia, de modestia, de magnitudine animi, de omni honestate dicuntur?



Pollicetur certe. Illa videamus, quae a te de amicitia dicta sunt. Non autem hoc: igitur ne illud quidem. Hoc loco tenere se Triarius non potuit. Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. Scaevola tribunus plebis ferret ad plebem vellentne de ea re quaeri. 


	Quonam, inquit, modo?
	Quasi ego id curem, quid ille aiat aut neget.
	Moriatur, inquit.
	Sed plane dicit quod intellegit.
	Easdemne res?
	Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis.
	Quibusnam praeteritis?
	Nullus est igitur cuiusquam dies natalis.



Sed tu istuc dixti bene Latine, parum plane. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Itaque ab his ordiamur. Tamen a proposito, inquam, aberramus. 


	Quia dolori non voluptas contraria est, sed doloris privatio.
	Quod si ita sit, cur opera philosophiae sit danda nescio.
	Istam voluptatem perpetuam quis potest praestare sapienti?
	Zenonis est, inquam, hoc Stoici.
	Facit igitur Lucius noster prudenter, qui audire de summo bono potissimum velit;




	At modo dixeras nihil in istis rebus esse, quod interesset.
	Paulum, cum regem Persem captum adduceret, eodem flumine invectio?
	Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo;
	Verum hoc loco sumo verbis his eandem certe vim voluptatis Epicurum nosse quam ceteros.
	Tum Piso: Quoniam igitur aliquid omnes, quid Lucius noster?
	Hoc Hieronymus summum bonum esse dixit.




