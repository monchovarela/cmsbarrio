Title: Stellaria oxyphylla B.L. Rob.
Description: Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.
Image: https://source.unsplash.com/random/?code
Date: 9/2/2021
Keywords: article
Category: Fintone
Author: Leenti
Tags: post
Published: true
Background: #dacb75
Color: #889917
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Conferam tecum, quam cuique verso rem subicias; Quo tandem modo? Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Aliter enim explicari, quod quaeritur, non potest. Materiam vero rerum et copiam apud hos exilem, apud illos uberrimam reperiemus. Duo Reges: constructio interrete. Tum mihi Piso: Quid ergo? Quamquam id quidem, infinitum est in hac urbe; 

Non laboro, inquit, de nomine. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Quid turpius quam sapientis vitam ex insipientium sermone pendere? Quae cum dixisset paulumque institisset, Quid est? Mene ergo et Triarium dignos existimas, apud quos turpiter loquare? Pisone in eo gymnasio, quod Ptolomaeum vocatur, unaque nobiscum Q. Proclivi currit oratio. Eadem nunc mea adversum te oratio est. 


Huic verbo omnes, qui ubique sunt, qui Latine sciunt, duas
res subiciunt, laetitiam in animo, commotionem suavem
iucunditatis in corpore.

Nihil enim desiderabile concupiscunt, plusque in ipsa
iniuria detrimenti est quam in iis rebus emolumenti, quae
pariuntur iniuria.



Sed virtutem ipsam inchoavit, nihil amplius. Omnium enim rerum principia parva sunt, sed suis progressionibus usa augentur nec sine causa; Quo plebiscito decreta a senatu est consuli quaestio Cn. Amicitiam autem adhibendam esse censent, quia sit ex eo genere, quae prosunt. Praeteritis, inquit, gaudeo. Sic consequentibus vestris sublatis prima tolluntur. Comprehensum, quod cognitum non habet? 

Et harum quidem rerum facilis est et expedita distinctio. Quid sequatur, quid repugnet, vident. Duarum enim vitarum nobis erunt instituta capienda. At enim sequor utilitatem. 


Ego autem tibi, Piso, assentior usu hoc venire, ut acrius
aliquanto et attentius de claris viris locorum admonitu
cogitemus.

Tenesne igitur, inquam, Hieronymus Rhodius quid dicat esse
summum bonum, quo putet omnia referri oportere?



Si enim ad populum me vocas, eum. Nec hoc ille non vidit, sed verborum magnificentia est et gloria delectatus. Est enim effectrix multarum et magnarum voluptatum. Nescio quo modo praetervolavit oratio. At, si voluptas esset bonum, desideraret. 

Obsecro, inquit, Torquate, haec dicit Epicurus? Nihil enim arbitror esse magna laude dignum, quod te praetermissurum credam aut mortis aut doloris metu. Illud urgueam, non intellegere eum quid sibi dicendum sit, cum dolorem summum malum esse dixerit. Quodsi ipsam honestatem undique pertectam atque absolutam. Satisne vobis videor pro meo iure in vestris auribus commentatus? Cetera illa adhibebat, quibus demptis negat se Epicurus intellegere quid sit bonum. Simus igitur contenti his. Heri, inquam, ludis commissis ex urbe profectus veni ad vesperum. Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim? Nulla profecto est, quin suam vim retineat a primo ad extremum. Restinguet citius, si ardentem acceperit. 

Quod non faceret, si in voluptate summum bonum poneret. Sin aliud quid voles, postea. Pudebit te, inquam, illius tabulae, quam Cleanthes sane commode verbis depingere solebat. Tibi hoc incredibile, quod beatissimum. Odium autem et invidiam facile vitabis. 


	Quod dicit Epicurus etiam de voluptate, quae minime sint voluptates, eas obscurari saepe et obrui.




	Quod quidem iam fit etiam in Academia.
	Et quidem illud ipsum non nimium probo et tantum patior, philosophum loqui de cupiditatibus finiendis.
	In primo enim ortu inest teneritas ac mollitia quaedam, ut nec res videre optimas nec agere possint.




	Hoc loco tenere se Triarius non potuit.
	Quicquid porro animo cernimus, id omne oritur a sensibus;
	Summum en�m bonum exposuit vacuitatem doloris;



Nec tamen ille erat sapiens quis enim hoc aut quando aut ubi aut unde? Terram, mihi crede, ea lanx et maria deprimet. Hoc simile tandem est? Quae contraria sunt his, malane? Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Propter nos enim illam, non propter eam nosmet ipsos diligimus. Quid censes in Latino fore? At iste non dolendi status non vocatur voluptas. Quid ergo hoc loco intellegit honestum? Qui enim existimabit posse se miserum esse beatus non erit. 

Duo enim genera quae erant, fecit tria. An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Superiores tres erant, quae esse possent, quarum est una sola defensa, eaque vehementer. Facile pateremur, qui etiam nunc agendi aliquid discendique causa prope contra naturam v�gillas suscipere soleamus. Si longus, levis dictata sunt. Ex ea difficultate illae fallaciloquae, ut ait Accius, malitiae natae sunt. Cum autem negant ea quicquam ad beatam vitam pertinere, rursus naturam relinquunt. Ergo hoc quidem apparet, nos ad agendum esse natos. 


	Atque hoc dabitis, ut opinor, si modo sit aliquid esse beatum, id oportere totum poni in potestate sapientis.



Nam Pyrrho, Aristo, Erillus iam diu abiecti. Que Manilium, ab iisque M. Nam prius a se poterit quisque discedere quam appetitum earum rerum, quae sibi conducant, amittere. Tu quidem reddes; Itaque vides, quo modo loquantur, nova verba fingunt, deserunt usitata. His similes sunt omnes, qui virtuti student levantur vitiis, levantur erroribus, nisi forte censes Ti. 


	Iam id ipsum absurdum, maximum malum neglegi.
	Sed quia studebat laudi et dignitati, multum in virtute processerat.
	Isto modo ne improbos quidem, si essent boni viri.
	Quae tamen a te agetur non melior, quam illae sunt, quas interdum optines.
	Si verbum sequimur, primum longius verbum praepositum quam bonum.




	Praeclare enim Plato: Beatum, cui etiam in senectute contigerit, ut sapientiam verasque opiniones assequi possit.
	Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim.




	Quis enim redargueret?
	Iam doloris medicamenta illa Epicurea tamquam de narthecio proment: Si gravis, brevis;
	Respondeat totidem verbis.
	Certe, nisi voluptatem tanti aestimaretis.
	In schola desinis.
	Qui enim existimabit posse se miserum esse beatus non erit.
	Pollicetur certe.
	Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis.
	Praeclare hoc quidem.
	An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat?
	A mene tu?
	An est aliquid, quod te sua sponte delectet?




	Tenent mordicus.
	Non igitur bene.
	Scrupulum, inquam, abeunti;
	Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur.
	Falli igitur possumus.
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;
	Etiam beatissimum?
	Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.
	Beatum, inquit.
	Ergo ita: non posse honeste vivi, nisi honeste vivatur?




