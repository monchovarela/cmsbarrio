Title: Rafinesquia Nutt.
Description: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.
Image: https://source.unsplash.com/random/?aqua
Date: 13/8/2021
Keywords: post
Category: Opela
Author: Photofeed
Tags: design
Published: true
Background: #041338
Color: #f1d2cf
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. At cum de plurimis eadem dicit, tum certe de maximis. Praeteritis, inquit, gaudeo. Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest. Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum. Beatus autem esse in maximarum rerum timore nemo potest. Id enim natura desiderat. Duo Reges: constructio interrete. Innumerabilia dici possunt in hanc sententiam, sed non necesse est. Claudii libidini, qui tum erat summo ne imperio, dederetur. Eiuro, inquit adridens, iniquum, hac quidem de re; 


	Mihi quidem Homerus huius modi quiddam vidisse videatur in iis, quae de Sirenum cantibus finxerit.




	Itaque haec cum illis est dissensio, cum Peripateticis nulla sane.
	Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur.
	Experiamur igitur, inquit, etsi habet haec Stoicorum ratio difficilius quiddam et obscurius.
	Ergo instituto veterum, quo etiam Stoici utuntur, hinc capiamus exordium.




Haec ego non possum dicere non esse hominis quamvis et belli
et humani, sapientis vero nullo modo, physici praesertim,
quem se ille esse vult, putare ullum esse cuiusquam diem
natalem.

Sed existimo te, sicut nostrum Triarium, minus ab eo
delectari, quod ista Platonis, Aristoteli, Theophrasti
orationis ornamenta neglexerit.



Fortitudinis quaedam praecepta sunt ac paene leges, quae effeminari virum vetant in dolore. Sed vos squalidius, illorum vides quam niteat oratio. Illis videtur, qui illud non dubitant bonum dicere -; Hanc quoque iucunditatem, si vis, transfer in animum; Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Idem iste, inquam, de voluptate quid sentit? 

Cuius similitudine perspecta in formarum specie ac dignitate transitum est ad honestatem dictorum atque factorum. Atqui iste locus est, Piso, tibi etiam atque etiam confirmandus, inquam; Nunc haec primum fortasse audientis servire debemus. Si quicquam extra virtutem habeatur in bonis. Praetereo multos, in bis doctum hominem et suavem, Hieronymum, quem iam cur Peripateticum appellem nescio. Quo modo autem optimum, si bonum praeterea nullum est? 

Haec para/doca illi, nos admirabilia dicamus. Aliter enim explicari, quod quaeritur, non potest. Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Egone quaeris, inquit, quid sentiam? 


	Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia?



At quanta conantur! Mundum hunc omnem oppidum esse nostrum! Incendi igitur eos, qui audiunt, vides. Quae quo sunt excelsiores, eo dant clariora indicia naturae. Id quaeris, inquam, in quo, utrum respondero, verses te huc atque illuc necesse est. 

An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia? Ut optime, secundum naturam affectum esse possit. At, si voluptas esset bonum, desideraret. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; Cur post Tarentum ad Archytam? An potest cupiditas finiri? Quia dolori non voluptas contraria est, sed doloris privatio. Cur id non ita fit? Atque hoc loco similitudines eas, quibus illi uti solent, dissimillimas proferebas. Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Scio enim esse quosdam, qui quavis lingua philosophari possint; Huius ego nunc auctoritatem sequens idem faciam. 


	Claudii libidini, qui tum erat summo ne imperio, dederetur.
	Tamen aberramus a proposito, et, ne longius, prorsus, inquam, Piso, si ista mala sunt, placet.
	Sed erat aequius Triarium aliquid de dissensione nostra iudicare.
	Dic in quovis conventu te omnia facere, ne doleas.



Duo enim genera quae erant, fecit tria. Quid de Platone aut de Democrito loquar? 


	Sed quid sentiat, non videtis.
	Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est?
	His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent.
	Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers?
	Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius.
	Quo modo autem optimum, si bonum praeterea nullum est?



Atque haec coniunctio confusioque virtutum tamen a philosophis ratione quadam distinguitur. Ut optime, secundum naturam affectum esse possit. Illi enim inter se dissentiunt. Eam tum adesse, cum dolor omnis absit; Rationis enim perfectio est virtus; Nam, ut sint illa vendibiliora, haec uberiora certe sunt. Videsne quam sit magna dissensio? Vide, quantum, inquam, fallare, Torquate. Quid igitur dubitamus in tota eius natura quaerere quid sit effectum? 


Sed in ceteris artibus cum dicitur artificiose, posterum
quodam modo et consequens putandum est, quod illi
�pigennhmatik�n appellant;

Nec enim, dum metuit, iustus est, et certe, si metuere
destiterit, non erit;



Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium? Si longus, levis; Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. 


	Magna laus.
	Terram, mihi crede, ea lanx et maria deprimet.
	Immo alio genere;
	Sed quae tandem ista ratio est?



Hoc non est positum in nostra actione. Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia. Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Dempta enim aeternitate nihilo beatior Iuppiter quam Epicurus; Miserum hominem! Si dolor summum malum est, dici aliter non potest. Utrum igitur tibi litteram videor an totas paginas commovere? Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; 


	Quae cum magnifice primo dici viderentur, considerata minus probabantur.
	An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat?
	Iam id ipsum absurdum, maximum malum neglegi.
	Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim.




	Reguli reiciendam;
	Quae hic rei publicae vulnera inponebat, eadem ille sanabat.
	Paria sunt igitur.
	Audio equidem philosophi vocem, Epicure, sed quid tibi dicendum sit oblitus es.
	Haeret in salebra.
	Quid interest, nisi quod ego res notas notis verbis appello, illi nomina nova quaerunt, quibus idem dicant?
	Primum divisit ineleganter;
	Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem.
	Efficiens dici potest.
	Videmusne ut pueri ne verberibus quidem a contemplandis rebus perquirendisque deterreantur?
	Tu quidem reddes;
	Omnia contraria, quos etiam insanos esse vultis.




