Title: Lechea pulchella Raf.
Description: Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.
Image: https://source.unsplash.com/random/?ruby
Date: 20/8/2021
Keywords: article
Category: Matsoft
Author: Rhybox
Tags: portfolio
Published: true
Background: #e28eb7
Color: #3d9aef
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Tollenda est atque extrahenda radicitus. Quid, quod homines infima fortuna, nulla spe rerum gerendarum, opifices denique delectantur historia? Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Sed haec omittamus; Duo Reges: constructio interrete. Idemque diviserunt naturam hominis in animum et corpus. Quid de Platone aut de Democrito loquar? 


Elicerem ex te cogeremque, ut responderes, nisi vererer ne
Herculem ipsum ea, quae pro salute gentium summo labore
gessisset, voluptatis causa gessisse diceres.

Aristoteles, Xenocrates, tota illa familia non dabit, quippe
qui valitudinem, vires, divitias, gloriam, multa alia bona
esse dicant, laudabilia non dicant.



Sed quot homines, tot sententiae; Quid de Pythagora? Sed quot homines, tot sententiae; Ergo in utroque exercebantur, eaque disciplina effecit tantam illorum utroque in genere dicendi copiam. Tu quidem reddes; 


	Nam e quibus locis quasi thesauris argumenta depromerentur, vestri ne suspicati quidem sunt, superiores autem artificio et via tradiderunt.




Sin eam, quam Hieronymus, ne fecisset idem, ut voluptatem
illam Aristippi in prima commendatione poneret.

Itaque eos id agere, ut a se dolores, morbos, debilitates
repellant.



Proclivi currit oratio. At enim sequor utilitatem. Dat enim intervalla et relaxat. Idem iste, inquam, de voluptate quid sentit? Dempta enim aeternitate nihilo beatior Iuppiter quam Epicurus; Itaque mihi non satis videmini considerare quod iter sit naturae quaeque progressio. Numquam audivi in Epicuri schola Lycurgum, Solonem, Miltiadem, Themistoclem, Epaminondam nominari, qui in ore sunt ceterorum omnium philosophorum. Qualem igitur hominem natura inchoavit? Quid autem habent admirationis, cum prope accesseris? Equidem e Cn. 


	Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus.



Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit? Haec mihi videtur delicatior, ut ita dicam, molliorque ratio, quam virtutis vis gravitasque postulat. Igitur ne dolorem quidem. Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Tu quidem reddes; Istic sum, inquit. Ergo, inquit, tibi Q. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; 

Quis Aristidem non mortuum diligit? Quem Tiberina descensio festo illo die tanto gaudio affecit, quanto L. Si stante, hoc natura videlicet vult, salvam esse se, quod concedimus; Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris? Nihil enim iam habes, quod ad corpus referas; Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Suam denique cuique naturam esse ad vivendum ducem. Non enim, si omnia non sequebatur, idcirco non erat ortus illinc. Ergo id est convenienter naturae vivere, a natura discedere. Perge porro; 

Cur, nisi quod turpis oratio est? Ut aliquid scire se gaudeant? Nam de isto magna dissensio est. Est enim effectrix multarum et magnarum voluptatum. 

Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit? Nam si propter voluptatem, quae est ista laus, quae possit e macello peti? Neque enim disputari sine reprehensione nec cum iracundia aut pertinacia recte disputari potest. Sin autem ad animum, falsum est, quod negas animi ullum esse gaudium, quod non referatur ad corpus. 


	Tu vero, inquam, ducas licet, si sequetur;
	His singulis copiose responderi solet, sed quae perspicua sunt longa esse non debent.
	In qua si nihil est praeter rationem, sit in una virtute finis bonorum;
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;




	Sed quanta sit alias, nunc tantum possitne esse tanta.
	Quibus natura iure responderit non esse verum aliunde finem beate vivendi, a se principia rei gerendae peti;
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit;




	Bona autem corporis huic sunt, quod posterius posui, similiora.
	An dolor longissimus quisque miserrimus, voluptatem non optabiliorem diuturnitas facit?
	Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers?
	Quam multa vitiosa! summum enim bonum et malum vagiens puer utra voluptate diiudicabit, stante an movente?
	Sapiens autem semper beatus est et est aliquando in dolore;
	Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest.



Erat enim res aperta. Universa enim illorum ratione cum tota vestra confligendum puto. Quae in controversiam veniunt, de iis, si placet, disseramus. Uterque enim summo bono fruitur, id est voluptate. Quid ergo aliud intellegetur nisi uti ne quae pars naturae neglegatur? Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Verum hoc idem saepe faciamus. Si longus, levis. 


	Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere.
	Nihilo beatiorem esse Metellum quam Regulum.
	Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis.
	Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat.
	Hic quoque suus est de summoque bono dissentiens dici vere Peripateticus non potest.




	Sed nimis multa.
	Fortes viri voluptatumne calculis subductis proelium ineunt, sanguinem pro patria profundunt, an quodam animi ardore atque impetu concitati?
	Qui convenit?
	Alterum significari idem, ut si diceretur, officia media omnia aut pleraque servantem vivere.
	Itaque fecimus.
	Non est ista, inquam, Piso, magna dissensio.
	Scaevolam M.
	Nihil minus, contraque illa hereditate dives ob eamque rem laetus.



Non est igitur summum malum dolor. Ad corpus diceres pertinere-, sed ea, quae dixi, ad corpusne refers? Etsi qui potest intellegi aut cogitari esse aliquod animal, quod se oderit? Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Claudii libidini, qui tum erat summo ne imperio, dederetur. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Iam in altera philosophiae parte. Sed ad bona praeterita redeamus. 

Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. At, si voluptas esset bonum, desideraret. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Nunc haec primum fortasse audientis servire debemus. Quid est, quod ab ea absolvi et perfici debeat? Nec vero pietas adversus deos nec quanta iis gratia debeatur sine explicatione naturae intellegi potest. 


	Moriatur, inquit.
	Cave putes quicquam esse verius.
	Quo modo?
	Tum mihi Piso: Quid ergo?
	Nulla erit controversia.
	Et quidem Arcesilas tuus, etsi fuit in disserendo pertinacior, tamen noster fuit;




