Title: Gamochaeta Weddell
Description: Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.
Image: https://source.unsplash.com/random/?iphone
Date: 18/10/2020
Keywords: web
Category: Biodex
Author: Thoughtmix
Tags: cms
Published: true
Background: #ff0d24
Color: #f82d67
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aperiendum est igitur, quid sit voluptas; Pauca mutat vel plura sane; Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. At ego quem huic anteponam non audeo dicere; Nihil opus est exemplis hoc facere longius. Duo Reges: constructio interrete. Eam si varietatem diceres, intellegerem, ut etiam non dicente te intellego; Ita enim vivunt quidam, ut eorum vita refellatur oratio. 


	Satisne igitur videor vim verborum tenere, an sum etiam nunc vel Graece loqui vel Latine docendus?
	Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt.
	Ita prorsus, inquam;
	Sed erat aequius Triarium aliquid de dissensione nostra iudicare.



Zenonis est, inquam, hoc Stoici. At iam decimum annum in spelunca iacet. Estne, quaeso, inquam, sitienti in bibendo voluptas? Verba tu fingas et ea dicas, quae non sentias? Ut necesse sit omnium rerum, quae natura vigeant, similem esse finem, non eundem. Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio? Age sane, inquam. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. 


Corporis igitur nostri partes totaque figura et forma et
statura quam apta ad naturam sit, apparet, neque est dubium,
quin frons, oculi, aures et reliquae partes quales propriae
sint hominis intellegatur.

Hoc est vim afferre, Torquate, sensibus, extorquere ex
animis cognitiones verborum, quibus inbuti sumus.



Nam de summo mox, ut dixi, videbimus et ad id explicandum disputationem omnem conferemus. Non est igitur summum malum dolor. Sed ad haec, nisi molestum est, habeo quae velim. Huic mori optimum esse propter desperationem sapientiae, illi propter spem vivere. Sumenda potius quam expetenda. 


	Nulla erit controversia.
	Cuius tanta tormenta sunt, ut in iis beata vita, si modo dolor summum malum est, esse non possit.
	Recte dicis;
	Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur?
	Numquam facies.
	Dicet pro me ipsa virtus nec dubitabit isti vestro beato M.



Quacumque enim ingredimur, in aliqua historia vestigium ponimus. Faceres tu quidem, Torquate, haec omnia; Qualem igitur hominem natura inchoavit? Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Hoc sic expositum dissimile est superiori. Sed quid sentiat, non videtis. Atque his de rebus et splendida est eorum et illustris oratio. Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Hanc quoque iucunditatem, si vis, transfer in animum; Vestri haec verecundius, illi fortasse constantius. 

Itaque ab his ordiamur. Nihil opus est exemplis hoc facere longius. Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem. Hoc loco tenere se Triarius non potuit. Cur igitur easdem res, inquam, Peripateticis dicentibus verbum nullum est, quod non intellegatur? Theophrasti igitur, inquit, tibi liber ille placet de beata vita? Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Qui non moveatur et offensione turpitudinis et comprobatione honestatis? 

Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. An haec ab eo non dicuntur? Ita redarguitur ipse a sese, convincunturque scripta eius probitate ipsius ac moribus. Tollenda est atque extrahenda radicitus. Quod ea non occurrentia fingunt, vincunt Aristonem; Eam tum adesse, cum dolor omnis absit; Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Nobis aliter videtur, recte secusne, postea; 

Si quicquam extra virtutem habeatur in bonis. Et nemo nimium beatus est; Moriatur, inquit. Non prorsus, inquit, omnisque, qui sine dolore sint, in voluptate, et ea quidem summa, esse dico. Ut placet, inquit, etsi enim illud erat aptius, aequum cuique concedere. 


	Sed eum qui audiebant, quoad poterant, defendebant sententiam suam.
	Quid affers, cur Thorius, cur Caius Postumius, cur omnium horum magister, Orata, non iucundissime vixerit?
	Semovenda est igitur voluptas, non solum ut recta sequamini, sed etiam ut loqui deceat frugaliter.
	Aliud igitur esse censet gaudere, aliud non dolere.



Summum a vobis bonum voluptas dicitur. Sed quia studebat laudi et dignitati, multum in virtute processerat. Atque his de rebus et splendida est eorum et illustris oratio. Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Quid de Platone aut de Democrito loquar? Quid in isto egregio tuo officio et tanta fide-sic enim existimo-ad corpus refers? Hoc non est positum in nostra actione. 

Videamus animi partes, quarum est conspectus illustrior; Est enim effectrix multarum et magnarum voluptatum. Etiam inchoatum, ut, si iuste depositum reddere in recte factis sit, in officiis ponatur depositum reddere; Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q. Tecum optime, deinde etiam cum mediocri amico. Si qua in iis corrigere voluit, deteriora fecit. Negat enim summo bono afferre incrementum diem. 


Ita, quae mutat, ea corrumpit, quae sequitur sunt tota
Democriti, atomi, inane, imagines, quae eidola nominant,
quorum incursione non solum videamus, sed etiam cogitemus;

Si stante, hoc natura videlicet vult, salvam esse se, quod
concedimus;



Nihilo magis. Est enim effectrix multarum et magnarum voluptatum. Praeclare enim Plato: Beatum, cui etiam in senectute contigerit, ut sapientiam verasque opiniones assequi possit. Quid sequatur, quid repugnet, vident. Quorum altera prosunt, nocent altera. Omnis enim est natura diligens sui. 


	Sint ista Graecorum;
	Videamus animi partes, quarum est conspectus illustrior;
	Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria.
	Cum id fugiunt, re eadem defendunt, quae Peripatetici, verba.
	Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim?
	Sine ea igitur iucunde negat posse se vivere?




	Sint ista Graecorum;
	At hoc in eo M.
	Proclivi currit oratio.
	Nondum autem explanatum satis, erat, quid maxime natura vellet.
	A mene tu?
	Quibus ego vehementer assentior.
	Quid enim?
	Isto modo ne improbos quidem, si essent boni viri.




	Est enim perspicuum nullam artem ipsam in se versari, sed esse aliud artem ipsam, aliud quod propositum sit arti.




	Quodsi Graeci leguntur a Graecis isdem de rebus alia ratione compositis, quid est, cur nostri a nostris non legantur?




	Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum.
	Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus.
	Huius, Lyco, oratione locuples, rebus ipsis ielunior.
	Hoc non est positum in nostra actione.




