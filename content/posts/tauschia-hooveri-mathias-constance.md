Title: Tauschia hooveri Mathias & Constance
Description: Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.
Image: https://source.unsplash.com/random/?green
Date: 24/12/2020
Keywords: portfolio
Category: Hatity
Author: Dynava
Tags: website
Published: true
Background: #9ed6f7
Color: #e457a7
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid nunc honeste dicit? Illa argumenta propria videamus, cur omnia sint paria peccata. Quid autem habent admirationis, cum prope accesseris? Minime vero, inquit ille, consentit. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Duo Reges: constructio interrete. De ingenio eius in his disputationibus, non de moribus quaeritur. Mihi enim satis est, ipsis non satis. 

Verum hoc idem saepe faciamus. Age sane, inquam. Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Quamvis enim depravatae non sint, pravae tamen esse possunt. Vide, ne etiam menses! nisi forte eum dicis, qui, simul atque arripuit, interficit. Tubulo putas dicere? 

Quid enim me prohiberet Epicureum esse, si probarem, quae ille diceret? Tu autem negas fortem esse quemquam posse, qui dolorem malum putet. Cave putes quicquam esse verius. Quid, si etiam iucunda memoria est praeteritorum malorum? Sed quamquam negant nec virtutes nec vitia crescere, tamen utrumque eorum fundi quodam modo et quasi dilatari putant. Mihi quidem Antiochum, quem audis, satis belle videris attendere. 

Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Nam illud quidem adduci vix possum, ut ea, quae senserit ille, tibi non vera videantur. Traditur, inquit, ab Epicuro ratio neglegendi doloris. Minime vero istorum quidem, inquit. Sint modo partes vitae beatae. Aliam vero vim voluptatis esse, aliam nihil dolendi, nisi valde pertinax fueris, concedas necesse est. Scientiam pollicentur, quam non erat mirum sapientiae cupido patria esse cariorem. 


Quae si a vobis talia dicerentur, qualibus Caius Marius uti
poterat, ut expulsus, egens, in palude demersus tropaeorum
recordatione levaret dolorem suum, audirem et plane
probarem.

Quae duo sunt, unum facit.



Hoc est vim afferre, Torquate, sensibus, extorquere ex animis cognitiones verborum, quibus inbuti sumus. Ut aliquid scire se gaudeant? Non potes, nisi retexueris illa. Quis enim potest ea, quae probabilia videantur ei, non probare? Quorum sine causa fieri nihil putandum est. Quo tandem modo? Quamquam te quidem video minime esse deterritum. Quid ait Aristoteles reliquique Platonis alumni? Nulla profecto est, quin suam vim retineat a primo ad extremum. Immo alio genere; 


	Quacumque enim ingredimur, in aliqua historia vestigium ponimus.
	Tertium autem omnibus aut maximis rebus iis, quae secundum naturam sint, fruentem vivere.




	Quae est enim, quae se umquam deserat aut partem aliquam sui aut eius partis habitum aut vini aut ullius earum rerum, quae secundum naturam sunt, aut motum aut statum?



Hunc vos beatum; Quid iudicant sensus? Bona autem corporis huic sunt, quod posterius posui, similiora. 

Dolor ergo, id est summum malum, metuetur semper, etiamsi non aderit; Facete M. Quid, quod res alia tota est? Traditur, inquit, ab Epicuro ratio neglegendi doloris. Tum Triarius: Posthac quidem, inquit, audacius. Haec para/doca illi, nos admirabilia dicamus. 

Quam illa ardentis amores excitaret sui! Cur tandem? Ita relinquet duas, de quibus etiam atque etiam consideret. Homines optimi non intellegunt totam rationem everti, si ita res se habeat. Primum quid tu dicis breve? Te enim iudicem aequum puto, modo quae dicat ille bene noris. Fortemne possumus dicere eundem illum Torquatum? Facile est hoc cernere in primis puerorum aetatulis. Satis est tibi in te, satis in legibus, satis in mediocribus amicitiis praesidii. Beatum, inquit. 


	A mene tu?
	Nos paucis ad haec additis finem faciamus aliquando;
	Venit ad extremum;
	Nec vero sum nescius esse utilitatem in historia, non modo voluptatem.




	Efficiens dici potest.
	Indicant pueri, in quibus ut in speculis natura cernitur.
	Eadem fortitudinis ratio reperietur.
	Quod autem principium officii quaerunt, melius quam Pyrrho;




	Oculorum, inquit Plato, est in nobis sensus acerrimus, quibus sapientiam non cernimus.
	Nunc de hominis summo bono quaeritur;
	Ergo adhuc, quantum equidem intellego, causa non videtur fuisse mutandi nominis.
	Dicet pro me ipsa virtus nec dubitabit isti vestro beato M.
	Qui ita affectus, beatum esse numquam probabis;




Tollit definitiones, nihil de dividendo ac partiendo docet,
non quo modo efficiatur concludaturque ratio tradit, non qua
via captiosa solvantur ambigua distinguantur ostendit;

Sin est etiam corpus, ista explanatio naturae nempe hoc
effecerit, ut ea, quae ante explanationem tenebamus,
relinquamus.




	Quae cum dixissem, magis ut illum provocarem quam ut ipse loquerer, tum Triarius leniter arridens: Tu quidem, inquit, totum Epicurum paene e philosophorum choro sustulisti.




	Quis enim potest ea, quae probabilia videantur ei, non probare?
	Haec non erant eius, qui innumerabilis mundos infinitasque regiones, quarum nulla esset ora, nulla extremitas, mente peragravisset.
	Nam diligi et carum esse iucundum est propterea, quia tutiorem vitam et voluptatem pleniorem efficit.



Ut id aliis narrare gestiant? Hoc loco tenere se Triarius non potuit. In eo enim positum est id, quod dicimus esse expetendum. Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Nam neque virtute retinetur ille in vita, nec iis, qui sine virtute sunt, mors est oppetenda. Nam ante Aristippus, et ille melius. Quaerimus enim finem bonorum. Quid autem habent admirationis, cum prope accesseris? 

Miserum hominem! Si dolor summum malum est, dici aliter non potest. Primum quid tu dicis breve? Mihi quidem Antiochum, quem audis, satis belle videris attendere. Philosophi autem in suis lectulis plerumque moriuntur. Equidem etiam Epicurum, in physicis quidem, Democriteum puto. Quid ad utilitatem tantae pecuniae? 


	Itaque rursus eadem ratione, qua sum paulo ante usus, haerebitis.
	Nonne odio multos dignos putamus, qui quodam motu aut statu videntur naturae legem et modum contempsisse?
	Scrupulum, inquam, abeunti;
	Ex eorum enim scriptis et institutis cum omnis doctrina liberalis, omnis historia.
	Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse;




