Title: Poa rhizomata Hitchc.
Description: Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.
Image: https://source.unsplash.com/random/?strage
Date: 17/10/2020
Keywords: cms
Category: Job
Author: Mynte
Tags: work
Published: true
Background: #26b9e2
Color: #6f0ec9
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ita enim vivunt quidam, ut eorum vita refellatur oratio. Sic vester sapiens magno aliquo emolumento commotus cicuta, si opus erit, dimicabit. Mihi, inquam, qui te id ipsum rogavi? Est enim effectrix multarum et magnarum voluptatum. Duo Reges: constructio interrete. Aut unde est hoc contritum vetustate proverbium: quicum in tenebris? Itaque ad tempus ad Pisonem omnes. Quod dicit Epicurus etiam de voluptate, quae minime sint voluptates, eas obscurari saepe et obrui. 


	Sed vos squalidius, illorum vides quam niteat oratio.




Tum Piso: Atqui, Cicero, inquit, ista studia, si ad
imitandos summos viros spectant, ingeniosorum sunt;

Quamvis enim depravatae non sint, pravae tamen esse possunt.




	Ita prorsus, inquam;
	Nunc agendum est subtilius.
	Scaevolam M.
	Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint.
	Quibusnam praeteritis?
	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?



Aliter enim nosmet ipsos nosse non possumus. Quid enim possumus hoc agere divinius? Hic nihil fuit, quod quaereremus. Et harum quidem rerum facilis est et expedita distinctio. Ita fit ut, quanta differentia est in principiis naturalibus, tanta sit in finibus bonorum malorumque dissimilitudo. 

Quid iudicant sensus? Ista ipsa, quae tu breviter: regem, dictatorem, divitem solum esse sapientem, a te quidem apte ac rotunde; Quid, cum fictas fabulas, e quibus utilitas nulla elici potest, cum voluptate legimus? Egone quaeris, inquit, quid sentiam? 

Summum en�m bonum exposuit vacuitatem doloris; Igitur ne dolorem quidem. Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Sed tamen est aliquid, quod nobis non liceat, liceat illis. Haec quo modo conveniant, non sane intellego. Et harum quidem rerum facilis est et expedita distinctio. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Praeteritis, inquit, gaudeo. Non enim iam stirpis bonum quaeret, sed animalis. Ita fit cum gravior, tum etiam splendidior oratio. 

Sic enim censent, oportunitatis esse beate vivere. Quibus rebus vita consentiens virtutibusque respondens recta et honesta et constans et naturae congruens existimari potest. Quis hoc dicit? Scio enim esse quosdam, qui quavis lingua philosophari possint; Videamus animi partes, quarum est conspectus illustrior; Servari enim iustitia nisi a forti viro, nisi a sapiente non potest. Satisne ergo pudori consulat, si quis sine teste libidini pareat? Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur. 

Summum en�m bonum exposuit vacuitatem doloris; Suo enim quisque studio maxime ducitur. Non pugnem cum homine, cur tantum habeat in natura boni; Venit ad extremum; Consequentia exquirere, quoad sit id, quod volumus, effectum. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. 


	Qui est in parvis malis.
	Qui enim voluptatem ipsam contemnunt, iis licet dicere se acupenserem maenae non anteponere.
	Non enim solum Torquatus dixit quid sentiret, sed etiam cur.
	Nummus in Croesi divitiis obscuratur, pars est tamen divitiarum.




	Si vero id etiam explanare velles apertiusque diceres nihil eum fecisse nisi voluptatis causa, quo modo eum tandem laturum fuisse existimas?



Illud mihi a te nimium festinanter dictum videtur, sapientis omnis esse semper beatos; Ratio quidem vestra sic cogit. Et ais, si una littera commota sit, fore tota ut labet disciplina. 

Profectus in exilium Tubulus statim nec respondere ausus; Quod equidem non reprehendo; Atqui perspicuum est hominem e corpore animoque constare, cum primae sint animi partes, secundae corporis. Vide ne ista sint Manliana vestra aut maiora etiam, si imperes quod facere non possim. Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est. Hoc dixerit potius Ennius: Nimium boni est, cui nihil est mali. 


	Sunt etiam turpitudines plurimae, quae, nisi honestas natura plurimum valeat, cur non cadant in sapientem non est facile defendere.
	Praeterea et appetendi et refugiendi et omnino rerum gerendarum initia proficiscuntur aut a voluptate aut a dolore.
	Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt.
	Tu autem inter haec tantam multitudinem hominum interiectam non vides nec laetantium nec dolentium?




Et ille, cum erubuisset: Noli, inquit, ex me quaerere, qui
in Phalericum etiam descenderim, quo in loco ad fluctum
alunt declamare solitum Demosthenem, ut fremitum assuesceret
voce vincere.

Quid, quod homines infima fortuna, nulla spe rerum
gerendarum, opifices denique delectantur historia?



Atqui reperies, inquit, in hoc quidem pertinacem; Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat Epicurus voluptatem. Tecum optime, deinde etiam cum mediocri amico. Quo modo autem philosophus loquitur? Vulgo enim dicitur: Iucundi acti labores, nec male Euripidesconcludam, si potero, Latine; Cur fortior sit, si illud, quod tute concedis, asperum et vix ferendum putabit? Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat. Sed fac ista esse non inportuna; Atqui, inquit, si Stoicis concedis ut virtus sola, si adsit vitam efficiat beatam, concedis etiam Peripateticis. 

Audeo dicere, inquit. Non minor, inquit, voluptas percipitur ex vilissimis rebus quam ex pretiosissimis. Ergo omni animali illud, quod appetiti positum est in eo, quod naturae est accommodatum. Satisne ergo pudori consulat, si quis sine teste libidini pareat? Maximas vero virtutes iacere omnis necesse est voluptate dominante. An est aliquid, quod te sua sponte delectet? Atqui, inquit, si Stoicis concedis ut virtus sola, si adsit vitam efficiat beatam, concedis etiam Peripateticis. Equidem etiam Epicurum, in physicis quidem, Democriteum puto. 


	Et quod est munus, quod opus sapientiae?
	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;
	Poterat autem inpune;
	Quis istud possit, inquit, negare?




	Quid iudicant sensus?
	Quid ergo attinet dicere: Nihil haberem, quod reprehenderem, si finitas cupiditates haberent?
	Perge porro;
	Quis istud possit, inquit, negare?
	Stoicos roga.
	Haec quo modo conveniant, non sane intellego.
	Recte, inquit, intellegis.
	Urgent tamen et nihil remittunt.




	Potius ergo illa dicantur: turpe esse, viri non esse debilitari dolore, frangi, succumbere.
	Sextilio Rufo, cum is rem ad amicos ita deferret, se esse heredem Q.
	Conclusum est enim contra Cyrenaicos satis acute, nihil ad Epicurum.
	Quod quidem nobis non saepe contingit.
	Sunt etiam turpitudines plurimae, quae, nisi honestas natura plurimum valeat, cur non cadant in sapientem non est facile defendere.
	Aliena dixit in physicis nec ea ipsa, quae tibi probarentur;




