Title: Pseudognaphalium helleri (Britton) Anderb. ssp. helleri
Description: In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.
Image: https://source.unsplash.com/random/?green
Date: 17/4/2021
Keywords: cms
Category: Otcom
Author: Dynazzy
Tags: design
Published: true
Background: #41f951
Color: #2851b2
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Obsecro, inquit, Torquate, haec dicit Epicurus? Huius ego nunc auctoritatem sequens idem faciam. Duo Reges: constructio interrete. Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Non est ista, inquam, Piso, magna dissensio. Cur igitur, inquam, res tam dissimiles eodem nomine appellas? Ipse Epicurus fortasse redderet, ut Sextus Peducaeus, Sex. At enim hic etiam dolore. 

Recte, inquit, intellegis. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Estne, quaeso, inquam, sitienti in bibendo voluptas? Quod equidem non reprehendo; 

Est enim effectrix multarum et magnarum voluptatum. Explanetur igitur. Prodest, inquit, mihi eo esse animo. Nam quid possumus facere melius? Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. At iam decimum annum in spelunca iacet. Ego vero volo in virtute vim esse quam maximam; Ut id aliis narrare gestiant? Quae in controversiam veniunt, de iis, si placet, disseramus. Ne amores quidem sanctos a sapiente alienos esse arbitrantur. 


	Tenuit permagnam Sextilius hereditatem, unde, si secutus esset eorum sententiam, qui honesta et recta emolumentis omnibus et commodis anteponerent, nummum nullum attigisset.



Mihi, inquam, qui te id ipsum rogavi? Expressa vero in iis aetatibus, quae iam confirmatae sunt. Ita ceterorum sententiis semotis relinquitur non mihi cum Torquato, sed virtuti cum voluptate certatio. Piso, familiaris noster, et alia multa et hoc loco Stoicos irridebat: Quid enim? Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt. Dicet pro me ipsa virtus nec dubitabit isti vestro beato M. Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Potius inflammat, ut coercendi magis quam dedocendi esse videantur. Si mala non sunt, iacet omnis ratio Peripateticorum. 


	Sullae consulatum?
	Tum, Quintus et Pomponius cum idem se velle dixissent, Piso exorsus est.
	Moriatur, inquit.
	Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius.
	Paria sunt igitur.
	Totum genus hoc Zeno et qui ab eo sunt aut non potuerunt aut noluerunt, certe reliquerunt.




Et si in ipsa gubernatione neglegentia est navis eversa,
maius est peccatum in auro quam in palea.

Aut pertinacissimus fueris, si in eo perstiteris ad corpus
ea, quae dixi, referri, aut deserueris totam Epicuri
voluptatem, si negaveris.



Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? Qua tu etiam inprudens utebare non numquam. Aeque enim contingit omnibus fidibus, ut incontentae sint. Quae duo sunt, unum facit. Deprehensus omnem poenam contemnet. Duo enim genera quae erant, fecit tria. Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Quo studio Aristophanem putamus aetatem in litteris duxisse? 

At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Cuius quidem, quoniam Stoicus fuit, sententia condemnata mihi videtur esse inanitas ista verborum. Sed videbimus. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Quare attende, quaeso. Quam tu ponis in verbis, ego positam in re putabam. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? Tu quidem reddes; Nec vero sum nescius esse utilitatem in historia, non modo voluptatem. Suo enim quisque studio maxime ducitur. 

Haec dicuntur inconstantissime. Non igitur de improbo, sed de callido improbo quaerimus, qualis Q. Si enim ad populum me vocas, eum. Sit hoc ultimum bonorum, quod nunc a me defenditur; Naturales divitias dixit parabiles esse, quod parvo esset natura contenta. Non igitur de improbo, sed de callido improbo quaerimus, qualis Q. 


	Nulla erit controversia.
	Atque ut ceteri dicere existimantur melius quam facere, sic hi mihi videntur facere melius quam dicere.
	Quis negat?
	Quibusnam praeteritis?
	Quid de Pythagora?
	Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam.
	Eam stabilem appellas.
	Consequatur summas voluptates non modo parvo, sed per me nihilo, si potest;



Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est. Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere. Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Sed haec nihil sane ad rem; Ergo ita: non posse honeste vivi, nisi honeste vivatur? 

Cur, nisi quod turpis oratio est? Quae diligentissime contra Aristonem dicuntur a Chryippo. Similiter sensus, cum accessit ad naturam, tuetur illam quidem, sed etiam se tuetur; Facile est hoc cernere in primis puerorum aetatulis. Istam voluptatem, inquit, Epicurus ignorat? Mihi enim erit isdem istis fortasse iam utendum. 

Eorum enim est haec querela, qui sibi cari sunt seseque diligunt. In quibus doctissimi illi veteres inesse quiddam caeleste et divinum putaverunt. Haec dicuntur inconstantissime. An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? At ego quem huic anteponam non audeo dicere; Quarum ambarum rerum cum medicinam pollicetur, luxuriae licentiam pollicetur. 


	Tibi hoc incredibile, quod beatissimum.
	Conferam tecum, quam cuique verso rem subicias;
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.
	Sed ad bona praeterita redeamus.




	Erit enim instructus ad mortem contemnendam, ad exilium, ad ipsum etiam dolorem.
	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Praeterea sublata cognitione et scientia tollitur omnis ratio et vitae degendae et rerum gerendarum.




	Hoc autem tempore, etsi multa in omni parte Athenarum sunt in ipsis locis indicia summorum virorum, tamen ego illa moveor exhedra.




	Age sane, inquam.
	Quacumque enim ingredimur, in aliqua historia vestigium ponimus.
	At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia?




	Tum Piso: Atqui, Cicero, inquit, ista studia, si ad imitandos summos viros spectant, ingeniosorum sunt;
	Tum Triarius: Posthac quidem, inquit, audacius.




Quarum adeo omnium sententia pronuntiabit primum de
voluptate nihil esse ei loci, non modo ut sola ponatur in
summi boni sede, quam quaerimus, sed ne illo quidem modo, ut
ad honestatem applicetur.

Haec quo modo conveniant, non sane intellego.




