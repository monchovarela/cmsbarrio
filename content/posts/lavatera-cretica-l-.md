Title: Lavatera cretica L.
Description: Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.
Image: https://source.unsplash.com/random/?android
Date: 5/11/2020
Keywords: design
Category: Holdlamis
Author: Voonix
Tags: article
Published: true
Background: #e1eb8d
Color: #e9066a
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Habent enim et bene longam et satis litigiosam disputationem. Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam; Quae fere omnia appellantur uno ingenii nomine, easque virtutes qui habent, ingeniosi vocantur. Conferam tecum, quam cuique verso rem subicias; Duo Reges: constructio interrete. 


Vobis autem, quibus nihil est aliud propositum nisi rectum
atque honestum, unde officii, unde agendi principlum
nascatur non reperietis.

Sin te auctoritas commovebat, nobisne omnibus et Platoni
ipsi nescio quem illum anteponebas?




	Consequentia exquirere, quoad sit id, quod volumus, effectum.
	Hanc in motu voluptatem -sic enim has suaves et quasi dulces voluptates appellat-interdum ita extenuat, ut M.
	Studet enim meus is audire Cicero quaenam sit istius veteris, quam commemoras, Academiae de finibus bonorum Peripateticorumque sententia.
	Cur igitur, cum de re conveniat, non malumus usitate loqui?




	Quid adiuvas?
	Ita graviter et severe voluptatem secrevit a bono.
	Audeo dicere, inquit.
	Est enim tanti philosophi tamque nobilis audacter sua decreta defendere.
	Peccata paria.
	Profectus in exilium Tubulus statim nec respondere ausus;
	Efficiens dici potest.
	Putabam equidem satis, inquit, me dixisse.




	Honestum igitur id intellegimus, quod tale est, ut detracta omni utilitate sine ullis praemiis fructibusve per se ipsum possit iure laudari.



Nihilo beatiorem esse Metellum quam Regulum. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Sit hoc ultimum bonorum, quod nunc a me defenditur; Sed ad haec, nisi molestum est, habeo quae velim. Disserendi artem nullam habuit. Negat enim summo bono afferre incrementum diem. Sed tamen intellego quid velit. Itaque his sapiens semper vacabit. 


Atque ut reliqui fures earum rerum, quas ceperunt, signa
commutant, sic illi, ut sententiis nostris pro suis
uterentur, nomina tamquam rerum notas mutaverunt.

Alterum significari idem, ut si diceretur, officia media
omnia aut pleraque servantem vivere.



Recte, inquit, intellegis. Tria genera bonorum; Haeret in salebra. Quod non faceret, si in voluptate summum bonum poneret. Si quidem, inquit, tollerem, sed relinquo. Incommoda autem et commoda-ita enim estmata et dustmata appello-communia esse voluerunt, paria noluerunt. 


	In qua quid est boni praeter summam voluptatem, et eam sempiternam?
	Unum nescio, quo modo possit, si luxuriosus sit, finitas cupiditates habere.
	Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat;
	Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D.




	Peccata paria.
	Itaque contra est, ac dicitis;
	Sed videbimus.
	Itaque rursus eadem ratione, qua sum paulo ante usus, haerebitis.
	Cur iustitia laudatur?
	Qui non moveatur et offensione turpitudinis et comprobatione honestatis?
	Perge porro;
	Ac tamen, ne cui loco non videatur esse responsum, pauca etiam nunc dicam ad reliquam orationem tuam.



Etenim semper illud extra est, quod arte comprehenditur. Immo vero, inquit, ad beatissime vivendum parum est, ad beate vero satis. Haec et tu ita posuisti, et verba vestra sunt. Quis animo aequo videt eum, quem inpure ac flagitiose putet vivere? Conferam tecum, quam cuique verso rem subicias; Et ille ridens: Video, inquit, quid agas; Idem iste, inquam, de voluptate quid sentit? Sin aliud quid voles, postea. Sed emolumenta communia esse dicuntur, recte autem facta et peccata non habentur communia. Non semper, inquam; Tum Torquatus: Prorsus, inquit, assentior; Quod dicit Epicurus etiam de voluptate, quae minime sint voluptates, eas obscurari saepe et obrui. 


	Quo modo igitur, inquies, verum esse poterit omnia referri ad summum bonum, si amic�tiae, si propinquitates, si reliqua externa summo bono non continentur?




	Certe, nisi voluptatem tanti aestimaretis.
	Nonne videmus quanta perturbatio rerum omnium consequatur, quanta confusio?
	Inscite autem medicinae et gubernationis ultimum cum ultimo sapientiae comparatur.
	Hunc ipsum Zenonis aiunt esse finem declarantem illud, quod a te dictum est, convenienter naturae vivere.
	Qua ex cognitione facilior facta est investigatio rerum occultissimarum.



Sapientem locupletat ipsa natura, cuius divitias Epicurus parabiles esse docuit. Sed ille, ut dixi, vitiose. Praeclarae mortes sunt imperatoriae; Et quidem, Cato, hanc totam copiam iam Lucullo nostro notam esse oportebit; Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; 


	Ita redarguitur ipse a sese, convincunturque scripta eius probitate ipsius ac moribus.
	Quod non faceret, si in voluptate summum bonum poneret.
	Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec.
	Voluptatem cum summum bonum diceret, primum in eo ipso parum vidit, deinde hoc quoque alienum;
	Quid ergo hoc loco intellegit honestum?



An eum discere ea mavis, quae cum plane perdidiceriti nihil sciat? Non igitur bene. Num igitur eum postea censes anxio animo aut sollicito fuisse? Quae sequuntur igitur? Et ais, si una littera commota sit, fore tota ut labet disciplina. Et quidem, inquit, vehementer errat; In qua quid est boni praeter summam voluptatem, et eam sempiternam? Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Perturbationes autem nulla naturae vi commoventur, omniaque ea sunt opiniones ac iudicia levitatis. 

Quia dolori non voluptas contraria est, sed doloris privatio. Verum tamen cum de rebus grandioribus dicas, ipsae res verba rapiunt; Nec vero alia sunt quaerenda contra Carneadeam illam sententiam. Illa videamus, quae a te de amicitia dicta sunt. Quonam, inquit, modo? Sed quid attinet de rebus tam apertis plura requirere? Ergo id est convenienter naturae vivere, a natura discedere. Et ille ridens: Video, inquit, quid agas; Atque ab his initiis profecti omnium virtutum et originem et progressionem persecuti sunt. Etsi ea quidem, quae adhuc dixisti, quamvis ad aetatem recte isto modo dicerentur. 

Quare obscurentur etiam haec, quae secundum naturam esse dicimus, in vita beata; Et ille ridens: Video, inquit, quid agas; 

Quo tandem modo? Scrupulum, inquam, abeunti; Quae quidem sapientes sequuntur duce natura tamquam videntes; Quae animi affectio suum cuique tribuens atque hanc, quam dico. Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat. Sic enim censent, oportunitatis esse beate vivere. Paulum, cum regem Persem captum adduceret, eodem flumine invectio? Sed ea mala virtuti magnitudine obruebantur. 

Est enim effectrix multarum et magnarum voluptatum. Negabat igitur ullam esse artem, quae ipsa a se proficisceretur; Sed nimis multa. Inde sermone vario sex illa a Dipylo stadia confecimus. De quibus cupio scire quid sentias. Is es profecto tu. 


