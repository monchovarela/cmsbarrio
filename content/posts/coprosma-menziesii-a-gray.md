Title: Coprosma menziesii A. Gray
Description: Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.
Image: https://source.unsplash.com/random/?red
Date: 26/11/2020
Keywords: article
Category: Daltfresh
Author: Brainverse
Tags: design
Published: true
Background: #75910f
Color: #0b0217
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit hoc ultimum bonorum, quod nunc a me defenditur; Iam in altera philosophiae parte. Tum Quintus: Est plane, Piso, ut dicis, inquit. Quamquam tu hanc copiosiorem etiam soles dicere. Duo Reges: constructio interrete. Quod, inquit, quamquam voluptatibus quibusdam est saepe iucundius, tamen expetitur propter voluptatem. 

Quae qui non vident, nihil umquam magnum ac cognitione dignum amaverunt. Scio enim esse quosdam, qui quavis lingua philosophari possint; Nam, ut paulo ante docui, augendae voluptatis finis est doloris omnis amotio. Videsne quam sit magna dissensio? Quicquid porro animo cernimus, id omne oritur a sensibus; Itaque hoc frequenter dici solet a vobis, non intellegere nos, quam dicat Epicurus voluptatem. Ergo hoc quidem apparet, nos ad agendum esse natos. 


	Quid igitur dubitamus in tota eius natura quaerere quid sit effectum?
	Sic consequentibus vestris sublatis prima tolluntur.
	Utilitatis causa amicitia est quaesita.



Teneo, inquit, finem illi videri nihil dolere. Satis est ad hoc responsum. Tecum optime, deinde etiam cum mediocri amico. Ego vero isti, inquam, permitto. Satis est tibi in te, satis in legibus, satis in mediocribus amicitiis praesidii. Id Sextilius factum negabat. 


Recte, inquit, intellegis.

Primum Theophrasti, Strato, physicum se voluit;




Is hoc melior, quam Pyrrho, quod aliquod genus appetendi
dedit, deterior quam ceteri, quod penitus a natura recessit.

Quis enim confidit semper sibi illud stabile et firmum
permansurum, quod fragile et caducum sit?



Suo genere perveniant ad extremum; Quorum altera prosunt, nocent altera. Iam id ipsum absurdum, maximum malum neglegi. Sint ista Graecorum; Sed haec nihil sane ad rem; Nam quibus rebus efficiuntur voluptates, eae non sunt in potestate sapientis. Negare non possum. Quis est tam dissimile homini. Huic ego, si negaret quicquam interesse ad beate vivendum quali uteretur victu, concederem, laudarem etiam; 

Utinam quidem dicerent alium alio beatiorem! Iam ruinas videres. Nihilo beatiorem esse Metellum quam Regulum. Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam. Quodsi ipsam honestatem undique pertectam atque absolutam. Non igitur potestis voluptate omnia dirigentes aut tueri aut retinere virtutem. Quid enim de amicitia statueris utilitatis causa expetenda vides. Quod cum dixissent, ille contra. 


	Est enim aliquid in his rebus probabile, et quidem ita, ut eius ratio reddi possit, ergo ut etiam probabiliter acti ratio reddi possit.



Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Quodsi ipsam honestatem undique pertectam atque absolutam. Quia dolori non voluptas contraria est, sed doloris privatio. Quid ei reliquisti, nisi te, quoquo modo loqueretur, intellegere, quid diceret? Neque solum ea communia, verum etiam paria esse dixerunt. Sed quod proximum fuit non vidit. Vulgo enim dicitur: Iucundi acti labores, nec male Euripidesconcludam, si potero, Latine; 

An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? Sed ad rem redeamus; Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum. Atqui iste locus est, Piso, tibi etiam atque etiam confirmandus, inquam; Sed tamen omne, quod de re bona dilucide dicitur, mihi praeclare dici videtur. Quo plebiscito decreta a senatu est consuli quaestio Cn. Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit; Tum Torquatus: Prorsus, inquit, assentior; 


	Tenent mordicus.
	Theophrasti igitur, inquit, tibi liber ille placet de beata vita?
	Immo videri fortasse.
	Quod autem in homine praestantissimum atque optimum est, id deseruit.
	Ita credo.
	In eo enim positum est id, quod dicimus esse expetendum.
	Quid Zeno?
	Gloriosa ostentatio in constituendo summo bono.
	Non semper, inquam;
	Dicam, inquam, et quidem discendi causa magis, quam quo te aut Epicurum reprehensum velim.
	Peccata paria.
	Tu enim ista lenius, hic Stoicorum more nos vexat.



Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia; Satis est ad hoc responsum. Te ipsum, dignissimum maioribus tuis, voluptasne induxit, ut adolescentulus eriperes P. Scisse enim te quis coarguere possit? Quo plebiscito decreta a senatu est consuli quaestio Cn. De hominibus dici non necesse est. At quicum ioca seria, ut dicitur, quicum arcana, quicum occulta omnia? Nam neque virtute retinetur ille in vita, nec iis, qui sine virtute sunt, mors est oppetenda. Aliter homines, aliter philosophos loqui putas oportere? 


	Natura sic ab iis investigata est, ut nulla pars caelo, mari, terra, ut po�tice loquar, praetermissa sit;



Ille enim occurrentia nescio quae comminiscebatur; Is es profecto tu. Quod autem in homine praestantissimum atque optimum est, id deseruit. Haec quo modo conveniant, non sane intellego. Haec para/doca illi, nos admirabilia dicamus. Haec quo modo conveniant, non sane intellego. Virtutibus igitur rectissime mihi videris et ad consuetudinem nostrae orationis vitia posuisse contraria. Quae cum praeponunt, ut sit aliqua rerum selectio, naturam videntur sequi; Ad eos igitur converte te, quaeso. 


	Equidem, sed audistine modo de Carneade?
	Suam denique cuique naturam esse ad vivendum ducem.
	Itaque homo in primis ingenuus et gravis, dignus illa familiaritate Scipionis et Laelii, Panaetius, cum ad Q.
	Id enim volumus, id contendimus, ut officii fructus sit ipsum officium.




	Ita enim vivunt quidam, ut eorum vita refellatur oratio.
	An dubium est, quin virtus ita maximam partem optineat in rebus humanis, ut reliquas obruat?
	Quis istud possit, inquit, negare?
	Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset.
	Audax negotium, dicerem impudens, nisi hoc institutum postea translatum ad philosophos nostros esset.



Quo tandem modo? Cur id non ita fit? Quod autem satis est, eo quicquid accessit, nimium est; Si quicquam extra virtutem habeatur in bonis. Virtutis, magnitudinis animi, patientiae, fortitudinis fomentis dolor mitigari solet. Quonam modo? 


	Vitiosum est enim in dividendo partem in genere numerare.
	Itaque his sapiens semper vacabit.
	Tanti autem aderant vesicae et torminum morbi, ut nihil ad eorum magnitudinem posset accedere.
	Itaque nostrum est-quod nostrum dico, artis est-ad ea principia, quae accepimus.
	Sed quid ages tandem, si utilitas ab amicitia, ut fit saepe, defecerit?




	Sed videbimus.
	Tum ille: Tu autem cum ipse tantum librorum habeas, quos hic tandem requiris?
	Moriatur, inquit.
	At multis malis affectus.




