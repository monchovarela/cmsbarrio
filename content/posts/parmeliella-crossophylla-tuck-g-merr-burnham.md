Title: Parmeliella crossophylla (Tuck.) G. Merr. & Burnham
Description: Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.
Image: https://source.unsplash.com/random/?ruby
Date: 20/7/2021
Keywords: article
Category: Y-find
Author: Roombo
Tags: post
Published: true
Background: #0c13d2
Color: #f79856
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; Iam quae corporis sunt, ea nec auctoritatem cum animi partibus, comparandam et cognitionem habent faciliorem. Aliter enim nosmet ipsos nosse non possumus. Nunc omni virtuti vitium contrario nomine opponitur. Is ita vivebat, ut nulla tam exquisita posset inveniri voluptas, qua non abundaret. Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat. 


	Fortasse id optimum, sed ubi illud: Plus semper voluptatis?
	Quod mihi quidem visus est, cum sciret, velle tamen confitentem audire Torquatum.



Quis est tam dissimile homini. Duo Reges: constructio interrete. Ergo opifex plus sibi proponet ad formarum quam civis excellens ad factorum pulchritudinem? Mihi quidem Antiochum, quem audis, satis belle videris attendere. Cupit enim d�cere nihil posse ad beatam vitam deesse sapienti. Non enim solum Torquatus dixit quid sentiret, sed etiam cur. Nos vero, inquit ille; Quis Pullum Numitorium Fregellanum, proditorem, quamquam rei publicae nostrae profuit, non odit? 

Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? A mene tu? Dic in quovis conventu te omnia facere, ne doleas. Quid turpius quam sapientis vitam ex insipientium sermone pendere? Quid, quod res alia tota est? Primum in nostrane potestate est, quid meminerimus? 


	Itaque fecimus.
	Est enim tanti philosophi tamque nobilis audacter sua decreta defendere.
	Si longus, levis;
	Inde igitur, inquit, ordiendum est.
	Ita credo.
	-, sed ut hoc iudicaremus, non esse in iis partem maximam positam beate aut secus vivendi.
	Nihilo magis.
	Beatus sibi videtur esse moriens.




	Erat enim Polemonis.
	Quae cum essent dicta, finem fecimus et ambulandi et disputandi.
	Reguli reiciendam;
	Graecum enim hunc versum nostis omnes-: Suavis laborum est praeteritorum memoria.
	Reguli reiciendam;
	Nam illud vehementer repugnat, eundem beatum esse et multis malis oppressum.
	Respondeat totidem verbis.
	Quo tandem modo?
	Moriatur, inquit.
	Hinc ceteri particulas arripere conati suam quisque videro voluit afferre sententiam.
	Quare attende, quaeso.
	Etenim nec iustitia nec amicitia esse omnino poterunt, nisi ipsae per se expetuntur.



Ut optime, secundum naturam affectum esse possit. Quae sequuntur igitur? Eam tum adesse, cum dolor omnis absit; Non autem hoc: igitur ne illud quidem. Qui est in parvis malis. Quam illa ardentis amores excitaret sui! Cur tandem? 

Quorum sine causa fieri nihil putandum est. Age nunc isti doceant, vel tu potius quis enim ista melius? Sequitur disserendi ratio cognitioque naturae; 

In schola desinis. Et quidem, inquit, vehementer errat; Cum autem venissemus in Academiae non sine causa nobilitata spatia, solitudo erat ea, quam volueramus. Qui ita affectus, beatum esse numquam probabis; Sed ad haec, nisi molestum est, habeo quae velim. Sed quae tandem ista ratio est? Huius ego nunc auctoritatem sequens idem faciam. Aut, Pylades cum sis, dices te esse Orestem, ut moriare pro amico? Quod cum accidisset ut alter alterum necopinato videremus, surrexit statim. Ergo hoc quidem apparet, nos ad agendum esse natos. 


	Non quam nostram quidem, inquit Pomponius iocans;
	Cum id quoque, ut cupiebat, audivisset, evelli iussit eam, qua erat transfixus, hastam.
	Nihil acciderat ei, quod nollet, nisi quod anulum, quo delectabatur, in mari abiecerat.
	Si enim ita est, vide ne facinus facias, cum mori suadeas.
	Num igitur eum postea censes anxio animo aut sollicito fuisse?
	Nos quidem Virtutes sic natae sumus, ut tibi serviremus, aliud negotii nihil habemus.



Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Si enim ad populum me vocas, eum. At modo dixeras nihil in istis rebus esse, quod interesset. Recte dicis; 


	Quibus rebus vita consentiens virtutibusque respondens recta et honesta et constans et naturae congruens existimari potest.
	Cuius similitudine perspecta in formarum specie ac dignitate transitum est ad honestatem dictorum atque factorum.
	Cum sciret confestim esse moriendum eamque mortem ardentiore studio peteret, quam Epicurus voluptatem petendam putat.
	Compensabatur, inquit, cum summis doloribus laetitia.




	Quam nemo umquam voluptatem appellavit, appellat;
	Nec vero alia sunt quaerenda contra Carneadeam illam sententiam.
	Dolere malum est: in crucem qui agitur, beatus esse non potest.
	Cetera illa adhibebat, quibus demptis negat se Epicurus intellegere quid sit bonum.
	Roges enim Aristonem, bonane ei videantur haec: vacuitas doloris, divitiae, valitudo;



Cum autem in quo sapienter dicimus, id a primo rectissime dicitur. Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? Si enim ad populum me vocas, eum. At ego quem huic anteponam non audeo dicere; Hoc tu nunc in illo probas. 


Hunc igitur finem illi tenuerunt, quodque ego pluribus
verbis, illi brevius secundum naturam vivere, hoc iis
bonorum videbatur extremum.

Quod, inquit, quamquam voluptatibus quibusdam est saepe
iucundius, tamen expetitur propter voluptatem.




Officium, aequitatem, dignitatem, fidem, recta, honesta,
digna imperio, digna populo Romano, omnia pericula pro re
publica, mori pro patria, haec cum loqueris, nos barones
stupemus, tu videlicet tecum ipse rides.

Sed id ne cogitari quidem potest quale sit, ut non repugnet
ipsum sibi.




	Multosque etiam dolores curationis causa perferant, ut, si ipse usus membrorum non modo non maior, verum etiam minor futurus sit, eorum tamen species ad naturam revertatur?




	Itaque idem natura victus, cui obsisti non potest, dicit alio loco id, quod a te etiam paulo ante dictum est, non posse iucunde vivi nisi etiam honeste.



Te enim iudicem aequum puto, modo quae dicat ille bene noris. Quid ergo hoc loco intellegit honestum? In contemplatione et cognitione posita rerum, quae quia deorum erat vitae simillima, sapiente visa est dignissima. Ita prorsus, inquam; Haec dicuntur fortasse ieiunius; Itaque sensibus rationem adiunxit et ratione effecta sensus non reliquit. Quo modo autem philosophus loquitur? Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. 

An haec ab eo non dicuntur? Mihi, inquam, qui te id ipsum rogavi? Quis istud possit, inquit, negare? Si alia sentit, inquam, alia loquitur, numquam intellegam quid sentiat; Hoc est non dividere, sed frangere. Tum Triarius: Posthac quidem, inquit, audacius. Duae sunt enim res quoque, ne tu verba solum putes. Hoc est non modo cor non habere, sed ne palatum quidem. 


