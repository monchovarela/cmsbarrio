Title: Tanacetum camphoratum Less.
Description: Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.
Image: https://source.unsplash.com/random/?blue
Date: 19/4/2021
Keywords: post
Category: Quo Lux
Author: Riffpedia
Tags: portfolio
Published: true
Background: #43b305
Color: #6cb57c
Robots: index,follow
Attrs: []
Template: post

----


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suo genere perveniant ad extremum; Cum ageremus, inquit, vitae beatum et eundem supremum diem, scribebamus haec. Quantum Aristoxeni ingenium consumptum videmus in musicis? Duo Reges: constructio interrete. Non igitur bene. An ea, quae per vinitorem antea consequebatur, per se ipsa curabit? 


Quid enim me prohiberet Epicureum esse, si probarem, quae
ille diceret?

Eorum enim est haec querela, qui sibi cari sunt seseque
diligunt.




	Hic cum uterque me intueretur seseque ad audiendum significarent paratos, Primum, inquam, deprecor, ne me tamquam philosophum putetis scholam vobis aliquam explicaturum, quod ne in ipsis quidem philosophis magnopere umquam probavi.



Sed fortuna fortis; Et quidem iure fortasse, sed tamen non gravissimum est testimonium multitudinis. Sed potestne rerum maior esse dissensio? Quare attende, quaeso. Conferam tecum, quam cuique verso rem subicias; Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. Hoc etsi multimodis reprehendi potest, tamen accipio, quod dant. Vide igitur ne non debeas verbis nostris uti, sententiis tuis. Quae autem natura suae primae institutionis oblita est? Nam, ut saepe iam dixi, in infirma aetate inbecillaque mente vis naturae quasi per caliginem cernitur; 

An vero displicuit ea, quae tributa est animi virtutibus tanta praestantia? Quae cum ita sint, effectum est nihil esse malum, quod turpe non sit. Hoc dictum in una re latissime patet, ut in omnibus factis re, non teste moveamur. Sedulo, inquam, faciam. Gerendus est mos, modo recte sentiat. Ut id aliis narrare gestiant? Consequens enim est et post oritur, ut dixi. Videamus animi partes, quarum est conspectus illustrior; Quod ea non occurrentia fingunt, vincunt Aristonem; 


	Si longus, levis.
	Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;
	Beatum, inquit.
	Istam voluptatem, inquit, Epicurus ignorat?



Sint ista Graecorum; Iis igitur est difficilius satis facere, qui se Latina scripta dicunt contemnere. Voluptatem cum summum bonum diceret, primum in eo ipso parum vidit, deinde hoc quoque alienum; Quem si tenueris, non modo meum Ciceronem, sed etiam me ipsum abducas licebit. 


	Quorum sine causa fieri nihil putandum est.
	Eadem nunc mea adversum te oratio est.
	Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta.
	Idem iste, inquam, de voluptate quid sentit?



Verum hoc idem saepe faciamus. Quodsi ipsam honestatem undique pertectam atque absolutam. Quo invento omnis ab eo quasi capite de summo bono et malo disputatio ducitur. Sin autem ad animum, falsum est, quod negas animi ullum esse gaudium, quod non referatur ad corpus. Nam ante Aristippus, et ille melius. Quis istum dolorem timet? 


	Tenent mordicus.
	Sin tantum modo ad indicia veteris memoriae cognoscenda, curiosorum.
	Qui est in parvis malis.
	Atque adhuc ea dixi, causa cur Zenoni non fuisset, quam ob rem a superiorum auctoritate discederet.
	Nam et complectitur verbis, quod vult, et dicit plane, quod intellegam;




	Ergo hoc quidem apparet, nos ad agendum esse natos.
	Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam.
	Cuius ad naturam apta ratio vera illa et summa lex a philosophis dicitur.
	Sequitur disserendi ratio cognitioque naturae;



Non est enim vitium in oratione solum, sed etiam in moribus. Verum hoc idem saepe faciamus. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Vos autem cum perspicuis dubia debeatis illustrare, dubiis perspicua conamini tollere. 

Eadem nunc mea adversum te oratio est. Negat esse eam, inquit, propter se expetendam. Nihilo beatiorem esse Metellum quam Regulum. Homines optimi non intellegunt totam rationem everti, si ita res se habeat. Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? Itaque sensibus rationem adiunxit et ratione effecta sensus non reliquit. Non risu potius quam oratione eiciendum? 

Laboro autem non sine causa; Apud imperitos tum illa dicta sunt, aliquid etiam coronae datum; Quaesita enim virtus est, non quae relinqueret naturam, sed quae tueretur. Primum Theophrasti, Strato, physicum se voluit; De hominibus dici non necesse est. Quid enim? 

Quo plebiscito decreta a senatu est consuli quaestio Cn. Videamus animi partes, quarum est conspectus illustrior; Expressa vero in iis aetatibus, quae iam confirmatae sunt. Quando enim Socrates, qui parens philosophiae iure dici potest, quicquam tale fecit? Quid enim ab antiquis ex eo genere, quod ad disserendum valet, praetermissum est? Ut id aliis narrare gestiant? Ecce aliud simile dissimile. Quid ergo attinet gloriose loqui, nisi constanter loquare? Non prorsus, inquit, omnisque, qui sine dolore sint, in voluptate, et ea quidem summa, esse dico. 

Ut aliquid scire se gaudeant? Qui autem de summo bono dissentit de tota philosophiae ratione dissentit. Haeret in salebra. Quid dubitas igitur mutare principia naturae? Quae duo sunt, unum facit. 


	Sensibus enim ornavit ad res percipiendas idoneis, ut nihil aut non multum adiumento ullo ad suam confirmationem indigerent;




Nec vero dico eorum metum mortis, qui, quia privari se vitae
bonis arbitrentur, aut quia quasdam post mortem formidines
extimescant, aut si metuant, ne cum dolore moriantur,
idcirco mortem fugiant;

Laelius clamores sof�w ille so lebat Edere compellans
gumias ex ordine nostros.




	Quis contra in illa aetate pudorem, constantiam, etiamsi sua nihil intersit, non tamen diligat?
	In qua quid est boni praeter summam voluptatem, et eam sempiternam?
	Tum Triarius: Posthac quidem, inquit, audacius.
	Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est.
	Nam nec vir bonus ac iustus haberi debet qui, ne malum habeat, abstinet se ab iniuria.




	Ita prorsus, inquam;
	Et summatim quidem haec erant de corpore animoque dicenda, quibus quasi informatum est quid hominis natura postulet.
	Quis enim redargueret?
	Conferam avum tuum Drusum cum C.
	Sed nimis multa.
	Quod cum dixissent, ille contra.
	Quo modo?
	Quid, si non sensus modo ei sit datus, verum etiam animus hominis?




