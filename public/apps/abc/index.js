window.onload = () => load();

let visualObj;
const visualOptions = {
  responsive: "resize",
  add_classes: true,
  clickListener: (evt) => playSynth(evt),
  oneSvgPerLine: true,
  jazzchords: true,
  indicate_changed:true,
};

function load() {
  visualObj = new ABCJS.Editor("abc", {
    canvas_id: "paper",
    synth: {
      el: "#midi",
      options: {
        displayLoop: !0,
        displayRestart: !0,
        displayPlay: !0,
        displayProgress: !0,
        displayWarp: !0,
      },
    },
    abcjsParams: visualOptions,
  });
}

function playSynth(evt){
  if (evt.midiPitches) {
    ABCJS.synth
      .playEvent(
        evt.midiPitches,
        evt.midiGraceNotePitches,
        visualObj.millisecondsPerMeasure()
      )
      .then(function (evt) {
        //console.log("note played");
      })
      .catch(function (evt) {
        console.log("error playing note", evt);
      });
  }
}