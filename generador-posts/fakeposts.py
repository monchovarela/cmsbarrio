import json
import requests
import re
import unidecode
import random

"""
- https://loripsum.net/ Api

- Options
=========================================================
(integer) - The number of paragraphs to generate.
short, medium, long, verylong - The average length of a paragraph.
decorate - Add bold, italic and marked text.
link - Add links.
ul - Add unordered lists.
ol - Add numbered lists.
dl - Add description lists.
bq - Add blockquotes.
code - Add code samples.
headers - Add headers.
allcaps - Use ALL CAPS.
prude - Prude version.
plaintext - Return plain text, no HTML.
============================================================

- Example short get 10 lorem paragrahps
https://loripsum.net/api/10

- Example long all html 
https://loripsum.net/api/10/medium/decorate/link/ul/ol/dl/bq/code/headers/allcaps/prude

- Example long all text 
https://loripsum.net/api/10/medium/decorate/link/ul/ol/dl/bq/code/headers/allcaps/text

"""


jsonFile = 'data.json'
output = 'posts'
content_url = 'https://loripsum.net/api/10/medium/decorate/link/ul/ol/dl/bq/code/plaintext'
# random words for random unsplash images
randomWords = 'design beauty city night modern minimal girl strage red blue green yellow aqua nude code developer cat android iphone javascript ruby php css css3'

def slugify(text):
    text = unidecode.unidecode(text).lower()
    return re.sub(r'[\W_]+', '-', text)

# random words
words = list(map(str, randomWords.split()))

# to count files
num = 0

file = open(jsonFile,'r',encoding="utf-8");
toJson = json.loads(file.read())
file.close()
for var in toJson:
    # create slug
    slug = slugify(var['Title'])

    # vars
    title = var['Title']
    desc = var['Description']
    image = var['Image']
    date = var['Date']
    keywords = var['Keywords']
    category = var['Category']
    author = var['Author']
    tags = var['Tags']
    published = var['Published']
    background = var['Background']
    color = var['Color']
    robots = var['Robots']
    attrs = var['Attrs']
    randomTags = random.choice(words)
    image = image + f"/?{randomTags}"

    # open file to save
    doc = open(f"{output}/{slug}.md","w+")

    # get randon lorem
    req = requests.get(content_url)
    content = req.text

    # count files
    num = num + 1

    # template
    tpl = f"""Title: {title}
Description: {desc}
Image: {image}
Date: {date}
Keywords: {keywords}
Category: {category}
Author: {author}
Tags: {tags}
Published: true
Background: {background}
Color: {color}
Robots: {robots}
Attrs: {attrs}
Template: post\n
----\n\n
{content}
"""
    doc.write(tpl)
    print(f"Save post {num} with title={slug}")
    doc.close()