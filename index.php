<?php

/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare(strict_types=1);

/*
 *  Definir el acceso a los archivos
 */
define('ACCESS', true);

/*
 * Definir el PATH al directorio raíz (sin trailing slash).
 */
define('ROOT_DIR', rtrim(dirname(__FILE__), '\\/'));

// X-Powered-By
header("X-Powered-By: Moncho Varela :)");

include ROOT_DIR.'/core/init.php';
