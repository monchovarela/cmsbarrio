<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/**
 * Acceso restringido
 */
defined("ACCESS") or die("No tiene acceso a este archivo");

return array(
    'name' => 'Tienda',
    'description' => 'Crea una tienda con https://reflowhq.com/',
    'enabled' => true,
    // configuracion tienda
    'id-tienda' => '267418190',// demo
    'productos-por-pagina' => '8',
    'plantilla' => 'cards',
    'plantilla-pago-realizado' => '/tienda/pago-realizado',
    'plantilla-pago-rechazado' => '/tienda/pago-rechazado',
    'cargando-texto' => 'Cargando... por favor espere'
);



