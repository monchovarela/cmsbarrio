<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare(strict_types=1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;
use Action\Action as Action;
use Shortcode\Shortcode as Shortcode;

/*
 * ====================================================
 * Ver Carrito
 *
 * [Carrito]
 * ====================================================
 */
Shortcode::add('Carrito',function($attrs){
    extract($attrs);
    $url = Barrio::urlBase();
    // cargamos el archivo config
    $config = include MODULES . '/tienda/tienda.config.php';
    $html = '<div data-reflow-type="shopping-cart"';
    $html .= 'data-reflow-success-url="'.$url.'/'.$config['plantilla-pago-realizado'].'"';
    $html .= 'data-reflow-cancel-url="'.$url.'/'.$config['plantilla-pago-rechazado'].'">'.$config['cargando-texto'].'</div>';
    return $html;
});

/*
 * ====================================================
 * Ver Productos
 *
 * [Productos]
 * ====================================================
 */
Shortcode::add('Productos',function($attrs){
    extract($attrs);
    // cargamos el archivo config
    $config = include MODULES . '/tienda/tienda.config.php';
    // url base
    $url = Barrio::urlBase();
    $html = '<div data-reflow-type="product-list"';
    $html .= 'data-reflow-perpage="'.$config['productos-por-pagina'].'"';
    $html .= 'data-reflow-order="price_desc"'; 
    $html .= 'data-reflow-layout="'.$config['plantilla'].'"';
    $html .= 'data-reflow-product-link="'.$url.'/tienda/producto?product={id}">'.$config['cargando-texto'].'</div>';
    return $html;
});


/*
 * ====================================================
 * Ver Producto
 *
 * [Producto]
 * ====================================================
 */
Shortcode::add('Producto',function($attrs){
    extract($attrs);
    $url = Barrio::urlBase();
    // cargamos el archivo config
    $config = include MODULES . '/tienda/tienda.config.php';
    $source = $url.'/tienda/carrito';
    $html = '<div id="product-comp"';
    $html .= 'data-reflow-type="product"';
    $html .= 'data-reflow-shoppingcart-url="'.$source.'">'.$config['cargando-texto'].'</div>';
    // incrustamos el codigo en el footer
    Action::add('footer',function(){
        echo '<script rel="javascript">
            const id = new URL(location.href).searchParams.get("product");
            document.querySelector("#product-comp").dataset.reflowProduct = id;
            console.log(id);
        </script>';
    });
    return $html;
});


/*
 * ====================================================
 *  Incustamos los estilos y los scripts en la plantilla
 *  si la url lleva el nombre de tienda.
 * ====================================================
 */
if(Barrio::urlSegment(0) === 'tienda'){
    Action::add('head',function(){
        $source = Barrio::urlBase().'/core/modules/tienda/assets/reflowhq.min.css';
        print('<link type="text/css" rel="stylesheet" href="'. $source.'"/>');
    });
    Action::add('footer',function(){
        // cargamos el archivo config
        $config = include MODULES . '/tienda/tienda.config.php';
        $source = Barrio::urlBase().'/core/modules/tienda/assets/reflowhq.min.js';
        print('<script src="'. $source.'" data-reflow-store="'.$config['id-tienda'].'" defer></script>');
    });
}