<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare(strict_types=1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;
use Filter\Filter as Filter;
use Shortcode\Shortcode as Shortcode;
use Minify\Minify as Minify;

if (array_key_exists('use', $_GET)) 
{

    include MODULES . '/markdown/Parsedown.php';
    include MODULES . '/markdown/ParsedownExtra.php';
    include MODULES . '/shortcodes/shortcodes.module.php';

    Filter::add('content', 'markdown', 1);
    function markdown($content)
    {
        return Minify::html(
            ParsedownExtra::instance()->text(
                Shortcode::parse(
                    $content
                )
            )
        );
    }
    
    $switch = ($_GET['use']) ? $_GET['use'] : null;
    switch ($switch) {
        case 'editor':
            require MODULES.'/editor/assets/index.php';
            break;
    }
    exit();
}