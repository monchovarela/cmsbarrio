<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/**
 * Acceso restringido
 */
defined("ACCESS") or die("No tiene acceso a este archivo");

use Filter\Filter as Filter;
use Minify\Minify as Minify;
use Shortcode\Shortcode as Shortcode;

/**
 * Extension Markdown.
 *
 *  @author Moncho Varela / Nakome
 *  @copyright 2016 Moncho Varela / Nakome
 *
 *  @version 1.0.0
 */

include MODULES . '/markdown/Parsedown.php';
include MODULES . '/markdown/ParsedownExtra.php';

Filter::add('content', 'markdown', 1);

function markdown($content)
{
    return Minify::html(
        ParsedownExtra::instance()->text(
            Shortcode::parse(
                $content
            )
        )
    );
}
