// cuando el dom este listo (Esto solo funciona en desktop)
document.addEventListener('DOMContentLoaded', () => {
  // escuchamos los eventos
  listen(
    document.getElementById("name"),
    "Escriba su nombre para saber a quién nos dirigimos"
  );
  listen(
    document.getElementById("email"),
    "Necesitamos que nos proporcione un correo válido"
  );
  listen(
    document.getElementById("phone"),
    "Necesitamos que nos proporcione un número de teléfono válido para poder llamarle"
  );
  listen(
    document.getElementById("subject"),
    "Necesitamos que nos proporcione asunto válido"
  );
  listen(
    document.getElementById("message"),
    "Necesitamos que nos proporcione un mensaje"
  );
  listen(
    document.getElementById("captcha"),
    "Necesitamos que nos proporcione un codigo válido"
  );
});
// function para escuchar eventos invalid y input
function listen(uid, txt) {
  uid.addEventListener("invalid", (evt) => isInValidInput(evt, txt));
  uid.addEventListener("input", (evt) => isValidInput(evt));
}
// No es valido
function isInValidInput(evt, txt) {
  evt.target.className = "is-invalid";
  evt.target.nextElementSibling.style.display = 'block';
  evt.target.setCustomValidity(txt);
}
// Es valido
function isValidInput(evt) {
  evt.target.className = "";
  evt.target.nextElementSibling.style.display = '';
  evt.target.setCustomValidity("");
}
