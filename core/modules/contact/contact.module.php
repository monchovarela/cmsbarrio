<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/**
 * Acceso restringido
 */
defined("ACCESS") or die("No tiene acceso a este archivo");

use Action\Action as Action;
use Barrio\Barrio as Barrio;
use Shortcode\Shortcode as Shortcode;
use Token\Token as Token;

/**
 * ====================================================
 * Guía rápida sobre cómo agregar a su sitio web:
 *
 * [Contact] // usa el del config.php
 * ====================================================
 */
Shortcode::add('Contact', function () {

    /**
     * Crea un codigo aleatorio
     *
     * @param string $input
     * @param int    $strlen
     *
     * @return string $random_string
     */
    function captcha(string $input = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890', int $strlen = 6)
    {
        $input_length = strlen($input);
        $random_string = '';
        for ($i = 0; $i < $strlen; ++$i) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }
    // cargamos el archivo config
    $config = include MODULES . '/contact/contact.config.php';
    // numero aleatorio
    $captchaValue = captcha($config['captcha'], 6);
    // iniciamos el valor error
    $error = '';
    // si se envia el formulario
    if (isset($_POST['enviarFormulario'])) {
        $token = isset($_POST['token']) ? $_POST['token'] : '';
        if (Token::check($token)) {
            // vars
            $recepient = $config['email'];
            $sitename = Barrio::urlBase();
            $subject = trim($_POST['subject']);
            $name = (string) trim($_POST['name']);
            $email = (string) trim($_POST['email']);
            $phone = (trim($_POST['phone'])) ? trim($_POST['phone']) : 'no proporcionado';
            $text = (string) trim($_POST['message']);
            $captcha = (int) trim($_POST['captcha']);
            $checkCaptcha = (int) trim($_POST['checkCaptcha']);
            // titulo del mensaje
            $pagetitle = $config['pagetitle'];
            // mensaje de salida
            $message = <<<EOT
            ---------------------------
            - Nombre: $name
            - Correo electronico: $email
            - Telefono: $phone
            - Asunto: $subject
            ---------------------------
            Mensaje: 
                $text\n
            EOT;
            // comprobamos el captcha
            if ($checkCaptcha == $captcha) {
                // enviamos mail
                if (mail($recepient, $pagetitle, $message, "Content-type: text/plain; charset=\"utf-8\" \nFrom: <$email>")) {
                    // success
                    $error = '<div class="contact-module-success">' . $config['successMsg'] . '</div>';
                    // volvemos al inicio
                    echo "<script rel='javascript'>setTimeout(() => {window.location.href = site_url;}," . $config['timeToRedirect'] . ");</script>";
                } else {
                    // error
                    $error = '<div class="contact-module-error"><strong>Error: </strong>' . $config['errorMsg'] . '</div>';
                }
            } else {
                // error
                $error = '<div class="contact-module-error"><strong>Error: </strong>' . $config['errorValidateMsg'] . '</div>';
            }
        } else {
            die('CRSF Detect');
        }
    }
    // texto captcha
    $captchatxt = sprintf($config['captchatxt'], $captchaValue);
    // token
    $token = Token::generate();
    // show error
    $html = $error;
    // formulario html
    $html .= <<<EOT
    <form id="contact" class="contact-module" method="post">
        <input type="hidden" name="token" value="{$token}" />
        <input type="hidden" name="checkCaptcha" value="{$captchaValue}" />
        <div>
            <label for="name">{$config['name']}</label>
            <input type="text" name="name" id="name" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" required/>
            <div class="invalid-feedback">{$config['invalidname']}</div>
        </div>
        <div>
            <label for="email">{$config['email']}</label>
            <input type="email" name="email" id="email" pattern="[a-z._%+-]+@[a-z.-]+\.[a-z]{2,4}" required />
            <div class="invalid-feedback">{$config['invalidemail']}</div>
        </div>
        <div>
            <label for="phone">{$config['phone']}</label>
            <input type="tel" name="phone" id="phone" pattern="[0-9]{9,9}" required />
            <div class="invalid-feedback">{$config['invalidphone']}</div>
        </div>
        <div>
            <label for="subject" class="form-label">{$config['subject']}</label>
            <input type="text" name="subject" id="subject" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" required />
            <div class="invalid-feedback">{$config['invalidsubject']}</div>
        </div>
        <div>
            <label for="message">{$config['message']}</label>
            <textarea name="message" id="message" rows="5" required></textarea>
            <div class="invalid-feedback">{$config['invalidmessage']}</div>
        </div>
        <div>
            <label for="number">{$captchatxt}</label>
            <input type="number" name="captcha" id="captcha" required />
            <div class="invalid-feedback">{$config['invalidcaptcha']}</div>
        </div>
        <p>{$config['privacypolicy']}</p>
        <input type="submit" name="enviarFormulario" value="{$config['btntxt']}" />
    </form>
    EOT;
    // style css
    Action::add('head', function () {
        echo '<link type="text/css" rel="stylesheet" href="' . Barrio::urlBase() . '/core/modules/contact/assets/contact.module.css"/>';
    });
    // javascript
    Action::add('footer', function () {
        echo '<script type="text/javascript" rel="javascript" src="' . Barrio::urlBase() . '/core/modules/contact/assets/contact.module.js"></script>';
    });
    return $html;
});
