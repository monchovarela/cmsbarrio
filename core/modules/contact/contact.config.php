<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/**
 * Acceso restringido
 */
defined('ACCESS') or die('No tiene acceso a este archivo');

return array(
    'name' => 'Contact',
    'description' => 'Basic and functional contact form',
    'enabled' => true,

    //.. custom vars
    'email' => 'demo@gmail.com',
    'captcha' => '1234567890',
    'captchatxt' => 'Por favor escriba <strong>%s</strong> para validar el mensaje.',
    'btntxt' => 'Enviar mensaje',
    'pagetitle' => 'Nuevo mensaje desde la web',
    'name' => 'Nombre',
    'invalidname' => 'Escriba su nombre para saber a quién nos dirigimos',
    'email' => 'Correo Electronico',
    'invalidemail' => 'Necesitamos que nos proporcione un correo válido',
    'phone' => 'Telefono',
    'invalidphone' => 'Necesitamos que nos proporcione un número de teléfono válido para poder llamarle',
    'subject' => 'Asunto',
    'invalidsubject' => 'Necesitamos que nos proporcione asunto válido',
    'message' => 'Mensaje',
    'invalidmessage' => 'Necesitamos que nos proporcione un mensaje',
    'invalidcaptcha' => 'Necesitamos que nos proporcione un codigo válido',
    'privacypolicy' => 'Politica de privacidad <a href="/">Aquí</a>',

    'successMsg' => 'Gracias tu mensaje ha sido enviado, volviendo al inicio en 5 segundos.',
    'errorMsg' => 'Lo siento hubo un problema al enviarlo por favor intentelo otra vez..',
    'errorValidateMsg' => ' el codigo de validación introducido es incorrecto',
    'timeToRedirect' => 5000,
);
