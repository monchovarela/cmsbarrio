<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;

include 'Filters.php';

class Api
{
    /**
     *  Render json.
     *
     *  @return array
     */
    public function json(array $data = [])
    {
        @header('Content-Type: application/json');
        exit(json_encode($data));
    }

    /**
     *  Resize image.
     *
     *  @param string $url
     *  @param string $width
     *  @param string $height
     *  @param int $quality
     *
     *  @return string image
     */
    public function resizeImg($url, $width, $height, $quality = 70)
    {
        return ResizeImages($url, $width, $height, $quality = 70);
    }

    /**
     *  Pages.
     *
     *  @return array
     */
    public function pages()
    {
        // check if exists name
        if (array_key_exists('name', $_GET)) {
            // init ApiFilter class
            $ApiFilter = new ApiFilter();
            // get name or null
            $name = ($_GET['name']) ? $_GET['name'] : 'blog';
            // check if is a dir
            if (is_dir(CONTENT . '/' . $name)) {
                // filter data
                if (array_key_exists('filter', $_GET)) {
                    // get filter
                    $filter = ($_GET['filter']) ? $_GET['filter'] : null;
                    // get pages
                    $pages = Barrio::run()->getHeaders($name, 'date', 'DESC', ['index', '404'], null);
                    // init output
                    $output = [];
                    // switch
                    switch ($filter) {
                        // index.php?api=file&data=pages&name=blog&filter=count
                        case 'count': // count
                            $arr = ['total' => count($pages)];
                            // push array
                            array_push($output, $arr);
                            break;
                        // index.php?api=file&data=pages&name=blog&filter=filtername
                        default:
                            $output = $ApiFilter->Attrs($output, $pages, $filter);
                            break;
                    }
                    // print json
                    $this->json($output);
                }
                // index.php?api=file&data=pages&name=blog&limit=3
                elseif (array_key_exists('limit', $_GET)) {
                    $limit = ($_GET['limit']) ? $_GET['limit'] : 3;
                    $output = $ApiFilter->Limit($name, $limit);
                    // print json
                    $this->json($output);
                } else {
                    // index.php?api=file&data=pages&name=blog
                    $output = $ApiFilter->Group($name);
                    // print json
                    $this->json($output);
                }
            }
        }
        $this->json(['status' => false]);
    }

    /**
     *  Pages.
     *
     *  @return array
     */
    public function page()
    {
        // index.php?api=file&data=page&name=blog
        // check if exists name
        if (array_key_exists('name', $_GET)) {
            // get name or null
            $name = ($_GET['name']) ? $_GET['name'] : '';
            $page = Barrio::run()->page($name);

            $ApiFilter = new ApiFilter();
            $arr = $ApiFilter->Parse($page);

            // print json
            $this->json($arr);
        }
        $this->json(['status' => false]);
    }

    /**
     *  File method.
     *
     *  @return array
     */
    public function file()
    {
        if (array_key_exists('data', $_GET)) {
            $data = ($_GET['data']) ? $_GET['data'] : null;

            switch ($data) {
                // index.php?api=file&data=pages
                case 'pages':$this->pages();
                    break;
                // index.php?api=file&data=page
                case 'page':$this->page();
                    break;
            }
        }
        $this->json(['status' => false]);
    }

    /**
     *  Images method.
     *
     *  @return array
     */
    public function image()
    {
        //index.php?api=image&url=public/notfound.jpg
        if (array_key_exists('url', $_GET)) {
            //index.php?api=image&url=public/notfound.jpg&w=1024
            if (array_key_exists('w', $_GET)) {
                //index.php?api=image&url=public/notfound.jpg&w=1024&h=768
                if (array_key_exists('h', $_GET)) {
                    if (array_key_exists('q', $_GET)) {
                        $this->resizeImg($_GET['url'], $_GET['w'], $_GET['h'], $_GET['q']);
                    } else {
                        $this->resizeImg($_GET['url'], $_GET['w'], $_GET['h']);
                    }
                } else {
                    if (array_key_exists('q', $_GET)) {
                        $this->resizeImg($_GET['url'], $_GET['w'], ($_GET['w'] / 2), $_GET['q']);
                    } else {
                        $this->resizeImg($_GET['url'], $_GET['w'], ($_GET['w'] / 2));
                    }
                }
            } elseif (array_key_exists('q', $_GET)) {
                $this->resizeImg($_GET['url'], 320, 180, $_GET['q']);
            } else {
                $this->resizeImg($_GET['url'], 320, 180, 50);
            }
        }
    }

    /**
     *  Manifest method.
     *
     *  @return array
     */
    public function manifest()
    {
        $iconUrl = Barrio::urlBase() . '/core/themes/' . Barrio::$config['theme'] . '/assets/icons';
        $json = [
            'id' => Barrio::$config['title'],
            'description' => Barrio::$config['description'],
            'start_url' => Barrio::urlBase(),
            'name' => Barrio::$config['title'],
            'short_name' => Barrio::$config['title'],
            'display' => Barrio::$config['display'],
            'theme_color' => Barrio::$config['theme_color'],
            'background_color' => Barrio::$config['background_color'],
            'icons' => [
                [
                    'src' => "$iconUrl/android-chrome-192x192.png",
                    "sizes" => "192x192",
                    "type" => "image/png"
                ],
                [
                    'src' => "$iconUrl/android-chrome-512x512.png",
                    "sizes" => "512x512",
                    "type" => "image/png"
                ],
            ],
        ];
        // render json
        $this->json($json);
    }

    /**
     *  Sitemap method.
     *
     *  @return array
     */
    public function sitemap()
    {
        @header("Content-type: text/xml");
        $pages = Barrio::run()->pages('', 'date', 'DESC');
        $html = '<?xml version="1.0" encoding="UTF-8"?>';
        $html .= '<urlset
            xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
            >';
        foreach ($pages as $page) {
            $url = trim($page['url']);
            $date = (int) $page['date'];
            $html .= '<url>
              <loc>' . $url . '</loc>
              <lastmod>' . date('c', $date) . '</lastmod>
              <priority>0.80</priority>
           </url>';
        }
        $html .= '</urlset>';
        exit($html);
    }

    /**
     *  help method.
     *
     *  @return array
     */
    public function help()
    {
        $url = Barrio::urlBase();
        @header('Content-Type: text/plain');
        $html = "{$url}/index.php?api=file&data=pages&name=blog\n";
        $html .="{$url}/index.php?api=file&data=pages&name=blog&limit=2\n";
        $html .="{$url}/index.php?api=file&data=pages&name=blog&filter=title\n";
        $html .="{$url}/index.php?api=file&data=pages&name=blog&filter=images\n";
        $html .="{$url}/index.php?api=file&data=pages&name=blog&filter=videos\n";
        $html .="{$url}/index.php?api=image&url=[public url]\n";
        $html .="{$url}/index.php?api=image&url=[public url]&w=[size width]\n";
        $html .="{$url}/index.php?api=image&url=[public url]&w=[size width]&h=[size height]\n";
        // Get sitemap
        $html .= "{$url}/index.php?api=sitemap\n";
        // Get manifest
        $html .= "{$url}/index.php?api=manifest\n";
        // Get robots
        $html .= "{$url}/index.php?api=robots\n";
        echo $html;
        exit();
    }

    /**
     * Robots
     */
    public function robots()
    {
        $url = Barrio::urlBase();
        @header('content-type: text/plain');
        $output = <<<EOT
        User-agent: Googlebot
        Disallow:
        User-agent: googlebot-image
        Disallow:
        User-agent: googlebot-mobile
        Disallow:
        User-agent: MSNBot
        Disallow:
        User-agent: Slurp
        Disallow:
        User-agent: Teoma
        Disallow:
        User-agent: Gigabot
        Disallow:
        User-agent: Robozilla
        Disallow:
        User-agent: ia_archiver
        Disallow:
        User-agent: baiduspider
        Disallow:
        User-agent: naverbot
        Disallow:
        User-agent: yeti
        Disallow:
        User-agent: yahoo-mmcrawler
        Disallow:
        User-agent: psbot
        Disallow:
        User-agent: yahoo-blogs/v3.9
        Disallow:
        User-agent: *
        Disallow:
        Disallow: /cgi-bin/
        Disallow: /themes/
        Disallow: /admin/
        Disallow: /content/
        Disallow: /tmp/
        Sitemap: {$url}/?api=sitemap
        EOT;
        file_put_contents(ROOT_DIR . '/robots.txt', $output);
        if (file_exists(ROOT_DIR . '/robots.txt')) {
            die(file_get_contents(ROOT_DIR . '/robots.txt'));
        }
        exit();
    }
}
