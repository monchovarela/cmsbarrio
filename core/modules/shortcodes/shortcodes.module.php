<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Action\Action as Action;
use Barrio\Barrio as Barrio;
use File\File as File;
use Filter\Filter as Filter;
use Minify\Minify as Minify;
use Shortcode\Shortcode as Shortcode;

/*
 * ================================
 * Site url
 *
 * [Site_url]
 * ================================
 */
Shortcode::add('Site_url', function ($attrs) {
    extract($attrs);
    // retorna la url base
    return Barrio::urlBase();
});

/*
 * ================================
 * Site current
 *
 * [Site_current]
 * ================================
 */
Shortcode::add('Site_current', function ($attrs) {
    extract($attrs);
    // retorna el hash/segmentos de la web
    return Barrio::urlCurrent();
});

/*
 * =============================================
 *   Link
 *
 *   [Link href='GxEc46k46gg']
 *   [Link title='Ir a' href='GxEc46k46gg']
 *   [Link alt='website' title='Ir a' href='GxEc46k46gg']
 *   [Link class='btn' title='Ir a' href='GxEc46k46gg']
 * =============================================
 */
Shortcode::add('Link', function ($attrs) {
    extract($attrs);

    $href = (isset($href)) ? $href : Barrio::urlBase();
    $name = (isset($name)) ? $name : 'link';
    $title = (isset($title)) ? 'title="' . $title . '"' : '';
    $class = (isset($class)) ? 'class="' . $class . '"' : '';

    if ($href) {
        $html = '<a ' . $class . ' href="' . $href . '" ' . $title . '>' . $name . '</a>';
        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ href ] no encontrada');
    }
});

/*
 * ================================
 * Details
 *
 * [Details title='example']Markdown Hidden content [/Details]
 * ================================
 */
Shortcode::add('Details', function ($attrs, $content) {
    extract($attrs);

    $title = (isset($title)) ? $title : 'Info';
    // comprimimos y parseamos con ParsedownExtra
    $content = Minify::html(
        ParsedownExtra::instance()->text(
            Shortcode::parse(
                $content
            )
        )
    );
    $output = Filter::apply('content', '<details><summary>' . $title . '</summary><div class="p-1">' . $content . '</div></details>');
    $output = preg_replace('/\s+/', ' ', $output);

    if ($content) {
        return $output;
    } else {
        return Barrio::error('Error [ content ] no encontrado');
    }
});

/*
 * ================================
 * Iframe
 *
 * [iframe src='monchovarela.es']
 * ================================
 */
Shortcode::add('Iframe', function ($attrs) {
    extract($attrs);

    $src = (isset($src)) ? $src : '';
    $class = (isset($class)) ? $class : 'iframe';
    $my = (isset($my)) ? 'my-' . $my : '';

    if ($src) {
        $html = '<section class="' . $class . ' ' . $my . '">';
        $html .= '<iframe src="https://' . $src . '" frameborder="0" allowfullscreen></iframe>';
        $html .= '</section>';
        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});

/*
 * =============================================
 *   Youtube
 *
 *   [Youtube id='GxEc46k46gg']
 *   [Youtube class='well' id='GxEc46k46gg']
 * =============================================
 */
Shortcode::add('Youtube', function ($attrs) {
    extract($attrs);

    $id = (isset($id)) ? $id : '';
    $class = (isset($class)) ? $class : 'iframe ratio ratio-16x9';
    $my = (isset($my)) ? 'my-' . $my : '';

    if ($id) {
        $html = '<section class="' . $class . ' ' . $my . '">';
        $html .= '<iframe src="//www.youtube.com/embed/' . $id . '" frameborder="0" allowfullscreen></iframe>';
        $html .= '</section>';
        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ id ] no encontrado');
    }
});

/*
 * =============================================
 *   Vimeo
 *
 *   [Vimeo id='149129821']
 *   [Vimeo class='iframe' id='149129821']
 * =============================================
 */
Shortcode::add('Vimeo', function ($attrs) {
    extract($attrs);

    $id = (isset($id)) ? $id : '';
    $class = (isset($class)) ? $class : 'iframe ratio ratio-16x9';
    $my = (isset($my)) ? 'my-' . $my : '';

    if ($id) {
        $html = '<section class="' . $class . ' ' . $my . '">';
        $html .= '<iframe src="https://player.vimeo.com/video/' . $id . '" frameborder="0" allowfullscreen></iframe>';
        $html .= '</section>';
        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ id ] no encontrado');
    }
});

/*
 * =============================================
 *   Video
 *
 *   [Video src='public/videos/movie.mp4']
 *   [Video class='iframe' src='public/videos/movie.mp4']
 *   [Video class='iframe' autoplay='' src='public/videos/movie.mp4']
 *   [Video class='iframe' autoplay='' autobuffer='' src='public/videos/movie.mp4']
 *   [Video class='iframe' autoplay='' autobuffer='' muted='' loop='' src='public/videos/movie.mp4']
 * =============================================
 */
Shortcode::add('Video', function ($attrs) {
    extract($attrs);

    $src = (isset($src)) ? $src : '';
    $ext = (isset($ext)) ? $ext : false;
    $class = (isset($class)) ? $class : 'video';
    $my = (isset($my)) ? 'my-' . $my : '';

    $autoplay = (isset($autoplay)) ? 'autoplay="true"' : '';
    $autobuffer = (isset($autobuffer)) ? 'autobuffer="true"' : '';
    $muted = (isset($muted)) ? 'muted="true"' : '';
    $loop = (isset($loop)) ? 'loop="true"' : '';
    $controls = (isset($controls)) ? 'controls="true"' : '';

    if ($src) {
        $url = Barrio::urlBase();
        $src = ($ext) ? $src : $url . '/' . $src;
        $html = '<section class="' . $class . ' ' . $my . '">';
        $html .= '<video src="' . $src . '" ' . $controls . ' ' . $autoplay . ' ' . $autobuffer . '  ' . $muted . ' ' . $loop . '> </video>';
        $html .= '</section>';
        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});

/*
 * ====================================================
 *   Texto
 *
 *   [Text]Color texto[/Text]
 *   [Text bg='blue']Color texto[/Text]
 *   [Text bg='blue' color='white']Color texto[/Text]
 * ====================================================
 */
Shortcode::add('Text', function ($attrs, $content) {
    extract($attrs);

    $class = (isset($class)) ? 'class="' . $class . '"' : 'class="txt p-2"';
    $color = (isset($color)) ? 'color:' . $color . ';' : '';
    $bg = (isset($bg)) ? 'background-color:' . $bg . ';' : '';

    $content = Parsedown::instance()->text($content);
    $output = Filter::apply('content', '<div ' . $class . ' style="' . $color . ' ' . $bg . '">' . $content . '</div>');
    $output = preg_replace('/\s+/', ' ', $output);

    if ($content) {
        return $output;
    } else {
        return Barrio::error('Error [ content ] no encontrado');
    }
});

/*
 * ====================================================
 *   Image
 *
 *   [Img src='//otraurl.com/public/image.jpg']
 *   [Img url='//google.es' src='//otraurl.com/public/image.jpg']
 *   [Img url='//google.es' title='Hello' src='//otraurl.com/public/image.jpg']
 *   [Img url='//google.es' title='Hello' class='well' src='//otraurl.com/public/image.jpg']
 *   [Img url='//google.es' title='Hello' class='well' ext='' src='//otraurl.com/public/image.jpg']
 * ====================================================
 */
Shortcode::add('Img', function ($attrs) {
    extract($attrs);

    $src = (isset($src)) ? $src : '';
    $url = (isset($url)) ? $url : '';
    $class = (isset($class)) ? 'class="' . $class . '"' : '';
    $ext = (isset($ext)) ? ($ext = ('true' == $ext) ? true : false) : false;
    $title = (isset($title)) ? $title : '';
    $site = Barrio::urlBase();
    $src = rtrim($src, '/');

    $html = '';
    $srcset = '';
    // exits $src
    if ($src) {
        if (true == $ext || 'true' == $ext) {
            $src = '//' . $src;
            $srcset = false;
        } else {
            // index.php?api=image&url=public/notfound.jpg&w=600
            $normal = $src;
            $src = Barrio::urlBase() . '/index.php?api=image&url=' . $src . '&w=';
            $srcset = 'loading="lazy" sizes="(max-width: 500px) 100vw, (max-width: 900px) 50vw, 800px" src="' . $normal . '" srcset="' . $src . '500 500w,' . $src . '800 800w,' . $src . '1000 1000w,' . $src . '1400 1400w"';
        }

        $image = ($srcset) ? $srcset : 'src="' . $src . '"';

        if ($title) {
            if ($url) {
                $html = '<a href="' . $url . '" title="' . $title . '"> <figure> <img ' . $image . ' ' . $class . '  alt="' . $title . '"/> <figcaption>' . $title . '</figcaption> </figure> </a>';
            } else {
                $html = '<figure><img ' . $class . ' ' . $image . ' alt="' . $title . '"/><figcaption>' . $title . '</figcaption></figure>';
            }
        } else {
            if ($url) {
                $html = '<a  href="' . $url . '" title="' . $title . '"><img ' . $class . ' ' . $image . ' /></a>';
            } else {
                $html = '<img ' . $class . ' ' . $image . ' alt="' . $title . '"/>';
            }
        }

        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});

/*
 * ====================================================
 *  Row
 * - class = css class
 *   [Row]
 *       bloques que sumen 12 en total
 *   [/Row]
 * ====================================================
 */
Shortcode::add('Row', function ($attrs, $content) {

    extract($attrs);

    $class = (isset($class)) ? $class : '';
    $num = (isset($num)) ? 'row-cols-' . $num : '';

    $output = Filter::apply('content', '<div class="row ' . $class . ' ' . $num . '">' . $content . '</div>');
    $output = preg_replace('/\s+/', ' ', $output);

    return $output;
});

/*
 * ====================================================
 * num = col number
 * class = class
 *
 * [Col num='8']
 *      texto en markdown
 * [/Col]
 * ====================================================
 */
Shortcode::add('Col', function ($attrs, $content) {

    extract($attrs);

    $num = (isset($num)) ? $num : '6';
    $class = (isset($class)) ? $class : '';

    $content = Parsedown::instance()->text($content);
    $content = Filter::apply('content', '<div class="col-' . $num . ' ' . $class . '">' . $content . '</div>');
    $content = preg_replace('/\s+/', ' ', $content);

    return $content;
});

/*
 * ================================
 * Style
 *
 * [Styles]body{};[/Styles]
 * [Styles minify=true]body{};[/Styles]
 * ================================
 */
Shortcode::add('Styles', function ($attrs, $content = '') {

    extract($attrs);

    $minify = ('true' == isset($minify)) ? true : false;

    if ($content) {

        $content = (true == $minify) ? Minify::css($content) : $content;

        return Action::add('head', function () use ($content) {
            $html = "\n\n\t";
            $html .= '<!-- Shortcode css -->';
            $html .= "\n\t";
            $html .= '<style rel="stylesheet">' . $content . '</style>';
            $html .= "\n\n";
            echo $html;
        });
    } else {
        return Barrio::error('Error [ contenido ] no encontrado en Style Shortcode');
    }
});
/*
 * ================================
 * Style file
 *
 * [Style src='//example.css']
 * [Style local=true src='content/example.css']
 * ================================
 */
Shortcode::add('Style', function ($attrs) {

    extract($attrs);

    $src = (isset($src)) ? $src : '';
    $local = (isset($local)) ? $local : false;

    if ($src) {
        return Action::add('head', function () use ($src, $local) {
            if ($local) {
                echo '<link rel="stylesheet" type="text/css" href="' . Barrio::urlBase() . '/' . $src . '"/>';
            } else {
                echo '<link rel="stylesheet" href="https://' . $src . '"/>';
            }
        });
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});
/*
 * ================================
 * Scripts
 *
 * [Scripts]console.log("test");[/Scripts]
 * [Scripts minify=true]console.log("test");[/Scripts]
 * ================================
 */
Shortcode::add('Scripts', function ($attrs, $content = '') {

    extract($attrs);

    $minify = (isset($minify)) ? $minify : false;

    if ($content) {

        $content = (true == $minify) ? Minify::js($content) : $content;

        return Action::add('footer', function () use ($content) {
            echo '<script rel="javascript">' . $content . '</script>';
        });

    } else {
        return Barrio::error('Error [ contenido ] no encontrado');
    }
});
/*
 * ================================
 * Script file
 *
 * [Script src='//example.js']
 * ================================
 */
Shortcode::add('Script', function ($attrs) {

    extract($attrs);

    $src = (isset($src)) ? $src : '';
    $local = (isset($local)) ? $local : false;

    if ($src) {
        return Action::add('footer', function () use ($src, $local) {
            if ($local) {
                echo '<script rel="javascript" src="' . Barrio::urlBase() . '/' . $src . '"></script>';
            } else {
                echo '<script rel="javascript" src="https:' . $src . '"></script>';
            }
        });
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});

/*
 * ================================
 * Escape or convert html tags
 *
 * [Esc]echo 'holas';[/Esc]
 * ================================
 */
Shortcode::add('Esc', function ($attr, $content) {
    $output = htmlspecialchars("$content", ENT_QUOTES);
    $output = str_replace('&#039;', "'", $output);

    return $output;
});

/*
 * ====================================================
 *  Code
 *   [Code type='php']
 *       php code here
 *   [/Code]
 * ====================================================
 */
Shortcode::add('Code', function ($attrs, $content) {

    extract($attrs);

    $type = (isset($type)) ? $type : 'php';

    if ($content) {

        $content = htmlentities(html_entity_decode($content));
        $output = Filter::apply('content', '<pre class="line-numbers language-' . $type . '"><code class="language-' . $type . '">' . $content . '</code></pre>');

        return $output;
    } else {
        return Barrio::error('Error [ contenido ] no encontrado');
    }
});

/*
 * ================================
 * Config
 *
 * Get config [Config name='title']
 * ================================
 */
Shortcode::add('Config', function ($attrs) {

    extract($attrs);

    if ($name) {
        return Barrio::$config[$name];
    } else {
        return Barrio::error('Error [ name ] no encontrado');
    }
});

/*
 * ====================================================
 * Divider
 *
 * [Divider]
 * [Divider num='2']
 * [Divider type='br' num='2']
 * ====================================================
 */
Shortcode::add('Divider', function ($attrs) {

    extract($attrs);

    $type = (isset($type)) ? $type : 'hr';
    $num = (isset($num)) ? $num : '4';
    $class = (isset($class)) ? $class : '';
    $color = (isset($color)) ? 'style="border-color:' . $color . '"' : '';

    if ('br' !== $type) {
        return '<hr class="' . $class . ' my-' . $num . '" ' . $color . '/>';
    } else {
        return '<br class="' . $class . ' my-' . $num . '" ' . $color . '/>';
    }
});

/*
 * ====================================================
 * Space
 *
 * [Space]
 * [Space num=2]
 * ====================================================
 */
Shortcode::add('Space', function ($attrs) {

    extract($attrs);

    $num = (isset($num)) ? $num : '2';

    return str_repeat('&nbsp;', (int) $num);
});

/*
 * ====================================================
 * Btn
 * text = texto del boton
 * id =  id del boton (opcional)
 * href = dirección  (opcional)
 *
 * [Btn text='hola']
 * [Btn text='hola' id='btn' href='//example.com']
 * ====================================================
 */
Shortcode::add('Btn', function ($attrs) {

    extract($attrs);

    $text = (isset($text)) ? $text : '';
    $class = (isset($class)) ? 'class="' . $class . '"' : 'class="btn btn-primary my-2"';
    $id = (isset($id)) ? 'id="' . $id . '"' : '';
    $href = (isset($href)) ? $href : '#';

    if ($text) {
        $html = '<a ' . $class . ' ' . $id . ' href="' . $href . '" title="' . $text . '">' . $text . '</a>';
        $html = preg_replace('/\s+/', ' ', $html);

        return $html;
    } else {
        return Barrio::error('Error [ text ] no encontrado');
    }
});

/*
 * ====================================================
 * Html
 * - Solo funciona con .txt
 * [Html src='/public/texto.txt']
 * ====================================================
 */
Shortcode::add('Html', function ($attrs) {

    extract($attrs);

    $src = (isset($src)) ? $src : '';

    if ($src) {

        $content = (is_file(ROOT_DIR . $src)) ? ROOT_DIR . $src : false;

        if ($content) {

            ob_start();
            @header('Content-Type: text/html; charset=utf-8');
            include ($content);
            $var = ob_get_contents();
            ob_end_clean();
            return $var;

        } else {
            Barrio::error('Error [ src ] donde esta el archivo incluido');
        }
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});

/*
 * ====================================================
 * Php
 *
 * [Php]echo 'holas';[/Php]
 * ====================================================
 */
/*
Shortcode::add('Php', function ($attr, $content) {
ob_start();
eval("$content");
return ob_get_clean();
});
 */

/*
 * ====================================================
 * Contar archivos
 *
 * [Count folder="posts"]
 * ====================================================
 */

Shortcode::add('Count', function ($attrs) {

    extract($attrs);

    $folder = (isset($folder)) ? $folder : '';

    if ($folder) {
        $count = (int) count(File::scan(CONTENT . '/' . $folder));
        $total = (int) ($count - 1);
        return $total;
    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});

/*
 * ====================================================
 * Listar archivos
 *
 * [List folder=""]
 * [List folder="" limit=""]
 * [List folder="" limit="" order="" ]
 * ====================================================
 */

Shortcode::add('List', function ($attrs) {

    extract($attrs);

    $limit = isset($limit) ? (int) $limit : (boolean) false;
    $order = isset($order) ? (string) $order : 'date';
    $folder = isset($folder) ? (string) $folder : '';

    if ($folder) {

        $files = Barrio::run()->getHeaders($folder, $order, 'DESC', ['index', '404'], (int) $limit);
        $html = '<ul style="list-style:none;margin:0;padding:0">';

        foreach ($files as $file) {
            $string = date('d-m-Y', $file['date']);
            $date = iconv('ISO-8859-2', 'UTF-8', strftime("%A, %d de %B de %Y", strtotime($string)));
            $html .= '<li><a href="' . $file['url'] . '" title="' . $file['title'] . '">' . $file['title'] . ' </a> - <small><em><strong>' . $date . '</strong></em></small></li>';
        }

        $html .= '</ul>';
        $html = preg_replace('/\s+/', ' ', $html);
        return Minify::html($html);

    } else {
        return Barrio::error('Error [ src ] no encontrado');
    }
});
