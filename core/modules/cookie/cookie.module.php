<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/**
 * Acceso restringido
 */
defined("ACCESS") or die("No tiene acceso a este archivo");

use Action\Action as Action;
use Minify\Minify as Minify;

/*
 * =============================================================
 *      incluimos el plugin de cookie
 *  Página web: https://www.osano.com/cookieconsent/download/
 * =============================================================
 */
Action::add('head', function () {
    // plantilla
    $html = <<<EOT
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
      <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js"></script>
      <script>
      window.addEventListener("load", function(){
        window.cookieconsent.initialise({
          "palette": {
            "popup": {
              "background": "#edeff5",
              "text": "#838391"
            },
            "button": {
              "background": "#4b81e8"
            }
          },
          "theme": "edgeless",
          "position": "top",
          "content": {
            "message": "Utilizamos cookies propias y de terceros. Si continúa navegando acepta su uso.",
            "dismiss": "Aceptar",
            "deny": "Declinar",
            "link": "Leer mas",
            "href": "https://textos-legales.edgartamarit.com/plantilla-politica-cookies-pagina-web/"
          }
        })
      });
      </script>
    EOT;

    echo Minify::html($html);
});
