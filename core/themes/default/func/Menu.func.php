<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;
use Minify\Minify as Minify;

if (!function_exists('arrayOfMenu')) {
    /**
     * Generar un menu
     *
     * @param array $nav
     *
     * @return string
     */
    function arrayOfMenu($nav)
    {
        $html = '';
        foreach ($nav as $k => $v) {
            $id = uniqid();
            // key exists
            if (array_key_exists($k, $nav)) {
                // not empty
                if ($k != '') {
                    // external page
                    if (preg_match("/http/i", $k)) {
                        $html .= '<li><a href="' . $k . '">' . ucfirst($v) . '</a></li>';
                    } else {
                        // is array
                        if (is_array($v)) {
                            // dropdown
                            $html .= '<li>';
                            $html .= '<a id="' . $id . '" onClick="return false;" href="#">' . ucfirst($k) . '</a>';
                            $html .= '<ul>' . arrayOfMenu($v) . '</ul>';
                            $html .= '</li>';
                        } else {
                            $active = Barrio::urlSegment(0);
                            $activeurl = str_replace('/', '', $k);
                            if ($active == $activeurl) {
                                $html .= '<li><a class="active" href="' . trim(Barrio::urlBase() . $k) . '">
                                    ' . ucfirst($v) . '</a></li>';
                            } else {
                                $html .= '<li><a href="' . trim(Barrio::urlBase() . $k) . '">' . ucfirst($v) . '</a></li>';
                            }
                        }
                    }
                }
            }
        }
        // show html
        // array del menu
        return $html;
    }
}
if (!function_exists('menu')) {
    /**
     * Metodo menu
     *
     * @return array
     */
    function menu()
    {
        // array del menu
        $nav = Barrio::$config['menu'];
        $url = Barrio::urlBase();
        $title = Barrio::$config['title'];
        $id = uniqid();
        $navigation = '<nav>
            <ul id="' . $id . '">
                <li>
                    <img class="logo" alt="logo" src="' . $url . '/core/themes/default/assets/icons/favicon.png"/>
                    <span>' . $title . '</span>
                </li>
                ' . arrayOfMenu($nav) . '
                <li class="float-right sticky">
                    <a href="#" class="font-more">
                        ᴀ-
                    </a>|
                    <a href="#" class="font-less">
                        A+
                    </a>
                </li>
                <li class="float-right sticky">
                    <a href="#" class="toogle-theme">🌛</a>
                </li>
            </ul>
        </nav>';

        return Minify::html($navigation);
    }
}
