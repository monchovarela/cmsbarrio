<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare(strict_types=1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;
use Session\Session as Session;

if (!function_exists('theme')) {
    /**
     * Theme
     *
     * @return string
     */
    function theme()
    {
        return (Session::exists('THEME')) ? Session::get('THEME') : Barrio::$config['theme'];
    }
}