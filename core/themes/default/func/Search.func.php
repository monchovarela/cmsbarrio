<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Action\Action as Action;
use Barrio\Barrio as Barrio;
use Minify\Minify as Minify;

if (!function_exists('search')) {
    /**
     * Metodo search
     */
    Action::add('theme_after', "search");
    function search()
    {
        $language = Barrio::$config['search'];

        // demo http://localhost/cmsbarrio/?buscar=
        if (isset($_POST['buscar'])) {

            // http://localhost/cmsbarrio/?buscar=Hola
            // $query = hola
            $query = $_POST['buscar'];

            // check query
            if ($query) {

                $name = '/';
                $num = 0;
                // get pages
                $data = Barrio::run()->getHeaders($name, 'date', 'DESC', ['index', '404'], $num);
                // get 5 words
                $name = urlencode(substr(trim($query), 0, 5));
                // init results and total
                $results = array();
                $total = 0;

                // loop data
                foreach ($data as $item) {

                    // raplace url with data url
                    $root = str_replace(Barrio::urlBase(), CONTENT, $item['url']);
                    // decode
                    $name = urldecode($name);
                    // formatear fecha
                    $string = date('d-m-Y', $item['date']);
                    $date = iconv('ISO-8859-2', 'UTF-8', strftime("%A, %d de %B de %Y", strtotime($string)));

                    // check title description and slug
                    if (preg_match("/$name/i", $item['title']) ||
                        preg_match("/$name/i", $item['description']) ||
                        preg_match("/$name/i", $item['slug']) ||
                        preg_match("/$name/i", $date)) {

                        // if obtain something show
                        $results[] = array(
                            'title' => (string) $item['title'],
                            'description' => (string) $item['description'],
                            'url' => (string) $item['url'],
                            'date' => (int) $item['date'],
                        );
                        // count results
                        $total++;

                    }
                }

                // template
                $output = '<ul style="list-style:none;margin:0;padding:0">';
                foreach ($results as $articulo) {
                    $string = date('d-m-Y', $articulo['date']);
                    $date = iconv('ISO-8859-2', 'UTF-8', strftime("%A, %d de %B de %Y", strtotime($string)));
                    $output .= '<li><a href="' . $articulo['url'] . '" title="' . $articulo['title'] . '">' . $articulo['title'] . ' </a> - <small><em><strong>' . $date . '</strong></em></small></li>';
                }
                $output .= '</ul>';

                $html = '<section class="form-results">';
                // if results show
                if ($results) {
                    $html .= $output;
                } else {
                    $html .= $language['no_results'] . ' ' . $query;
                }
                $html .= '</section>';

                echo Minify::html($html);
            }
        }
    }
}
