<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;
use Minify\Minify as Minify;
use Text\Text as Text;

if (!function_exists('posts')) {
    /**
     * Obtener un lista articulos
     *
     * @param string $name
     * @param int $num
     * @param string $nav
     */
    function posts($name, $num = 0, $nav = true)
    {
        // get pages
        $posts = Barrio::run()->getHeaders($name, 'date', 'DESC', ['index', '404']);

        // limit
        $limit = ($num) ? $num : Barrio::$config['pagination'];

        // init
        $blogPosts = array();

        if ($posts) {

            foreach ($posts as $f) {
                // push on blogposts
                array_push($blogPosts, $f);
            }

            // fragment
            $articulos = array_chunk($blogPosts, $limit);

            // get number
            $pgkey = isset($_GET['page']) ? $_GET['page'] : 0;
            $items = $articulos[$pgkey];

            $html = '<ul style="list-style:none;margin:0;padding:0">';
            foreach ($items as $articulo) {
                $string = date('d-m-Y', $articulo['date']);
                $date = iconv('ISO-8859-2', 'UTF-8', strftime("%A, %d de %B de %Y", strtotime($string)));
                $html .= '<li><a href="' . $articulo['url'] . '" title="' . $articulo['title'] . '">' . $articulo['title'] . ' </a> - <small><em><strong>' . $date . '</strong></em></small></li>';
            }
            $html .= '</ul>';

            // print
            echo Minify::html($html);
            // total = post / limit - 1
            $total = ceil(count($posts) / $limit);

            // if empty start in 0
            $p = 0;
            if (empty($_GET['page'])) {
                $p = 0;
            } else {
                $p = isset($_GET['page']) ? $_GET['page'] : 0;
            }

            if (count($posts) > $limit) {
                // pagination
                $pagination = '<nav class="my-3">';

                if ($p > 0) {
                    $pagination .= '<a href="?page=' . ($p - 1) . '"><span aria-hidden="true">&laquo;</span> Anteriores</a>';
                }

                $disabled = ($p == ($total - 1)) ? 'class="disabled"' : 'href="?page=' . ($p + 1) . '"';
                $pagination .= '<a  ' . $disabled . '> Siguientes <span aria-hidden="true">&raquo;</span></a>';
                $pagination .= '</nav>';

                // print
                if ($nav) {
                    echo Minify::html($pagination);
                }

            }
        }
    }
}
