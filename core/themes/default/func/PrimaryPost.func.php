<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/*
 * Acceso restringido
 */
defined('ACCESS') or exit('No tiene acceso a este archivo');

use Barrio\Barrio as Barrio;
use Minify\Minify as Minify;
use Text\Text as Text;

if (!function_exists('primaryPost')) {
    /**
     * Obtener el primer articulo.
     */
    function primaryPost(string $name = 'blog', int $num = 1)
    {
        // Obtenemos el array de las páginas
        $articulos = Barrio::run()->getHeaders($name, 'date', 'DESC', ['index', '404'], $num);

        $html = '';
        $template = '';
        foreach ($articulos as $articulo) {

            $title = $articulo['title'];
            $description = Text::short($articulo['description'], 50);
            $url = $articulo['url'];

            // Convertimos la fecha
            $string = date('d-m-Y', $articulo['date']);
            $date = iconv('ISO-8859-2', 'UTF-8', strftime("%A, %d de %B de %Y", strtotime($string)));

            // Comprobamos si hay imagen
            $image = '';
            if ($articulo['image']) {
                $src = (preg_match('/http/s', $articulo['image'])) ? $articulo['image'] : Barrio::urlBase().'/'.$articulo['image'];
                $image = '<figure class="aspect-ratio"><img src="' . $src . '" /></figure>';
            }

            // Comprobamos si hay video
            $video = '';
            if ($articulo['video']) {
                $src = (preg_match('/http/s', $articulo['video'])) ? $articulo['video'] : Barrio::urlBase().'/'.$articulo['video'];
                $video = '<figure class="aspect-ratio"><video src="' . $src . '" autoplay="" autobuffer="" muted="" loop=""> </video></figure>';
            }

            // Comprobamos si primero hay video
            $blockImage = '';
            if ($articulo['video']) {
                $blockImage .= $video;
            } elseif ($articulo['image']) {
                $blockImage .= $image;
            } else {
                $blockImage .= '<div class="aspect-ratio">' . $articulo['title'] . '</div>';
            }

            // plantilla
            $template .= "<article class=\"col-6\">
                $blockImage
            </article>
            <article class=\"col-6\">
                <header class=\"m-0\">
                    <h3 class=\"m-0\">$title</h3>
                    <time datetime=\"$date\">$date</time>
                </header>
                <section><p>$description</p></section>
                <footer class=\"text-right\"><a href=\"$url\">Ver mas</a></footer>
            </article>";

        }
        $output = '<section class="row mb-5">' . $template . '</section>';
        return Minify::html($output);
    }
}
