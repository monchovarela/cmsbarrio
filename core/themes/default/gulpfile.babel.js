import gulp from "gulp";
// Optimizar imagenes
import imagemin from "gulp-imagemin";
// Crear sourcemaps para css y js
import sourcemaps from "gulp-sourcemaps";
// Necesario para compilar javascript moderno
import rollup from "rollup-stream";
import buffer from "vinyl-buffer";
import source from "vinyl-source-stream";
import minify from "gulp-minify";
// Necesario para compilar Sass
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

// Ruta del root
const assets = "./assets";
// Borrar archivos y carpetas
gulp.task("clean", () =>
  gulp
    .src(
      [
        `${assets}/css`,
        `${assets}/js/*-dist.js`,
        `${assets}/js/*-dist.map`,
        `${assets}/js/*-dist.min.js`,
        `${assets}/js/*-dist.min.js.map`,
      ],
      { read: true, allowEmpty: true }
    )
    .pipe(clean({ force: true }))
);
// Sass
gulp.task("sass", () =>
  gulp
    .src(`${assets}/scss/*.scss`)
    .pipe(
      sass({
        outputStyle: "nested",
        precision: 10,
        includePaths: ["."],
        onError: console.error.bind(console, "Sass error:"),
      })
    )
    .pipe(autoprefixer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(csso())
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(`${assets}/css`))
);
// Comprimir Js
gulp.task("javascript", () =>
  rollup({ input: `${assets}/js/src/main.js`, format: "iife", sourcemap: true })
    .pipe(source("main.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(minify({ ext: { min: ".min.js" } }))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(`${assets}/js/`))
);
// Optimizar Imagenes
gulp.task("images", () =>
  gulp
    .src(`${assets}/img/*`)
    .pipe(imagemin())
    .pipe(gulp.dest(`${assets}/img/`))
);

gulp.task("default", (done) => {
  gulp.watch(
    [`${assets}/scss/**/*.scss`, `${assets}/scss/*.scss`],
    gulp.series("clean", "sass")
  );
  gulp.watch(
    [`${assets}/js/src/**/*.js`, `${assets}/js/src/main.js`],
    gulp.series("javascript")
  );
  done();
});

// Por defecto
gulp.task("public", gulp.series("clean", "images", "sass", "javascript"));
