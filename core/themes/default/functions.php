<?php
/*
 * Declara al principio del archivo, las llamadas a las funciones respetarán
 * estrictamente los indicios de tipo (no se lanzarán a otro tipo).
 */
declare (strict_types = 1);

/**
 * Acceso restringido
 */
defined("ACCESS") or die("No tiene acceso a este archivo");

use Filter\Filter as Filter;
use Session\Session as Session;

/*
 * ================================
 *       Array de Funciones
 * ================================
 */
$files = array(
    'Assets', 'Actions', 'Theme', 'Config',
    'Url', 'UrlCurrent', 'ImageToDataUri',
    'Menu', 'Posts', 'PrimaryPost', 'LastPosts',
    'NavFolder', 'Search',
);

foreach ($files as $file) {
    $source = CORE . '/themes/default/func/' . $file . '.func.php';
    if (file_exists($source)) {
        include $source;
    } else {
        throw new Exception("Error incluyendo archivos de funciones del theme, por favor reviselo", 1);
    }
}

/*
 * ================================
 *       Filtros de ejemplo
 * ================================
 */

Filter::add('content', 'replaceWordPressText', 1);
if (!function_exists('replaceWordPressText')) {
    function replaceWordPressText($content)
    {
        // si encuentra la palabra WordPress la cambia por el link de la wiki
        return str_replace('WordPress', '<a title="Ver en la Wiki" rel="noopener" target="_blank" href="https://es.wikipedia.org/wiki/WordPress" title="WordPress">WordPress</a>', $content);
    }
}
Filter::add('content', 'replaceSGBDText', 1);
if (!function_exists('replaceSGBDText')) {
    function replaceSGBDText($content)
    {
        // si encuentra la palabra SGBD la cambia por el link de la wiki
        return str_replace('SGBD', '<a title="Ver en la Wiki" rel="noopener" target="_blank" href="https://es.wikipedia.org/wiki/SGBD" title="SGBD">SGBD</a>', $content);
    }
}
