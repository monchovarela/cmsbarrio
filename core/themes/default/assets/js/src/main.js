import {
  ToggleDarkMode,
  CheckSettingsTheme,
} from "./components/ToggleDarkMode";
import AddFontSize from "./components/AddFontSize";
import { _ } from "./components/Utils";

document.addEventListener('DOMContentLoaded',documentReady);

function documentReady() {
  
  CheckSettingsTheme();

  _(".font-more").addEventListener("click", (evt) => AddFontSize(-1));
  _(".font-less").addEventListener("click", (evt) => AddFontSize(1));
  _(".toogle-theme").addEventListener("click", (evt) => ToggleDarkMode(evt));
}
