const _ = el => document.querySelector(el);
const def = x => typeof x !== 'undefined'
// short log
const log = c => def(c) ? console.log(c) : 0;

export {
    _,
    def,
    log
}