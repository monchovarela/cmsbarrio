const AddFontSize = addPx => {
    let html = document.querySelector("html");
    let currentSize = parseFloat(
        window.getComputedStyle(html, null).getPropertyValue("font-size")
    );
    html.style.fontSize = currentSize + addPx + "px";
    return false;
}

export default AddFontSize;