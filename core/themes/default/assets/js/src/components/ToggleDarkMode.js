import Storage from './Storage';
import { _ , def,log} from './Utils';

const CheckSettingsTheme = () => {
  let themebtn = _(".toogle-theme"),
  logo = _(".logo");
  if (Storage.val.color === "dark") {
      themebtn.innerText = "☀️";
      logo.src = `${site_url}/core/themes/default/assets/icons/favicon-light.png`;
  } else {
      themebtn.innerText = "🌛";
      logo.src = `${site_url}/core/themes/default/assets/icons/favicon.png`;
  }
  document.documentElement.setAttribute("data-theme", def(Storage.val.color) ? Storage.val.color : 'light');
}


const ToggleDarkMode = el => {   
  let theme = "light",
    logo = document.querySelector(".logo");
    if (el.target.innerText == "🌛") {
      el.target.innerText = "☀️";
      theme = "dark";
      Storage.val = { color: "dark" };
      logo.src =  `${site_url}/core/themes/default/assets/icons/favicon-light.png`;
    } else {
      el.target.innerText = "🌛";
      Storage.val = { color: "light" };
      logo.src =  `${site_url}/core/themes/default/assets/icons/favicon.png`;
    }
    document.documentElement.setAttribute("data-theme", theme);
    log(theme);
    return false;
  }
  
export {
  CheckSettingsTheme,
  ToggleDarkMode
}