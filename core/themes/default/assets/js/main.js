(function () {
'use strict';

/**
 * Storage theme settings
 *
 * set Storage.val = {color: 'blue'}
 * get Storage.val
 */
 const Storage = {
    get val() {
      this.data = window.localStorage.getItem("theme_settings");
      return (this.data) ? JSON.parse(this.data) : {};
    },
    set val(value) {
      this.data = JSON.stringify(value);
      window.localStorage.setItem("theme_settings", this.data);
    },
  };

const _ = el => document.querySelector(el);
const def = x => typeof x !== 'undefined';
// short log
const log = c => def(c) ? console.log(c) : 0;

const CheckSettingsTheme = () => {
  let themebtn = _(".toogle-theme"),
  logo = _(".logo");
  if (Storage.val.color === "dark") {
      themebtn.innerText = "☀️";
      logo.src = `${site_url}/core/themes/default/assets/icons/favicon-light.png`;
  } else {
      themebtn.innerText = "🌛";
      logo.src = `${site_url}/core/themes/default/assets/icons/favicon.png`;
  }
  document.documentElement.setAttribute("data-theme", def(Storage.val.color) ? Storage.val.color : 'light');
};


const ToggleDarkMode = el => {   
  let theme = "light",
    logo = document.querySelector(".logo");
    if (el.target.innerText == "🌛") {
      el.target.innerText = "☀️";
      theme = "dark";
      Storage.val = { color: "dark" };
      logo.src =  `${site_url}/core/themes/default/assets/icons/favicon-light.png`;
    } else {
      el.target.innerText = "🌛";
      Storage.val = { color: "light" };
      logo.src =  `${site_url}/core/themes/default/assets/icons/favicon.png`;
    }
    document.documentElement.setAttribute("data-theme", theme);
    log(theme);
    return false;
  };

const AddFontSize = addPx => {
    let html = document.querySelector("html");
    let currentSize = parseFloat(
        window.getComputedStyle(html, null).getPropertyValue("font-size")
    );
    html.style.fontSize = currentSize + addPx + "px";
    return false;
};

document.addEventListener('DOMContentLoaded',documentReady);

function documentReady() {
  
  CheckSettingsTheme();

  _(".font-more").addEventListener("click", (evt) => AddFontSize(-1));
  _(".font-less").addEventListener("click", (evt) => AddFontSize(1));
  _(".toogle-theme").addEventListener("click", (evt) => ToggleDarkMode(evt));
}

}());
//# sourceMappingURL=main.js.map
